#!/usr/bin/env python

# coding: utf-8

# ===================================================================
# ===================================================================
# ===================================================================
# IMPORTS

# --------------------------------------------------------------------
# system
import os
import psutil

from pathlib import Path
import sys

path_root = Path(__file__).parents[0]
sys.path.insert(0, str(path_root))

from rcparams import rcParams

import glob
import sys
import multiprocessing
from copy import copy
import pickle

# import dill as pickle #using dill for serialized data
# pickle.settings['recurse'] = True
import argparse
from datetime import datetime
import socket
import logging
from shutil import copy as copyfile

# from shutil import rmtree
import tempfile

# remove some warnings?
# https://docs.python.org/3/library/warnings.html
# import warnings
# warnings.simplefilter('ignore', UserWarning)

# --------------------------------------------------------------------
# PyMC versions & arviz
if rcParams["pmversion"] == 5:
    import pymc as pm
    import pytensor as theano
    import pytensor.tensor as tt

elif rcParams["pmversion"] == 4:
    import pymc as pm
    import aesara as theano
    import aesara.tensor as tt

elif rcParams["pmversion"] == 3:
    import pymc3 as pm
    import theano
    import theano.tensor as tt

    theano.config.compute_test_value = "ignore"
    # from theano.compile.ops import as_op
    # theano.config.compile.wait = 10000
    # theano.config.compile.timeout = 100000
    # theano.config.compute_test_value = 'ignore'
    # theano.config.base_compiledir='/tmp/{}/theano.NOBACKUP'.format(username)
    # theano.config.compile.timeout = 2000 #to avoid: INFO (theano.gof.compilelock): Waiting for existing lock by process #Time to wait before an unrefreshed lock is broken and stolen. This is in place to avoid manual cleanup of locks in case a process crashed and left a lock in place.


if rcParams["inference_backend"] == "gpu":
    # performance tips: https://jax.readthedocs.io/en/latest/gpu_performance_tips.html
    # https://github.com/openxla/xla/blob/f660f7287ead2cd1fa4f5524a8e84645feda0032/xla/debug_options_flags.cc

    os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"] = "True"
    os.environ["XLA_FLAGS"] = (
        "--xla_gpu_enable_triton_softmax_fusion=true "
        "--xla_gpu_triton_gemm_any=true "
        "--xla_gpu_enable_highest_priority_async_stream=true "
    )

    # NOT working: "--xla_gpu_disable_async_collectives=allreduce,allgather,reducescatter,collectivebroadcast,alltoall,collectivepermute"
    # "--xla_gpu_enable_async_collectives=true "  # not recognized
    # "-xla_gpu_enable_async_all_reduce=true " #
    # "--xla_gpu_enable_async_all_gather=true "#
    # "--xla_gpu_enable_async_reduce_scatter=true " #
    # "--xla_gpu_enable_latency_hiding_scheduler=true "  #
    # "--xla_gpu_use_runtime_fusion=true "
    # "--xla_gpu_graph_level=0 "
    # "--xla_gpu_all_reduce_combine_threshold_bytes=33554432 "
    # os.environ["JAX_TRACEBACK_FILTERING"] = "off"  # uncomment only when debugging

    import jax  # don't need jax in this routine per se but we just wanna check potential GPU capabilities
    import blackjax

import arviz as az

# --------------------------------------------------------------------
# misc

# from scipy.interpolate import interp1d  # RegularGridInterpolator, griddata,

# from random import sample as random_sample

# import h5py

# --------------------------------------------------------------------
# local libraries

import importlib
import Library.lib_mc
import Library.lib_main
import Library.lib_misc
import Library.lib_class

# import Library.lib_alt

importlib.reload(Library.lib_mc)
importlib.reload(Library.lib_main)
importlib.reload(Library.lib_misc)
importlib.reload(Library.lib_class)
# importlib.reload(Library.lib_alt)

from Library.lib_mc import *
from Library.lib_main import *
from Library.lib_misc import getgitv
from Library.lib_class import *

# from Library.lib_alt import *

import numpy as np


# --------------------------------------------------------------------
# this is for when calling script within a wrapper, as a function
class args:
    def __init__(
        self,
        inputfile="",
        verbose=False,
        debug=False,
        order=None,
        nsamples_per_job=None,  # SMC: number of "particles" (independent chains) per job, otherwise == number of "draws" per chain/job
        refine=0,
        refine_force_memory=False,
        no_lims=False,
        force_missing=False,
        cluster=False,
        method_step=None,
        return_model_context=False,
        save_tuned_samples=False,
        priorfromposterior=False,
        # startfromposterior=False,
        fix_seed=False,
        iterate=False,
        ncores=0,
        no_individual_likelihoods=False,
        addpredict=False,
        # incomplete_ok=False,
        bimodality_test=False,
    ):
        args.inputfile = str(inputfile)
        args.verbose = bool(verbose)
        args.debug = bool(debug)
        args.order = order
        args.nsamples_per_job = nsamples_per_job
        args.refine = int(refine)
        args.refine_force_memory = bool(refine_force_memory)
        args.no_lims = bool(no_lims)
        args.force_missing = bool(force_missing)
        args.cluster = bool(cluster)
        args.method_step = method_step
        args.return_model_context = bool(return_model_context)
        args.save_tuned_samples = bool(save_tuned_samples)
        args.priorfromposterior = bool(priorfromposterior)
        # args.startfromposterior = bool(startfromposterior)
        args.fix_seed = bool(fix_seed)
        args.iterate = bool(iterate)
        args.ncores = int(ncores)
        args.no_individual_likelihoods = bool(no_individual_likelihoods)
        args.addpredict = bool(addpredict)
        # args.incomplete_ok = bool(incomplete_ok)
        args.bimodality_test = bool(bimodality_test)


# --------------------------------------------------------------------
def main(args=None):
    from Library.lib_class import (
        ObservationSet,
        Observation,
        Observations,
    )  # #temporary workaround for pickling

    # cwd = os.getcwd()
    # print(cwd, __file__)
    # print('basename:    ', os.path.basename(__file__))
    # print('dirname:     ', os.path.dirname(__file__))
    # print('abspath:     ', os.path.abspath(__file__))
    # print('abs dirname: ', os.path.dirname(os.path.abspath(__file__)))
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # COMMAND-LINE SETTINGS

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS inference step")
        # parser.version = "0.0.0"
        parser.add_argument("inputfile", type=str, help="name_of_input_file")
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        parser.add_argument(
            "--debug", "-d", help="Debugging information", action="store_true"
        )
        parser.add_argument(
            "--order",
            "-o",
            help="interpolation order (0: nearest, 1: linear; default=0)",
            type=int,
        )
        parser.add_argument(
            "--iterate",
            "-i",
            help="use order 0 with SMC first and then order 1 with NUTS (tests only)",
            action="store_true",
        )
        parser.add_argument(
            "--nsamples_per_job",
            "-n",
            help="force number of particles (independent chains) for SMC or number of draws per chain for other samplers. ",
            type=int,
        )
        parser.add_argument(
            "--refine",
            "-r",
            help="use a refined grid. Upsample factor is calculated automatically based on available memory but it can be overridden by providing a number after -r. Do not make this the last tag otherwise the input file could be interpreted as the optional argument to -r.",
            nargs="?",
            type=int,
            default=0,
            const=0,
        )
        parser.add_argument(
            "--refine-force-memory",
            "-rfm",
            help="force creation of arrays with large memory when refining grid",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--force-missing",
            "-fm",
            help="force inference of missing observables (e.g., to be explicitly inferred or NaN). Usually done in post-processing",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--cluster", "-c", help="use N CPUs (not N-1)", action="store_true"
        )  # - in args are always
        parser.add_argument(
            "--method-step",
            "-m",
            help="Step method to be used [SMC, NUTS]. Default: SMC. Other possible value: chi2 (preliminary)",
            type=str,
        )
        parser.add_argument(
            "--return-model-context",
            "-mc",
            help="Only return model context",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--no-lims",
            "-nl",
            help="ignore limits (meant for tests)",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--save-tuned-samples",
            "-t",
            help="save tuned samples (Default: False; meant for tests)",
            action="store_true",
        )  # - in args are always replaced with
        parser.add_argument(
            "--no-individual-likelihoods",
            "-nil",
            help="do not save individual likelihoods (Default: False)",
            action="store_true",
        )  # - in args are always replaced with _
        # parser.add_argument(
        #     "--startfromposterior",
        #     "-sfp",
        #     help="use posteriors as starting values [experimental]",
        #     action="store_true",
        # )  # - in args are always replaced with _
        parser.add_argument(
            "--priorfromposterior",
            "-pfp",
            help="use posteriors as priors [experimental]",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--ncores",
            "-nc",
            help="force number of cores used (for SMC)",
            nargs="?",
            type=int,
            default=0,
        )
        parser.add_argument(
            "--fix-seed",
            "-fs",
            help="fix random seed for reproducible results",
            action="store_true",
        )
        parser.add_argument(
            "--incomplete-ok",
            "-iok",
            help="OK to use incomplete grids with distributions (power-law, normal etc...)",
            action="store_true",
        )
        parser.add_argument(
            "--bimodality-test",
            "-bt",
            help="[only used for testsuite -t 1.1]",
            action="store_true",
        )
        # parser.add_argument('-V', action='version')
        args = parser.parse_args()
    # else: args from main function (e.g., from wrapper)

    verbose = args.verbose  # we'll use this a lot

    global logger
    # start logging now (console + file)
    if args.return_model_context:
        logname = "context"
    else:
        logname = "search"
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logger = setup_custom_logger(logname, loglevel=loglevel)

    # welcome and disclaimers
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("       ┍ M U L ┑")
    logging.info("       ┝ T I G ┥")
    logging.info("       ┕ R I S ┙")
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("     ┍           ┑")
    logging.info("     ┝ INFERENCE ┥")
    logging.info("     ┕           ┙")
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("Thanks for using MULTIGRIS.")
    logging.info(
        "Please make sure to check the README file, the notebooks, and the publication (https://ui.adsabs.harvard.edu/abs/2022A%26A...667A..34L/abstract)."
    )
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")

    # parsing git repository commit
    gitv, gitv_remote, gitv_date, gitv_submodules = getgitv(root_directory)

    # keep summary of mgris version for later to be together with the other summaries

    if gitv_remote != gitv:
        logging.warning(" /!\\ /!\\ /!\\ ")
        logging.warning(
            "Your version of MULTIGRIS is not the most recent one, consider updating it"
        )
        logging.warning(" /!\\ /!\\ /!\\ ")

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # SYSTEM SETTINGS AND SOME DEFAULT HARD-CODED SETTINGS

    # --------------------------------------------------------------------
    # if we wanna have reproducible results
    if args.fix_seed:
        random_seed = 1234590
    else:
        random_seed = None
    np.random.seed(random_seed)

    # --------------------------------------------------------------------
    # resdict will contain results (to be pickled later)
    resdict = {}
    resdict.update({"gitv": gitv})
    resdict.update({"gitv_date": gitv_date})
    resdict.update({"gitv_submodules": gitv_submodules})

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # READ INPUT PARAMETERS

    printsection("Reading input parameters")

    input_f = args.inputfile
    if not FileExists(input_f):
        # if there's only one input*.txt file, try and find it (i.e., possible to use directory only as input)
        f = glob.glob(args.inputfile + "/input*.txt")
        if len(f) == 1:
            input_f = f[0]
        else:
            raise OSError("No input file found", input_f)

    # parse input file
    input_params = readinputfile(input_f, root_directory, verbose=verbose)

    # those will be often later
    input_file_lines = input_params["input_file_lines"]
    context = input_params["context"]
    directory_models = input_params["directory_models"]
    output_directory = input_params["output_directory"]
    params_name = input_params["params_name"]
    observation_sets = input_params["observation_sets"]
    tolog = input_params["tolog"]
    tolinear = input_params["tolinear"]
    obs_to_predict = input_params["obs_to_predict"]
    use_scaling = input_params["use_scaling"]
    true_params = input_params["true_parameters"]
    n_comps = input_params["n_comps"]
    sorting = input_params["sorting"]
    sysunc = input_params["sysunc"]
    ncores_max = input_params["ncores_max"]
    hierarchical = input_params["hierarchical"]
    use_scaling_specific = input_params["use_scaling_specific"]

    if use_scaling_specific is None:
        use_scaling_specific = [None]  # [] makes it easier later...

    # if input_params["pmversion"] > 3 and input_params["hierarchical"]:
    #    panic("Hierarchical version working so far only with PyMC3")

    # ===================================================================
    # move output log now that we know where output directory is
    os.makedirs(output_directory + "/Logs/", exist_ok=True)

    movelogfile(logname, output_directory + "/Logs/")

    # also move input file in output directory
    try:
        copyfile(
            input_f, output_directory + "/input.txt"
        )  # copy because we only want a backup
    except:
        logging.warning("Input file not copied because identical to destination")

    # cores
    ncores = multiprocessing.cpu_count()
    if args.ncores > 0:
        # override?
        ncores_eff = args.ncores
    else:
        # we use all cores available (-1 is personal computer for OS)...
        ncores_eff = ncores if args.cluster else ncores - 1
        # ...with a maximum set in rcparams
        ncores_eff = min([ncores_max, ncores_eff])

    # available memory
    # mem_gb = os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_PHYS_PAGES") / (1024.0 ** 3) #prefer the following for multiplatform
    mem_gb = psutil.virtual_memory().total / (1024.0**3)

    starttime = datetime.now()

    gputxt1, gputxt2 = "", ""
    if input_params["inference_backend"] == "gpu":
        gputxt1 = """
    ┝   - GPU device: 
    ┝     - {}
    ┝   - Drivers
    ┝     - CUDA : {}
    ┝     - CUDnn: {}""".format(
            jax.devices()[0].device_kind,
            jax._src.lib.cuda_versions.cuda_runtime_get_version(),
            jax._src.lib.cuda_versions.cudnn_get_version(),
        )
        gputxt2 = """
    ┝ JAX: 
    ┝ - version: {}
    ┝ Blackjax: 
    ┝ - version: {}""".format(
            jax.__version__,
            blackjax.__version__,
        )

    logging.info(
        """
    ┍========================================┑
    ┝========= MULTIGRIS summary ============┥
    ┝========================================┙
    ┝ git version & date:
    ┝ - {} ({})
    ┝ - {}
    ┝ git submodules:
    ┝ - {} 
    ┝========================================
    ┝ PyMC version  : {}
    ┝ Tensor library: {}
    ┝ - Compile directory:
    ┝   - {}
    ┝ - Precision: {}
    ┕========================================
    """.format(
            gitv,
            gitv_date,
            gitv_remote,
            gitv_submodules,
            pm.__version__,
            input_params["tensorlib"],
            importlib.import_module(input_params["tensorlib"]).config.base_compiledir,
            theano.config.floatX,
        )
    )

    logging.info(
        """
    ┍========================================┑
    ┝=========== System summary =============┥
    ┝========================================┙
    ┝ Date/time: {}
    ┝========================================
    ┝ Machine: {}
    ┝ - Cores (used/total): {}/{}
    ┝ - System memory     : {}Gb{}
    ┝========================================
    ┝ Python:
    ┝ - {}
    ┝   - Prefix: {}
    ┝   - Version: {}{}
    ┕========================================
    """.format(
            starttime.strftime("%d/%m/%Y %H:%M:%S"),
            socket.gethostname(),
            ncores_eff,
            ncores,
            round(mem_gb, 1),
            gputxt1,
            sys.executable,
            sys.exec_prefix,
            sys.version.replace("\n", ""),
            gputxt2,
        )
    )

    logging.info(
        """
    ┍========================================┑
    ┝============= Run summary ==============┥
    ┝========================================┙
    ┝ Input file:
    ┝ - {}
    ┝ Context:
    ┝ - {}
    ┝ Output directory:
    ┝ - {}
    ┕========================================
    """.format(
            input_f, context, output_directory
        )
    )
    # ===================================================================

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # PROCESS INPUT PARAMETERS

    # =============================
    # sanity checks

    if (
        input_params["inference_upsample"] > 0
        and input_params["type_constraints"] == "real"
    ):
        panic("Discrete sampling is only when using indices, not real values...")

    if params_name is None or directory_models is None:
        panic("Some mandatory parameters are missing...")

    # no need to check observations which are already checked in read_inputfile

    # checking number of amples

    if args.nsamples_per_job is None:
        nsamples_per_job = int(input_params["nsamples_per_job_min"])
        logging.warning(
            "/!\\ Number of samples per job not provided, forcing the minimum ({}) defined in rcParams".format(
                nsamples_per_job
            )
        )
    else:
        nsamples_per_job = limitsamples(args.nsamples_per_job, input_params)

    if input_params["inference_backend"] == "cpu":
        # checking total (x chains)
        njobs = int(input_params["njobs_per_core"] * ncores_eff)
        if nsamples_per_job * njobs > input_params["nsamples_max"]:
            nsamples_per_job = int(input_params["nsamples_max"] / njobs)
            logging.warning(
                "/!\\ Total samples above limit ({}) defined in rcParams, rescaling to number of jobs: {}".format(
                    int(input_params["nsamples_max"]), nsamples_per_job
                )
            )
    else:
        njobs = int(
            input_params["njobs"]
        )  # in SMC's case == njobs, this is to get the rhat diagnostic

    # updating potential changes
    input_params["njobs"] = njobs
    input_params["nsamples_per_job"] = nsamples_per_job

    nsamples_prior = min([500, nsamples_per_job])

    # ===================================================================
    # ===================================================================
    # ===================================================================

    printsection("Observations")

    # =============================
    # convert observations to object
    # need to create class here so pickling of class can work

    # select only observation sets that are not disabled
    tmp = {}
    for oset in observation_sets.keys():
        if not observation_sets[oset]["disabled"]:
            tmp.update({oset: observation_sets[oset]})
    observation_sets = tmp

    # pre-process all obs blocks to convert to single dict of class ObservationSet
    observations = Observations(observation_sets, input_params, verbose=verbose)

    # if log scale, check that delta_ref was provided (especially for limits)
    for s in observations.sets:
        if observations.sets[s].obs_valuescale == "log":
            # print(s)
            for o in observations.sets[s].obs:
                if (
                    ~np.isfinite(np.sum(observations.sets[s].obs[o].delta))
                    and observations.sets[s].delta_ref is None
                ):
                    panic(
                        f"/!\\ delta is nan for observable {o} but delta_ref was not provided"
                    )

    if hierarchical and n_comps > 1:
        panic("Hierarchical approach can only be used for one component for now")

    # remember number of observation sets (objects in sample) for hierarchical approach
    # for now we assume a single component per object in sample
    if hierarchical:
        n_obj = observations.nblocks
        n_comps = n_obj
        for i, s in enumerate(observations.sets.keys()):
            if f"SET{i+1}" not in s:  # _ has been dropped at this point
                panic(
                    "Hierarchical method expects observation sets named ending with _SET1, _SET2 etc..."
                )
    else:  # this way get_iset function still works
        n_obj = 1

    # =============================
    # masks

    if verbose:
        logging.info("Setting masks")

    for s in observations.sets:
        if observations.sets[s].n_obs() == 0:
            panic("Observation block is empty!")
        observations.sets[
            s
        ].set_masks()  # for mask_obs_nan, i.e., no values and/or no error bars. We don't want to include limits here as we will consider them anyway (even w/o errors)
        # computes order_of_magnitude_obs and order_of_magnitude_obs_std for each obj, also calculates value_offset
        observations.sets[s].compute_oom()

    order_of_magnitude_obs = (
        observations.order_of_magnitude_obs()
    )  # array with oom for each obs set
    order_of_magnitude_obs_std = observations.order_of_magnitude_obs_std()

    # flatten all obs blocks now that all the obs pre-processing is done
    observations.flatten()
    # switch back to single obs set now that all obs blocks have been processed
    observations = observations.oset
    # get mask for new merged obs set
    observations.set_masks()
    if not args.force_missing:
        observations.ignore_missing()

    if not hierarchical:  # we recalculate the oom with all sets
        observations.compute_oom()  # also calculates value_offset
        order_of_magnitude_obs = [
            observations.order_of_magnitude_obs
        ]  # array with single value
        order_of_magnitude_obs_std = [observations.order_of_magnitude_obs_std]

    # for the hierarchical approach we need to associate observations with each obs set (object)
    for o in observations.obs:
        observations.obs[o].iobj = get_iset(o, n_obj)

    s, nstab, sdev, maxv = check_nsigma(observations, verbose=True)
    logging.info(s)
    if sdev > 1 or maxv > 3:
        logging.error(
            " /!\\ Some tracers seem to deviate significantly as far as the detection level is concerned, this may cause a pathological posterior"
        )

    if observations.values == []:
        panic("No valid observations")

    # other masks
    single_normalizing = ""
    if use_scaling == "none":
        if verbose:
            logging.info("No prior on scaling")
        observations.mask_obs_scaling_references = []
    elif use_scaling == "all":
        if verbose:
            logging.info(
                "Using all extensive observables with detections for prior on scaling"
            )
        observations.mask_obs_scaling_references = (
            observations.mask_obs_detected_extensive
        )  # i.e., no upper limits
    elif "[" in use_scaling:  # subset
        use_scaling = [
            s.strip() for s in use_scaling.replace("[", "").replace("]", "").split(",")
        ]
        observations.mask_obs_scaling_references = [
            o for o in use_scaling if o in observations.mask_obs_detected_extensive
        ]
        if len(observations.mask_obs_scaling_references) == 0:
            panic("Not enough valid observations for prior on scaling")
        if verbose:
            logging.info(
                "Using subset of observables for prior on scaling: {}".format(
                    observations.mask_obs_scaling_references
                )
            )
    else:  # single tracer
        single_normalizing = use_scaling
        if verbose:
            logging.info(
                "Using single observable with detection for prior on scaling: {}".format(
                    single_normalizing
                )
            )
        if single_normalizing not in observations.names:
            panic("Tracer used for scaling not in list of observables")
        if observations.obs[single_normalizing].limit or not np.isfinite(
            observations.obs[single_normalizing].value
        ):
            panic("Tracer used for scaling is non-finite or a limit")
        observations.mask_obs_scaling_references = [single_normalizing]
        # # if only one observable to be used, we don't wanna use it as constraint like the other ones
        # observations.mask_obs_detected = [
        #     o
        #     for o in observations.mask_obs_detected
        #     if o not in observations.mask_obs_scaling_references
        # ]

    if verbose:
        logging.info(
            "mask_obs_intensive            : {}".format(observations.mask_obs_intensive)
        )
        logging.info(
            "mask_obs_extensive            : {}".format(observations.mask_obs_extensive)
        )
        logging.info(
            "mask_obs_no_eb_lims           : {}".format(
                observations.mask_obs_no_eb_lims
            )
        )
        if args.force_missing:
            logging.info(
                "mask_obs_nan              : {}".format(observations.mask_obs_nan)
            )
        logging.info(
            "mask_obs_detected             : {}".format(observations.mask_obs_detected)
        )
        logging.info(
            "mask_obs_detected (extensive) : {}".format(
                observations.mask_obs_detected_extensive
            )
        )
        logging.info(
            "mask_obs_lims                 : {}".format(observations.mask_obs_lims)
        )
        logging.info(
            "mask_obs_up_lims              : {}".format(observations.mask_obs_up_lims)
        )
        logging.info(
            "mask_obs_lo_lims              : {}".format(observations.mask_obs_lo_lims)
        )
        logging.info(
            "mask_obs_scaling_references   : {}".format(
                observations.mask_obs_scaling_references
            )
        )

    # if (len(mask_obs_detected)<len(mask_obs_scaling_references)+2) and 'all' not in use_scaling:
    #  panic('Warning: not enough detections considering number of scaling references')

    # observations to predict need to be processed here if we just want to return the model context. This is just to get them as observed for PPC later on in post-processing, values don't matter
    if (
        args.return_model_context and args.addpredict
    ):  # test because this messes up the calculation of the PPC for the observables with detection
        for o in obs_to_predict:
            observations.add(
                Observation(
                    o,
                    value=np.nanmedian(observations.values),
                    delta=np.repeat(np.nanmedian(observations.delta), 2),
                    delta_type="relative",
                )
            )  # n_obs automatically updated
        for i, o in enumerate(observations.names):
            observations.obs[o].delta = np.repeat(np.nanmedian(observations.delta), 2)
            observations.obs[o].value = np.nanmedian(observations.values)
            observations.obs[o].limit = False
            observations.obs[o].upper = False
            observations.obs[o].lower = False

    # =============================
    # checking whether enough constraints exist for each sysunc

    if sysunc is not None:
        for s in sysunc:
            tmp = [
                l
                for l in sysunc[s][2]
                if l in observations.names or l in observations.obsnames_u
            ]
            if len(tmp) == 0:
                # check whether observation set
                tmp = [l for l in observations.names if f"{{{sysunc[s][2][0]}}}" in l]
                if len(tmp) == 0:
                    logging.critical(sysunc, observations.names)
                    panic(
                        "Not recognized as an observation set name: {}".format(
                            sysunc[s][2][0]
                        )
                    )
                else:
                    sysunc[s][2] = tmp
            elif len(tmp) == 1:
                logging.critical(sysunc, observations.names)
                panic("Not enough constraints for systematic uncertainty: {}".format(s))

    # =============================
    # create input parameter dict to pass to functions
    inp = input_params

    # saving context for reproducibility
    # will be checked later in mgris_process and mgris_post_process for consistency
    with open(context + "/context_input.txt") as f:
        context_input = f.readlines()
        inp.update({"context_input": context_input})
    with open(context + "/pre-processing.py") as f:
        ontext_pre_processing = f.readlines()
        inp.update({"context_pre_processing": ontext_pre_processing})

    # update with modified values
    inp.update({"observations": observations})

    # update with other parameters
    inp.update({"n_comps": n_comps})
    inp.update({"hierarchical": hierarchical})
    inp.update({"n_obj": n_obj})
    inp.update({"input_f": input_f})
    inp.update({"interp_order": args.order})
    # inp.update({"rcParams": rcParams}) #already in inp through input_params
    inp.update({"no_lims": args.no_lims})
    inp.update(
        {"ignored": observations.ignored}
    )  # will be used in post-processing step

    # save all args
    inp.update({"args": args})

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # GRID INPUT
    printsection("Grid input")

    grid = make_grid_wrapper(inp, verbose=verbose)
    if len(grid.values) == 0:
        panic("No values in grid, check hard constraints?")

    # ===================
    # very specific tests

    # temporary solution to switch off optical lines beyond IF
    # for k in grid.values.keys():
    #    #if k[-1]=='A': #optical line
    #    if k in ('H16562.81A', ''):
    #        print('Switching off optical line {} after IF'.format(k))
    #        for c in set(grid.values['cut']):
    #            if c>1:
    #                print(c)
    #                ttt = pd.merge(grid.values.loc[grid.values['cut']==1], grid.values.loc[grid.values['cut']==c], how='right', on=[g for g in grid.params.names if g!='cut'], suffixes=(None, '_y')).set_index(grid.values.loc[grid.values['cut']==c].index)
    #                #print(k)
    #                grid.values[k].loc[grid.values['cut']==c] = ttt[k].values

    # ===================
    # ##bimodality/degeneracy test
    if args.bimodality_test:
        logging.warning("!!!!!!!! BIMODALITY TEST ON !!!!!!!!!!!!!!!")
        # bimodality test
        # what do we duplicate/mirror?
        ##duplicate
        # d = {
        #    "age": [[0.699, 0], [0.778, 0.301], [0.903, 0.477], [1, 0.602]],
        #    "Z": [[-0.668, -2.19], [-0.491, -1.899], [-0.19, -1.19], [0.111, -0.889]],
        #    "n": [[3, 0], [4, 1]],
        #    "u": [[-3, 0], [-4, -1]],
        #    "Lx": [[-2, -40], [-1, -3]],
        #    "Tx": [[6, 0], [7, 5]],
        # }
        ## mirror
        # d = {
        #     "age": [[0, 1], [0.903, 0.301], [0.778, 0.477], [0.699, 0.602],],
        #     "u": [[0, -4], [-1, -3]],
        #     "n": [[0, 4], [1, 3]],
        #     "Z": [[0.111, -2.19], [-0.19, -1.889], [-0.491, -1.19]],
        #     "Lx": [[-1, -40], [-2, -3]],
        #     "Tx": [[7, 0], [6, 5]],
        # }
        d = {
            "Z": [[grid.params.values["Z"][6], grid.params.values["Z"][2]]],
        }
        for dd in d:
            for vals in d[dd]:
                ttt = pd.merge(
                    grid.values.loc[grid.values[dd] == vals[0]],
                    grid.values.loc[grid.values[dd] == vals[1]],
                    how="left",
                    on=[g for g in grid.params.names if g != dd],
                    suffixes=("_x", None),
                ).set_index(grid.values.loc[grid.values[dd] == vals[0]].index)
                for o in observations.names:  # works per column only dont know why
                    grid.values[o].loc[grid.values[dd] == vals[0]] = ttt[o]
        breakpoint()
    # ===================

    # interpolation order
    if args.order == 0:
        if inp["linterp_params"] != []:
            logging.warning("/!\\ Forcing interpolation order to 0 for all parameters")
        inp["linterp_params"] = []
    elif args.order == 1:
        if inp["linterp_params"] != grid.params.names:
            logging.warning("/!\\ Forcing interpolation order to 1 for all parameters")
        inp["linterp_params"] = grid.params.names
    elif len(inp["linterp_params"]) > 0:  # args.order is None (undefined)
        logging.info(
            "No forced interpolation, will use interpolation from input file for parameters ({})".format(
                inp["linterp_params"]
            )
        )

    for p in inp["linterp_params"]:
        if p not in grid.params.names:
            panic(
                f"Parameter [{p}] is not recognized for linear interpolation (interpolation method must be the same for all components)"
            )

    if args.method_step is None:
        logging.warning(
            "Setting step method automatically to SMC, can be overridden with -m"
        )
        step_in = "SMC"
    else:
        step_in = args.method_step.strip()

    # discrete sampling
    if inp["inference_upsample"] > 0 and inp["discrete_params"] == []:
        logging.warning("/!\\ Forcing discrete sampling for all paraneters")
        inp["discrete_params"] = grid.params.names

    if inp["discrete_params"] != [] and step_in == "SMC":
        logging.warning(
            "/!\\ SMC with discrete sampling has not been tested, switching to NUTS+Metropolis-within-Gibbs for all parameters"
        )  # cannot modify because not a global variable
        step_in = "NUTS"

    inp.update({"method_step": step_in})

    # inp['ignore_model_upper_limits'] = True
    # for o in observations.names:
    #    if len(grid.values[o][~np.isfinite(grid.values[o])])>0 and inp['ignore_model_upper_limits']:
    #       logging.warning('/!\\ Dropping {} for inference because it contains model upper limits'.format(o))
    #       grid.drop(o, axis=1)
    #       observations.drop(o)
    # or better, remove lines with ULs

    if verbose:
        logging.debug("")
        logging.debug("# components     : {}".format(n_comps))
        logging.debug("# parameters     : {}".format(grid.params.n))
        logging.debug("# models         : {}".format(grid.n_models()))
        logging.debug("# observables    : {}".format(observations.n))

    if verbose:
        logging.debug("")
        logging.debug("Parameter values:")
        for p in grid.params.names:
            logging.debug("...{} : {}".format(p, grid.params.values[p]))

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # FINE GRID EXPECTATIONS

    if args.refine > 0:
        printsection("Fine grid expectations", level=2)

        # factor to oversample 1D array with N points
        # 2: add one point so about 2N-1 more points, 3: add two points so about 3N-1 more points etc...
        # exactly: new number for elements = upsample_factor*(N-1)+1
        if args.refine != 0:  # - in args are always replaced with underscore
            upsample_factor = args.refine
            if verbose:
                logging.debug("Upsample factor: {}".format(upsample_factor))
            tmp = grid.params.upsample_memtest(upsample_factor)
            # tmp = params.n*np.prod([upsample_factor*(len(params.values[p])-1)+1 for p in params.values])
            if (
                mbsize(tmp, 32) > 1e3 * inp["maxgridmemsize"]
                and not args.refine_force_memory
            ):  # max maxgridmemsize Gb for a 32-bit
                raise MemoryError(
                    "/!\\ Memory for 32-bit array is more than {}Gb... Force the code to go ahead anyway using -rfm flag".format(
                        inp["maxgridmemsize"]
                    )
                )  # 64-bit because using griddata when refining

        else:  # automatic, based on maximum size allowed (1Gb for now)
            tmp, uf = 0, 1
            while (
                mbsize(tmp, 32) < 1e3 * inp["maxgridmemsize"]
            ):  # max 1Gb for a 32-bit array?
                tmp = grid.params.upsample_memtest(uf)
                # tmp = grid.params.n*np.prod([uf*(len(grid.params.values[p])-1)+1 for p in grid.params.values])
                uf += 1
            upsample_factor = uf - 1
            logging.info("maximum upsample factor = {}".format(upsample_factor))

        tmp = grid.params.upsample_memtest(upsample_factor)
        # tmp = grid.params.n*np.prod([upsample_factor*(len(grid.params.values[p])-1)+1 for p in grid.params.values])
        logging.info(
            "Expected size for fine grid                  : {} elements".format(tmp)
        )
        logging.info(
            "Expected memory size for fine grid in 32bits : {} Mb".format(
                round(mbsize(tmp, 32), 1)
            )
        )  # that's the precision we want for most arrays (16bits too small for C code in PyMC3)
        logging.info(
            "Expected memory size for fine grid in 16bits : {} Mb".format(
                round(mbsize(tmp, 16), 1)
            )
        )
        del tmp

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # GRIDS
    # make obs_grid used in the pymc3 model for interpolation
    (
        grid,
        obs_grid,
        mask_grid,
        mask_ul_grid,
        minvalue,
        completion_fraction,
        order_of_magnitude,
        order_of_magnitude_std,
    ) = make_obs_grid_wrapper(
        inp,
        grid,
        observations.obsnames_u,
        args.refine_force_memory,
        args.refine,
        verbose=verbose,
    )
    # test
    # completion_fraction = 100
    incomplete_grid = completion_fraction < 100

    # ==================
    # very specific test
    # test typical deltas in grid a priori
    # if 1==1:
    #     tmp = deepcopy(obs_grid)
    #     tmp[tmp==0] = np.nan
    #     for i in range(len(tmp)):
    #         x = []
    #         for j in range(tmp[i].ndim):
    #             x += [list(np.abs(tmp[i]-np.roll(tmp[i], 1, axis=j)).flatten()),]
    #             x = np.array(x).flatten()
    #             m, s = np.nanmean(x), np.nanstd(x)
    #             print(m, s)
    #             breakpoint()
    #     del tmp
    # ==================

    model_uls = (
        mask_ul_grid.min() == 0
    )  # we'll use this afterwards to switch on/off the model ULs calculations which take some time
    # model_uls = False
    if model_uls:
        logging.warning("/!\\ Model table contains upper limits")
    if inp["ignore_model_upper_limits"]:
        logging.warning("/!\\ Ignoring model upper limits according to rcparams")
        model_uls = False

    # if args.refine: #grid has changed
    #        inp['grid'] = deepcopy(grid)

    inp["completion_fraction"] = completion_fraction
    inp["order_of_magnitude"] = order_of_magnitude
    inp["order_of_magnitude_std"] = order_of_magnitude_std

    observed_offset = {}
    for o in observations.names:
        observed_offset.update({o: observations.obs[o].value_offset})

    order_of_magnitude_each = []
    order_of_magnitude_each_std = []
    for i, o in enumerate(observations.names):
        j = observations.getidx(o)
        order_of_magnitude_each += [
            np.nanmedian(obs_grid[j] * (mask_grid == 1) * (mask_ul_grid == 1)),
        ]
        order_of_magnitude_each_std += [
            MADAM(obs_grid[j] * (mask_grid == 1) * (mask_ul_grid == 1)),
        ]
    # order_of_magnitude_each = np.nanmedian(obs_grid*(mask_grid==1)*(mask_ul_grid==1), axis=tuple(1+np.arange(len(obs_grid.shape)-1)))

    logging.info(
        "Ord. of mag. (grid) : {} {}".format(order_of_magnitude, order_of_magnitude_std)
    )
    logging.info(
        "Ord. of mag. (each) : {} {}".format(
            order_of_magnitude_each, order_of_magnitude_each_std
        )
    )
    logging.info(
        "Ord. of mag. (obs)  : {} {}".format(
            order_of_magnitude_obs, order_of_magnitude_obs_std
        )
    )
    logging.info(
        "New grid minimum    : {} ({})".format(minvalue, minvalue + order_of_magnitude)
    )

    # inp.update({'obs_grid': deepcopy(obs_grid)}) #we wanna save it before replacing mask
    # store.put('obs_grid', pd.DataFrame(obs_grid), format='table')
    # store.create_dataset('obs_grid', data=obs_grid, compression="gzip", compression_opts=9)
    inp.update({"order_of_magnitude_obs": order_of_magnitude_obs})
    inp.update({"order_of_magnitude_obs_std": order_of_magnitude_obs_std})
    inp.update({"minvalue": minvalue})

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # MODEL

    # =============================
    # now we can read the configuration blocks for potential constraints in PyMC3
    configuration = readconfiguration(
        inp, input_file_lines, grid.params, verbose=verbose
    )
    inp["configuration_constraints"] = configuration
    if configuration is None:
        set_params, set_params_replace = {}, {}  # default if no constraints
    else:
        set_params = configuration["set_params"]
        set_params_replace = configuration["set_params_replace"]
        logging.info("")
        if len(configuration["constraints"]) > 0:
            logging.info("Requested priors using default distribution :")
            logging.info("---------------------------------------------")
            for tmp in configuration["constraints"]:
                logging.info("- {} : {}".format(tmp, configuration["constraints"][tmp]))
        if len(configuration["constraints_replace"]) > 0:
            logging.info("\nRequested priors using a new distribution :")
            logging.info("-------------------------------------------")
            for tmp in configuration["constraints_replace"]:
                logging.info(
                    "- {} : {}".format(tmp, configuration["constraints_replace"][tmp])
                )

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # ALTERNATIVE METHODS
    if args.method_step == "chi2":
        printsection("Chi2 method")

        alt_chi2(grid, observations, inp=inp)

        return

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # BAYESIAN METHOD

    printsection("Model preparation")

    # if we wanna replace the weakly informative priors with the posterior from a previous run
    # meant for very specific cases, use at your own risk!
    if args.priorfromposterior:
        logging.info("Prior from posterior")
        if not FileExists(output_directory + "trace_process.netCDF"):
            panic("/!\\ File not found")
        trace_tmp = az.from_netcdf(output_directory + "trace_process.netCDF")
        goodchains, badchains = analyze_chains(trace_tmp)
        trace_tmp.posterior = trace_tmp.posterior.sel(chain=goodchains)
        pfp_means = trace_tmp.posterior.mean(skipna=True)
        pfp_sdevs = trace_tmp.posterior.var(skipna=True) ** 0.5
        del trace_tmp

    # =============================
    # remove result files first, especially trace_all
    if not args.return_model_context:
        for f in ("ppc", "trace", "trace_all", "trace_process", "trace_post-process"):
            # we can remove trace_process even if pfp is set
            if f == "trace":
                continue
            tmp = output_directory + f + ".netCDF"
            if FileExists(tmp):
                os.remove(tmp)

    # =============================
    # theano variables

    # update obs_grid for mgris_process
    # store.put('obs_grid', pd.DataFrame(obs_grid), format='table') #we can overwrite as long as store is not closed
    o_t = theano.shared(obs_grid).astype(
        inp["precision"]
    )  # cannot be 16-bit otherwise C is disabled and CPU sampling is *very* slow

    if incomplete_grid:
        m_t = theano.shared(mask_grid).astype("int16")
    if model_uls:
        m_ul_t = theano.shared(mask_ul_grid).astype("int16")

    n_obs = obs_grid.shape[0]
    del obs_grid, mask_grid, mask_ul_grid

    # =============================
    # model itself
    def run_model(inp, getContextOnly=False):
        with pm.Model(
            check_bounds=False
        ) as model:  # after 3.11 update of PyMC3: check_bounds=False
            # =============================
            # new params? for literal priors

            if "pmextra" in inp.keys():  # backward compatibility
                if inp["pmextra"] != "":
                    logging.info(f"Extra commands for PyMC: {inp['pmextra']}")
                    eval(inp["pmextra"])

            # =============================
            # systematic uncertainties and scale_specific for test à la Cormier

            # systematic uncertainties as nuisance variables, for some sets of tracers
            # default is 0 (log)
            scale_sysunc = {}
            for i, o in enumerate(observations.names):
                scale_sysunc.update({o: 0})
            if sysunc is not None:
                # make list of priors as normal distribution around 0 with sdev
                pm_ss = {}
                for s in sysunc:
                    pm_ss.update(
                        {
                            s: pm.Normal(
                                s,
                                sysunc[s][0],
                                sigma=sysunc[s][1],
                                dtype=inp["precision"],
                            )
                        }
                    )
                    for i, o in enumerate(observations.names):
                        if (
                            o in sysunc[s][2]
                            or observations.getobsname(o) in sysunc[s][2]
                        ):
                            scale_sysunc[o] += pm_ss[s]

            # use_scaling_specific : additionnal scaling, uniform between 0 and 1 by steps of 0.2 (useful to compute the covering fraction 'a la Cormier'
            # default is 0 (log)
            scale_specific = {o: 0 for o in observations.names}
            if use_scaling_specific[0] is not None:
                pm_scale_specific = pm.Deterministic(
                    "scale_specific",
                    tt.log10(
                        0.2 * pm.Categorical("cat_scale_specific", np.ones(6) / 6.0)
                        + 1e-8
                    ),
                )  # +1e-8 is to avoid log10 of  0
                # pm_specific = pm.Uniform('scale_specific', -3, 0, testval=-0.01)
                for i, o in enumerate(observations.names):
                    if (
                        o in use_scaling_specific
                        or observations.getobsname(o) in use_scaling_specific
                    ):
                        logging.info(
                            "Setting scale_specific constraint for {}".format(o)
                        )
                        scale_specific[o] = pm_scale_specific

            # =========================
            # parameters

            # if step_in== 'SMC':
            infcode = input_params["infcode"]
            if type(infcode) == str:
                infcode = float(infcode)

            # float(-1000)#-np.inf) #float is in case we wanna put an integer in there instead of inf
            # having a low value but not too low prevent divergences
            # invalid data are saved in the trace

            # parse parameters to infer directly or that need hyperparameters
            inp = grid.parse_params(inp)

            if n_comps > 2 and len(grid.params.names_notinf) > 0 and not hierarchical:
                panic("Normal/power-law distributions only work with 1 or 2 sectors")

            if incomplete_grid and grid.params.names_notinf != []:
                msg = "Grid is incomplete and some parameters ({}) follow a distribution (power-law, normal etc...)\nThis could lead to unexpected results during integration".format(
                    grid.params.names_notinf
                )

                # if args.incomplete_ok:
                #     logging.warning(msg)
                # else:
                #     msg += "\nYou can force the inference using the -iok option\nThis will perform the inference and ignore all MCMC iterations considering a subpart of the grid with invalid data due to the grid incompleteness"
                #     panic(msg)

            # ============================================
            # power-law/normal params
            for ps in grid.params.names_notinf:
                p = getp(ps)
                tmp = [f"idx_lowerbound_{ps}", f"idx_upperbound_{ps}"]

                if ps in grid.params.names_plaw:
                    tmp += [
                        f"alpha_{ps}",
                    ]
                elif ps in grid.params.names_smoothplaw:
                    tmp += [
                        f"alpha_{ps}",
                        f"lowerscale_{ps}",
                        f"upperscale_{ps}",
                    ]
                elif ps in grid.params.names_brokenplaw:
                    tmp += [
                        f"alpha1_{ps}",
                        f"alpha2_{ps}",
                        f"pivot_{ps}",
                    ]
                elif ps in grid.params.names_normal:
                    tmp += [
                        f"mu_{ps}",
                        f"sigma_{ps}",
                    ]
                elif ps in grid.params.names_doublenormal:
                    tmp += [
                        f"mu1_{ps}",
                        f"mu2_{ps}",
                        f"sigma1_{ps}",
                        f"sigma2_{ps}",
                        f"ratio21_{ps}",
                    ]
                else:
                    panic(f"Parameter {ps} not recognized")

                for p2 in tmp:
                    if p2 not in set_params_replace:  # default one
                        panic(
                            f"Parameter ({p2}) is from a special distribution and needs to be constrained in the configuration block, there are no default values"
                        )
                    else:
                        # new distribution priors to replace default one
                        logging.info("Setting new distribution for {}".format(p2))
                        if p2 in (f"idx_lowerbound_{ps}", f"idx_upperbound_{ps}"):
                            if (
                                "pm.Normal" in configuration["constraints_replace"][p2]
                            ):  # we don't wanna change a deterministic, we just wanna add boundaries to the Normal distribution
                                # get params from requested prior
                                mu = configuration["constraints_replace"][p2][
                                    configuration["constraints_replace"][p2].index(
                                        "mu="
                                    )
                                    + 3 :
                                ]
                                mu = mu[0 : mu.index(",")]
                                sigma = configuration["constraints_replace"][p2][
                                    configuration["constraints_replace"][p2].index(
                                        "sigma="
                                    )
                                    + 6 :
                                ]
                                sigma = sigma[0 : sigma.index(",")]
                                # update with boundaries
                                if p2 == f"idx_lowerbound_{ps}":
                                    configuration["constraints_replace"][p2] = (
                                        "pm.TruncatedNormal(p2, mu={}, sigma={}, lower=0, upper={}, testval=1)".format(
                                            mu, sigma, len(grid.params.values[p]) - 1
                                        )
                                    )
                                # elif p2 == f"idx_upperbound_{ps}":
                                #     configuration["constraints_replace"][
                                #         p2
                                #     ] = "pm.TruncatedNormal(p2, mu={}, sigma={}, lower=0, upper={}, testval={})".format(
                                #         mu,
                                #         sigma,
                                #         len(grid.params.values[p]) - 1,
                                #         len(grid.params.values[p]) - 2,
                                #     )
                                elif p2 == f"idx_upperbound_{ps}":
                                    configuration["constraints_replace"][p2] = (
                                        "pm.TruncatedNormal(p2, mu={}, sigma={}, lower=model['idx_lowerbound_{}'], upper={}, testval={})".format(
                                            mu,
                                            sigma,
                                            ps,
                                            len(grid.params.values[p]) - 1,
                                            len(grid.params.values[p]) - 2,
                                        )
                                    )
                        if verbose:
                            logging.info(
                                "EVAL (ID 0): "
                                + configuration["constraints_replace"][p2]
                                .replace("p2", f"'{p2}'")
                                .replace("[p]", f"[{p}]")
                            )
                        eval(configuration["constraints_replace"][p2])
                    if p2 in set_params:  # some priors on existing distribution?
                        for command_string in configuration["constraints"][
                            p2
                        ]:  # loop because there can be several priors
                            logging.info("Setting prior for {}".format(p2))
                            if verbose:
                                logging.info("EVAL (ID 1): " + command_string)
                            eval(command_string)

            # for p in grid.params.names_smoothplaw: GPU/JAX we need real boundary values
            for ps in grid.params.names_notinf:
                p = getp(ps)
                logging.info(f"Adding real values boundaries for distribution on: {ps}")
                pm.Deterministic(
                    f"lowerbound_{ps}",
                    grid.params.interp_t_idx2vals(p, model[f"idx_lowerbound_{ps}"]),
                )
                pm.Deterministic(
                    f"upperbound_{ps}",
                    grid.params.interp_t_idx2vals(p, model[f"idx_upperbound_{ps}"]),
                )

            for ps in grid.params.names_notinf:
                modelvars = (
                    model.value_vars if input_params["pmversion"] >= 4 else model.vars
                )
                vnames = [v.name.replace("_interval__", "") for v in modelvars]
                # we can also just use lower=upper in TruncatedNormal
                # interval is for TruncatedNormal
                # if (
                #     f"idx_lowerbound_{ps}" in vnames
                #     and f"idx_upperbound_{ps}" in vnames
                # ):
                #     logging.info(f"Adding prior for boundaries ({ps})")
                #     pm.Potential(
                #         f"prior_bounds_{ps}",
                #         tt.switch(
                #             model[f"idx_lowerbound_{ps}"]
                #             < model[f"idx_upperbound_{ps}"],
                #             0,
                #             infcode,
                #         ),
                #     )
                # still useful to have some priors to make sure we have at least one grid value between the boundaries
                if (
                    f"idx_lowerbound_{ps}" in vnames
                    and f"idx_upperbound_{ps}" in vnames
                ):
                    logging.info(f"Adding prior for boundaries ({ps})")
                    pm.Potential(
                        f"prior_bounds_{ps}",
                        tt.switch(
                            model[f"idx_lowerbound_{ps}"] + 1
                            < model[f"idx_upperbound_{ps}"],
                            0,
                            infcode,
                        ),
                    )

                # implicit: allowing mu to be outside of boundaries
                if f"mu_{ps}" in vnames:
                    logging.warning(f"Make sure that the prior on sigma is > 0")
                    logging.warning(f"(e.g., sigma_cut_0 = 1 1 0.1 2)")
                #     logging.info(f"Adding sigma prior ({ps})")
                #     pm.Potential(
                #         f"prior_sigma_{ps}",
                #         tt.switch(
                #             model[f"sigma_{ps}"] > 0,
                #             0,
                #             infcode,
                #         ),
                #     )
                #     logging.info(f"Adding mu prior with respect to boundaries ({ps})")
                #     pm.Potential(
                #         f"prior_mubounds_{ps}",
                #         tt.switch(
                #             (
                #                 grid.params.interp_t_idx2vals(
                #                     p, model[f"idx_lowerbound_{ps}"]
                #                 )
                #                 < model[f"mu_{ps}"]
                #             )
                #             * (
                #                 grid.params.interp_t_idx2vals(
                #                     p, model[f"idx_upperbound_{ps}"]
                #                 )
                #                 > model[f"mu_{ps}"]
                #             ),
                #             0,
                #             infcode,
                #         ),
                #     )
                if f"pivot_{ps}" in vnames:
                    logging.info(f"Adding prior for pivot ({ps})")
                    pm.Potential(
                        f"prior_pivot_{ps}",
                        # alternative to next part: use pivot_{ps} val sdev min max in priors
                        # following is useful only to have free pivot *and* boundaries
                        # but it may make the inference very slow because of the idx2val interpolation
                        tt.switch(
                            (model[f"lowerbound_{ps}"] < model[f"pivot_{ps}"])
                            * (model[f"upperbound_{ps}"] > model[f"pivot_{ps}"]),
                            0,
                            infcode,
                        ),
                    )
                if f"mu1_{ps}" in vnames:
                    # ratio 21 between density in mu2 vs. mu1
                    if f"ratio21_{ps}" in [v.name for v in modelvars]:
                        logging.info(f"Adding prior for mu1<mu2 ({ps})")
                        pm.Potential(
                            f"prior_mu_{ps}",
                            tt.switch(
                                model[f"mu1_{ps}"] < model[f"mu2_{ps}"],
                                0,
                                infcode,
                            ),
                        )
                    else:
                        logging.info(f"ratio21 sign will serve as prior ({ps})")

            # ============================================
            # grid params
            if inp["type_constraints"] == "real":  # working on real values
                # testval
                tab = grid.values.loc[
                    ~grid.values[observations.obsnames_u]
                    .replace([np.inf, -np.inf], np.nan)
                    .isna()
                    .any(axis=1)
                ]
                notfound = False
                # ignore edges
                for p in grid.params.names_inf_p:
                    tab = tab.loc[(tab[p] > np.min(tab[p])) & (tab[p] < np.max(tab[p]))]
                    if len(tab) == 0:
                        logging.warning(
                            "Could not find a proper testvalue, trying edges..."
                        )
                        tab = grid.values.loc[
                            ~grid.values[observations.obsnames_u]
                            .replace([np.inf, -np.inf], np.nan)
                            .isna()
                            .any(axis=1)
                        ]
                        for p in grid.params.names_inf_p:
                            tab = tab.loc[
                                (tab[p] >= np.min(tab[p])) & (tab[p] <= np.max(tab[p]))
                            ]
                        if len(tab) == 0:
                            notfound = True
                if not notfound:  # go ahead anyway and choose one value in tab
                    tab = tab.iloc[int(0.5 * (len(tab) - 1))]
                    testvals = {
                        grid.params.names_inf_p[i]: t
                        for i, t in enumerate(tab[grid.params.names_inf_p])
                    }
                else:
                    logging.warning(
                        "Could not find a proper testvalue, trying middle of grid..."
                    )
                    # reset tab
                    tab = grid.values.loc[
                        ~grid.values[observations.obsnames_u]
                        .replace([np.inf, -np.inf], np.nan)
                        .isna()
                        .any(axis=1)
                    ]
                    tab = tab.iloc[int(0.5 * (len(grid.values) - 1))]
                    testvals = {
                        p: grid.values[p].iloc[int(0.5 * (len(grid.values) - 1))]
                        for p in grid.params.names_inf_p
                    }
                # test if only two values, don't take the edge
                for p in grid.params.names_inf_p:
                    if len(grid.params.values[p]) == 2:
                        testvals[p] = 0.5 * np.sum(grid.params.values[p])
                # if verbose:
                #     logging.info("testvals = {}".format(testvals))
                #     logging.info("testobs  = {}".format(tab[observations.obsnames_u]))

                # default priors not appearing as constraint in context
                for ps in grid.params.names_inf_p:
                    p = getp(ps)
                    testval = testvals[
                        p
                    ]  # int(np.round(grid.params.interp_vals2idx(p)(grid.values[p].iloc[int(0.5*(len(grid.values)-1)))])) #middle of the grid
                    mean = np.mean(
                        grid.params.values[p]
                    )  # 0.5*(np.min(grid.params.values[p])+np.max(grid.params.values[p]))
                    sigma = np.max(
                        [
                            np.abs(mean - np.min(grid.params.values[p])),
                            np.abs(mean - np.max(grid.params.values[p])),
                        ]
                    )  # abs(0.5*(np.min(grid.params.values[p])+np.max(grid.params.values[p])))
                    if ps not in set_params_replace:  # default one
                        logging.info(
                            "Setting default distribution for {}, with mean={} and sigma={}".format(
                                ps, mean, sigma
                            )
                        )
                        if (
                            inp["inference_upsample"] > 0
                        ):  # for indices we can use a discrete distribution...
                            panic("Discrete sampling available only for indices")
                        else:  # or continuous
                            if args.priorfromposterior:
                                mean = float(pfp_means[ps])
                                sigma = float(pfp_sdevs[ps])
                                logging.info(
                                    "Prior from posterior for {}: {} {}".format(
                                        ps, mean, sigma
                                    )
                                )
                            pm.TruncatedNormal(
                                ps,
                                mu=mean,
                                sigma=sigma,
                                lower=np.min(grid.params.values[p]),
                                upper=np.max(grid.params.values[p]),
                                testval=testval,
                            )

                # Now we define the priors
                tmp = []  # array for sector
                for ps in grid.params.names_inf:
                    p = getp(ps)
                    ips = "idx_{}".format(ps)
                    testval = testvals[
                        p
                    ]  # int(grid.params.interp_vals2idx(p)(grid.values[p].iloc[int(0.5*(len(grid.values)-1))])) #middle of the grid
                    if (
                        ps in set_params_replace
                    ):  # new distribution priors to replace default one
                        logging.info("Setting new distribution for {}".format(ps))
                        if verbose:
                            logging.info(
                                "EVAL (ID 2): "
                                + configuration["constraints_replace"][ps]
                            )
                        eval(configuration["constraints_replace"][ps])
                    # define idx conversion
                    pm.Deterministic(ips, grid.params.interp_t_vals2idx(p, model[ps]))
                    tmp += [
                        model[ips],
                    ]
                    if ps in set_params:
                        for command_string in configuration["constraints"][
                            ps
                        ]:  # loop because there can be several priors
                            logging.info("Setting prior for {}".format(ps))
                            if verbose:
                                logging.info("EVAL (ID 3): " + command_string)
                            eval(command_string)

            else:  # working on indices
                # testval
                tab = grid.values.loc[
                    ~grid.values[observations.obsnames_u]
                    .replace([np.inf, -np.inf], np.nan)
                    .isna()
                    .any(axis=1)
                ]
                notfound = False
                # ignore edges
                for p in grid.params.names_inf_p:
                    tab = tab.loc[(tab[p] > np.min(tab[p])) & (tab[p] < np.max(tab[p]))]
                    if len(tab) == 0:
                        logging.error(
                            "Could not find a proper testvalue, trying edges..."
                        )
                        tab = grid.values.loc[
                            ~grid.values[observations.obsnames_u]
                            .replace([np.inf, -np.inf], np.nan)
                            .isna()
                            .any(axis=1)
                        ]
                        for p in grid.params.names_inf_p:
                            tab = tab.loc[
                                (tab[p] >= np.min(tab[p])) & (tab[p] <= np.max(tab[p]))
                            ]
                        if len(tab) == 0:
                            notfound = True
                if not notfound:  # go ahead anyway and choose one value in tab
                    tab = tab.iloc[int(0.5 * (len(tab) - 1))]
                    testvals = {
                        grid.params.names_inf_p[i]: int(
                            np.round(
                                grid.params.interp_vals2idx(grid.params.names_inf_p[i])(
                                    t
                                )
                            )
                        )
                        for i, t in enumerate(tab[grid.params.names_inf_p])
                    }
                else:
                    logging.error(
                        "Could not find a proper testvalue, trying middle of grid..."
                    )
                    # reset tab
                    tab = grid.values.loc[
                        ~grid.values[observations.obsnames_u]
                        .replace([np.inf, -np.inf], np.nan)
                        .isna()
                        .any(axis=1)
                    ]
                    if len(tab) == 0:
                        print(grid.values)
                        logging.error("Some tracers are not valid, no model left...")
                        exit()
                    tab = tab.iloc[int(0.5 * (len(grid.values) - 1))]
                    testvals = {
                        p: int(
                            np.round(
                                grid.params.interp_vals2idx(p)(
                                    grid.values[p].iloc[
                                        int(0.5 * (len(grid.values) - 1))
                                    ]
                                )
                            )
                        )
                        for p in grid.params.names_inf_p
                    }
                # test if only two values, don't take the edge
                for p in grid.params.names_inf_p:
                    if len(grid.params.values[p]) == 2:
                        testvals[p] = 0.5
                # if verbose:
                #     logging.info("testvals = {}".format(testvals))
                #     logging.info("testobs  = {}".format(tab[observations.obsnames_u]))

                # default priors not appearing as constraint in context
                for ps in grid.params.names_inf:
                    ips = "idx_{}".format(ps)
                    p = getp(ps)
                    testval = testvals[p]
                    # int(np.round(grid.params.interp_vals2idx(p)(grid.values[p].iloc[int(0.5*(len(grid.values)-1)))])) #middle of the grid
                    mean = 0.5 * (len(grid.params.values[p]) - 1)
                    sigma = 0.5 * (len(grid.params.values[p]) - 1)
                    if ips not in set_params_replace:  # default one
                        logging.info(
                            "Setting default distribution for {}, with mean={} and sigma={}".format(
                                ips, mean, sigma
                            )
                        )
                        if (
                            inp["inference_upsample"] > 0
                            and p in inp["discrete_params"]
                        ):  # for indices we can use a discrete distribution...
                            new = inp["inference_upsample"] * len(grid.params.values[p])
                            if np.mod(inp["inference_upsample"], 2) == 0:
                                new -= 1  # 2N-1 is necessary to keep existing elements in expanded array. For a factr of 2 we add one element between two existing elements
                            fac = (len(grid.params.values[p]) - 1) / (new - 1)
                            # print(p, new, fac)
                            # print(set(pm.Categorical.dist(p=np.ones(new)/new).random(size=1000)*fac) )
                            # tmp11 = pm.Categorical('cat_{}'.format(ps), p=np.ones(new, dtype='float32')/new, testval=testval/fac)
                            pm.Deterministic(
                                ips,
                                fac
                                * pm.Categorical(
                                    "cat_{}".format(ips),
                                    p=np.ones(new, dtype=inp["precision"]) / new,
                                    testval=testval / fac,
                                ),
                            )
                        else:  # or continuous
                            if args.priorfromposterior:
                                mean = float(pfp_means["idx_{}".format(ips)])
                                sigma = float(pfp_sdevs["idx_{}".format(ips)])
                                logging.info(
                                    "Prior from posterior for {}: {} {}".format(
                                        ips, mean, sigma
                                    )
                                )
                            pm.TruncatedNormal(
                                ips,
                                mu=mean,
                                sigma=sigma,
                                lower=0,
                                upper=len(grid.params.values[p]) - 1,
                                testval=testval,
                            )

                # Now we define the priors
                tmp = []  # array for sector
                for ps in grid.params.names_inf:
                    ips = "idx_{}".format(ps)
                    p = getp(ps)
                    testval = testvals[p]
                    # int(grid.params.interp_vals2idx(p)(grid.values[p].iloc[int(0.5*(len(grid.values)-1))])) #middle of the grid
                    if ips in set_params_replace:
                        # new distribution priors to replace default one
                        logging.info("Setting new distribution for {}".format(ips))
                        if verbose:
                            logging.info(
                                "EVAL (ID 4): "
                                + configuration["constraints_replace"][ips]
                            )
                        # if configuration['constraints_replace'][ps][0]=='X':
                        #    setattr(model, ps, getattr(model, configuration['constraints_replace'][ps][1:]))
                        # elif configuration['constraints_replace'][ps][0]=='Y':
                        #    setattr(model, ps, theano.shared(float(configuration['constraints_replace'][ps][1:])))
                        # else:
                        eval(configuration["constraints_replace"][ips])
                    tmp += [
                        getattr(model, ips),
                    ]
                    if ips in set_params:
                        for command_string in configuration["constraints"][
                            ips
                        ]:  # loop because there can be several priors
                            logging.info("Setting prior for {}".format(ips))
                            if verbose:
                                logging.info("EVAL (ID 5): " + command_string)
                            eval(command_string)

            # =========================
            # covering factors and sorting

            # =========================

            # define each sector's covering factor
            if hierarchical:
                w = [
                    1,
                ] * n_obj  # for now only one component per object
            elif n_comps == 1:
                w = [
                    pm.Deterministic(
                        "w",
                        tt.as_tensor_variable(
                            [
                                1,
                            ]
                        ),
                    ),
                ]
            else:
                if sorting == "w":
                    testval = np.arange(1, n_comps + 1) / sum(np.arange(1, n_comps + 1))
                    testval = testval[::-1]
                else:
                    testval = np.ones(n_comps) / n_comps
                    testval = None

                # RV to ignore n sectors by setting a prior to their w /!\\ only for one group
                # sectors are ignored starting with the last one
                if inp["n_comps_vary"]:
                    logging.warning(
                        "/!\\ Testing number of components is preliminary, use with caution"
                    )
                    if inp["discrete_params"] == []:
                        consider_ncomps = pm.Uniform("ncomps", 1, n_comps)
                        # 1, 2, ... n_comps #testval makes a nan interval which is a pb with NUTS

                        # goal:
                        # e.g., ncomps=2
                        # i                          0   1
                        # ncomps-i                   2   1
                        # e.g., consider_ncomps=2
                        # n_comps-consider_ncomps    0
                        # result                     F   F
                        # e.g., consider_ncomps=1
                        # n_comps-consider_ncomps    1
                        # result                     F   T

                        if rcParams["inference_backend"] == "gpu":
                            # somehow not working with Dirichlet
                            pm.TruncatedNormal(
                                "wtmp",
                                mu=np.ones(n_comps),
                                sigma=1,
                                lower=0,
                                upper=1,
                                testval=np.ones(n_comps, dtype=inp["precision"]),
                            )
                            w2 = pm.Deterministic(
                                "w2", model["wtmp"] / tt.sum(model["wtmp"])
                            )
                            wtmp = w2 * [
                                tt.switch(
                                    tt.round(n_comps - consider_ncomps)
                                    >= (n_comps - i),
                                    0,
                                    1,
                                )
                                for i in range(n_comps)
                            ]
                        else:
                            w2 = pm.Dirichlet(
                                "w2",
                                a=np.ones(n_comps, dtype=inp["precision"]),
                                testval=testval,
                            )
                            wtmp = w2 * [
                                tt.switch(
                                    tt.round(n_comps - consider_ncomps)
                                    >= (n_comps - i),
                                    0,
                                    1,
                                )
                                for i in range(n_comps)
                            ]
                    else:
                        consider_ncomps = pm.Categorical(
                            "ncomps", np.ones(n_comps)
                        )  # 0, 1, ... n_comps-1 #testval makes a nan interval which is a pb with NUTS
                        w2 = pm.Dirichlet(
                            "w2",
                            a=np.ones(n_comps, dtype=inp["precision"]),
                            testval=testval,
                        )
                        wtmp = w2 * [
                            tt.switch(
                                (n_comps - consider_ncomps - 1) >= (n_comps - i), 0, 1
                            )
                            for i in range(n_comps)
                        ]
                    w = pm.Deterministic("w", wtmp / tt.sum(wtmp))
                else:
                    if rcParams["inference_backend"] == "gpu":
                        # somehow not working with Dirichlet
                        pm.TruncatedNormal(
                            "w2",
                            mu=np.ones(n_comps),
                            sigma=1,
                            lower=0,
                            upper=1,
                            testval=np.ones(n_comps, dtype=inp["precision"]),
                        )
                        w = pm.Deterministic("w", model["w2"] / tt.sum(model["w2"]))
                    else:
                        w = pm.Dirichlet(
                            "w",
                            a=np.ones(n_comps),
                            testval=np.ones(n_comps, dtype=inp["precision"]),
                        )

            if sorting != "none":  # sort with set parameter
                logging.info("Sorting with set parameter: {}".format(sorting))

                if sorting == "w":
                    tmp0, tmp1 = (
                        getattr(model, sorting)[s - 1],
                        getattr(model, sorting)[s],
                    )
                else:
                    if inp["type_constraints"] == "indices":
                        tmp0, tmp1 = (
                            getattr(model, "idx_{}_{}".format(sorting, s - 1)),
                            getattr(model, "idx_{}_{}".format(sorting, s)),
                        )
                    else:
                        tmp0, tmp1 = (
                            getattr(model, "{}_{}".format(sorting, s - 1)),
                            getattr(model, "{}_{}".format(sorting, s)),
                        )
                pm.Potential(
                    "prior_sort_{}_{}".format(sorting, s),
                    tt.switch(tt.gt(tmp0, tmp1), 0.0, infcode),
                )

            else:
                logging.info("No sorting during inference")

            # priors on w?
            for s in range(n_comps):
                # cannot be replaced, just new priors
                if f"w_{s}" in set_params:
                    for command_string in configuration["constraints"][
                        f"w_{s}"
                    ]:  # loop because there can be several priors
                        logging.info("Setting prior for {}".format(f"w_{s}"))
                        for i in range(n_comps):
                            command_string = command_string.replace(
                                f"'w_{i}'", f"model['w2'][{i}]"
                            )
                        if verbose:
                            logging.info("EVAL (ID 6): " + command_string)
                        eval(command_string)

            # =========================
            # scaling factor

            # priors on scale?
            scale_prior = False
            if configuration is not None:
                for ps in ("scale", "scale_eff"):
                    if ps in configuration["constraints_replace"]:
                        command_string = configuration["constraints_replace"][ps]
                        logging.info("Setting new distribution for {}".format(ps))
                        if verbose:
                            logging.debug("EVAL (ID 7): " + command_string)
                        eval(command_string)
                        if ps == "scale":
                            scale = model["scale"]
                        else:
                            scale = (
                                model["scale_eff"]
                                - inp["order_of_magnitude_obs"]
                                + inp["order_of_magnitude"]
                            )
                        scale_prior = True

            if not scale_prior:
                if use_scaling == "none":
                    # scale = np.repeat(order_of_magnitude-order_of_magnitude_obs-np.log10(n_groups), n_groups) #this will be reset after next block
                    scale = pm.Deterministic(
                        "scale",
                        tt.as_tensor_variable(
                            [
                                order_of_magnitude - order_of_magnitude_obs[iobj]
                                for iobj in range(n_obj)
                            ]
                        ),
                    )
                elif use_scaling == "all" or type(use_scaling) != str:
                    if use_scaling == "all":
                        scalings = [
                            [
                                observed_offset[o] - order_of_magnitude_each[i]
                                for i, o in enumerate(observations.names)
                                if observations.obs[o].extensive
                                and o not in use_scaling_specific
                                and not observations.obs[o].limit
                                and observations.obs[o].iobj == iobj
                            ]
                            for iobj in range(n_obj)
                        ]
                    else:  # array of tracers
                        scalings = [
                            [
                                observed_offset[o] - order_of_magnitude_each[i]
                                for i, o in enumerate(observations.names)
                                if observations.obs[o].extensive
                                and o not in use_scaling_specific
                                and not observations.obs[o].limit
                                and observations.obs[o].iobj == iobj
                                and o in use_scaling
                            ]
                            for iobj in range(n_obj)
                        ]

                    if args.priorfromposterior:
                        mean = np.atleast_1d(pfp_means["scale"].data)
                        sigma = np.atleast_1d(pfp_sdevs["scale"].data)
                        logging.info(
                            "Prior from posterior for scale: {} {}".format(mean, sigma)
                        )
                    else:
                        # RVs are float32 so make it consistent here
                        mean = np.array(
                            [np.mean(scalings[iobj]) for iobj in range(n_obj)],
                            dtype=input_params["precision"],
                        )
                        # conservative std accounting for dispersion of values in grid and in observed values
                        sigma = np.array(
                            [
                                np.std(scalings[iobj])
                                + np.mean(order_of_magnitude_each_std)
                                + order_of_magnitude_obs_std[iobj]
                                for iobj in range(n_obj)
                            ],
                            dtype=input_params["precision"],
                        )
                    if np.max(sigma) == 0:
                        if len(scalings[0]) == 1:
                            # temporary fix for single detection, possibly with upper/lower limits on the side
                            sigma = [order_of_magnitude_std]
                        else:
                            logging.error("Sigma=0, maybe all fluxes are =?")
                    # scale_mean = mean
                    # scale_sigma = sigma

                    # if grid.params.names_notinf != []:
                    #    logging.warning(
                    #        "Scaling for power-law/normal distributions is difficult to predict, will choose an arbitrary large sigma..."
                    #    )
                    #    sigma += 0

                    # sigma = np.std(scalings) #(1/(N-1))*np.sum(obs-mod-mean)
                    logging.info("Mean, sigma for scale: {}, {}".format(mean, sigma))
                    if n_obj == 1:
                        scale = pm.Normal(
                            "scale",
                            mu=mean[0],
                            sigma=sigma[0],
                            testval=mean[0],
                            # shape=(1,),
                            dtype=inp["precision"],
                        )
                        # shape 1 causes issues for blackjax
                        scale = [
                            scale,
                        ]
                    else:
                        scale = pm.Normal(
                            "scale",
                            mu=mean,
                            sigma=sigma,
                            testval=np.repeat(mean[0], n_obj),
                            shape=(n_obj,),
                            dtype=inp["precision"],
                        )
                else:  # single tracer
                    scale = [
                        0,
                    ] * n_obj

            # potentials on scale?
            if configuration is not None:
                for ps in ("scale", "scale_eff"):
                    if ps in configuration["constraints"]:
                        command_string = configuration["constraints"][ps]
                        logging.info("Setting prior for {}".format(ps))
                        if verbose:
                            logging.debug("EVAL (ID 8): " + command_string)
                        eval(command_string)

            # =========================
            # context specific rules?
            scale_context = {
                o: np.repeat(0, n_comps) for o in inp["observations"].names
            }  # default

            try:  # backward compatibility
                context_specific_rules = vars(
                    __import__(
                        "{}.Library.lib".format(inp["contextshort"].replace("/", ".")),
                        globals(),
                        locals(),
                        ["lib"],
                    )
                )["context_specific_rules"]
            except:  # old one, not allowing contexts within subdirs
                context_specific_rules = vars(
                    __import__(
                        "Contexts.{}.Library.lib".format(inp["contextshort"]),
                        globals(),
                        locals(),
                        ["lib"],
                    )
                )["context_specific_rules"]

            rules = context_specific_rules(inp, verbose=verbose)
            inp["active_rules_details"] = {}
            for i, rule in enumerate(rules):
                if rule in inp["active_rules"]:
                    logging.info("Activating context-specific rule : {}".format(rule))
                    logging.info("----------------------------------")
                    inp["active_rules_details"].update({rule: rules[rule]})
                    cs_args = rules[rule]["cs_args"]
                    logging.info(f"Model parameters: {rules[rule]['params_eval']}")
                    logging.info(f"csargs = {rules[rule]['cs_args']}")
                    logging.info(rules[rule]["scale_context"])
                    eval(rules[rule]["params_eval"])
                    scale_context = eval(
                        rules[rule]["scale_context"], locals(), globals()
                    )
                    # if there are several scalings, do sum in the context-specific library #scale_context = {o: tt.sum([tt.log10(val) for val in scale_context[o]]) for o in scale_context.keys()}
                    if type(scale_context[o]) == list:  # per comp
                        scale_context = {
                            o: tt.log10(scale_context[o]) for o in scale_context.keys()
                        }
                    else:  # single comp, forcing list with one element
                        scale_context = {
                            o: tt.log10([scale_context[o] for s in range(n_comps)])
                            for o in scale_context.keys()
                        }
                    logging.info("")

            # =========================
            if verbose:
                logging.info("")
                logging.info("Summary of parameter distributions : ")
                logging.info("------------------------------------")

            # =========================
            # model predictions

            allfunc = {
                "o_t": {},
            }
            if incomplete_grid:
                allfunc.update({"m_t": {}})
            if model_uls:
                allfunc.update({"m_ul_t": {}})

            for s in range(n_comps):
                tmp_o = o_t
                if incomplete_grid:
                    tmp_m = m_t
                if model_uls:
                    tmp_m_ul = m_ul_t

                # single -> keep one element
                for i, p in enumerate(grid.params.names):
                    ps = f"{p}_{s}"

                    sliceupto = (slice(None),) * (i + 1)

                    if (
                        ps not in grid.params.names_notinf
                        and p not in inp["linterp_params"]
                    ):
                        logging.info(
                            "{} {}: single value (nearest neighbor grid interpolation)".format(
                                p, s
                            )
                        )
                        logging.info(tmp_o.shape.eval())

                        # tmp_o = tmp_o.swapaxes(i + 1, 0)[
                        #    [tt.cast(tt.round(model[f"idx_{ps}"]), "int16")]
                        # ].swapaxes(0, i + 1)
                        # maybe a bit faster...
                        tmp_o = tmp_o[
                            sliceupto
                            + (
                                [
                                    tt.cast(tt.round(model[f"idx_{ps}"]), "int16"),
                                ],
                            )
                        ]
                        if incomplete_grid:
                            # tmp_m = tmp_m.swapaxes(i + 1, 0)[
                            #    [tt.cast(tt.round(model[f"idx_{ps}"]), "int16")]
                            # ].swapaxes(0, i + 1)
                            # maybe a bit faster...
                            tmp_m = tmp_m[
                                sliceupto
                                + (
                                    [
                                        tt.cast(tt.round(model[f"idx_{ps}"]), "int16"),
                                    ],
                                )
                            ]
                        if model_uls:
                            # tmp_m_ul = tmp_m_ul.swapaxes(i + 1, 0)[
                            #    [tt.cast(tt.round(model[f"idx_{ps}"]), "int16")]
                            # ].swapaxes(0, i + 1)
                            # maybe a bit faster...
                            tmp_m_ul = tmp_m_ul[
                                sliceupto
                                + (
                                    [
                                        tt.cast(tt.round(model[f"idx_{ps}"]), "int16"),
                                    ],
                                )
                            ]
                    logging.info("-> {}".format(tmp_o.shape.eval()))

                # linear -> keep two elements and merge
                for i, p in enumerate(grid.params.names):
                    ps = f"{p}_{s}"
                    if p in inp["linterp_params"]:
                        logging.info(
                            "{} {}: single value (linear interpolation)".format(p, s)
                        )
                        logging.info(tmp_o.shape.eval())

                        dx = tt.cast(
                            model[f"idx_{ps}"] - tt.floor(model[f"idx_{ps}"]),
                            inp["precision"],
                        )
                        if inp["linterp_upsample"] > 0:
                            dx = inp["linterp_upsample"] ** -1 * tt.round(
                                dx * inp["linterp_upsample"]
                            )

                        cell = tt.cast(tt.floor(model[f"idx_{ps}"]), "int16")
                        cellup = tt.cast(
                            tt.clip(
                                1 + cell,
                                0,
                                len(grid.params.values[p]) - 1,
                            ),
                            "int16",
                        )

                        if incomplete_grid:
                            tmp_o = tmp_o.swapaxes(i + 1, 0)

                            # we stick to the valid cell if the other one is masked
                            # because it's unlikely we'll draw dx precisely 0.0 or 1.0
                            # tmp_o = tmp_m[[cell]] * tmp_o[[cell]] * tt.switch(
                            #     ((1 - dx) > 0.5) * tt.eq(tmp_m[[cellup]], 0), 1, 1 - dx
                            # ) + tmp_m[[cellup]] * tmp_o[[cellup]] * tt.switch(
                            #     (dx > 0.5) * tt.eq(tmp_m[[cell]], 0), 1, dx
                            # )
                            # trying faster version w/o switches
                            tmp_m = tmp_m.swapaxes(i + 1, 0)
                            tmp_o = tmp_m[[cell]] * tmp_o[[cell]] * (
                                1 - dx * tmp_m[[cellup]]
                            ) + tmp_m[[cellup]] * tmp_o[[cellup]] * (
                                dx * tmp_m[[cell]] + (1 - dx) * (1 - tmp_m[[cellup]])
                            )
                            tmp_o = tmp_o.swapaxes(0, i + 1)

                            tmp_m = (tmp_m[[cell]] + tmp_m[[cellup]]) >= 1
                            tmp_m = tmp_m.swapaxes(0, i + 1)
                        else:
                            sliceupto = (slice(None),) * (i + 1)
                            # tmp_o = tmp_o[[cell]] * (1 - dx) + tmp_o[[cellup]] * dx
                            tmp_o = (
                                tmp_o[
                                    sliceupto
                                    + (
                                        [
                                            cell,
                                        ],
                                    )
                                ]
                                * (1 - dx)
                                + tmp_o[
                                    sliceupto
                                    + (
                                        [
                                            cellup,
                                        ],
                                    )
                                ]
                                * dx
                            )
                            # tmp_o = tmp_o.swapaxes(0, i + 1)
                            # necessary otherwise tmp_m has too many non-0 dimensions
                            # this way tmp_m.shape == tmp_o.shape
                            # tmp_m = tmp_m[[cell]].swapaxes(0, i + 1)
                        if model_uls:
                            tmp_m_ul = tmp_m_ul.swapaxes(i + 1, 0)
                            tmp_m_ul = tmp_m[[cell]] * tmp_m_ul[[cell]] * tt.switch(
                                ((1 - dx) > 0.5) * tt.eq(tmp_m[[cellup]], 0), 1, 1 - dx
                            ) + tmp_m[[cellup]] * tmp_m_ul[[cellup]] * tt.switch(
                                (dx > 0.5) * tt.eq(tmp_m[[cell]], 0), 1, dx
                            )
                            tmp_m_ul = tt.gt(tmp_m_ul, 0.999)
                            tmp_m_ul = tmp_m_ul.swapaxes(0, i + 1)
                        logging.info("-> {}".format(tmp_o.shape.eval()))

                # LOC?
                ignore_dynamic_slicing = False  # tests (typically for CPU sampling)
                for i, p in enumerate(grid.params.names):
                    ps = f"{p}_{s}"
                    if ps not in grid.params.names_notinf:
                        continue
                    logging.info(
                        "{} {}: distribution (power-law, normal...)".format(p, s)
                    )
                    logging.info(tmp_o.shape.eval())

                    pgrid = grid.params.values[p]
                    dgrid = np.append(
                        abs(pgrid[:-1] - np.roll(pgrid, -1)[:-1]),
                        abs((pgrid - np.roll(pgrid, -1))[-2]),
                    )

                    idx = (pgrid >= model[f"lowerbound_{ps}"]) * (
                        pgrid <= model[f"upperbound_{ps}"]
                    )
                    if rcParams["inference_backend"] == "gpu" or ignore_dynamic_slicing:
                        # difficult to enable dynamic slicing in jax, using a mask for now...
                        # for all params
                        bmask = tt.switch(
                            idx,
                            0,
                            -np.inf,
                            # values outside range will be contributing to 0
                        )

                        # idx = tt.switch(
                        #     (pgrid >= model[f"lowerbound_{ps}"])
                        #     * (pgrid <= model[f"upperbound_{ps}"]),
                        #     int(1),
                        #     int(0),
                        # )
                        # pgrid = tt.compress(idx, pgrid, 0)

                    else:
                        # CPU
                        # enabling dynamic slicing, i.e., only selecting subgrid in memory
                        pgrid = theano.shared(pgrid)
                        dgrid = theano.shared(dgrid)

                        pgrid = pgrid[idx]
                        dgrid = dgrid[idx]

                    # weight calculation
                    if ps in inp["plaw_params"]:
                        weight = (1 + model[f"alpha_{ps}"]) * pgrid
                    elif ps in inp["smoothplaw_params"]:
                        weight = (1 + model[f"alpha_{ps}"]) * pgrid
                        corr = -tt.exp(
                            -(10 ** model[f"lowerscale_{ps}"])
                            * (pgrid - model[f"lowerbound_{ps}"])
                        ) - tt.exp(
                            -(10 ** model[f"upperscale_{ps}"])
                            * (model[f"upperbound_{ps}"] - pgrid)
                        )
                        # trying to avoid floating errors
                        corr = tt.maximum(corr, -10)
                        weight += corr
                    elif ps in inp["brokenplaw_params"]:
                        weight = (
                            (1 + model[f"alpha1_{ps}"])
                            * (pgrid - model[f"pivot_{ps}"])
                            * (pgrid <= model[f"pivot_{ps}"])
                        ) + (
                            (1 + model[f"alpha2_{ps}"])
                            * (pgrid - model[f"pivot_{ps}"])
                            * (pgrid > model[f"pivot_{ps}"])
                        )
                    elif ps in inp["normal_params"]:
                        weight = 0.4342944819032518 * (
                            -((model[f"mu_{ps}"] - pgrid) ** 2)
                            / (2.0 * model[f"sigma_{ps}"] ** 2)
                        )
                    elif ps in inp["doublenormal_params"]:
                        weight = tt.log10(
                            tt.exp(
                                -((model[f"mu1_{ps}"] - pgrid) ** 2)
                                / (2.0 * model[f"sigma1_{ps}"] ** 2)
                            )
                            + 10 ** model[f"ratio21_{ps}"]
                            * tt.exp(
                                -((model[f"mu2_{ps}"] - pgrid) ** 2)
                                / (2.0 * model[f"sigma2_{ps}"] ** 2)
                            )
                        )
                    else:
                        logging.panic(f"Parameter {ps} not recognized")

                    # account for irregular grid, need this *before normalizing*
                    weight += tt.log10(dgrid)

                    # apply mask if no dynamic slicing is possible
                    if rcParams["inference_backend"] == "gpu" or ignore_dynamic_slicing:
                        weight += bmask

                    # normalize weights (using only values within boundaries!)
                    weight -= tt.clip(tt.log10(tt.sum(10**weight)), -100, 100)

                    # reshape to be able to sum once swapaxed
                    weight = weight.reshape([pgrid.shape[0]] + [1] * (tmp_o.ndim - 1))

                    # apply weights
                    if rcParams["inference_backend"] == "gpu" or ignore_dynamic_slicing:
                        # GPU / no dynamic slicing

                        tmp_o = (tmp_o.swapaxes(i + 1, 0) + weight).swapaxes(0, i + 1)
                        logging.info("-> {}".format(tmp_o.shape.eval()))

                        if incomplete_grid:
                            bmask = bmask.reshape(
                                [pgrid.shape[0]] + [1] * (tmp_o.ndim - 1)
                            )
                            # outside boundaries = 1 and keep tmp_m = tmp_m within
                            tmp_m = (
                                (tmp_m.swapaxes(i + 1, 0) - 2) * (10**bmask) + 2
                            ).swapaxes(0, i + 1)
                        if model_uls:
                            bmask = bmask.reshape(
                                [pgrid.shape[0]] + [1] * (tmp_o.ndim - 1)
                            )
                            # outside boundaries = 1 and keep tmp_m_ul = tmp_m_ul within
                            tmp_m_ul = (tmp_m_ul - 2) * (10**bmask) + 2
                            tmp_m_ul = (
                                (tmp_m_ul.swapaxes(i + 1, 0) - 2) * (10**bmask) + 2
                            ).swapaxes(0, i + 1)
                            tmp_m_ul = tt.clip(tmp_m_ul, 0, 1)

                    else:
                        # CPU
                        # dynamic slicing

                        sliceupto = (slice(None),) * (i + 1)

                        tmp_o = (tmp_o.swapaxes(i + 1, 0)[idx] + weight).swapaxes(
                            0, i + 1
                        )
                        # tmp_m[sliceupto + (idx,)] += weight
                        logging.info("-> (example) {}".format(tmp_o.shape.eval()))

                        if incomplete_grid:
                            tmp_m = tmp_m[sliceupto + (idx,)]
                        if model_uls:
                            tmp_m_ul = tmp_m_ul[sliceupto + (idx,)]

                # integration of observables and masks
                if len(grid.params.names_notinf) > 0:
                    # operations along all axes except 0 (obs)
                    axis = tuple(range(1, tmp_o.ndim))  # (1,2...)
                    tmp_o = tt.log10(tt.sum(10**tmp_o, axis=axis))

                    if incomplete_grid:
                        tmp_m = tt.prod(tmp_m, axis=axis)
                    if model_uls:
                        tmp_m_ul = tt.prod(tmp_m_ul, axis=axis)

                # if hierarchical:
                # somehow some axes not broadcastable so squeeze doesn't remove all the 0-dimensions
                axis = tuple(range(1, tmp_o.ndim))
                # tuple([i + 1 for i in range(tmp_o.ndim - 1)])  # (1,2...)
                allfunc["o_t"].update({s: tmp_o.squeeze().astype(inp["precision"])})
                if incomplete_grid:
                    allfunc["m_t"].update({s: tmp_m.squeeze().astype("int16")})
                if model_uls:
                    allfunc["m_ul_t"].update(
                        {s: tmp_m_ul.squeeze(axis=axis).astype("int16")}
                    )
                # else:
                #     allfunc["o_t"].update({s: tmp_o.squeeze().astype("float64")})
                #     allfunc["m_t"].update({s: tmp_m.squeeze().astype("int16")})
                #     # allfunc["m_t"].update({s: np.ones(n_obs).astype(np.int16)}) # tests
                #     if model_uls:
                #         allfunc["m_ul_t"].update(
                #             {s: tmp_m_ul.squeeze().astype("int16")}
                #         )
                #         # allfunc["m_ul_t"].update({s: np.ones(n_obs).astype(np.int16)}) # tests

            # ==================================
            # create model predictions

            predicted_combined_obs = {}
            if model_uls:
                predicted_combined_obs_min = (
                    {}
                )  # that's the minimum value for several components if there are model ULs
            predicted_combined_mask = {}
            predicted_combined_maskul = {}

            for i, o in enumerate(observations.names):
                j = observations.getidx(o)
                # print(i, o, inp['observations'].obs[o].extensive, j, o_t[j].min().eval(), o_t[j].mean().eval())

                iobj = observations.obs[o].iobj

                if (
                    o in use_scaling_specific
                ):  # scaling a posterior some tracers: we do not consider the second sector
                    # note that this won't work for hierarchical approach because for now we force n_comps = 1
                    if hierarchical:
                        logging.error(
                            "use_scaling_specific is not compatible with hierarchical for now"
                        )
                    predicted_combined_obs.update(
                        {
                            o: scale_specific[o]
                            + scale[iobj]
                            * theano.shared(inp["observations"].obs[o].extensive)
                            + logsum_t(
                                [w[0]], [scale_context[o][0] + allfunc["o_t"][0][j]]
                            )
                        }
                    )
                    # that's the minimum value for several components if there are model ULs
                    if model_uls:
                        predicted_combined_obs_min.update({o: infcode})
                else:
                    if hierarchical:  # we don't combine components
                        predicted_combined_obs.update(
                            {
                                o: scale[iobj]
                                * theano.shared(inp["observations"].obs[o].extensive)
                                + allfunc["o_t"][iobj][j]
                            }
                        )
                    else:  # we combine components
                        predicted_combined_obs.update(
                            {
                                o: scale_specific[o]
                                + scale[iobj]
                                * theano.shared(inp["observations"].obs[o].extensive)
                                + logsum_t(
                                    w,
                                    [
                                        scale_context[o][s] + allfunc["o_t"][s][j]
                                        for s in range(n_comps)
                                    ],
                                )
                            }
                        )

                    if model_uls:
                        # that's the minimum value for several components if there are model ULs, we take only the sectors with no model ULs
                        if hierarchical:  # we don't combine components
                            predicted_combined_obs_min.update(
                                {
                                    o: scale_specific[o]
                                    + scale[iobj]
                                    * theano.shared(
                                        inp["observations"].obs[o].extensive
                                    )
                                    + scale_context[o][iobj]
                                    + allfunc["o_t"][iobj][j]
                                }
                            )
                        else:  # we combine components
                            if n_comps == 1:
                                predicted_combined_obs_min.update(
                                    {
                                        o: scale_specific[o]
                                        + scale[iobj]
                                        * theano.shared(
                                            inp["observations"].obs[o].extensive
                                        )
                                        + logsum_t(
                                            w * allfunc["m_ul_t"][0][j],
                                            [
                                                scale_context[o][s]
                                                + allfunc["o_t"][s][j]
                                                for s in range(n_comps)
                                            ],
                                        )
                                    }
                                )
                            else:
                                predicted_combined_obs_min.update(
                                    {
                                        o: scale_specific[o]
                                        + scale[iobj]
                                        * theano.shared(
                                            inp["observations"].obs[o].extensive
                                        )
                                        + logsum_t(
                                            [
                                                w[s] * allfunc["m_ul_t"][s][j]
                                                for s in range(n_comps)
                                            ],
                                            [
                                                scale_context[o][s]
                                                + allfunc["o_t"][s][j]
                                                for s in range(n_comps)
                                            ],
                                        )
                                    }
                                )

                # no need to do the full calculation with w, we just need to know if there are values that are not 1 (invalid data) in the parameter values
                # =one only if value=1 or linear interpolation between several one-values
                if incomplete_grid:
                    if hierarchical:  # we don't combine components
                        predicted_combined_mask.update(
                            {o: tt.all(tt.eq(allfunc["m_t"][iobj][j], 1))}
                        )
                    else:  # we combine components
                        predicted_combined_mask.update(
                            {
                                o: tt.all(
                                    tt.eq(
                                        [allfunc["m_t"][s][j] for s in range(n_comps)],
                                        1,
                                    )
                                )
                            }
                        )
                else:
                    predicted_combined_mask.update({o: 1})

                if model_uls:
                    if hierarchical:  # we don't combine components
                        predicted_combined_maskul.update(
                            {o: tt.all(tt.eq(allfunc["m_ul_t"][iobj][j], 1))}
                        )
                    else:  # we combine components
                        predicted_combined_maskul.update(
                            {
                                o: tt.all(
                                    tt.eq(
                                        [
                                            allfunc["m_ul_t"][s][j]
                                            for s in range(n_comps)
                                        ],
                                        1,
                                    )
                                )
                            }
                        )
                else:
                    predicted_combined_maskul.update({o: 1})

            # =========================
            if single_normalizing != "":
                scale = [
                    pm.Deterministic(
                        "scale",
                        observed_offset[single_normalizing]
                        - predicted_combined_obs[single_normalizing],
                    )
                ] * n_obj
            else:  # scale was already defined and used for predicted_combined_obs
                scale = [
                    0,
                ] * n_obj
            inp.update({"single_normalizing": single_normalizing})

            # =========================
            # Observables
            # no need to test intensive params, scales have been * by 0
            pred_obs = {}
            pred_obs_min = {}
            for o in observations.names:
                iobj = observations.obs[o].iobj

                pred_obs.update(
                    {o: scale[iobj] + scale_sysunc[o] + predicted_combined_obs[o]}
                )  # scale may now be 0
                if model_uls:
                    pred_obs_min.update(
                        {
                            o: scale[iobj]
                            + scale_sysunc[o]
                            + predicted_combined_obs_min[o]
                        }
                    )
            if getContextOnly:  # make some deterministic variables
                tmp = [pm.Deterministic(o, pred_obs[o]) for o in observations.names]

            # =================================================
            # will contain all the probability distributions for each tracer
            # StudentT distribution has more power in the wings, which is useful to consider that outliers have a larger probability than with a normal distribution => Student-T or Normal with a larger dispersion with delta_mod

            all_dist = {i: [] for i in range(n_obj)}

            if verbose:
                logging.info("")
                logging.info("Summary of observables (normalized)  : ")
                logging.info("--------------------------------------")

            for o in observations.names:
                # tmpup and tmplo wrt obs
                obs_valuescale = observations.obs[o].obs_valuescale
                strext = "extensive" if observations.obs[o].extensive else "intensive"

                iobj = observations.obs[o].iobj

                if obs_valuescale == "log":
                    if input_params["pmversion"] >= 4:
                        tmpup = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=pred_obs[o],
                                sigma=observations.obs[o].delta[0],
                            ),
                            observed_offset[o],
                        )
                        tmpupnorm = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=0.0,
                                sigma=observations.obs[o].delta[0],
                            ),
                            0.0,
                        )  # peak value, for normalizing
                        tmplo = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=pred_obs[o],
                                sigma=observations.obs[o].delta[1],
                            ),
                            observed_offset[o],
                        )
                        tmplonorm = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=0.0,
                                sigma=observations.obs[o].delta[1],
                            ),
                            0.0,
                        )  # peak value, for normalizing
                    else:
                        tmpup = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=pred_obs[o],
                            sigma=observations.obs[o].delta[0],
                        ).logp(observed_offset[o])
                        tmpupnorm = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=0.0,
                            sigma=observations.obs[o].delta[0],
                        ).logp(
                            0.0
                        )  # peak value, for normalizing
                        tmplo = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=pred_obs[o],
                            sigma=observations.obs[o].delta[1],
                        ).logp(observed_offset[o])
                        tmplonorm = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=0.0,
                            sigma=observations.obs[o].delta[1],
                        ).logp(
                            0.0
                        )  # peak value, for normalizing
                    # nsigma = (10**observed_offset[o]-10**pred_obs[o])/10**observations.obs[o].delta

                    if model_uls:
                        if input_params["pmversion"] >= 4:
                            tmpupmin = pm.logp(
                                pm.StudentT.dist(
                                    inp["StudentT_nu"],
                                    mu=pred_obs_min[o],
                                    sigma=observations.obs[o].delta[0],
                                ),
                                observed_offset[o],
                            )
                            tmplomin = pm.logp(
                                pm.StudentT.dist(
                                    inp["StudentT_nu"],
                                    mu=pred_obs_min[o],
                                    sigma=observations.obs[o].delta[1],
                                ),
                                observed_offset[o],
                            )
                        else:
                            tmpupmin = pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=pred_obs_min[o],
                                sigma=observations.obs[o].delta[0],
                            ).logp(observed_offset[o])
                            tmplomin = pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=pred_obs_min[o],
                                sigma=observations.obs[o].delta[1],
                            ).logp(observed_offset[o])

                    strval = observed_offset[o]
                    strdelta = observations.obs[o].delta
                else:  # if obs_valuescale is linear, only change the predicted and observed values, delta scale was not changed but we do need to normalize it because observed offset has been scaled
                    if observations.obs[o].extensive:
                        norm = 1 / 10 ** order_of_magnitude_obs[iobj]
                    else:  # intensive
                        norm = 1.0

                    if input_params["pmversion"] >= 4:
                        tmpup = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=10 ** pred_obs[o],
                                sigma=observations.obs[o].delta[0] * norm,
                            ),
                            10 ** observed_offset[o],
                        )
                        # peak value, for normalizing:
                        tmpupnorm = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=0.0,
                                sigma=observations.obs[o].delta[0] * norm,
                            ),
                            0.0,
                        )
                        tmplo = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=10 ** pred_obs[o],
                                sigma=observations.obs[o].delta[1] * norm,
                            ),
                            10 ** observed_offset[o],
                        )
                        # peak value, for normalizing:
                        tmplonorm = pm.logp(
                            pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=0.0,
                                sigma=observations.obs[o].delta[1] * norm,
                            ),
                            0.0,
                        )
                        # nsigma = (10**observed_offset[o]-10**pred_obs[o])/(observations.obs[o].delta*norm)
                    else:
                        tmpup = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=10 ** pred_obs[o],
                            sigma=observations.obs[o].delta[0] * norm,
                        ).logp(10 ** observed_offset[o])
                        tmpupnorm = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=0.0,
                            sigma=observations.obs[o].delta[0] * norm,
                        ).logp(
                            0.0
                        )  # peak value, for normalizing
                        tmplo = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=10 ** pred_obs[o],
                            sigma=observations.obs[o].delta[1] * norm,
                        ).logp(10 ** observed_offset[o])
                        tmplonorm = pm.StudentT.dist(
                            inp["StudentT_nu"],
                            mu=0.0,
                            sigma=observations.obs[o].delta[1] * norm,
                        ).logp(
                            0.0
                        )  # peak value, for normalizing

                    if model_uls:
                        if input_params["pmversion"] >= 4:
                            tmpupmin = pm.logp(
                                pm.StudentT.dist(
                                    inp["StudentT_nu"],
                                    mu=10 ** pred_obs_min[o],
                                    sigma=observations.obs[o].delta[0] * norm,
                                ),
                                10 ** observed_offset[o],
                            )
                            tmplomin = pm.logp(
                                pm.StudentT.dist(
                                    inp["StudentT_nu"],
                                    mu=10 ** pred_obs_min[o],
                                    sigma=observations.obs[o].delta[1] * norm,
                                ),
                                10 ** observed_offset[o],
                            )
                        else:
                            tmpupmin = pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=10 ** pred_obs_min[o],
                                sigma=observations.obs[o].delta[0] * norm,
                            ).logp(10 ** observed_offset[o])
                            tmplomin = pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=10 ** pred_obs_min[o],
                                sigma=observations.obs[o].delta[1] * norm,
                            ).logp(10 ** observed_offset[o])

                    strval = 10 ** observed_offset[o]
                    strdelta = np.array(observations.obs[o].delta) * norm

                if o in observations.mask_obs_detected:
                    # pm.Deterministic('pred_{}'.format(o), pred_obs[o])
                    if verbose:
                        logging.info(
                            f"- {o} [{obs_valuescale},{strext}]: observed value = {strval: <5,.2f} [{strdelta[0]: <5,.2f},{strdelta[1]: <5,.2f}]"
                        )
                    if model_uls:
                        all_dist[iobj] += [
                            tt.switch(
                                tt.gt(observed_offset[o], pred_obs[o]),
                                tt.switch(
                                    tt.eq(predicted_combined_maskul[o], 1),
                                    # **** model ****               **** obs *****
                                    tmplo - tmplonorm,
                                    # modelmin --->               <--- model                    **** obs *****
                                    # infcode), #model ULs: rigorously it should be <tmplo-tmplonorm
                                    # tt.log10(10**tmplo+10**tmplomin)-tmplonorm), #model ULs: rigorously it should be <tmplo-tmplonorm
                                    tt.log10(10**tmplo + 10**tmplomin)
                                    - 0.30102999566
                                    - tmplonorm,
                                ),  # model ULs: taking mean (predobs_min expected to be very close to predobs)
                                tt.switch(
                                    tt.eq(predicted_combined_maskul[o], 1),
                                    # **** obs *****                 **** model ****
                                    tmpup - tmpupnorm,
                                    #  **** obs *****                <--- model
                                    tt.switch(
                                        tt.gt(observed_offset[o], pred_obs_min[o]),
                                        # modelmin --->              **** obs*****                <--- model
                                        0,
                                        #  **** obs *****        modelmin --->                     <--- model
                                        # tmpupmin-tmpupnorm)))
                                        tt.log10(10**tmpup + 10**tmpupmin)
                                        - 0.30102999566
                                        - tmpupnorm,
                                    ),
                                ),
                            ),  # model ULs: taking mean (predobs_min expected to be very close to predobs)
                        ]
                    else:  # ignore model ULs:
                        if (
                            observations.obs[o].delta[0] == observations.obs[o].delta[1]
                        ):  # symmetric EBs, avoiding switch
                            all_dist[iobj] += [
                                tmpup - tmpupnorm,
                            ]  # same as tmplo-tmplonorm
                        else:
                            all_dist[iobj] += [
                                tt.switch(
                                    tt.gt(observed_offset[o], pred_obs[o]),
                                    # **** model ****               **** obs *****
                                    tmplo - tmplonorm,
                                    # **** obs*****                 **** model ****
                                    tmpup - tmpupnorm,
                                ),
                            ]

                else:
                    # OBS RANGES
                    if (
                        o in observations.mask_obs_lo_lims
                        and o in observations.mask_obs_up_lims
                        and not args.no_lims
                    ):  # range
                        if verbose:
                            logging.info(
                                f"- {o} [{obs_valuescale},{strext}]: range [{strval: <5,.2f}, {sstrdelta[0]: <5,.2f}]"
                            )
                        all_dist[iobj] += [
                            tt.switch(
                                tt.lt(pred_obs[o], observed_offset[o]),
                                # **** model ****                obs LL -->
                                infcode,  # should be <tmpup-tmpupnorm
                                tt.switch(
                                    tt.gt(pred_obs[o], observations.obs[o].delta[0]),
                                    # obs LL --->      <--- obs UL          **** model ****
                                    infcode,
                                    # obs LL --->          **** model ****           <--- obs UL
                                    0.0,
                                ),
                            ),
                        ]

                    # OBS ULS
                    elif o in observations.mask_obs_up_lims and not args.no_lims:  # UL
                        if verbose:
                            logging.info(
                                f"- {o} [{obs_valuescale},{strext}]: upper limit <{strval: <5,.2f} [{strdelta[0]: <5,.2f},{strdelta[1]: <5,.2f}]"
                            )
                        if input_params["pmversion"] >= 4:
                            tmpup_ul = pm.logp(
                                pm.StudentT.dist(
                                    inp["StudentT_nu"],
                                    mu=10 ** pred_obs[o],
                                    sigma=1
                                    / inp["upper_limit_sigma"]
                                    * 10 ** observed_offset[o],
                                ),
                                0.0,
                            )
                            tmpupnorm_ul = pm.logp(
                                pm.StudentT.dist(
                                    inp["StudentT_nu"],
                                    mu=0.0,
                                    sigma=1
                                    / inp["upper_limit_sigma"]
                                    * 10 ** observed_offset[o],
                                ),
                                0.0,
                            )  # peak value, for normalizing
                        else:
                            tmpup_ul = pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=10 ** pred_obs[o],
                                sigma=1
                                / inp["upper_limit_sigma"]
                                * 10 ** observed_offset[o],
                            ).logp(0.0)
                            tmpupnorm_ul = pm.StudentT.dist(
                                inp["StudentT_nu"],
                                mu=0.0,
                                sigma=1
                                / inp["upper_limit_sigma"]
                                * 10 ** observed_offset[o],
                            ).logp(
                                0.0
                            )  # peak value, for normalizing

                        if model_uls:
                            # account for model ULs
                            all_dist[iobj] += [
                                tt.switch(
                                    tt.eq(predicted_combined_maskul[o], 1),
                                    # **** obs UL = detection at 0 with sigma=UL ****      **** model ****
                                    # **** model ****       **** obs UL = detection at 0 with sigma=UL ****
                                    tmpup_ul - tmpupnorm_ul,
                                    tt.switch(
                                        tt.lt(pred_obs_min[o], observed_offset[o]),
                                        # modelmin --->         <--- model        **** obs UL = detection at 0 with sigma=UL ****
                                        0.0,
                                        # **** obs UL = detection at 0 with sigma=UL ****           modelmin --->         <--- model
                                        infcode,
                                    ),
                                ),
                            ]
                        else:
                            # **** obs UL = detection at 0 with sigma=UL ****      **** model ****
                            # **** model ****       **** obs UL = detection at 0 with sigma=UL ****
                            all_dist[iobj] += [
                                tmpup_ul - tmpupnorm_ul,
                            ]

                    # OBS LLS
                    elif o in observations.mask_obs_lo_lims and not args.no_lims:  # LL
                        if verbose:
                            logging.info(
                                f"- {o} [{obs_valuescale},{strext}]: lower limit >{strval: <5,.2f} [{strdelta[0]: <5,.2f},{strdelta[1]: <5,.2f}]"
                            )
                        if obs_valuescale == "log":
                            # tmplo, tmplonorm = -100, 0

                            # need an error bar!
                            all_dist[iobj] += [
                                tt.switch(
                                    tt.gt(observed_offset[o], pred_obs[o]),
                                    # **** model ****         obs LL --->
                                    # or
                                    # <--- model              obs LL --->
                                    tmplo - tmplonorm,
                                    # obs LL --->            **** model ****
                                    # or
                                    # obs LL --->            <--- model
                                    0.0,
                                ),  # with peak-normalization
                            ]  # model UL? same expression anyway
                        else:  # if obs_valuescale is linear, consider LL as a hard cut
                            all_dist[iobj] += [
                                tt.switch(
                                    tt.gt(observed_offset[o], pred_obs[o]),
                                    # **** model ****         obs LL --->
                                    # or
                                    # <--- model              obs LL --->
                                    infcode,
                                    # obs LL --->            **** model ****
                                    # or
                                    # obs LL --->            <--- model
                                    0.0,
                                ),  # with peak-normalization
                            ]  # model UL? same expression anyway

                if o in inp["obs_skiplikelihood"]:
                    # remove from likelihood calculation
                    all_dist[iobj] = all_dist[iobj][0:-2]
                else:
                    if not args.no_individual_likelihoods:
                        pm.Deterministic(f"likelihood_{o}", all_dist[iobj][-1])

            # only if grid is incomplete
            if incomplete_grid:
                # for o in observations.mask_obs_detected:
                #     pm.Deterministic(f"mask_{o}", tt.min(predicted_combined_mask[o]))
                tmp_mask = tt.prod(
                    [
                        predicted_combined_mask[o]
                        for o in observations.mask_obs_detected
                        if o not in inp["obs_skiplikelihood"]
                        # and observations.obs[o].iobj == iobj
                    ],
                    dtype="int32",
                )
                pm.Deterministic(
                    "mask",
                    tmp_mask,
                )

                if rcParams["inference_backend"] == "cpu" and ~np.isfinite(infcode):
                    # -inf is OK with CPU, avoid the switch
                    for iobj in range(n_obj):
                        all_dist[iobj] += [
                            2
                            - 1 / tmp_mask,  # tmp_mask = 0 -> -inf, tmp_mask = 1  -> 1
                        ]
                else:
                    for iobj in range(n_obj):
                        all_dist[iobj] += [
                            tt.switch(tt.eq(tmp_mask, 0), infcode, 0),
                        ]

            # if we do not wanna draw models with ULs
            # if model_uls:
            # tmp_invul = pm.Deterministic('mask_ul', tt.min([predicted_combined_maskul[o] for o in observations.mask_obs_detected]))
            # all_dist[iobj] += [tt.switch(tt.eq(tmp_inv, 0),
            #                        infcode,
            #                        0),]

            # sum of all likelihoods in log (linear multiplication)
            for iobj in range(n_obj):
                # pm.Potential(f"likelihood{iobj}", tt.sum(all_dist[iobj]))
                # float64 here for likelihood_logp to avoid precision issues in SMC resampling
                pm.Potential(
                    f"likelihood{iobj}",
                    tt.sum(all_dist[iobj], dtype="float64"),  # float64 important!
                )
                pm.Deterministic(f"log_likelihood{iobj}", tt.sum(all_dist[iobj]))

            # save log likelihood for comparisons
            pm.Deterministic(
                f"log_likelihood",
                tt.sum([tt.sum(all_dist[iobj]) for iobj in range(n_obj)]),
            )

            # equivalent to = pm.DensityDist('like', tt.sum(all_dist), observed={'o1':..., 'o2':...})
            # see also https://nbviewer.jupyter.org/github/OriolAbril/Exploratory-Analysis-of-Bayesian-Models/blob/multi_obs_ic/content/Section_04/Multiple_likelihoods.ipynb
            # by doing the sum we have only one likelihood

            # Distribution based on a given log density function.
            # def logp(obs):
            #   return [-0.5*((pred_obs[o]-obs[o])/tt.max([delta_mod, delta[o]]))**2. for o in mask_obs_detected]
            # ymod = pm.DensityDist('ymod', logp, observed=observed_offset)

            # =================================================
            # observables with no measurements => infer (only if --force-missing tag, otherwise done in mgris_process)
            if len(observations.mask_obs_nan) > 0:
                if verbose:
                    logging.debug(
                        "Observables with no measurements => infer  : {}".format(
                            observations.mask_obs_nan
                        )
                    )

            # =========================
            # explicit lines to infer (test for plaw when mgris_process was not ready for it yet) (only if --force-missing tag?)
            # if grid.params.names_notinf != []:
            # for o in observations.names:
            #    pm.Deterministic(o, pred_obs[o] + order_of_magnitude_obs)

            # =========================
            # Calculate prior predictive sample to compare prior & posterior later on in mgris_process

            prior_predictive = True

            if prior_predictive:
                logging.info("")
                logging.info("Calculating prior predictive sample...")
                logging.info("--------------------------------------")
                logging.info("Number of draws: {}".format(nsamples_prior))
                var_names = [
                    v.name for v in model.unobserved_RVs if "likelihood_" not in v.name
                ]
                prior_predictive = pm.sample_prior_predictive(
                    samples=nsamples_prior, var_names=var_names, random_seed=random_seed
                )

                # check boundaries priors with -inf likelihood
                priors_inf = [
                    k
                    for k in prior_predictive.keys()
                    if "prior_" in k and prior_predictive[k].size == n_samples
                ]
                # print(priors_inf)

                # get only good samples (remove inf values)
                if priors_inf != []:
                    samples_ok = []
                    for s in range(n_samples):
                        if np.isfinite(
                            np.prod([prior_predictive[k][s] for k in priors_inf])
                        ):
                            samples_ok += [
                                s,
                            ]
                    # update all arrays
                    if len(samples_ok) > 0:
                        for k in prior_predictive.keys():
                            if prior_predictive[k].size > 1:
                                prior_predictive[k] = prior_predictive[k][samples_ok]

                logging.info(az.summary(prior_predictive))

                resdict["prior_predictive"] = prior_predictive

            # =========================
            if getContextOnly:
                return model
            else:
                logging.info("\n")

                logging.info("Calculating posterior distribution...")
                logging.info("-------------------------------------")

                logging.info("✨")
                logging.info("✨✨")
                logging.info("✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info(
                    "✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨ INFERENCE START"
                )
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨")
                logging.info("✨✨")
                logging.info("✨")

                logging.info("\n")

                # =============================
                # how many "free" parameters (RVs)?
                # will be used to guesstimate the number of draws needed
                modelvars = (
                    model.value_vars if input_params["pmversion"] >= 4 else model.vars
                )
                freev = [
                    v
                    for v in modelvars
                    if (
                        "scale" not in v.name
                        or "lowerscale" in v.name
                        or "upperscale" in v.name
                    )
                    # and "sysunc" not in v.name (?)
                ]
                nfreev = len(freev)
                nfreev += n_comps  # scale
                logging.info("Random variables ({}) : ".format(nfreev))
                logging.info("-----------------------".format(nfreev))
                # logging.info(modelvars)
                for j in modelvars:
                    logging.info(f"- {j.name}")

                if len(inp["linterp_params"]) >= 1:
                    logging.info("")
                    logging.info(
                        "Linear interpolation for   : {}".format(inp["linterp_params"])
                    )

                # =============================
                logging.info("")
                logging.info("Model test point : ")
                logging.info("------------------")
                if input_params["pmversion"] >= 4:
                    modeltestpoint = model.point_logps()
                    for v in modeltestpoint:
                        logging.info(f"- {v}: {modeltestpoint[v]: <5,.2f}")
                else:
                    for RV in model.basic_RVs:
                        logging.info(
                            "- {}: {}".format(
                                RV.name, np.round(RV.logp(model.test_point), 2)
                            )
                        )
                # make sure all logp are finite
                # logging.info('Model test point check')
                # model.check_test_point()

                # =============================
                # backward compatibility
                if "continuous_value_vars" not in dir(model):
                    model.continuous_value_vars = model.cont_vars
                if "discrete_value_vars" not in dir(model):
                    model.discrete_value_vars = model.disc_vars
                # =============================

                # ===================================

                animate = False

                nsamples_per_job = inp["nsamples_per_job"]
                njobs = inp["njobs"]

                # ===================================
                # start values

                start = None  # if start is None, PyMC should start from test point

                # ===================================
                # settings for sampler
                if step_in == "SMC":
                    if model.continuous_value_vars == []:
                        panic("SMC needs at least one continuous random variable")

                    if rcParams["inference_backend"] == "gpu":
                        step_str = "Sequential Monte-Carlo, {} kernel".format(
                            input_params["smc_kernel"]
                        )

                        logging.info(
                            """
                   ┍========================================┑
                   ┝=========== Sampler summary ============┥
                   ┝========================================┙
                   ┝ Sampling step method : {} ({})
                   ┝ # (successive) jobs  : {}
                   ┝ # particles / job    : {} (min={}, max={})
                   ┝ # particles (total)  : {}
                   ┕========================================
                   """.format(
                                step_in,
                                step_str,
                                njobs,
                                nsamples_per_job,
                                int(inp["nsamples_per_job_min"]),
                                int(inp["nsamples_per_job_max"]),
                                nsamples_per_job * njobs,
                            )
                        )
                        # input_params["smc_iterations_to_diagnose"] = min(
                        #    [500, 100 + 2 * nfreev * 10]
                        # )
                        from PyMC.PyMC5.GPU.sampling_smc_jax import (
                            sample_smc_blackjax as sample_smc,
                        )

                        if jax.default_backend() != "gpu":
                            panic("GPU will not be used, is that really intended?")

                        closelogfiles()
                        traces = []
                        stime = 0
                        for n in range(njobs):
                            if random_seed is None:
                                rs = None
                            else:
                                rs = random_seed + n
                            t = sample_smc(
                                random_seed=rs,
                                n_particles=nsamples_per_job,
                                kernel=input_params["smc_kernel"],
                                inner_kernel_params={
                                    "step_size": input_params["smc_step_size"],
                                    "integration_steps": input_params[
                                        "smc_integration_steps"
                                    ],
                                },
                                iterations_to_diagnose=input_params[
                                    "smc_iterations_to_diagnose"
                                ],
                                target_essn=input_params["smc_target_essn"],
                                num_mcmc_steps=input_params["smc_mcmc_steps"],
                            )
                            stime += t.posterior.running_time_seconds
                            traces += [
                                t,
                            ]
                            gc.collect()
                            jax.clear_caches()

                        trace = t
                        trace.posterior = xarray.concat(
                            [traces[n].posterior for n in range(njobs)], dim="chain"
                        ).assign_coords(chain=range(njobs))
                        del t, traces
                        closelogfiles()
                        logger = setup_custom_logger(logname, loglevel=loglevel)
                        movelogfile(logname, output_directory + "/Logs/")

                        # jax.clear_caches()

                        # if dimensions are (ndraws, njobs), revert to scale model parameter as (nobjs, ndraws, njobs)
                        if trace.posterior["scale"].ndim == 2:
                            trace.posterior["scale"] = trace.posterior[
                                "scale"
                            ].expand_dims(dim={"scale_dim_0": 1}, axis=2)
                            prior_predictive.prior["scale"] = prior_predictive.prior[
                                "scale"
                            ].expand_dims(dim={"scale_dim_0": 1}, axis=2)

                        lml = np.zeros(1)
                        logging.warning(
                            "Sampler                : {}".format(
                                trace.posterior.sampler
                            )
                        )
                        logging.warning(
                            "iterations             : {}".format(
                                trace.posterior.iterations
                            )
                        )
                        logging.warning(
                            "iterations to diagnose : {}".format(
                                trace.posterior.iterations_to_diagnose
                            )
                        )
                        logging.warning(
                            "MCMC steps             : {}".format(
                                trace.posterior.num_mcmc_steps
                            )
                        )
                        logging.warning(
                            "Step size              : {}".format(
                                trace.posterior.step_size
                            )
                        )
                        if "integration_steps" in trace.posterior.keys():
                            logging.warning(
                                "Integration steps      : {}".format(
                                    trace.posterior.integration_steps
                                )
                            )
                        # lambda_evolution, log_likelihood_increments

                    else:  # CPU
                        step_str = "Sequential Monte-Carlo, Independent Metropolis-Hastings kernel"  #
                        parallel = True
                        if not input_params["pmversion"] >= 4:
                            for i, var in enumerate(modelvars):
                                if (
                                    type(modelvars[i].distribution)
                                    == pm.distributions.mixture.Mixture
                                ):
                                    parallel = False

                        if ncores_eff == 1:
                            parallel = False

                        logging.info(
                            """
                   ┍========================================┑
                   ┝=========== Sampler summary ============┥
                   ┝========================================┙
                   ┝ Sampling step method         : {} ({})
                   ┝ Parallel                     : {}
                   ┝ # cores (jobs)               : {}
                   ┝ # samples (IMH chains) / job : {} (min={}, max={})
                   ┝ # samples (total)            : {} (max={})
                   ┕========================================
                   """.format(
                                step_in,
                                step_str,
                                parallel,
                                ncores_eff,
                                nsamples_per_job,
                                int(inp["nsamples_per_job_min"]),
                                int(inp["nsamples_per_job_max"]),
                                nsamples_per_job * njobs,
                                int(inp["nsamples_max"]),
                            )
                        )

                        if input_params["pmversion"] >= 4:
                            # with IMH chains, it may be more efficient to use modified samplers to force IMH sampling for any given [iobj], while allowing other [iobj] to be sampled at the same time

                            test_independent = False
                            # import version with independent iobj likelihoods (testing phase)?
                            if hierarchical and test_independent:
                                if input_params["pmversion"] == 4:
                                    from PyMC.PyMC4.sampling_h import sample_smc
                                else:
                                    from PyMC.PyMC5.sampling_h import sample_smc
                            else:
                                sample_smc = pm.sample_smc

                                # from PyMC.PyMC5.kernels import IMH, MH, ABC

                                # ABC requires some rewriting of mgris_search

                            closelogfiles()
                            trace = sample_smc(
                                nsamples_per_job,
                                random_seed=random_seed,
                                # kernel=IMH,
                                cores=ncores_eff,
                                parallel=parallel,
                                start=start,
                                threshold=input_params["smc_threshold"],
                                correlation_threshold=input_params[
                                    "smc_corr_threshold"
                                ],
                            )
                            closelogfiles()
                            logger = setup_custom_logger(logname, loglevel=loglevel)
                            movelogfile(logname, output_directory + "/Logs/")

                            stime = trace.sample_stats._t_sampling

                            if (
                                trace.sample_stats.log_marginal_likelihood.shape[0]
                                != trace.posterior.dims["chain"]
                            ):
                                lml = np.array(
                                    [
                                        np.array(
                                            trace.sample_stats.log_marginal_likelihood.sel(
                                                draw=d
                                            )
                                        )[0][-1]
                                        for d in range(
                                            trace.sample_stats.log_marginal_likelihood.shape[
                                                1
                                            ]
                                        )
                                    ]
                                )
                                del (
                                    trace.sample_stats
                                )  # or just trace.sample_stats = trace.sample_stats.drop('log_marginal_likelihood')
                            else:
                                lml = np.array(
                                    [
                                        np.array(
                                            trace.sample_stats.log_marginal_likelihood.sel(
                                                chain=c
                                            )
                                        )[-1]
                                        for c in range(
                                            trace.sample_stats.log_marginal_likelihood.shape[
                                                0
                                            ]
                                        )
                                    ]
                                )
                        else:  # PyMC 3
                            if hierarchical:
                                from PyMC.PyMC3.sample_smc3 import sample_smc
                            elif animate:
                                from PyMC.sample_smc_savetrace import sample_smc
                            else:
                                sample_smc = pm.sample_smc
                            trace = sample_smc(
                                nsamples_per_job,
                                cores=ncores_eff,
                                parallel=parallel,
                                start=start,
                                threshold=inp["smc_threshold"],
                                p_acc_rate=inp["smc_acc_rate"],
                                random_seed=random_seed,
                            )  # tune_steps=False, n_steps=100,
                            # idata_kwargs=dict(log_likelihood=False)?
                            lml = trace.report.log_marginal_likelihood
                            trace = az.data.convert_to_inference_data(
                                trace
                            )  # not yet an option for return_inferencedata=True in sample_smc
                            stime = trace.posterior.sampling_time

                    resdict.update({"stime": stime})
                    resdict.update({"log_marginal_likelihood_chains": lml})
                    resdict.update({"log_marginal_likelihood": lml.mean()})

                else:
                    # NUTS sampler

                    # start values (may be overridden for NUTS)
                    # start = pm.find_MAP() #find_MAP somehow messes up the solution for @as_op funcs

                    # by default None (starting at prior mean)
                    # start = None

                    # random start values for each chain
                    # start = []
                    # for i in range(njobs):
                    #    tmp = {}
                    #    #problem with randomizing w is that wtot can be a free parameter... We randomize only if wtot==1
                    #    if wtot==1:
                    #       wtmp = sorted(np.random.random(n_sectors))[::-1]
                    #       wtmp /= np.sum(wtmp) * wtot
                    #    #else: w_s will start at same values for each chain, which is fine
                    #    for s in range(n_sectors):
                    #       tmp.update({'w_s{}'.format(s): wtmp[s]})
                    #       for p in params_name:
                    #          if inp['type_constraints']=='real': #real values
                    #             tmp.update({'{}_s{}'.format(p, s): np.random.uniform(np.min(Params_values[p]), np.max(Params_values[p]))})
                    #          else:
                    #             tmp.update({'idx_{}_s{}'.format(p, s): (len(Params_values[p])-1)*np.random.random()})
                    #    start += [tmp,]
                    # start = None #it is recommended to use init rather than start
                    if inp["linterp_params"] == []:
                        init = "adapt_diag"
                    else:
                        init = "advi+adapt_diag_grad"

                    if rcParams["inference_backend"] == "gpu":
                        nuts_sampler = "numpyro"
                        step_str = nuts_sampler
                        ncores_eff2 = 1
                        njobs2 = 1
                    else:
                        nuts_sampler = "pymc"
                        ncores_eff2 = ncores_eff
                        njobs2 = njobs

                        if modelvars == []:
                            logging.error("/!\\ No continuous variables to sample")
                            step = (
                                pm.CategoricalGibbsMetropolis()
                            )  # model.discrete_value_vars)
                            step_str = "Metropolis-within-Gibbs"
                        else:
                            step_str = "No U-turn sampler"
                            if model.discrete_value_vars != []:
                                step = pm.NUTS()
                            else:
                                if input_params["pmversion"] >= 4:
                                    start, step = pm.init_nuts(
                                        chains=njobs, progressbar=True, model=model
                                    )
                                else:
                                    start, step = pm.init_nuts(
                                        init,
                                        chains=njobs,
                                        progressbar=True,
                                        model=model,
                                    )  # progressbar or n_init only for advi #init_nuts implemented in pm.sample, using start values from init_nuts can result in bad initial energy, but maybe removing jitter would solve that

                            # any discrete RVs? #let pymc3 choose automatically the continuous (NUTS) and discrete (Metropolis-withing-Gibbs) sampler
                            # if inp['inference_upsample']>0 and inp['discrete_params']!=[]:
                            if model.discrete_value_vars != []:
                                step_str += (
                                    "+Metropolis-within-Gibbs for categorical variables"
                                )
                            #    step = [pm.CategoricalGibbsMetropolis(model.discrete_value_vars), step]
                            #    #increase ndraws because discrete sampling is effectively Gibbs-Metropolis for most parameters (i.e., except w, scale, delta_scale)
                            #    #ndraws_coeff[step_in] = np.array(ndraws_coeff[step_in]) * 1.5

                    # tune samples
                    # tune is per job
                    tune_per_job = int(0.25 * nsamples_per_job)
                    # if args.no_tuning:
                    #    tune = 0 #override for tests, we would like to see how the chains evolve from different starting points

                    # adding tuning samples
                    nsamples_per_job += tune_per_job

                    logging.info(
                        """
               ┍========================================┑
               ┝=========== Sampler summary ============┥
               ┝========================================┙
               ┝ Sampling step method  : {} ({})
               ┝ # cores               : {}
               ┝ # chains              : {}
               ┝ # samples /chain      : {} (min={}, max={})
               ┝ # tune samples /chain : {}
               ┝ # samples (total)     : {} (max={})
               ┕========================================
                """.format(
                            step_in,
                            step_str,
                            ncores_eff2,
                            njobs2,
                            nsamples_per_job,
                            int(inp["nsamples_per_job_min"]),
                            int(inp["nsamples_per_job_max"]),
                            tune_per_job,
                            nsamples_per_job * njobs2,
                            int(inp["nsamples_max"]),
                        )
                    )

                    if verbose and input_params["pmversion"] >= 4:  # q0 in pm4?
                        try:  # not working for all samplers
                            q0 = step._logp_dlogp_func.dict_to_array(model.test_point)
                            p0 = step.potential.random()
                            start_ene = step.integrator.compute_state(q0, p0)
                            logp, dlogp = step.integrator._logp_dlogp_func(q0)
                            v = step.integrator._potential.velocity(p0)
                            kinetic = step.integrator._potential.energy(p0, velocity=v)
                            logging.info(
                                """
                       =============================================
                        Sampler statistics
                        - Potentials           : {}
                        - Start energy         : {}
                        - logp                 : {}
                        - dlogp                : {}
                        - velocity             : {}
                        - kinetic              : {}
                       =============================================
                        """.format(
                                    p0, start_ene.energy, logp, dlogp, v, kinetic
                                )
                            )
                        except:
                            logging.error("Sampler initial statistics not available...")

                    # =============================================
                    # =============================================
                    # =============================================
                    if animate:
                        init, start, step = None, None, pm.NUTS()
                        args.save_tuned_samples = True

                    if rcParams["inference_backend"] == "gpu":
                        from pymc.sampling.jax import sample_numpyro_nuts

                        closelogfiles()
                        trace = sample_blackjax_nuts(
                            nuts_sampler="numpyro",
                            chains=1,
                            draws=1000,
                            chain_method="parallel",
                            tune=100,
                        )
                        closelogfiles()
                        logger = setup_custom_logger(logname, loglevel=loglevel)
                        movelogfile(logname, output_directory + "/Logs/")
                    else:
                        closelogfiles()
                        trace = pm.sample(
                            nuts_sampler=nuts_sampler,
                            draws=nsamples_per_job,
                            # cores=ncores_eff2,
                            chains=njobs2,
                            # tune=tune_per_chain,
                            discard_tuned_samples=not args.save_tuned_samples,
                            # init=init,
                            # start=start,
                            return_inferencedata=True,
                            chain_method="vectorized",  # parallel / vectorized
                            # callback=None,
                            random_seed=random_seed,
                        )  # can use return_inferencedata=True once pm.Mixture can be serialized in netCDF files
                        closelogfiles()
                        logger = setup_custom_logger(logname, loglevel=loglevel)
                        movelogfile(logname, output_directory + "/Logs/")
                        # idata_kwargs=dict(log_likelihood=False)?

                    # burnin = sampler.tune(tune=3000, start=map_soln, step_kwargs=dict(target_accept=0.9))
                    # =============================================
                    # =============================================
                    # =============================================
                    stime = trace.sample_stats.sampling_time
                    resdict.update({"stime": stime})

                    # saving trace if we wanna rerun the program without the inference
                    logging.info("Saving multi-trace...")

                resdict.update({"nsamples_per_job": nsamples_per_job})
                resdict.update(
                    {
                        "var_names": [
                            v.name.replace("_interval__", "")
                            # interval is for TruncatedNormal
                            for v in model.continuous_value_vars
                            + model.discrete_value_vars
                        ]
                    }
                )

                logging.info("")
                logging.info("✨")
                logging.info("✨✨")
                logging.info("✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨")
                logging.info("✨✨")
                logging.info("✨")
                logging.info("✨✨")
                logging.info("✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info(
                    "✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨ INFERENCE END"
                )
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨✨✨✨")
                logging.info("✨✨✨✨")
                logging.info("✨✨")
                logging.info("✨")

            return trace, model

            # this is if we wanna update the initial values based on previous iteration
            # doesn't seem to change/improve the results
            # tabres = {}
            # for i in range(n_params):
            #   for s in range(n_sectors):
            #      p = '{}_s{}'.format(params_name[i], s)
            #      tabres.update({p: np.quantile(trace[p], 0.5).astype(dtype)})

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # make some room before running model

    printsection("Cleaning")

    try:  # that's in case there is no refine, but there is true_params and we don't need to interpolate true_params
        del grid_arr
    except NameError:
        grid_arr = 0

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # RUN MODEL

    if args.return_model_context:
        printsection("Running model for full context")
        # with or without inference we wanna get the model context anyway afterward (e.g., for PPC in mgris_process)
        # get full context for PPC later on
        logging.info("Run for full model context (e.g., for PPC later on)...")
        model = run_model(inp, getContextOnly=True)
        # inp['model'] = model
        return model

    call_gc()  # do some cleanup beforehand

    printsection("Running model")

    starttime_inference = datetime.now()

    # inference
    logging.info("Inference run started   : {}".format(starttime_inference))

    if args.iterate:
        args.priorfromposterior = False
        # step_in = "SMC"
        trace, model = run_model(inp)
        az.to_netcdf(trace, inp["output_directory"] + "trace.netCDF")
        call_gc()  # do some cleanup beforehand
        args.priorfromposterior = True
        # step_in = "SMC"
        trace, model = run_model(inp)
    else:
        trace, model = run_model(inp)

    # move log_likelihood to inference data group for LOO/WAIC
    if "log_likelihood" in trace.posterior.keys() and "log_likelihood" not in dir(
        trace
    ):
        trace.add_groups(log_likelihood=trace.posterior["log_likelihood"])

    # seems like arviz gets confused with type of beta variable (SMC)
    if "sample_stats" in trace.keys():
        if "beta" in trace.sample_stats.keys():
            trace.sample_stats["beta"] = trace.sample_stats["beta"].astype(float)

    if verbose:
        logging.info("Trace posterior :")
        logging.info("-----------------")
        logging.info(trace.posterior)

    if np.median(resdict["prior_predictive"].prior["log_likelihood"]) > np.median(
        trace.log_likelihood.to_array()
    ) or abs(
        np.median(resdict["prior_predictive"].prior["log_likelihood"])
        - np.median(trace.log_likelihood.to_array())
    ) < 0.01 * abs(
        np.median(resdict["prior_predictive"].prior["log_likelihood"])
    ):
        logging.warning(
            "Prior likelihood     : {}".format(
                np.median(resdict["prior_predictive"].prior["log_likelihood"])
            )
        )
        logging.warning(
            "Posterior likelihood : {}".format(
                np.median(trace.log_likelihood.to_array())
            )
        )
        panic(
            "Posterior likelihood ~ prior likelihood, this is not normal. Either the sampler got stuck in masked areas or the sampler kernel parameter need some tuning (e.g., iterations_to_diagnose?). "
        )

    az.to_netcdf(trace, inp["output_directory"] + "trace.netCDF")

    # now we can save the input parameters
    with open(inp["output_directory"] + "input.pkl", "wb") as f:
        pickle.dump(inp, f, protocol=4)
        if verbose:
            logging.info(
                "Input parameters saved: {}\n".format(inp["output_directory"] + tmp)
            )

    endtime_inference = datetime.now()
    logging.info("Inference ended     : {}".format(endtime_inference))
    logging.info(
        "Time used           : {} min.".format(
            round((endtime_inference - starttime_inference).total_seconds() / 60, 1)
        )
    )

    logging.info(f"Sampling time used  : {resdict['stime']/60: .2f} min.")

    resdict.update({"time_inf": endtime_inference - starttime_inference})
    resdict.update({"time_sampling": resdict["stime"]})
    resdict.update(
        {
            "unobserved_RVs": [
                i.name.replace("_interval__", "") for i in model.unobserved_RVs
            ]
        }
    )

    # this is if we wanna update the initial values based on previous iteration
    # trace, tabres = run_model(interp_function)
    # trace, tabres = run_model(interp_function, start=tabres)

    # Important to to GC if we wanna run the program in a single Python session
    call_gc()

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # RESULTS
    printsection("Results and verifications")

    # if step_in == "SMC":
    # marginal likelihood https://docs.pymc.io/notebooks/Bayes_factor.html
    # marginal likelihood of given model is the probability of the observed data given the model (i.e., it is the normalizing constant of Bayes’ theorem)
    # comparing models is totally fine if all models are assumed to have the same a priori probability (e.g., useful to compare prior hypotheses).
    # Bayes factors: ratio between the marginal likelihood of two models. The larger the BF the better the model. To ease the interpretation of BFs some authors have proposed tables with levels of support or strength, just a way to put numbers into words. 1-3: anecdotal, 3-10: moderate, 10-30: strong, 30-100: very strong, >> 100: extreme
    # logging.info("")
    # logging.info(
    #     "Marginal likelihood of model (can be compared to other models with same a priori probability)"
    # )
    # logging.info(resdict["log_marginal_likelihood_chains"])
    # logging.info(resdict["log_marginal_likelihood"])
    # resdict.update({'nsteps': resdict['report'].nsteps})

    # compare: BF_smc = np.exp(trace_mod2.report.log_marginal_likelihood - trace_mod1.report.log_marginal_likelihood)
    # exit()

    logging.info(az.summary(trace))

    # logging.info(az.loo(trace, model))
    # resdict.update({'loo': az.loo(trace, model)})
    # resdict.update({'waic': az.waic(trace, model)})

    logging.info("")
    logging.info("Gelman-Rubin convergence test for multiple chains")
    logging.info(az.rhat(trace))

    # ===================================================================
    # ===================================================================
    # ===================================================================

    # #------------------------------------------------
    # #make new input file with narrowed ranges?
    # if sum(config)==1: #eventually need to check the ranges for all sectors
    #   printsection('Writing input zoom file for second iteration on sub-grid...')
    #   with open(input_f, 'r') as f:
    #      tmp = f.readlines()
    #   changed = False
    #   for p in grid.params.names:
    #      hpdi2 = hpdi['idx_'+p+'_s0'].data #to_dict()['data']
    #      #remove all previous occurences of range
    #      tmp = [l for l in tmp if not('range_{}'.format(p) in l and '#' not in l)]
    #      #make new line
    #      bounds_idx = [int(np.floor(hpdi2[0])), int(np.ceil(hpdi2[1]))]
    #      if (bounds_idx[0]>0) or (bounds_idx[1]<len(grid.params.values[p])-1):
    #         changed = True
    #      bounds_values = [grid.params.values[p][b] for b in bounds_idx]
    #      if args.verbose:
    #         logging.info('...{}: {} {}'.format(p, bounds_idx, bounds_values))
    #      if bounds_idx[1]-bounds_idx[0]<=2:
    #         logging.warning('.../!\\ Probably not enough elements in sub-grid for parameter {}'.format(p))

    #      newl = 'range_{}\t[{},{}]\n'.format(p, bounds_values[0], bounds_values[1])
    #      tmp += [newl,]
    #   #write new file
    #   if changed:
    #      with open(inp['output_directory']+'/zoomed_input_file.txt', 'w') as f:
    #         for l in tmp:
    #            f.write(l)
    #   else:
    #      logging.warning('/!\\ Sub-grid is the same as grid, zoomed file not created')

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # CLEANING UP

    printsection("Cleaning up")

    # del some vars if using in script
    del trace, grid, o_t
    if incomplete_grid:
        del m_t
    call_gc()
    # store.close()

    endtime = datetime.now()

    resdict.update({"time_fullrun": endtime - starttime})

    with open(inp["output_directory"] + "results_search.pkl", "wb") as f:
        pickle.dump(resdict, f, protocol=4)
        if verbose:
            logging.info(
                "Results values saved: {}".format(
                    inp["output_directory"] + "results_search.pkl"
                )
            )

    # ===================================================================
    # ===================================================================
    # ===================================================================
    logging.info("")
    logging.info(
        " You can now run mgris_process to calculate the final and effective products"
    )
    logging.info("")

    logging.info(
        """
    ==========================================
    - Date/time         : {}
    - Duration          : {} min.
    ==========================================
    """.format(
            endtime.strftime("%d/%m/%Y %H:%M:%S"),
            round((endtime - starttime).total_seconds() / 60.0, 1),
        )
    )

    closelogfiles()
    # log doesn't exist anymore, just call gc_collect
    gc.collect()  # do some cleanup


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    closelogfiles()
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupt!")
        # log doesn't exist anymore, just call gc_collect
        gc.collect()
