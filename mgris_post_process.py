#!/usr/bin/env python

# coding: utf-8

# ===================================================================
# ===================================================================
# ===================================================================
# IMPORTS

# --------------------------------------------------------------------
# system
import os
import glob

# import dill as pickle #using dill for serialized data
import argparse
import logging
from datetime import datetime

# remove some warnings
# https://docs.python.org/3/library/warnings.html
# import warnings
# warnings.simplefilter('ignore', UserWarning)

from pathlib import Path
import sys

path_root = Path(__file__).parents[0]
sys.path.insert(0, str(path_root))

# global parameters
from rcparams import rcParams

# --------------------------------------------------------------------
# PyMC3 & arviz
if rcParams["pmversion"] >= 4:
    import pymc as pm
else:
    import pymc3 as pm

import arviz as az

# --------------------------------------------------------------------
# numpy
try:
    import jax.numpy as np
except:
    import numpy as np

# --------------------------------------------------------------------
# misc
from copy import deepcopy
import pickle
import re

# --------------------------------------------------------------------
# local libraries
import importlib
import Library.lib_mc
import Library.lib_main
import Library.lib_alt
import Library.lib_plots

importlib.reload(Library.lib_mc)
importlib.reload(Library.lib_main)
importlib.reload(Library.lib_alt)
importlib.reload(Library.lib_plots)
from Library.lib_mc import *
from Library.lib_main import *
from Library.lib_alt import *
from Library.lib_plots import *
from Library.lib_class import *

import mgris_search


# --------------------------------------------------------------------
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        inputfile="",
        verbose=False,
        debug=False,
        clean=False,
        skip_plots=False,
        ppc=False,
        filter_chains=False,
        force=False,
    ):
        args.inputfile = str(inputfile)
        args.verbose = bool(verbose)
        args.debug = bool(debug)
        args.clean = bool(clean)
        args.skip_plots = bool(skip_plots)
        args.ppc = bool(ppc)
        args.filter_chains = bool(filter_chains)
        args.force = bool(force)


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"
    # ===================================================================
    # ===================================================================
    # ===================================================================
    # SYSTEM SUMMARY AND SOME DEFAULT HARD-CODED SETTINGS

    # --------------------------------------------------------------------
    # posterior predictive
    ppc_samples = rcParams["ppc_samples"]

    # ===================================================================

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS post-processing step")
        parser.add_argument("inputfile", type=str, help="name_of_input_file")
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        parser.add_argument(
            "--debug", "-d", help="Debugging information", action="store_true"
        )
        parser.add_argument(
            "--ppc",
            "-ppc",
            help="posterior predictive check [y/n, Y/N, 0/1, True/False, true/false] [Default=N]",
            action="store_true",
        )
        parser.add_argument(
            "--skip-plots",
            "-sp",
            help="Skip the diagnostics plots",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--clean",
            "-c",
            help="remove temporary files for PPC of observables to predict",
            action="store_true",
        )
        parser.add_argument(
            "--filter-chains",
            "-fc",
            help="[experimental] ignore bad chains for results and plots",
            action="store_true",
        )
        parser.add_argument(
            "--force",
            "-f",
            help="Force the run even if context files differ",
            action="store_true",
        )
        args = parser.parse_args()

    verbose = args.verbose  # we'll use this a lot

    starttime = datetime.now()

    global logger
    # start logging now (console + file)
    logname = "postprocess"
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logger = setup_custom_logger(logname, loglevel=loglevel)

    # welcome and disclaimers
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("          ┍ M U L ┑")
    logging.info("          ┝ T I G ┥")
    logging.info("          ┕ R I S ┙")
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("     ┍                 ┑")
    logging.info("     ┝ POST-PROCESSING ┥")
    logging.info("     ┕                 ┙")
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("Thanks for using MULTIGRIS.")
    logging.info(
        "Please make sure to check the README file, the notebooks, and the publication (https://ui.adsabs.harvard.edu/abs/2022A%26A...667A..34L/abstract)."
    )
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")

    logging.info(
        """
    ┍========================================┑
    ┝=========== System summary =============┥
    ┝========================================┙
    ┝ Date/time:
    ┝ - {}
    ┕========================================┙
   """.format(
            starttime.strftime("%d/%m/%Y %H:%M:%S")
        )
    )

    if args.ppc is None:
        args.ppc = False

    # ===================================================================
    # ===================================================================
    # ===================================================================

    # re-read parameters for post-processing that may have been changed manually after inference is done
    input_f = args.inputfile
    if not FileExists(input_f):
        # try and find if there's only one input*.txt file
        f = glob.glob(args.inputfile + "/input*.txt")
        if len(f) == 1:
            input_f = f[0]
        else:
            raise OSError("No input file found", input_f)

    input_params = readinputfile(
        input_f, root_directory, verbose=verbose
    )  # read also the post-processing input file

    output_directory = input_params["output_directory"]
    directory_models = input_params["directory_models"]

    # possibly update output_directory
    if not FileExists(output_directory, is_dir=True):
        output_directory = "/".join(os.path.abspath(input_f).split("/")[:-1]) + "/"

    obs_to_predict = input_params["obs_to_predict"]
    params_to_predict = input_params["params_to_predict"]

    # post_processing_file = input_params['post_processing_file']
    extensive_secondary_parameters = input_params["extensive_secondary_parameters"]
    intensive_secondary_parameters = input_params["intensive_secondary_parameters"]
    # group_norm = input_params['group_norm']

    # probability of credible interval to be used throughout
    # interval that spans most of the distribution
    ci = input_params["ci"]

    # post-processing
    if input_params["post_processing_file"] is not None:
        post_processing_file, commoncolumn = input_params["post_processing_file"]

    # reread observation sets in case it was updated
    observation_sets = input_params["observation_sets"]
    allobservations = Observations(observation_sets, input_params, verbose=verbose)
    # switch back to single obs set now that all obs blocks have been processed
    allobservations = allobservations.oset
    allobservations.set_masks()

    logging.info(
        """
    ==========================================
    Run summary
    - input file         : {}
    - output directory   : {}
    ==========================================
    """.format(
            input_f, output_directory
        )
    )

    # ===================================================================
    # move output log

    movelogfile("postprocess", output_directory + "/Logs")

    # ===================================================================
    # the rest we get from inp
    with open(output_directory + "input.pkl", "rb") as f:
        inp = pickle.load(f)

    # update so it can be used in add2trace
    inp.update({"extensive_secondary_parameters": extensive_secondary_parameters})
    inp.update({"intensive_secondary_parameters": intensive_secondary_parameters})
    # inp.update({'group_norm': group_norm})

    inp.update({"output_directory": output_directory})
    inp.update({"directory_models": directory_models})

    # backward compatibility
    if "plaw_params" not in inp.keys():
        inp["plaw_params"] = []
    if "normal_params" not in inp.keys():
        inp["normal_params"] = []

    # backward compatibility
    if "intensive_obs" not in inp.keys():
        inp.update({"intensive_obs": []})
    if "intensive_secondary_parameters_noweight" not in inp.keys():
        inp["intensive_secondary_parameters_noweight"] = []

    # post-processing
    if input_params["post_processing_file"] is not None:
        inp.update({"post_processing_file": [post_processing_file, commoncolumn]})

    directory_models = inp["directory_models"]
    params = inp["params_name"]
    method_step = inp["method_step"]
    context = inp["context"]
    interp_order = inp["interp_order"]
    order_of_magnitude = inp["order_of_magnitude"]
    ignored = inp[
        "ignored"
    ]  # tracers that were ignored because nan or uncertainty was nan

    if not FileExists(inp["context"], is_dir=True):
        logging.error("/!\\ Context directory not found, trying working directory..")
        inp["context"] = inp["context"][inp["context"].index("Contexts/") :] + "/"
        inp["context"] = inp["directory_models"]

    # verifying whether context files have been changed or not
    verify_context(inp, args.force)

    # tracers that were ignored because nan or uncertainty was nan --> add them to predict
    ignored = [inp["observations"].getobsname(o) for o in ignored]
    obs_to_predict += ignored

    # making sure we do not include tracers already in observations (non nans)
    # obs_to_predict = [o for o in obs_to_predict if o not in inp['observations'].obsnames_u] #comment if we wanna calculate tracers already in inp['observations'].names AND also comment in add2trace: jo = inp['observations'].getidx(o) if o in inp['observations'].names else io
    obs_to_predict = GetUniques(obs_to_predict)

    # ===================================================================
    # ===================================================================
    # ===================================================================

    logging.info("Loading multi-trace...")

    # load only last part of trace?
    # nchains = trace.posterior.dims['chain']
    # ndraws = trace.posterior.dims['draw']
    # nsamples = 500
    # trace = trace.sel(chain=np.arange(nchains), draw=np.arange(ndraws-nsamples, ndraws))
    # or
    # nsamples = ndraws

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # POST-PROCESSING

    if "plaw_params" not in inp.keys():  # backward compatibility
        inp["plaw_params"] = []
    if "normal_params" not in inp.keys():  # backward compatibility
        inp["normal_params"] = []
    # if "smoothaw_params" not in inp.keys():  # backward compatibility
    #     inp["smoothplaw_params"] = []
    if "brokenplaw_params" not in inp.keys():  # backward compatibility
        inp["brokenplaw_params"] = []
    if "doublenormal_params" not in inp.keys():  # backward compatibility
        inp["doublenormal_params"] = []

    # first selecting only extensive parameters if plaw or normal
    # do this early on so that n_predpar and params_to_predict are set
    if inp["plaw_params"] + inp["normal_params"] != []:
        ignored = [p for p in params_to_predict if p in intensive_secondary_parameters]
        if ignored != []:
            logging.warning(
                "Skipping intensive parameters: {} (not yet implemented for power-law/normal distributions)".format(
                    ignored
                )
            )
        params_to_predict = [
            p for p in params_to_predict if p in extensive_secondary_parameters
        ]

    n_predobs = len(obs_to_predict)
    n_predpar = len(params_to_predict)

    if n_predobs + n_predpar == 0:
        logging.warning("Nothing to predict...")
        return

    new_trace_f = output_directory + "trace_post-process.netCDF"
    if args.clean and FileExists(new_trace_f):
        os.remove(new_trace_f)
        logging.warning("Removing {}".format(new_trace_f))

    if FileExists(new_trace_f):
        trace = az.from_netcdf(new_trace_f)

    else:
        # start from process if it's there (it should be)
        if FileExists(output_directory + "trace_process.netCDF"):
            trace = az.from_netcdf(output_directory + "trace_process.netCDF")
        else:
            logging.warning(
                "Using trace from search script, trace from process script was not found"
            )
            trace = az.from_netcdf(output_directory + "trace.netCDF")

        # obs to predict from input file
        if n_predobs > 0 or n_predpar > 0:
            # grid = make_grid_wrapper(inp, verbose=verbose)
            # grid, obs_grid, mask_grid, mask_ul_grid, minvalue, completion_fraction, order_of_magnitude = make_obs_grid_wrapper(inp, grid, inp['observations'].obsnames_u, False, False, verbose=verbose)

            if inp["post_processing_file"] is not None:
                logging.info(
                    f'Reading post-processing file {inp["post_processing_file"]}'
                )
                grid = merge_grids_wrapper(
                    inp,
                    list(set(inp["observations"].obsnames_u + obs_to_predict)),
                    params_to_predict,
                    verbose=verbose,
                )
            else:  # no specific pp table
                grid = make_grid_wrapper(
                    inp, add=obs_to_predict + params_to_predict, verbose=verbose
                )

            # parse parameters to infer directly or that need hyperparameters
            inp = grid.parse_params(inp)

            inp["grid"] = grid

            # ===================================================================

            tmpn = len(inp["observations"].obsnames_u)
            (
                grid,
                obs_grid,
                mask_grid,
                mask_ul_grid,
                minvalue,
                completion_fraction,
                order_of_magnitude,
                order_of_magnitude_std,
            ) = make_obs_grid_wrapper(
                inp,
                grid,
                # we need the tracers used for search to get things like order of magnitude
                inp["observations"].obsnames_u + obs_to_predict + params_to_predict,
                None,
                None,
                verbose=verbose,
                obs_to_predict=obs_to_predict,
                params_to_predict=params_to_predict,
            )

            # secondary parameters
            # if n_predpar > 0:
            #     logging.info(
            #         "Checking masked values in entire grid for parameters to predict"
            #     )
            #     for i in range(n_predpar):
            #         logging.info(f"- {params_to_predict[i]}")
            #         flag = np.where(new_obs_grid[n_predobs + i] == -np.inf)
            #         frac = len(flag[0]) / len(new_obs_grid[n_predobs + i].flatten())
            #         if frac > 0:
            #             logging.warning(f" - % -inf = {np.round(100*frac,1)}")
            #         new_obs_grid[n_predobs + i][flag] = -1000
            #         # very low value in log to avoid a -inf, not very pretty but well...

            #         if params_to_predict[i] in inp["extensive_secondary_parameters"]:
            #             flag = np.isnan(new_obs_grid[n_predobs + i])
            #             frac = np.sum(flag.flatten()) / len(
            #                 new_obs_grid[n_predobs + i].flatten()
            #             )
            #             if frac > 0:
            #                 logging.warning(f" - % nan = {np.round(100*frac,1)}")
            #                 if rcParams["gridnanisnan"]:
            #                     new_obs_grid[n_predobs + i][flag] = np.nan
            #                 else:
            #                     new_obs_grid[n_predobs + i][flag] = inp["minvalue"]
            #         else:
            #             flag = np.isnan(new_obs_grid[n_predobs + i])
            #             frac = np.sum(flag.flatten()) / len(
            #                 new_obs_grid[n_predobs + i].flatten()
            #             )
            #             if frac > 0:
            #                 logging.warning(f" - % nan = {np.round(100*frac,1)}")
            #                 new_obs_grid[n_predobs + i][flag] = np.nan  # ?

            # # invalid data, reset to nan
            # for i, o in enumerate(new_obs_grid):
            #     new_obs_grid[i][mask_allnans] = np.nan
            obs_grid = obs_grid[tmpn:]
            inp["obs_grid"] = obs_grid  # to be used in interp function
            # update inp with new grid and obs grid
            # with open(inp['output_directory']+'pp_grid.pkl', "wb") as f:
            #    pickle.dump(inp['obs_grid'], f, protocol=4)

            # ===================================================================

            logging.info("- Adding new observables/secondary parameters to trace")

            # new wrapper for sectors or distributions
            trace = add2trace_wrapper(
                trace,
                inp,
                obs_to_predict,
                labels_params=params_to_predict,
                scaling_params=[
                    p in extensive_secondary_parameters for p in params_to_predict
                ],
                pp=True,
            )

            # get all results from now on in linear scale if needed
            s = []
            for o in obs_to_predict:
                ind = np.where(np.array(allobservations.obsnames) == o)[0]
                if len(ind) == 0:
                    continue
                if (
                    allobservations.obs[allobservations.names[ind[0]]].obs_valuescale
                    == "linear"
                ):
                    # if o in inp['observations'].obs.keys(): #nan of inf obs
                    #    if inp['observations'].obs[o].obs_valuescale=='linear':
                    s += [
                        o,
                    ]
                    trace.posterior[o] = 10 ** trace.posterior[o].astype("float64")
            if len(s) > 0:
                logging.warning(
                    "Switching draw values to linear for {}".format(", ".join(s))
                )

            # save changes to avoid having to redo
            trace.to_netcdf(new_trace_f)

    # =========================
    # =========================
    # =========================
    # =========================

    # Chains that are stuck in low probability density region
    goodchains, badchains = analyze_chains(trace)
    if len(badchains) > 0:
        logging.error(
            "- /!\\ Some chains seem to be stuck in burn-in phase or in local maximum (significantly lower likelihood over chain length): {}".format(
                badchains
            )
        )
        if args.filter_chains:
            trace.posterior = trace.posterior.sel(chain=goodchains)
            # reindex
            trace.posterior = trace.posterior.assign_coords(
                {"chain": np.arange(trace.posterior.dims["chain"])}
            )
            logging.warning(
                "- /!\\ These chains have been ignored for the output but it is advised to run the model again with longer chains"
            )

    # =========================
    # =========================
    # =========================
    # =========================

    # percentage of NaNs
    for io, o in enumerate(obs_to_predict + params_to_predict):
        p_nans = round(
            100
            * len(np.where(np.isnan(trace.posterior[o].data.flatten()))[0])
            / len(trace.posterior[o].data.flatten()),
            1,
        )
        if p_nans > 0:
            logging.warning("...{}: NaNs = {}%".format(o, p_nans))
        p_infs = round(
            100
            * len(np.where(np.isinf(trace.posterior[o].data.flatten()))[0])
            / len(trace.posterior[o].data.flatten()),
            1,
        )
        if p_infs > 0:
            logging.warning("...{}: Infs = {}%".format(o, p_infs))

    # we could replace but hdi below can skip nans
    # ind = np.where(~np.isfinite(trace.posterior[o].data))
    # trace.posterior[o].data[ind] = np.nanmedian(trace.posterior[o].data)

    # add some related params
    params_to_predict_full = deepcopy(params_to_predict)

    # get params also in linear if values <1e3
    tmp = []
    for p in params_to_predict:
        if trace.posterior[p].mean(skipna=True) < 3:
            trace.posterior["{}_linear".format(p)] = deepcopy(trace.posterior[p])
            trace.posterior["{}_linear".format(p)].values = (
                10 ** trace.posterior[p].values
            )
            tmp += [
                "{}_linear".format(p),
            ]
    if verbose:
        logging.info("Added linear quantities for {}".format(tmp))
    params_to_predict_full += tmp

    # get fraction for each group
    params_to_predict_full += [
        "f_g{}_{}".format(g, p)
        for p in params_to_predict
        for g in range(len(inp["groups"]))
    ]

    # get fraction for each group/component
    tmp = []
    for p in params_to_predict:
        # if p in inp['extensive_secondary_parameters']:
        for s in range(inp["n_comps"]):
            pfrac = "f_s_{}_{}".format(p, s)
            if "f_s_{}".format(p) not in trace.posterior.keys():
                continue
            trace.posterior[pfrac] = trace.posterior["f_s_{}".format(p)][:, :, s]
            tmp += [
                pfrac,
            ]
    if verbose:
        logging.info("Added components {}".format(tmp))
    params_to_predict_full += tmp

    if args.clean:
        # update trace with new keys
        trace.to_netcdf(new_trace_f)

    # =========================
    kind = ["median", "hpdi"]
    altkind = "mean"  # will be between ()

    estimators = get_estimators(inp, trace.posterior, ci=ci)

    resdict = {}
    resdict.update({"estimators": estimators})
    resdict.update({"obs_to_predict": obs_to_predict})
    resdict.update({"params_to_predict": params_to_predict})

    # =========================
    results = ""

    # ===================================================================
    # ===================================================================
    # ===================================================================

    with open(output_directory + "results_post-process.pkl", "wb") as f:
        pickle.dump(resdict, f, protocol=4)
        if verbose:
            logging.info(
                "Results saved in: {}\n".format(
                    output_directory + "results_post-process.pkl"
                )
            )

    # =========================

    if obs_to_predict is not None:
        if len(obs_to_predict) > 0:
            results += logappend(
                """

 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 ▒▒▒▒ PREDICTED OBSERVABLES ▒▒▒▒
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

 [log/linear scale same as model table by default]"""
            )

            if inp["tolog"] != []:
                if "obs" in inp["tolog"]:
                    results += logappend(
                        " [- /!\\ Except for all predicted observables for which log was forced]"
                    )
                if len(inp["tolog"]) > 1:  # not only obs
                    tmp = list(set(inp["tolog"]) - set(["obs"]))
                    results += logappend(
                        " [- /!\\ Except for {} for which log was forced]".format(tmp)
                    )

            for o in obs_to_predict:
                # if o in inp["observations"].names:
                if o in inp["observations"].obs.keys():
                    results += logappend(
                        f" [- For {o}: {inp['observations'].obs[o].obs_valuescale}]"
                    )

            results += logappend("")
            results += logappend("")

            fieldlength = int(np.max([len(o) for o in allobservations.names]))

            h = f"\n   {'Quantity': <{fieldlength}} : "
            h += f" ({altkind: <10}) {kind[0]: <10} {'+': <10}  {'-': <10} \n"
            header = h + "  " + "_" * len(h)

            results += logappend(header)

            for i, o in enumerate(obs_to_predict):
                l = [o2 for o2 in allobservations.names if o in o2]
                if l == []:  # tracer is not from any obs block
                    results += logappend(
                        estformat(
                            inp,
                            estimators,
                            [
                                [
                                    o,
                                ],
                            ],
                            kind=kind,
                        )
                    )
                else:  # tracer is from at least one obs block
                    for i2, o2 in enumerate(l):
                        # pvalue = pvalue_calc(trace, allobservations, estimators, o2)
                        results += logappend(
                            estformat(
                                inp,
                                estimators,
                                [
                                    [
                                        o,
                                    ],
                                ],
                                kind=kind,
                                observations=allobservations,
                                pvalue=None,
                                idx=i2,
                            )
                        )
            results += logappend("")

            # for i in range(n_predobs):
            #   results += logappend(estformat(inp, estimators, [obs_to_predict[i]], kind=kind, observations=allobservations))

    if params_to_predict is not None:
        if len(params_to_predict) > 0:
            results += logappend("")
            results += logappend(
                """

 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 ▒▒ PREDICTED PARAMETERS ▒▒
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

 [Interpolation used is the same as for inference]
     """
            )

            # if rcParams["inference_upsample"] == 0:
            # results += logappend(
            # "[/!\\ Continuous sampling is used for *primary* parameters even for nearest neighbor interpolation (to benefit from SMC), not for secondary parameters]"
            # )
            # results += logappend("[This could lead to small differences]")
            for p in params_to_predict:
                if p in inp["intensive_secondary_parameters_noweight"]:
                    results += logappend(
                        " [/!\\ Mixing weight not applied for secondary parameters {}]".format(
                            p
                        )
                    )
            results += logappend("")
            results += logappend("")

            for i, p in enumerate(params_to_predict):
                # if '_linear' in params_to_predict[i] and estimators[params_to_predict[i]]['mean']>1e3:
                #   continue

                # add related quantities in blocks
                related = [
                    p2
                    for p2 in params_to_predict_full
                    if re.search(
                        ".*\_(" + re.escape(p) + "\_[0-9]*|" + re.escape(p) + "$)", p2
                    )
                    is not None
                    or p2 == p + "_linear"
                    or p == p2
                ]
                related = [r for r in related if r in estimators.keys()]
                # print(related)
                results += logappend(
                    estformat(
                        inp,
                        estimators,
                        [
                            related,
                        ],
                        kind=kind,
                    )
                )
                results += logappend("")

    # logging.info(results)

    # ===================================================================
    # write results

    with open(inp["output_directory"] + "results_pp.txt", "w") as f:
        f.write(results)

    # ===================================================================
    # ===================================================================
    # ===================================================================

    # test to calculate PPC for new tracers, even though they don't have observed values
    if args.ppc and obs_to_predict != []:
        printsection("Calculating PPC")
        # if we wanna use the updated model context to get the PPC for these new tracers
        inpp = output_directory + "input_pp.pkl"
        if args.clean and FileExists(inpp):
            os.remove(inpp)
            logging.warning("Removing {}".format(inpp))

        if not FileExists(inpp):
            logging.info(
                "Running mgris_search to get model context with new tracers..."
            )

            # first remove handlers
            log = logging.getLogger()  # root logger
            for hdlr in log.handlers[:]:  # remove all old handlers
                log.removeHandler(hdlr)

            # getting model context
            inp["model"] = mgris_search.main(
                args=mgris_search.args(
                    inputfile=input_f,
                    nsamples_per_job=10,
                    return_model_context=True,
                    method_step=method_step,
                    order=interp_order,
                    verbose=verbose,
                    addpredict=True,
                )
            )  # force_missing=True

            # remove handlers
            log = logging.getLogger()  # root logger
            for hdlr in log.handlers[:]:  # remove all old handlers
                log.removeHandler(hdlr)

            # re-use post-process log
            logger = setup_custom_logger(
                "post-process",
                directory=output_directory,
                filemode="a",
                loglevel=loglevel,
            )

        else:
            logging.info("Loading saved model context...")
            with open(inpp, "rb") as f:
                inp2 = pickle.load(f)
            inp["model"] = inp2["model"]

        logging.info("Posterior predictive sampling...")
        if args.clean or not FileExists(output_directory + "ppc_PP.netCDF"):
            ppc = pm.fast_sample_posterior_predictive(
                trace, samples=ppc_samples, model=inp["model"], var_names=obs_to_predict
            )
            ppc = az.convert_to_inference_data(ppc)
            az.to_netcdf(ppc, output_directory + "ppc_PP.netCDF")
        else:
            ppc = az.from_netcdf(
                output_directory + "ppc_PP.netCDF"
            )  # to get the right data structure for ppc_calc

        hpdi = az.hdi(ppc.posterior, hdi_prob=ci)  # skip NaNs not necessary here
        # quants = ppc.posterior.quantile(0.5)
        quants = ppc.posterior.mean(skipna=True)
        logging.info("PPC=")
        logging.info(hpdi)
        logging.info(quants)

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # PLOTS
    if not args.skip_plots:
        plotdir = inp["output_directory"] + "/Plots/"
        alpha_plot_trace = {"alpha": 0.5}

        printsection("Making plots")

        # ===================================================================
        if obs_to_predict != []:
            printsection("[trace/observables]", level=2)
            fig = az.plot_trace(
                trace.posterior,
                var_names=obs_to_predict,
                combined=True,
                divergences="bottom",
                compact=True,
                plot_kwargs=alpha_plot_trace,
                trace_kwargs=alpha_plot_trace,
                hist_kwargs=alpha_plot_trace,
                rug_kwargs=alpha_plot_trace,
                fill_kwargs=alpha_plot_trace,
            )
            # confidence intervals
            for i, k in enumerate(obs_to_predict):
                hdi = az.hdi(trace.posterior[k], hdi_prob=ci).to_array().data[0]
                fig[i, 0].axvspan(hdi[0], hdi[1], color="tab:blue", alpha=0.2)

            plt.tight_layout()
            fig[0, 0].get_figure().savefig(
                plotdir + "Trace_observables_predict_combined.pdf"
            )
            plt.close("all")

        # ===================================================================
        if params_to_predict != []:
            tmp = []
            for p in params_to_predict:
                tmp += [
                    p,
                ]
                if "f_s_{}".format(p) in trace.posterior.keys():
                    if p in inp["extensive_secondary_parameters"]:
                        trace.posterior["f2_s_{}".format(p)] = deepcopy(
                            trace.posterior["f_s_{}".format(p)]
                        )
                        trace.posterior["f_s_{}".format(p)] = (
                            trace.posterior[p] - trace.posterior["f_s_{}".format(p)]
                        )
                    else:
                        trace.posterior["f2_s_{}".format(p)] = deepcopy(
                            trace.posterior["f_s_{}".format(p)]
                        )
                if "f2_s_{}".format(p) in trace.posterior.keys():
                    tmp += [
                        "f2_s_{}".format(p),
                    ]
            printsection("[trace/parameters]", level=2)
            fig = az.plot_trace(
                trace.posterior,
                var_names=tmp,
                combined=True,
                divergences="bottom",
                compact=True,
                plot_kwargs=alpha_plot_trace,
                trace_kwargs=alpha_plot_trace,
                hist_kwargs=alpha_plot_trace,
                rug_kwargs=alpha_plot_trace,
                fill_kwargs=alpha_plot_trace,
            )
            # confidence intervals
            for i, k in enumerate(tmp):
                hdi = az.hdi(trace.posterior[k], hdi_prob=ci).to_array().data[0]
                if len(hdi.shape) == 2:  # fractions
                    for h in hdi:
                        fig[i, 0].axvspan(h[0], h[1], color="tab:blue", alpha=0.2)
                else:
                    fig[i, 0].axvspan(hdi[0], hdi[1], color="tab:blue", alpha=0.2)
            plt.tight_layout()
            fig[0, 0].get_figure().savefig(
                plotdir + "Trace_params_predict_combined.pdf"
            )
            plt.close("all")

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # CLEANING UP

    printsection("Cleaning up")

    call_gc()

    closelogfiles()


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    global logger

    # ===================================================================
    # logging console + file
    logger = setup_custom_logger("postprocess")

    closelogfiles()
    main()
