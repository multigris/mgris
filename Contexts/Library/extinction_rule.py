import numpy as np
import re
from copy import deepcopy

# https://dust-extinction.readthedocs.io/en/stable/dust_extinction/reference_api.html#models
from dust_extinction.grain_models import WD01  # , J13, C11

# from dust_attenuation.radiative_transfer import WG00, N09
# https://dust-attenuation.readthedocs.io/en/latest/dust_attenuation/install.html#using-pip
# pip3 install git+https://github.com/karllark/dust_attenuation.git


# ==========================================
# ==========================================
# ==========================================
# ==========================================
def extinction_rule_persector(inp, verbose=False):
    # ==========================================
    # EXTINCTION

    log = "\nExtinction rule :\n"
    log += "------------------\n"

    # grain_model = WD01('LMCAvg')
    grain_model = WD01("MWRV31")
    # from WD01.possnames.keys()
    # grain_model = J13()

    # validity domain
    log += "Validity domain: {}\n".format(grain_model.x_range)

    # read all wavelengths
    inp["observations"].wavelength_inv = np.array(inp["observations"].values) * 0.0

    log += "Reading wavelengths:\n"

    # first, get 1/wave[um]
    fac = np.ones(len(inp["observations"].names))
    for i, name in enumerate(inp["observations"].names):
        try:
            specie, wavelength, unit = parse_cloudy_label(
                name
            )  # wavelength will be in um

            log += "- {}: {} {} [{}]\n".format(name, specie, wavelength, unit)

            if wavelength < 1 / grain_model.x_range[1]:
                wavelength = 1 / grain_model.x_range[1]
                log += "  below wavelength validity range --> taking wavelength as {}um\n".format(
                    wavelength
                )

            if wavelength > 1 / grain_model.x_range[0]:  # large wavelengths
                wavelength = 1 / grain_model.x_range[0]  # but:
                fac[i] = 0.0  # log1- will be taken afterwards
                log += "  above wavelength validity range --> no correction\n"

            inp["observations"].wavelength_inv[i] = 1 / wavelength

        except:
            log += "- Observable {} is not recognized as a line\n".format(name)

    cs_args = [grain_model, fac]

    params_eval = "pm.HalfNormal('ebv', sigma={}, shape=({},))".format(
        inp["ebv_sigma"], inp["n_comps"]
    )  # possibility to have several commands with ;

    # we want to extinguish *the model*
    scale_context = "{o: [cs_args[1][i]*cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=model['ebv'][s]) for s in range(n_comps)] for i,o in enumerate(inp['observations'].names)}"

    # scale_context = ""
    # for n in range(inp['n_comps']):
    #     scale_context += "{{o: [{}*cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=model['ebv'][n]) for n in range(inp['n_comps']] for i,o in enumerate(inp['observations'].names))}};".format(fac[i])
    # #we'll take the log with tt afterwards

    scale_context_add2trace = scale_context.replace(
        "model['ebv'][s]", "trace.posterior['ebv'].sel(ebv_dim_0=s)"
    )

    return (
        {
            "extinction_persector": {
                "params": ["ebv"],
                "params_eval": params_eval,  # pm parameters
                "scale_context": scale_context,  # for mgris_search
                "scale_context_add2trace": scale_context_add2trace,  # for add2trace in mgris_process and mgris_post-process
                "cs_args": cs_args,
            }  # args to pass for the evals
        },
        log,
    )


# ==========================================
# ==========================================#
# ==========================================
# ==========================================


def extinction_rule(inp, verbose=False):
    # ==========================================
    # EXTINCTION

    log = "\nExtinction rule :\n"
    log += "-----------------\n"

    # grain_model = WD01('LMCAvg')
    grain_model = WD01("MWRV31")
    # from WD01.possnames.keys()
    # grain_model = J13()

    # validity domain
    log += "Validity domain: {}\n".format(grain_model.x_range)

    # read all wavelengths
    inp["observations"].wavelength_inv = np.array(inp["observations"].values) * 0.0

    log += "Reading wavelengths:\n"

    # first, get 1/wave[um]
    fac = np.ones(
        len(inp["observations"].names)
    )  # if wavelength is outside validity range will multiply by 0 (log10 will be taken afterwards)
    for i, name in enumerate(inp["observations"].names):
        try:
            specie, wavelength, unit = parse_cloudy_label(
                name
            )  # wavelength will be in um

            log += "- {}: {} {} [{}]\n".format(name, specie, wavelength, unit)

            if wavelength < 1 / grain_model.x_range[1]:
                wavelength = 1 / grain_model.x_range[1]
                log += "  below wavelength validity range --> taking wavelength as {}um\n".format(
                    wavelength
                )

            if wavelength > 1 / grain_model.x_range[0]:  # large wavelengths
                wavelength = 1 / grain_model.x_range[0]  # but:
                fac[i] = 0.0  # log10 will be taken afterwards
                log += "  above wavelength validity range --> no correction\n"

            inp["observations"].wavelength_inv[i] = 1 / wavelength

        except:
            log += "- Observable {} is not recognized as a line\n".format(name)

    cs_args = [grain_model, fac]

    params_eval = "pm.HalfNormal('ebv', sigma={})".format(
        inp["ebv_sigma"]
    )  # possibility to have several commands with ;

    # we want to extinguish *the model*
    scale_context = "{o: cs_args[1][i]*cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=model['ebv']) for i,o in enumerate(inp['observations'].names)}"

    scale_context_add2trace = scale_context.replace("model[", "trace.posterior[")

    return (
        {
            "extinction": {
                "params": ["ebv"],
                "params_eval": params_eval,  # pm parameters
                "scale_context": scale_context,  # for mgris_search
                "scale_context_add2trace": scale_context_add2trace,  # for add2trace in mgris_process and mgris_post-process
                "cs_args": cs_args,
            }  # args to pass for the evals
        },
        log,
    )


# -------------------------------------------------------------------
def attenuation_rule(inp, verbose=False):
    # ==========================================
    # ATTENUATION

    log = ""

    att_model = N09()

    # validity domain
    log += "Validity domain: {}\n".format(att_model.x_range)

    # read all wavelengths
    inp["observations"].wavelength_inv = np.array(inp["observations"].values) * 0.0

    log += "Reading wavelengths:\n"

    # first, get 1/wave[um]
    fac = np.ones(len(inp["observations"].names))
    for i, name in enumerate(inp["observations"].names):
        try:
            specie, wavelength, unit = parse_cloudy_label(
                name
            )  # wavelength will be in um
            log += "- {} {} [{}]\n".format(specie, wavelength, unit)

            wavelength = 1e-3
            if wavelength < 1 / grain_model.x_range[1]:
                wavelength = 1 / grain_model.x_range[1]
                log += "  below wavelength validity range --> taking wavelength as {}um\n".format(
                    wavelength
                )

            if wavelength > 1 / grain_model.x_range[0]:  # large wavelengths
                wavelength = 1 / grain_model.x_range[0]  # but:
                fac[i] = 0.0  # log1- will be taken afterwards
                log += "  above wavelength validity range --> no correction\n"

            inp["observations"].wavelength_inv[i] = 1 / wavelength

        except:
            log += "- Observable {} is not recognized as a line\n".format(name)

    cs_args = [
        att_model,
    ]

    params_eval = "pm.HalfNormal('av', sigma=10); pm.HalfNormal('ampl', sigma=10); pm.Normal('slope', mu=0, sigma=0.3)"  # possibility to have several commands with ;

    # we want to attenuate *the model*
    scale_context = "{{o: {}*cs_args[0](Av=model['av'], ampl=model['ampl'], slope=model['slope'])(inp['observations'].wavelength_inv[i]) for i,o in enumerate(inp['observations'].names)}}".format(
        fac[i]
    )

    scale_context_add2trace = scale_context.replace("model[", "trace.posterior[")

    return (
        {
            "extinction": {
                "params": ["av", "slope", "ampl"],
                "params_eval": params_eval,  # pm parameters
                "scale_context": scale_context,  # for mgris_search
                "scale_context_add2trace": scale_context_add2trace,  # for add2trace in mgris_process and mgris_post-process
                "cs_args": cs_args,
            }  # args to pass for the evals
        },
        log,
    )


# ==========================================
# ==========================================
# ==========================================
# ==========================================
def parse_cloudy_label(name_in):
    """
    Wavelength returned in microns
    """

    name = name_in[0 : name_in.index("{")] if "{" in name_in else deepcopy(name_in)

    alt = False
    if "_" in name:
        alt = True

    if alt:
        name = name.replace("_", "")

    charlabel = re.search("([A-Z,a-z]+).*", name).groups()[0]
    nchars = len(charlabel)
    if nchars <= 2:  # that's a line
        if name[0:5] == "^13CO":
            specie, wavelength, unit = re.search(
                "(\^13[A-Z][A-Z,a-z])(.*)\+*.*([m,A]+)", name
            ).groups()
        elif charlabel in ("CO",):
            specie, wavelength, unit = re.search(
                "(^[A-Z][A-Z,a-z])(.*)\+*.*([m,A]+)", name
            ).groups()
        else:
            specie, wavelength, unit = re.search(
                "(^[A-Z][A-Z,a-z]*[1-9])(.*)\+*.*([m,A]+)", name
            ).groups()
    else:  # pah feature etc...
        # specie, wavelength, unit = re.search("(^[A-Z]+)(.+)([m,A]+)", name).groups()
        specie, wavelength, unit = re.search(
            "(^[A-Z][A-Z,a-z]+)(.+)([m,A]+)", name
        ).groups()

    if "." not in str(wavelength) and unit == "m":
        print(
            f"Comma is missing from label: {name_in}, this may cause issues with wavelength sampling"
        )

    if alt:
        wavelength = wavelength[:-2] + "." + wavelength[-2:]

    # get average wavelength if, e.g., 6548+84 (i.e., 6548+6584)

    if "+" in wavelength:
        tmp = wavelength.split("+")
        tmp[1] = tmp[0][0 : len(tmp[0]) - len(tmp[1])] + tmp[1]
        wavelength = 0.5 * (float(tmp[0]) + float(tmp[1]))
    else:
        wavelength = float(wavelength)

    if unit == "A":
        wavelength *= 1e-4
        unit = "um"

    return specie, wavelength, unit
