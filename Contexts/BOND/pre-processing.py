# This is when running the script from the root mgris directory
import sys, os

s = os.path.dirname(os.path.abspath(__file__))
s = s[: s.index("Contexts/")]
sys.path.insert(0, s)

import argparse
import pandas as pd
from Library.lib_mc import logsum_simple
from Library.lib_main import readparam, read_data, check_duplicates
from Library.lib_misc import FileExists, inputRL


try:
    import jax.numpy as np

    # making sure that device is visible
    jax.default_backend()
    jax.devices()
except:
    import numpy as np


def main(args=None):

    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"
    grid_directory = root_directory + "Grids/"

    # =================================================================
    # get grids from context input file
    with open(root_directory + "context_input.txt", "r") as f:
        input_file_lines = f.readlines()
    # remove comments and empty lines, and carriage returns
    input_file_lines = list(
        np.array(
            [
                l.replace("\n", "").strip()
                for l in input_file_lines
                if l[0] != "#" and l != "\n"
            ]
        )
    )

    grid_file = readparam(input_file_lines, ["grid_file"])
    labels_file = [
        grid_directory + "list_observables.dat",
    ]  # convenient list of keys
    params_file = [
        grid_directory + "list_parameters.dat",
    ]  # convenient list of keys
    files = [
        grid_directory + grid_file,
    ]

    post_processing_file = readparam(input_file_lines, ["post_processing_file"])
    if post_processing_file is not None:
        post_processing_file = post_processing_file[0]
        labels_file += [grid_directory + "list_post-processing.dat"]
        files += [
            grid_directory + post_processing_file,
        ]

    # =================================================================
    # pre-process and convert

    for i, f in enumerate(files):

        print("\n------------------------------------")

        # keeping all columns...
        columns = None
        # ...or keeping only some columns for memory issues
        # columns = readparam(input_file_lines, ['primary_parameters'])+['Stop_Av10'] #mandatory
        # columns += ['Al22660.35A',] #some tracers
        data = read_data(
            f,  # will read either .csv or .fth files
            columns=columns,
            delimiter=readparam(input_file_lines, ["delimiter"]),  # if ascii
        )

        # -------------------------------------------------------------------
        print("Making some changes...")
        # UPDATE

        data = check_duplicates(
            data, columns=readparam(input_file_lines, ["primary_parameters"])
        )

        # rename/clean column labels
        # data = data.rename(columns={tmp: tmp.replace(' ', '').replace("'", '').replace('(', '').replace(')', '') for tmp in data.keys()})

        # secondary parameters
        data["Zsun"] = data["Z"] + (12 - 8.69)
        data["Zsun"] = round(data["Zsun"] * 1000) / 1000  # keep precision from table
        del data["Z"]

        # observables and primary parameters
        if i == 0:  # ony for main grid

            # adding some sums
            data["S26716+30A"] = logsum_simple([data["S26716.44A"], data["S26730.82A"]])

            # observables not in the main table will be also looked for in the post-processing table
            # but you can alwas include here some parameters from the post-processing table, e.g., to use as observable IF rows are exactly in the same order in both the main table and the post-processing table
            # data['MHI'] = pd.read_csv(files[1], sep=readparam(input_file_lines, ['delimiter']))['MHI']

            with open(params_file[i], "w") as of:
                for p in readparam(input_file_lines, ["primary_parameters"]):
                    of.write("\n\nPrimary parameter: {}\n".format(p))
                    of.write(str(sorted(set(data[p].values))))
            print("Writing list of parameters in {}".format(params_file[i]))

            # keep a convenient (full) list of observables
            observable_list = [
                d + "\n"
                for d in data.keys()
                if d not in readparam(input_file_lines, ["primary_parameters"])
            ]
            with open(labels_file[i], "w") as of:
                for o in observable_list:
                    of.write(o)
            print("Writing list of observables in {}".format(labels_file[i]))

            ffile = grid_directory + "/model_grid.fth"

        if i == 1:  # only for post-processing grid
            #    data['...'] = data['...']  - ...
            ffile = grid_directory + "/model_grid_post_processing.fth"

        # -------------------------------------------------------------------
        # change to luminosity?
        # [np.log10(np.power(10,values[list_of_lines[j]][ind[0]])/(4*np.pi*(distance*1e6*pc2cm)**2)) for j in range(n_lines)]

        # this is to avoid appending
        # ffile = os.path.splitext(f)[0]+'.fth'
        if os.path.exists(ffile):
            os.remove(ffile)

        print("Writing new Feather file... {}".format(ffile))
        data.to_feather(ffile)

    print("------------------------------------")


# --------------------------------------------------------------------
if __name__ == "__main__":

    main()
