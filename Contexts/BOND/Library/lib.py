import numpy as np

import matplotlib

# matplotlib.use('TkAgg')

from matplotlib.patches import Arc, Wedge
from matplotlib import rc
import matplotlib.pyplot as plt
import random

from scipy.interpolate import interp1d
from math import pi

from scipy.stats import norm

# global parameters
from rcparams import rcParams

import logging

# from Contexts.Library.lib import torad, plot_lineangle, planck, spectrumplot, context_specific_plots#, context_specific_rules

from Contexts.Library.extinction_rule import extinction_rule, parse_cloudy_label

# -------------------------------------------------------------------
def context_specific_plots(trace, inp, verbose=False):

    # logging.info('- spectrum plots')
    # spectrumplot(trace, inp)

    logging.info("- sector plots")
    try:
        sectorplot(trace, inp, verbose=verbose)
        sectorplot(trace, inp, explode=False, verbose=verbose)
    except:
        logging.error("/!\ Sector plot not working")

    return


# -------------------------------------------------------------------
def context_specific_rules(inp, verbose=False):
    # any custom rules for inference here

    rules = {}

    res, log = extinction_rule(inp, verbose=verbose)
    rules.update(res)
    logging.info(log)

    # ==========================================
    # OTHER RULES

    # params_eval = "pm.HalfNormal('ebv', sigma=10)" #possibility to have several commands with ;
    # scale_context = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=model['ebv']) for i,o in enumerate(inp['observations'].names)}" #possibility to have several commands with ;
    # scale_context_add2trace = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=trace.posterior['ebv']) for i,o in enumerate(inp['observations'].names)}" #possibility to have several commands with ;
    # we'll take the log with tt afterwards
    # rules.update({'extinction': {'params': ['ebv'],
    #                              'params_eval': params_eval, #pm parameters
    #                              'scale_context': scale_context, #for mgris_search
    #                              'scale_context_add2trace': scale_context_add2trace, #for add2trace in mgris_process and mgris_post-process
    #                              'cs_args': cs_args} #args to pass for the evals
    # })

    return rules


# -------------------------------------------------------------------
def context_specific_plots(trace, inp, verbose=False):

    # logging.info('- spectrum plots')
    # spectrumplot(trace, inp)

    logging.info("- sector plots")
    try:
        sectorplot(trace, inp, verbose=verbose)
        sectorplot(trace, inp, explode=False, verbose=verbose)
    except:
        logging.error("/!\ Sector plot not working")

    return


# -------------------------------------------------------------------

# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# CUSTOM FUNCTIONS BELOW
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------

# -------------------------------------------------------------------
# -------------------------------------------------------------------
# plot sectors
global RMIN
RMIN = 2


# U to radius (same L for source for all sectors)
def U2r(U):
    return -(U - 1 - RMIN)


# cut to n
def cut2n(cut):
    return interp1d([0, 1, 2, 3], [0, 0, 1.5, 3])(cut)


def cut2rout(rin, cut):
    scale = 1.3  # to expand sectors
    return scale * (rin + cut)


def sectorplot(trace, inp, explode=True, verbose=False):  # g->group
    """
    Density: color
    U: radius
    """

    n_comps = inp["n_comps"]
    groups = inp["groups"]
    n_groups = len(groups)

    plt.clf()

    matplotlib.rcParams["text.usetex"] = True
    if "text.latex.unicode" in matplotlib.rcParams.keys():
        matplotlib.rcParams["text.latex.unicode"] = True
    elif "text.latex.preview" in matplotlib.rcParams.keys():
        matplotlib.rcParams["text.latex.preview"] = True

    plt.rc("text", usetex=False)
    plt.rc("font", family="serif")

    scales = [
        np.quantile(np.sum([trace["w"][s] for s in groups[g]], axis=(0)), 0.5)
        for g in range(n_groups)
    ]
    wratio = scales - np.min(scales) + 1
    wratio /= np.max(wratio)
    wratio = [np.max([w, 0.2]) for w in wratio]

    fig, axs = plt.subplots(
        1, n_groups, figsize=(5 * n_groups, 6), gridspec_kw={"width_ratios": wratio}
    )
    if n_groups == 1:
        axs = [
            axs,
        ]

    # color map for density
    cmap = matplotlib.cm.get_cmap("YlOrRd")
    normalize = matplotlib.colors.Normalize(vmin=0, vmax=5)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=normalize)

    z_text, z_stars, z_center_spot, z_ugrid, z_wedge_in, z_wedge_out, z_arc, z_line = (
        300,
        250,
        240,
        240,
        230,
        22,
        250,
        250,
    )
    # depth = 4
    cols = rcParams["sector_colors"]
    starspread, starsize = 2, 4

    for g in range(n_groups):

        # age is supposed to be the same for all input sectors
        age = 4 * 10 ** (np.quantile(trace["age_{}".format(groups[g][0])], 0.5))

        nstars = round(10 * scales[g] * 10 ** (float(trace["scale"].median().data)))

        sectors = [
            {
                "w": float(
                    (
                        trace["w_{}".format(s)]
                        / np.sum(
                            [trace["w_{}".format(ss)] for ss in groups[g]], axis=(0)
                        )
                    )
                    .mean()
                    .data
                ),
                "u": float(trace["U_{}".format(s)].median().data),
                "n": 2,
            }
            for s in groups[g]
        ]
        for j, s in enumerate(groups[g]):
            if "cut_0" in trace.keys():
                sectors[j].update(
                    {"cut": float(trace["hbfrac_{}".format(s)].median().data)}
                )
            else:
                sectors[j].update({"cut": 3})

        # special case for DIG sector beyond matter-bounded sector
        if "DIG" in inp["configuration"]:
            # normalize first
            maxw = np.max([s["w"] for s in sectors])
            for i, s in enumerate(sectors):
                sectors[s]["w"] /= maxw

        axs[g].set_aspect(
            "equal"
        )  # possible to use only if we don't use gridspec option in subplots
        axs[g].set_axis_off()
        axs[g].set_xlim(-15, 15)
        axs[g].set_ylim(-15, 15)

        if n_groups > 1:
            axs[g].set_title("Group #{}".format(g), y=1.0, pad=-14 * wratio[g])

        # draw angles, arcs, wedges
        theta1 = 0  # *u.deg
        angle_scale = 2
        if angle_scale > 360 * np.min([s["w"] for s in sectors]):
            angle_scale = int(np.ceil(360 * np.min([s["w"] for s in sectors])))
        # angle_scale = int(np.ceil(360*np.min([s['w'] for s in sectors])))
        max_angle = int(360 / angle_scale)

        remaining_angles = range(0, max_angle)  # initial list of all possible angle

        for i, s in enumerate(sectors):

            # reinitialize if DIG sector beyond matter-bounded sector
            if "DIG" in inp["configuration"]:
                remaining_angles = range(0, max_angle)

            rin = U2r(s["u"])
            rout = cut2rout(rin, s["cut"])  # depth

            if explode:
                angles = random.sample(
                    set(remaining_angles),
                    np.min([int(round(max_angle * s["w"])), len(remaining_angles)]),
                )
                for a in angles:
                    wedge_in = axs[g].add_artist(
                        Wedge(
                            (0.0, 0.0),
                            rin,
                            a * angle_scale,
                            (a + 1) * angle_scale,
                            facecolor="white",
                            color="white",
                            alpha=1,
                            fill=True,
                            zorder=z_wedge_in,
                        )
                    )
                    wedge_in.set_antialiased(True)
                    # wedge_out = axs[g].add_artist(Wedge((0.,0.), rout, a*angle_scale, (a+1)*angle_scale, facecolor=cmap(normalize(s['n'])), hatch=None, alpha=0.75, fill = True, zorder=z_wedge_out))
                    # wedge_out.set_antialiased(True)
                    for ii, cut in enumerate(np.arange(s["cut"], 0, -0.1)):
                        n = s["n"] * (1 + cut2n(cut))
                        wedge_out = axs[g].add_artist(
                            Wedge(
                                (0.0, 0.0),
                                cut2rout(rin, cut),
                                a * angle_scale,
                                (a + 1) * angle_scale,
                                facecolor=cmap(normalize(n)),
                                hatch=None,
                                alpha=0.75,
                                fill=True,
                                zorder=z_wedge_out + ii,
                            )
                        )
                        wedge_out.set_antialiased(True)

                    # arc_in = axs[g].add_patch(Arc((0, 0), 2*rin, 2*rin, theta1=a*angle_scale, theta2=(a+1)*angle_scale, edgecolor=cols[i], lw=2, zorder=z_arc))
                    # arc_out = axs[g].add_patch(Arc((0, 0), 2*rout, 2*rout, theta1=a*angle_scale, theta2=(a+1)*angle_scale, edgecolor=cols[i], lw=2, zorder=z_arc))

                remaining_angles = np.setdiff1d(remaining_angles, angles)

            else:
                theta2 = theta1 + 360 * s["w"]  # *u.deg

                plot_lineangle(
                    axs[g],
                    [0.0, 0.0],
                    theta1,
                    rout,
                    color="black",
                    linestyle="-",
                    zorder=z_line,
                )
                plot_lineangle(
                    axs[g],
                    [0.0, 0.0],
                    theta2,
                    rout,
                    color="black",
                    linestyle="-",
                    zorder=z_line,
                )

                wedge_in = axs[g].add_artist(
                    Wedge(
                        (0.0, 0.0),
                        rin,
                        theta1,
                        theta2,
                        color="white",
                        facecolor="white",
                        alpha=1,
                        fill=True,
                        zorder=z_wedge_in,
                    )
                )
                # wedge_out = axs[g].add_artist(Wedge((0.,0.), rout, theta1, theta2, facecolor=cmap(normalize(s['n'])), hatch=None, alpha=1, fill = True, zorder=z_wedge_out))
                for ii, cut in enumerate(np.arange(s["cut"], 0, -0.1)):
                    n = s["n"] * (1 + cut2n(cut))
                    wedge_out = axs[g].add_artist(
                        Wedge(
                            (0.0, 0.0),
                            cut2rout(rin, cut),
                            theta1,
                            theta2,
                            facecolor=cmap(normalize(n)),
                            hatch=None,
                            alpha=1,
                            fill=True,
                            zorder=z_wedge_out + ii,
                        )
                    )
                    wedge_out.set_antialiased(True)

                # annotation
                angle = 0.5 * (theta1 + theta2)
                x = rout * np.cos(torad(angle))
                y = rout * np.sin(torad(angle))
                t = axs[g].annotate(
                    "#{}".format(i + 1),
                    xy=(x, y),
                    xytext=(x, y),
                    zorder=z_text,
                    fontweight="bold",
                    color=cols[i],
                    fontsize=10,
                    horizontalalignment={-1: "right", 1: "left"}[int(np.sign(x))],
                    verticalalignment={-1: "top", 1: "bottom"}[int(np.sign(y))],
                )
                t.set_bbox(dict(facecolor="white", alpha=0.7, edgecolor="white"))

                # U grid
                for uvalue in np.arange(-4, 0):
                    if U2r(uvalue) < rin:
                        axs[g].add_patch(
                            Arc(
                                (0, 0),
                                2 * U2r(uvalue),
                                2 * U2r(uvalue),
                                theta1=theta1,
                                theta2=theta2,
                                edgecolor="gray",
                                linestyle=":",
                                lw=2,
                                zorder=z_ugrid,
                            )
                        )

                # arcs
                arc_in = axs[g].add_patch(
                    Arc(
                        (0, 0),
                        2 * rin,
                        2 * rin,
                        theta1=theta1,
                        theta2=theta2,
                        edgecolor=cols[i],
                        lw=2,
                        zorder=z_arc,
                    )
                )
                arc_out = axs[g].add_patch(
                    Arc(
                        (0, 0),
                        2 * rout,
                        2 * rout,
                        theta1=theta1,
                        theta2=theta2,
                        edgecolor=cols[i],
                        lw=2,
                        zorder=z_arc,
                    )
                )

                # ionization front
                r = cut2rout(rin, 1)
                if r < rout:
                    arc_IF = axs[g].add_patch(
                        Arc(
                            (0, 0),
                            2 * r,
                            2 * r,
                            theta1=theta1,
                            theta2=theta2,
                            edgecolor="gray",
                            lw=2,
                            zorder=z_arc,
                        )
                    )

                # dissociation front
                r = cut2rout(rin, 2)
                if r < rout:
                    arc_DF = axs[g].add_patch(
                        Arc(
                            (0, 0),
                            2 * r,
                            2 * r,
                            theta1=theta1,
                            theta2=theta2,
                            edgecolor="gray",
                            lw=2,
                            zorder=z_arc,
                        )
                    )

                theta1 = theta2

        # center spot
        center = axs[g].add_artist(
            Wedge((0.0, 0.0), RMIN, 0.0, 360.0, color="yellow", alpha=1)
        )
        center.zorder = z_center_spot

        # star cluster
        for i in range(nstars):
            x, y = 0, 0
            ds = starsize * np.random.uniform(low=1, high=15 / age)
            dx, dy = (
                starspread * np.array(np.random.random() - 0.5),
                starspread * np.array(np.random.random() - 0.5),
            )
            axs[g].plot(
                x + dx,
                y + dy,
                "*",
                color="white",
                markersize=4 * ds,
                fillstyle="full",
                markeredgewidth=0.0,
                markeredgecolor="cyan",
                alpha=np.min([1, 2 / nstars]),
                zorder=z_stars,
            )  # markeredgecolor='gray'
            axs[g].plot(
                x + dx,
                y + dy,
                "*",
                color="cyan",
                markersize=3 * ds,
                fillstyle="full",
                markeredgewidth=0.0,
                markeredgecolor="cyan",
                alpha=np.min([1, 2 / nstars]),
                zorder=z_stars,
            )
            axs[g].plot(
                x + dx,
                y + dy,
                "*",
                color="blue",
                markersize=2 * ds,
                fillstyle="full",
                markeredgewidth=0.0,
                markeredgecolor="blue",
                alpha=np.min([1, 5 / nstars]),
                zorder=z_stars,
            )

        # ------------------------------
        # fake up the array of the scalar mappable. Urgh…
        # sm._A = []
        # cb = plt.colorbar(sm, orientation='horizontal', pad = 0.05, shrink=0.7)
        # cb.set_label('Log density [cm$^{-3}$]', labelpad=-50, y=1.05, rotation=0, fontsize=10)

    # ------------------------------
    # fig.suptitle('Preliminary', fontsize=10, fontweight='bold')
    fig.subplots_adjust(
        left=0.0, bottom=0.0, right=1.0, top=1.0, wspace=0.0, hspace=0.0
    )
    # plt.show()

    exploded_str = "_exploded" if explode else ""

    fig.savefig(
        "{}/sectors{}.png".format(inp["output_directory"] + "/Plots/", exploded_str)
    )
    plt.close(fig)


# -------------------------------------------------------------------
def spectrumplot(trace, inp):

    inp["observations"].names

    lines = {}

    for name in inp["observations"].names:

        specie, wavelength, unit = parse_cloudy_label(name)

        lines.update(
            {
                name: {
                    "specie": specie,
                    "wavelength": np.log10(float(wavelength)),
                    "modflux": float(trace[name].mean()),
                    "obsflux": inp["observations"].values[
                        np.where(np.array(inp["observations"].names) == name)[0][0]
                    ],
                }
            }
        )

    # ===============================================

    fig, axs = plt.subplots(3, 1, figsize=(5, 6), sharex=True)

    sdev = 0.005
    x = np.arange(
        np.min([lines[line]["wavelength"] for line in lines]),
        np.max([lines[line]["wavelength"] for line in lines]),
        0.001,
    )

    continuum = np.sum(
        [planck(10 ** x * 1e-6, T) for T in np.arange(100, 10, -10)], axis=0
    )

    totalobs = np.sum(
        [
            10 ** (lines[line]["obsflux"] - inp["order_of_magnitude_obs"])
            * norm.pdf(x, lines[line]["wavelength"], sdev)
            for line in lines
        ],
        axis=0,
    )

    totalmod = np.sum(
        [
            10 ** (lines[line]["modflux"] - inp["order_of_magnitude_obs"])
            * norm.pdf(x, lines[line]["wavelength"], sdev)
            for line in lines
        ],
        axis=0,
    )

    for ax in axs:
        ax.set_xlim([np.min(x), np.max(x)])

    axs[2].set_xlabel("Log wavelength [um]")

    axs[0].plot(x, totalmod + continuum, color="red", alpha=0.5)
    axs[0].plot(x, totalobs + continuum, color="blue", alpha=0.5)

    axs[1].plot(x, totalmod, color="red", alpha=0.5)
    axs[1].plot(x, totalobs, color="blue", alpha=0.5)

    axs[2].plot(x, totalmod - totalobs, color="black")
    axs[2].axhline(0, color="black")

    axlinear = axs[0].twiny()
    axlinear.set_xlim([10 ** axs[0].get_xlim()[0], 10 ** axs[0].get_xlim()[1]])
    axlinear.set_xscale("log")
    axlinear.set_xlabel("Wavelength [um]")

    for line in lines:

        modflux = 10 ** (
            lines[line]["obsflux"] - inp["order_of_magnitude_obs"]
        ) * norm.pdf(x, lines[line]["wavelength"], sdev)

        obsflux = 10 ** (
            lines[line]["modflux"] - inp["order_of_magnitude_obs"]
        ) * norm.pdf(x, lines[line]["wavelength"], sdev)

        axs[0].plot(x, modflux + continuum, color="red", alpha=0.5)
        axs[0].plot(x, obsflux + continuum, color="blue", alpha=0.5)

        axs[1].plot(x, modflux, color="red", alpha=0.5)
        axs[1].plot(x, obsflux, color="blue", alpha=0.5)

        for ax in axs:
            ax.axvline(lines[line]["wavelength"], color="black", alpha=0.2)

        axs[1].text(
            lines[line]["wavelength"],
            np.max(modflux),
            line,
            rotation="vertical",
            alpha=0.2,
            fontsize="small",
        )

    axs[0].set_yscale("log")

    fig.subplots_adjust(wspace=0, hspace=0)

    fig.savefig("{}/spectra.png".format(inp["output_directory"] + "/Plots/"))
    plt.close(fig)

    return
