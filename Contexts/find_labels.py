import argparse
import os
import glob

# ===================================================================
# ===================================================================
# ===================================================================


class args:
    def __init__(
        self,
        inputs="",
    ):
        args.ncomps = inputs


def main(args=None):
    if args is None:
        parser = argparse.ArgumentParser(description="Find labels")
        parser.add_argument("labels", type=str, help="Labels", nargs="+")
        parser.add_argument(
            "--operator-and",
            "-and",
            help="Boolean operator and [default: or]",
            action="store_true",
        )
        args = parser.parse_args()

    # dirs = next(os.walk("."))[1]
    alldirs = list(glob.iglob("./**/list_observables.dat", recursive=True))
    contexts = [
        d.replace("./", "").replace("/Grids/list_observables.dat", "") for d in alldirs
    ]
    labels = [l for l in args.labels if l not in contexts]

    results = {}

    for context in contexts:
        print("")
        print("----------------------------")
        print(context)
        results.update({context: {}})
        fname = context + "/Grids/list_observables.dat"

        matches = []
        if args.operator_and:
            with open(fname, "r") as f:
                for line in f.readlines():
                    m = True
                    for label in labels:
                        if label not in line:
                            m = False
                    if m:
                        matches += [
                            line.replace("\n", ""),
                        ]

                tmp = "_AND_".join(labels)
                print(tmp, matches)
                results[context].update({tmp: matches})

        else:
            for label in labels:
                print("----------------------------")

                with open(fname, "r") as f:
                    for line in f.readlines():
                        if label in line:
                            matches += [
                                line.replace("\n", ""),
                            ]

                print(label, matches)
                results[context].update({label: matches})

    print()
    print("===============================================")
    print(results)
    print("===============================================")

    return results


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    main()
