import os
import argparse
import pandas as pd


def main(args=None):

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS processing step")
        parser.add_argument("inputfile", type=str, help="name_of_input_file")
        args = parser.parse_args()

    f = args.inputfile

    # fs_gb = os.path.getsize(f)/1e9
    # print('Available memory: {}Gb, file size: {}Gb'.format(np.round(mem_gb, 2), np.round(fs_gb, 2)))

    # if need to read chunks because ascii file is too big:
    # chunksize = 1e5
    # k = 0
    # for chunk in pd.read_csv(f, chunksize=chunksize, sep=readparam(input_file_lines, ['delimiter'])):
    #     if k==0:
    #         data = chunk
    #     else:
    #         data = pd.concat([data, chunk])
    #     k += 1

    try:
        data = pd.read_csv(f, sep=",")
    except:
        try:
            data = pd.read_csv(f, sep="\t")
        except:
            raise Exception("Cannot recognized delimiter")
    data.to_feather(os.path.splitext(f)[0] + ".fth")
    print("Data written to ", os.path.splitext(f)[0] + ".fth")

    print("\n")

    print("Columns that are likely to be parameters: ")

    for k in data.select_dtypes(include="number").keys():
        tmp = set(data[k])
        if len(tmp) < 0.01 * len(data):
            print(k)
            print(tmp)


# --------------------------------------------------------------------
if __name__ == "__main__":

    main()
