# global parameters
from rcparams import rcParams

import logging

# -------------------------------------------------------------------
def context_specific_plots(trace, inp, verbose=False):

    # any custom plot script or call to functions here

    return


# -------------------------------------------------------------------
def context_specific_rules(inp, verbose=False):
    # any custom rules for inference here

    rules = {}

    # ==========================================
    # RULE number 1

    # params_eval = "pm.HalfNormal('ebv', sigma=10)" #possibility to have several commands with ;
    # scale_context = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=model['ebv']) for i,o in enumerate(inp['observations'].names)}" #possibility to have several commands with ;
    # scale_context_add2trace = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=trace.posterior['ebv']) for i,o in enumerate(inp['observations'].names)}" #possibility to have several commands with ;
    # we'll take the log with tt afterwards
    # rules.update({'extinction': {'params': ['ebv'],
    #                              'params_eval': params_eval, #pm parameters
    #                              'scale_context': scale_context, #for mgris_search
    #                              'scale_context_add2trace': scale_context_add2trace, #for add2trace in mgris_process and mgris_post-process
    #                              'cs_args': cs_args} #args to pass for the evals
    # })

    return rules


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# CUSTOM FUNCTIONS BELOW
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
