import os
import shutil
import glob
import sys
import argparse
from Library.lib_main import readinputfile
from Library.lib_misc import FileExists, inputAC
import numpy as np
from tqdm import tqdm

# global parameters
from rcparams import rcParams


# ===================================================================
# ===================================================================
# ===================================================================
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        inputfiles="",
        batchn=1,
        nsamples_per_job=None,
        pythonexec="",
        workingdirectory="",
        outputprefix="",
        lowpriority=False,
        skip_plots=True,
        clean=False,
        cluster=False,
        force=False,
        forcesearch=False,
        forceprocess=False,
        forcepostprocess=False,
        # incomplete_ok=True,
        verbose=False,
    ):
        args.inputfiles = inputfiles
        args.nsamples_per_job = nsamples_per_job
        args.batchn = int(batchn)
        args.pythonexec = str(pythonexec)
        args.workingdirectory = str(workingdirectory)
        args.outputprefix = str(outputprefix)
        args.lowpriority = bool(lowpriority)
        args.skip_plots = bool(skip_plots)
        args.clean = (bool(clean),)
        args.cluster = bool(cluster)
        args.force = bool(force)
        args.forcesearch = bool(forcesearch)
        args.forceprocess = bool(forcepostprocess)
        args.forcepostprocess = bool(forcepostprocess)
        # args.incomplete_ok = bool(incompleteok)
        args.verbose = bool(verbose)


def chunks(l, n):
    """Yield n number of striped chunks from l."""
    for i in range(0, n):
        yield l[i::n]


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    tensorlib = rcParams["tensorlib"]

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS wrapper")
        parser.add_argument(
            "--inputfiles", "-i", type=str, help="input files", nargs="+"
        )
        parser.add_argument(
            "--batchn",
            "-b",
            help="How many batch jobs to produce. ",
            type=int,
        )
        parser.add_argument(
            "--nsamples_per_job",
            "-n",
            help="force number of particles (independent chains) per job for SMC or number of draws per chain for other samplers. ",
            type=int,
        )
        parser.add_argument("--pythonexec", "-p", help="Python executable", type=str)
        parser.add_argument(
            "--workingdirectory", "-d", help="Working directory", type=str
        )
        parser.add_argument(
            "--outputprefix", "-o", help="Output prefix for chunk bash files", type=str
        )
        parser.add_argument("--lowp", "-l", help="Low priority", action="store_true")
        parser.add_argument(
            "--skip-plots",
            "-sp",
            help="Skip plots [default=True]",
            action="store_true",
            default=True,
        )
        # parser.add_argument(
        #     "--incomplete_ok",
        #     "-iok",
        #     help="OK to use incomplete grids with distributions (power-law, normal etc...)",
        #     action="store_true",
        # )
        parser.add_argument(
            "--clean",
            "-C",
            help="Clean batch files and all job outputs",
            action="store_true",
        )
        parser.add_argument(
            "--force",
            "-f",
            help="Force all steps",
            action="store_true",
        )
        parser.add_argument(
            "--forcesearch",
            "-fs",
            help="Force search step",
            action="store_true",
        )
        parser.add_argument(
            "--forceprocess",
            "-fp",
            help="Force processing step",
            action="store_true",
        )
        parser.add_argument(
            "--forcepostprocess",
            "-fpp",
            help="Force post-processing step",
            action="store_true",
        )
        parser.add_argument(
            "--cluster",
            "-c",
            help="Cluster use, with Slurm header",
            action="store_true",
        )
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        args = parser.parse_args()

    verbose = args.verbose

    cluster = args.cluster

    prefix = args.outputprefix

    inputfiles = args.inputfiles
    if inputfiles is None:
        inputfiles = inputAC(
            "Provide directory or list of input files (--inputfiles/-i) [~ and tab completion possible]: "
        )
        if len(inputfiles.split(" ")) == 1:
            if inputfiles[-1] != "/":
                inputfiles += "/"
            if inputfiles[-1] != "*":
                inputfiles += "*"
            inputfiles = glob.glob(inputfiles)
        cluster = input(
            "Will the jobs run a SLURM cluster (--cluster/-c) [Y/n]? "
        ).lower() in ["y", ""]
        prefix = inputAC("Enter a job name: ")

    nsamples_per_job = args.nsamples_per_job
    if nsamples_per_job is None:
        nsamples_per_job = int(
            input("Provide number of samples per chain (--nsamples_per_job/-n): ")
        )

    batchn = args.batchn
    if batchn is None:
        batchn = int(input("Provide number of jobs to produce (--batchn/-b): "))

    prefix = prefix + "_" if prefix is not None else ""

    lowp = "/usr/bin/nice -n 19 /usr/bin/ionice -c2 -n7 " if args.lowp else ""

    pythonexec = args.pythonexec
    if pythonexec is None:
        pythonexec = sys.executable
    print(
        f"/!\\ Will use python executable {pythonexec} (can be changed with --pythonexec/-p)"
    )

    workingdirectory = args.workingdirectory
    if workingdirectory is None:
        workingdirectory = os.getcwd()
        print(
            f"/!\\ Will use current working directory {workingdirectory} (can be changed with --workingdirectory/-d)"
        )

    if args.clean:
        # shutil.rmtree(f"{workingdirectory}/jobs/")
        files = glob.glob(f"{workingdirectory}/jobs/*.bash")
        for f in files:
            os.remove(f)
        files = glob.glob(f"{workingdirectory}/jobs/tmp/*.bash")
        for f in files:
            os.remove(f)

    tobeprocessed = []

    os.makedirs(f"{workingdirectory}/jobs/tmp", exist_ok=True)
    os.makedirs(f"{workingdirectory}/jobs/out", exist_ok=True)

    for inputfile in tqdm(inputfiles):
        input_params = readinputfile(inputfile, root_directory, verbose=verbose)
        output_directory = input_params["output_directory"]

        # print(output_directory)

        if (
            args.force
            or (not FileExists(output_directory + "/trace.netCDF") or args.forcesearch)
            or (
                not FileExists(output_directory + "/trace_process.netCDF")
                or args.forceprocess
            )
            or (
                not FileExists(output_directory + "/trace_post-process.netCDF")
                or args.forcepostprocess
            )
        ):
            tobeprocessed += [
                inputfile,
            ]

    print("Files to be processed (first 10): ", tobeprocessed[0:10])

    nspc = f"-n {nsamples_per_job}"

    # switches
    if args.skip_plots:
        nspc += " -sp"
    # if args.incomplete_ok:
    # batch requires iok to prevent stops
    nspc += " -iok"

    allchunks = "#!/bin/bash\n"

    flags = ""
    if args.force:
        flags += "--force "
    if args.forcesearch:
        flags += "--forcesearch "
    if args.forceprocess:
        flags += "--forceprocess "
    if args.forcepostprocess:
        flags += "--forcepostprocess "

    if len(tobeprocessed) == 0:
        print("/!\\ No file to process, will write empty bash...")
        # writing an empty bash is useful if a script runs the batch file automatically after generating it
    else:
        for ic, c in enumerate(chunks(tobeprocessed, batchn)):
            if len(c) == 0:
                continue
            # print(c)

            s = " ".join([os.path.abspath(c1) for c1 in c])
            fname = f"{workingdirectory}/jobs/tmp/{prefix}chunk{ic+1}.bash"
            with open(fname, "w") as f:
                print(f"Writing {fname}")
                if cluster:
                    header = f"""#!/bin/bash
if [ -z "$2" ]
then
    echo "No job name supplied"
    name="sjob-"
else
    name="sjob-$2-"
fi
echo $name

sbatch <<EOT
#!/bin/bash
#SBATCH --job-name=$name$1
#SBATCH --output={workingdirectory}/jobs/out/%x.%j.out
#SBATCH --error={workingdirectory}/jobs/out/%x.%j.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=4500
#SBATCH --mem=16G
export {tensorlib.upper()}_FLAGS='floatX=float32,base_compiledir=/tmp/{tensorlib}.NOBACKUP_{prefix}job{ic+1}'
# if possible use scratch folder (e.g., /feynman/scratch/dap/lfemi/vleboute/) or /tmp/
module add anaconda
cd {root_directory}
{pythonexec} {root_directory}mgris_wrapper.py {nspc} {flags} {s}
EOT
#run with bash chunk{ic+1}.bash [optional job name]
"""
                    f.write(header)
                else:
                    f.write(
                        f"{lowp}{sys.executable} {root_directory}mgris_wrapper.py {nspc} {s}"
                    )

            if cluster:
                allchunks += f"bash {workingdirectory}/jobs/tmp/{prefix}chunk{ic+1}.bash {prefix}\n"
            else:
                allchunks += f"bash {workingdirectory}/jobs/tmp/{prefix}chunk{ic+1}.bash {prefix} > {workingdirectory}/jobs/out/{prefix}chunk{ic+1}.out &\n"

    fname = f"{workingdirectory}/jobs/{prefix}allchunks.bash"
    print(f"Writing {fname}")
    with open(fname, "w") as f:
        f.write(allchunks)


# input list of files
# update with files with no results
# define how many batches
# create n bash files either as python xxx ot sbatch xxx

# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    main()
