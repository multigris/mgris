# LML goes up again with ncomps when few lines --> prefer LOO

import os
import argparse

import arviz as az
import pickle
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

import logging

from Library.lib_misc import FileExists
from Library.lib_main import setup_custom_logger, closelogfiles, printsection

# from @OriolAbril
# You can compare models with different priors and parameters as long as they all use the same data and you define the atomic observation concept form the notebooks in the same way.


# ===================================================================
# ===================================================================
# ===================================================================
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        result_directories="",
        output="",
        ncomps=False,
        ncompsexpected=None,
        verbose=False,
        nopenalization=False,
        debug=False,
    ):
        args.result_directories = result_directories
        args.ncomps = ncomps
        args.ncompsexpected = ncompsexpected
        args.output = output
        args.verbose = bool(verbose)
        args.nopenalization = bool(nopenalization)
        args.debug = bool(debug)


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS model comparison")
        parser.add_argument(
            "result_directories", type=str, help="Result directory names", nargs="+"
        )
        parser.add_argument("--output", "-o", type=str, help="Output name", default="")
        parser.add_argument(
            "--ncomps",
            "-n",
            help="compare models *with different number of components* to estimate ncomps",
            action="store_true",
        )
        parser.add_argument(
            "--ncompsexpected",
            "-ne",
            type=int,
            help="expected ncomps [for tests]",
            nargs=1,
        )
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        parser.add_argument(
            "--nopenalization", "-np", help="No penalization term", action="store_true"
        )
        parser.add_argument(
            "--debug", "-d", help="Debugging information", action="store_true"
        )
        args = parser.parse_args()

    outdir = "ModelComparison/"
    os.makedirs("ModelComparison/", exist_ok=True)

    global logger
    # start logging now
    # logging console + file
    logname = "compare"
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logger = setup_custom_logger(logname, directory=outdir, loglevel=loglevel)

    models = args.result_directories

    if len(models) < 2:
        logging.error("Provide at least two models")
        exit()

    logging.info("/!\\ Model comparison is only meaningful if:")
    logging.info(
        "- the data is the same and observations defined the same way between the two models"
    )
    logging.info(
        "- all chains have passed the burn-in phase and have converged (toward the same solution)"
    )
    logging.info("- there are significantly more observations than parameters")

    # remove trailing /
    for i, m in enumerate(models):
        if m[-1] == "/":
            models[i] = models[i][:-1]

    if args.ncomps:  # we wanna estimate ncomps
        ncompsexpected = (
            args.ncompsexpected[0] if args.ncompsexpected is not None else 0
        )

        outname = args.output if args.output != "" else "testncomps"

        methods = {}
        methods.update({"pnsigma": {}})
        methods.update({"likelihood": {"col": "black", "res": {}}})
        methods.update({"loo": {"col": "blue", "res": {}}})
        methods.update({"waic": {"col": "red", "res": {}}})
        methods.update({"lml": {"col": "green", "res": {}}})
        methods.update({"ncomps": {"col": "black", "res": {}, "all": {}}})
        methods.update({"pdict": {}})
        methods.update({"ncompsexpected": ncompsexpected})

        logging.info("")
        logging.info("")
        logging.info("Run: {}".format(outname))
        logging.info("")
        logging.info("")

        for m in models:
            printsection("Reading model: {}".format(m))
            t = az.from_netcdf(m + "/trace_process.netCDF")
            tp = az.from_netcdf(m + "/trace_process.netCDF").posterior
            n = tp.dims["w_dim_0"]

            if "ncomps" in tp.keys():
                printsection("- ncomps vary allow max {}".format(n), level=2)

                tab = tp["ncomps"].data.flatten()
                tmp = [len(tab[tab == i + 1]) / len(tab) for i in np.arange(n)]
                methods["ncomps"]["all"].update({n: tmp})
                methods["ncomps"]["res"].update(
                    {n: np.median(tp["ncomps"].data.flatten())}
                )
                # print(methods['ncomps']['res'])

            else:
                printsection("- ncomps fix = {}".format(n), level=2)

                tmp = np.median(
                    (
                        np.array(
                            az.from_netcdf(m + "/trace_process.netCDF")
                            .log_likelihood["log_likelihood"]
                            .sum(axis=(2))
                            .median(axis=1)
                        )
                    )
                )
                methods["likelihood"]["res"].update({n: tmp})

                with open(m + "/results_search.pkl", "rb") as f:
                    res = pickle.load(f)

                    params = [
                        l
                        for l in set(res["unobserved_RVs"])
                        if "idx_" in l or "scale" in l
                    ]
                    if "w_stickbreaking__" in res["unobserved_RVs"]:
                        params += ["w_{}".format(i) for i in range(n)]
                    rhatmean = az.rhat(t, var_names=params).to_array().data.mean()
                    logging.info("rhatmean = {}".format(rhatmean))

                    if "log_marginal_likelihood" in res.keys():
                        tmp = res["log_marginal_likelihood"]
                        methods["lml"]["res"].update({n: tmp})

                with open(m + "/results_process.pkl", "rb") as f:
                    res = pickle.load(f)
                    methods["pnsigma"].update({n: res["pnsigma"]})

                methods["loo"]["res"].update({n: az.loo(t).elpd_loo})
                logging.info(az.loo(t))

                methods["waic"]["res"].update({n: az.waic(t).elpd_waic})
                logging.info(az.waic(t))

                params = set(["_".join(p.split("_")[:-1]) for p in params])
                params = [p + "_0" for p in params if p != ""]
                pdict = {}
                for p in params:
                    if (
                        p.replace("_0", "_1") in t.posterior.keys()
                    ):  # check if params common to all comps
                        if float(t.posterior[p].mean()) == float(
                            t.posterior[p.replace("_0", "_1")].mean()
                        ):
                            pdict.update({p: float(t.posterior[p].mean())})
                    else:
                        pdict.update({p: float(t.posterior[p].mean())})
                logging.info(pdict.keys())

                with open(m + "/input.pkl", "rb") as f:
                    res = pickle.load(f)
                if "secondary_parameters" in res.keys():
                    tmp = res["secondary_parameters"]
                else:
                    tmp = res["params_to_predict"]
                if FileExists(m + "/trace_post-process.netCDF"):
                    params += tmp
                    tpp = az.from_netcdf(m + "/trace_post-process.netCDF").posterior
                    for p in tmp:
                        if p in tpp.keys():
                            pdict.update({p: float(tpp[p].mean())})
                methods["pdict"].update({n: pdict})
                logging.info(pdict.keys())

        for m in ("loo", "waic", "lml", "likelihood"):
            methods[m]["res"] = {
                j: methods[m]["res"][j] for j in sorted(methods[m]["res"])
            }

        # remove lower number in ncompsvary than minimum for ncompsfix
        # mins = np.min(list(methods[m]['res'].keys()))
        # if 'ncomps' in methods.keys():
        #     for k in methods['ncomps']['all'].keys():
        #         methods['ncomps']['all'][k] = methods['ncomps']['all'][k][mins-1:]

        # =====================
        if ncompsexpected == 0:
            tmp = [d for d in methods["pdict"].keys()]
            tmp = int(np.max(tmp))
            nparams = len([d for d in methods["pdict"][tmp].keys()])
            fig, axs = plt.subplots(nparams, 1, figsize=(7, 4 * nparams), sharex=True)
            for j, p in enumerate(methods["pdict"][tmp].keys()):
                axs[j].plot(
                    methods["pdict"].keys(),
                    [methods["pdict"][i][p] for i in methods["pdict"].keys()],
                    "o-",
                    label=p,
                )
                axs[j].legend()
                axs[j].set_xticks([1, 2, 3])
            fig.subplots_adjust(hspace=0, wspace=0)
            fig.tight_layout()
            fig.savefig("{}/params_variation_ncomps_{}.pdf".format(outdir, outname))
        else:
            tmp = [d for d in methods["pdict"].keys()]
            tmp = int(np.max(tmp))
            nparams = len([d for d in methods["pdict"][tmp].keys()])
            fig, ax = plt.subplots(1, 1, figsize=(7, 4), sharex=True)
            print(methods["pdict"][tmp].keys())
            for j, p in enumerate(methods["pdict"][tmp].keys()):
                if p in ("MH2_C", "idx_Tx_0"):
                    continue
                ax.plot(
                    methods["pdict"].keys(),
                    [
                        methods["pdict"][i][p] - methods["pdict"][ncompsexpected][p]
                        for i in methods["pdict"].keys()
                    ],
                    "o-",
                    label=p,
                )
            ax.legend(ncol=4)
            ax.set_title(outname)
            ax.set_xticks([1, 2, 3])
            ax.set_ylabel("$\Delta$ log")
            ax.set_xlabel("# of components")
            ax.set_ylim([-1, 1])
            ax.axvline(ncompsexpected, color="black")
            ax.axhline(0, color="black")
            fig.subplots_adjust(hspace=0, wspace=0)
            fig.tight_layout()
            fig.savefig("{}/params_variation_ncomps_{}.pdf".format(outdir, outname))

        # =====================

        # if methods['ncomps']['all']=={}:
        #    fig, ax = plt.subplots(1, 1, figsize=(7,4))
        #    axs = [ax,]
        # else:
        fig, axs = plt.subplots(3, 1, figsize=(6, 9), sharex=True)

        # =====================
        n = np.array([l for l in methods["loo"]["res"]])
        vals = np.array([methods["pnsigma"][j][1] for j in n])
        axs[2].plot(n, vals, color="black", marker="o", alpha=0.5)
        vals = np.array([methods["pnsigma"][j][2] for j in n])
        axs[2].plot(n, vals, color="black", marker="o", alpha=0.5)
        axs[2].set_xlabel("Fit with # components")
        axs[2].set_ylabel("% of draws agreeing within 2- and 3-sigma")
        # axs[2].legend()

        # =====================

        for m in ("likelihood", "loo", "lml", "waic"):
            n = np.array([l for l in methods[m]["res"]])
            vals = np.array([methods[m]["res"][l] for l in methods[m]["res"]])
            print(m, vals)

            for jj, j in enumerate(np.arange(n.min(), n.max() + 1)):
                tmp = 10 ** vals[0 : jj + 1] / np.sum(10 ** vals[0 : jj + 1])
                tmp[np.isnan(tmp)] = 1  # rounding errors
                methods[m].update({"weights_{}".format(j): tmp})
                if np.argmin(vals) != 0 and np.argmin(vals) != len(vals) - 1:
                    logging.warning(
                        "/!\\ LML has a minimum, probably due to lack of tracers..."
                    )

            axs[0].plot(
                n,
                vals - vals[np.argmax(vals)],
                marker="o",
                label=m,
                color=methods[m]["col"],
                alpha=0.5,
            )
            axs[0].plot(
                n[np.argmax(vals)],
                0,
                marker="o",
                ms=20,
                color=methods[m]["col"],
                alpha=0.5,
            )

        axs[0].axhline(0, color="black")
        axs[0].set_xlabel("Fit with # components")
        axs[0].set_ylabel("Lopg/value")
        axs[0].legend()

        # ===========================
        # ncompsvary (?)
        if methods["ncomps"]["all"] != {}:
            m = "ncomps"
            n = np.array([l for l in methods[m]["all"]])
            vals = np.array([methods[m]["all"][l] for l in methods[m]["all"]])

            methods[m].update(
                {
                    "weights_{}".format(l): methods[m]["all"][l]
                    for l in methods[m]["all"]
                }
            )

            for i, nn in enumerate(n):  # loop on allowmax
                axs[1].plot(
                    [1, nn],
                    [nn, nn],
                    color=methods[m]["col"],
                    linestyle="dotted",
                    alpha=0.5,
                )
                for j in np.arange(nn):
                    axs[1].scatter(
                        j + 1,
                        nn,
                        marker="o",
                        s=100 * np.array(vals[i][j]),
                        facecolor=methods[m]["col"],
                        alpha=0.5,
                    )

            n = np.array([l for l in methods[m]["res"]])
            vals = np.array([methods[m]["res"][l] for l in methods[m]["res"]])
            axs[1].scatter(
                vals,
                n,
                marker="o",
                s=200,
                edgecolor=methods[m]["col"],
                facecolor="none",
                label="ncomps",
                alpha=0.5,
            )

            axs[1].axhline(0, color="black")
            axs[1].set_ylabel("Allowing max # components")
            axs[1].set_xlabel("Inferred # of components")

        # ===========================
        preferred = (
            ("lml", "loo", "waic", "ncomps")
            if methods["lml"]["res"] != {}
            else ("loo", "ncomps")
        )

        for m in preferred:
            n = np.array([l for l in methods[m]["res"]])
            if methods[m]["res"] == {}:
                continue
            for i in methods[m]["res"].keys():
                methods[m]["flag_{}".format(i)] = False
                methods[m]["flag_{}".format(i + 1)] = False
                if "weights_{}".format(i) not in methods[m].keys():
                    continue
                # it doesn't make sense if the solution with max 3 components is 2 and the solution for max 2 components is 1...
                if "weights_{}".format(i + 1) in methods[m].keys():
                    if (
                        np.argmax(methods[m]["weights_{}".format(i)])
                        != np.argmax(methods[m]["weights_{}".format(i + 1)])
                        and np.argmax(methods[m]["weights_{}".format(i + 1)]) <= i - 1
                    ):
                        logging.warning("{}: pb".format(m))
                        # by default we prefer a low number of comps
                        methods[m]["flag_{}".format(i + 1)] = True

        logging.info(methods)

        # ===========================

        final = ""
        res = {}
        tmp = np.array([l for l in methods["loo"]["res"]])
        for n in tmp:
            logging.info("====================================")
            logging.info("Solution with maximum {} components: ".format(n))
            for im, m in enumerate(preferred):
                if methods[m]["res"] == {}:
                    continue
                if "weights_{}".format(n) not in methods[m].keys():
                    continue

                logging.info(m)
                if methods[m]["flag_{}".format(n)]:
                    logging.warning("pb")
                else:
                    tmp2 = methods[m]["weights_{}".format(n)]
                    if im == 0:
                        res.update({n: np.zeros(len(tmp2))})
                    res[n] += tmp2
                    for ii, i in enumerate(methods["loo"]["res"].keys()):
                        if i > n:
                            continue
                        logging.info(
                            "{}: {}%".format(
                                i,
                                np.round(
                                    100 * methods[m]["weights_{}".format(n)][ii], 2
                                ),
                            )
                        )

            print("Average:")
            res[n] /= np.sum(res[n])
            for ii, i in enumerate(methods["loo"]["res"].keys()):
                if i > n:
                    continue
                logging.info("{}: {}%".format(i, np.round(100 * res[n][ii], 2)))

            # final += 'max #{}: #{} with {}% '.format(n,
            #                                         1+np.argsort(res[n])[::-1][0],
            #                                         np.round(100*res[n][np.argsort(res[n])[::-1][0]], 2))

            # axs[0].set_title(final)

        logging.info("====================================")
        with open("{}/{}.pkl".format(outdir, outname), "wb") as f:
            pickle.dump(methods, f, protocol=4)
            pickle.dump(res, f, protocol=4)

        fig.tight_layout()
        fig.savefig("{}/{}.pdf".format(outdir, outname))

        logging.info("Files: ")
        logging.info("{}/{}.pdf".format(outdir, outname))
        logging.info("{}/{}.pkl".format(outdir, outname))
        logging.info("{}/params_variation_ncomps_{}.pdf".format(outdir, outname))

    # ===============================================================
    # ===============================================================
    # ===============================================================
    # ===============================================================
    # ===============================================================
    # ===============================================================
    # ===============================================================
    # ===============================================================
    # ===============================================================
    else:  # general comparison
        models2 = [m.split("/")[-1] for m in models]
        if len(set(models2)) != len(models2):
            models2 = [m + f"_{i+1}" for i, m in enumerate(models2)]

        cdict = {
            models2[i]: az.from_netcdf(m + "/trace_process.netCDF")
            for i, m in enumerate(models)
        }  # trace or trace_process

        # LOO, WAIC, log_marginal_likelihood (for SMC)
        likes = {}
        loos = {}
        waics = {}
        lml = {}
        dims = []
        for i, m in enumerate(models):
            printsection(m)

            trace = cdict[models2[i]]
            if "x" in trace.log_likelihood.dims.keys():
                dims += [
                    trace.log_likelihood.dims["x"],
                ]
            else:
                dims += [
                    0,
                ]

            loos.update({models2[i]: az.loo(trace, pointwise=True)})
            logging.info(loos[models2[i]])

            waics.update({models2[i]: az.waic(trace, pointwise=True)})
            logging.info(waics[models2[i]])

            # likes.update({models2[i]: trace.log_likelihood['log_likelihood'].median(axis=1).to_numpy()})
            # prod on all tracers then median on draws for each axis
            likes.update(
                {
                    models2[i]: np.array(
                        trace.log_likelihood["log_likelihood"]
                        .sum(axis=(2))
                        .median(axis=1)
                    )
                }
            )

            with open(m + "/input.pkl", "rb") as f:
                inp = pickle.load(f)
                # mpm = mgris_search.main(args=mgris_search.args(inputfile=inp['input_f'], nsamples=100, return_model_context=True, method_step=inp['method_step'], order=inp['interp_order']))
                if inp["method_step"] == "SMC":
                    with open(m + "/results_search.pkl", "rb") as f:
                        res = pickle.load(f)
                        lml.update({models2[i]: res["log_marginal_likelihood_chains"]})

        printsection("Comparison")

        if np.min(dims) != np.max(dims):
            logging.error(
                "Some datasets do not have the same number of observed RVs..."
            )
            exit()

        # weight methods
        method = "BB-pseudo-BMA"
        # method = 'stacking'
        # method: Literal[stacking, BB - pseudo - BMA, pseudo - MA] = 'stacking'

        logging.info("==================================")
        logging.info(
            """# rank, the ranking of the models starting from 0 (best model) to the number of models.
# loo, the values of LOO (or WAIC). The DataFrame is always sorted from best LOO/WAIC to worst.
# p_loo, the value of the penalization term. We can roughly think of this value as the estimated effective number of parameters (but do not take that too seriously).
# d_loo, the relative difference between the value of LOO/WAIC for the top-ranked model and the value of LOO/WAIC for each model. For this reason we will always get a value of 0 for the first model.
# weight, the weights assigned to each model. These weights can be loosely interpreted as the probability of each model being true (among the compared models) given the data.
# se, the standard error for the LOO/WAIC computations. The standard error can be useful to assess the uncertainty of the LOO/WAIC estimates. By default these errors are computed using stacking.
# dse, the standard errors of the difference between two values of LOO/WAIC. The same way that we can compute the standard error for each value of LOO/WAIC, we can compute the standard error of the differences between two values of LOO/WAIC. Notice that both quantities are not necessarily the same, the reason is that the uncertainty about LOO/WAIC is correlated between models. This quantity is always 0 for the top-ranked model.
# warning, If True the computation of LOO/WAIC may not be reliable.
# loo_scale, the scale of the reported values. The default is the log scale as previously mentioned. Other options are deviance – this is the log-score multiplied by -2 (this reverts the order: a lower LOO/WAIC will be better) – and negative-log – this is the log-score multiplied by -1 (as with the deviance scale, a lower value is better)."""
        )
        logging.info("==================================")

        s = "_".join([m for m in models2])
        plotname = "comp_{}.pdf".format(s)
        fig, ax = plt.subplots(1, 1, figsize=(5, 5))

        logging.info("")
        logging.info("==================================")
        logging.info("Likelihood:")
        tmp = np.array([likes[m] for m in models2])
        mmax = np.max(tmp.mean(axis=1))
        ax.fill_between(
            np.arange(len(models2)),
            tmp.min(axis=1) - mmax,
            tmp.max(axis=1) - mmax,
            color="black",
            alpha=0.1,
        )
        # ax.fill_between(np.arange(len(models2)), tmp.min(axis=1)-tmp.max(), tmp.max(axis=1)-tmp.max(), color='black', alpha=0.5, label='Likelihood')
        # ax.plot(np.arange(len(models2)), tmp-np.max(tmp), 'o-', label='Likelihood', color='black', alpha=1)
        ax.plot(
            np.arange(len(models2)),
            [np.mean(l) - mmax for l in tmp],
            "o-",
            color="black",
            label="Likelihood",
        )
        for i, l in enumerate(tmp):
            logging.info("{} : {} (mean={})".format(models2[i], l, np.mean(l)))

        if trace.log_likelihood["log_likelihood"].ndim == 2:
            logging.warning("/!\\ Not an element-wise logp")
        else:
            logging.info("")
            logging.info("==================================")
            logging.info("LOO:")
            df = az.compare(cdict, ic="loo", method=method)
            logging.info(df)

            if args.nopenalization:
                tmp = np.array([loos[m].elpd_loo + loos[m].p_loo for m in models2])
                ax.plot(
                    np.arange(len(models2)),
                    tmp - np.max(tmp),
                    "o-",
                    label="LOO",
                    color="red",
                )
                tmp2 = np.array(
                    [
                        [
                            loos[m].elpd_loo + loos[m].p_loo - 0.5 * loos[m].se,
                            loos[m].elpd_loo + loos[m].p_loo + 0.5 * loos[m].se,
                        ]
                        for m in models2
                    ]
                )
                ax.fill_between(
                    np.arange(len(models2)),
                    tmp2.min(axis=1) - tmp.max(),
                    tmp2.max(axis=1) - tmp.max(),
                    color="red",
                    alpha=0.1,
                )
            else:
                tmp = np.array([loos[m].elpd_loo for m in models2])
                ax.plot(
                    np.arange(len(models2)),
                    tmp - np.max(tmp),
                    "o-",
                    label="LOO",
                    color="red",
                )
                tmp2 = np.array(
                    [
                        [
                            loos[m].elpd_loo - 0.5 * loos[m].se,
                            loos[m].elpd_loo + 0.5 * loos[m].se,
                        ]
                        for m in models2
                    ]
                )
                ax.fill_between(
                    np.arange(len(models2)),
                    tmp2.min(axis=1) - tmp.max(),
                    tmp2.max(axis=1) - tmp.max(),
                    color="red",
                    alpha=0.1,
                )

            logging.info("")
            logging.info("==================================")
            logging.info("WAIC:")
            df = az.compare(cdict, ic="waic", method=method)
            logging.info(df)

            if args.nopenalization:
                tmp = np.array([waics[m].elpd_waic + waics[m].p_waic for m in models2])
                mmax = np.max(tmp)
                ax.plot(
                    np.arange(len(models2)),
                    tmp - mmax,
                    "o-",
                    label="WAIC",
                    color="blue",
                )
                tmp2 = np.array(
                    [
                        [
                            waics[m].elpd_waic + waics[m].p_waic - 0.5 * waics[m].se,
                            waics[m].elpd_waic + waics[m].p_waic + 0.5 * waics[m].se,
                        ]
                        for m in models2
                    ]
                )
                ax.fill_between(
                    np.arange(len(models2)),
                    tmp2.min(axis=1) - mmax,
                    tmp2.max(axis=1) - mmax,
                    color="blue",
                    alpha=0.1,
                )
            else:
                tmp = np.array([waics[m].elpd_waic for m in models2])
                mmax = np.max(tmp)
                ax.plot(
                    np.arange(len(models2)),
                    tmp - mmax,
                    "o-",
                    label="WAIC",
                    color="blue",
                )
                tmp2 = np.array(
                    [
                        [
                            waics[m].elpd_waic - 0.5 * waics[m].se,
                            waics[m].elpd_waic + 0.5 * waics[m].se,
                        ]
                        for m in models2
                    ]
                )
                ax.fill_between(
                    np.arange(len(models2)),
                    tmp2.min(axis=1) - mmax,
                    tmp2.max(axis=1) - mmax,
                    color="blue",
                    alpha=0.1,
                )

        logging.info("")
        logging.info("==================================")
        logging.info("Log marginal likelihood (SMC):")

        njobs = trace.log_likelihood.dims["chain"]

        if lml != {}:
            tmp = np.array([lml[m] for m in models2])
            mmax = np.max(tmp.mean(axis=1))
            ax.plot(
                np.arange(len(models2)),
                [np.mean(l) - mmax for l in tmp],
                "o-",
                color="green",
                label="SMC marg. likelihood",
            )
            for i, l in enumerate(tmp):
                logging.info("{} : {} (mean={})".format(models2[i], l, np.mean(l)))
                tmp2 = [
                    np.exp(tmp[i, ic]) / np.sum(np.exp(tmp[:, ic]))
                    for ic in range(njobs)
                ]
                logging.info("weights = {} (mean={})".format(tmp2, np.mean(tmp2)))

            ax.fill_between(
                np.arange(len(models2)),
                tmp.min(axis=1) - mmax,
                tmp.max(axis=1) - mmax,
                color="green",
                alpha=0.1,
            )
            # ax.plot(np.arange(len(models2)), tmp-np.max(tmp), 'o-', label='SMC marg. lik.', color='black', alpha=0.25)

        logging.info("==================================")
        logging.info("")

        ax.axhline(0, color="black", alpha=0.25)
        plt.xticks(np.arange(len(models2)), models2)
        ax.set_xlabel("Model")
        ax.set_ylabel("$\Delta$ Likelihood")
        plt.xticks(rotation=25)
        ax.legend(loc="lower left")
        fig.tight_layout()
        fig.savefig("{}/{}".format(outdir, plotname))
        plt.close(fig)
        logging.info("Plot created: {}/{}".format(outdir, plotname))

        # fig1 = az.plot_compare(df, insample_dev=False)
        # fig1.get_figure().savefig(plotname)
        # https://docs.pymc.io/notebooks/model_comparison.html

        # The empty circle represents the values of LOO and the black error bars associated with them are the values of the standard deviation of LOO.
        # The value of the highest LOO, i.e the best estimated model, is also indicated with a vertical dashed grey line to ease comparison with other LOO values.
        # For all models except the top-ranked one we also get a triangle indicating the value of the difference of WAIC between that model and the top model and a grey errobar indicating the standard error of the differences between the top-ranked WAIC and WAIC for each model.

        # also check pm.compare or az.compare

        # ============================

        # If all Pareto k small, model is likely to be ok (although there can be better models)

        # If high Pareto k values

        # If p_loo << the number of parameters p, then the model is likely to be misspecified. PPC is likely to detect the problem, too. Try using overdispersed model, or add more structural information (nonlinearity, mixture model, etc.).

        # If p_loo > the number of parameters p, then the model is likely to be badly misspecified. If the number of parameters p<<n, then PPC is likely to detect the problem, too. Case example https://rawgit.com/avehtari/modelselection_tutorial/master/roaches.html 126

        # If p_loo > the number of parameters p, then the model is likely to be badly misspecified. If the number of parameters p is relatively large compared to the number of observations p>n/5 (more accurately we should count number of observations influencing each parameter as in hierarchical models some groups may have small n and some groups large n), it is possible that PPC doesn’t detect the problem. Case example Recommendations for what to do when k exceeds 0.5 in the loo package? 195

        # If p_loo < the number of parameters p and the number of parameters p is relatively large compared to the number of observations p>n/5, it is likely that model is so flexible or population prior is so weak that it’s difficult to predict for left out observation even if the model is true one. Case example is the simulated 8 schools in https://arxiv.org/abs/1507.04544 69 and Gaussian processes and spatial models with short correlation lengths.

        # ============================
        # https://github.com/OriolAbril/calaix_de_sastre/blob/master/arviz_ic_multiple_observations/exp_vs_power_pymc_discourse/pymc3_example.ipynb
        # https://arviz-devs.github.io/arviz/user_guide/pymc3_refitting_xr_lik.html
    # https://nbviewer.jupyter.org/github/OriolAbril/Exploratory-Analysis-of-Bayesian-Models/blob/multi_obs_ic/content/Section_04/Multiple_likelihoods.ipynb


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    global logger
    # ===================================================================
    # logging console + file
    logger = setup_custom_logger("compare")

    closelogfiles()
    main()
