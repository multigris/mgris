# ========================================
# ========================================
# ========================================
# Global variables
#
# Changing some of these parameters may result in unexpected behavior
# Make sure to run thorough tests
#
# Global variables are also saved in the result pickle file for a given run for convenience
# ========================================
# ========================================
# ========================================

import os
import numpy as np
import matplotlib.colors as mcolors

rcParams = {}

# ========================================
# OBSERVATIONS

#   ░▒▓██████▓▒░  ░▒▓███████▓▒░   ░▒▓███████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓███████▓▒░   ░▒▓██████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░        ░▒▓█▓▒░
#   ░▒▓██████▓▒░  ░▒▓███████▓▒░  ░▒▓███████▓▒░
#
#

# If observations are provided in linear scale, upper limits are considered
# as a measurement =0 and the 2-sigma error corresponds to the measured upper limit.
rcParams.update({"upper_limit_sigma": 2.0})
# default = 2 (i.e., 2 sigma)

# ========================================
# GRID

#   ░▒▓██████▓▒░  ░▒▓███████▓▒░  ░▒▓█▓▒░ ░▒▓███████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒▒▓███▓▒░ ░▒▓███████▓▒░  ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#   ░▒▓██████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓███████▓▒░
#
#

# Models with all observables=NaNs have 0-probability for the parameter set
# the remaining NaNs are replaced by default by the minimum value in the grid
# Instead, it is possible to ignore the parameter set altogether setting the following parameter to True
rcParams.update({"gridnanisnan": False})
# default = False

# Maximum array size for the refined grid (used only if the latter option is enabled)
rcParams.update({"maxgridmemsize": 1})
# default = 1 (in Gb)

# ========================================
# MODEL & SAMPLING

#  ░▒▓██████████████▓▒░   ░▒▓██████▓▒░  ░▒▓███████▓▒░  ░▒▓████████▓▒░ ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓██████▓▒░   ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░  ░▒▓██████▓▒░  ░▒▓███████▓▒░  ░▒▓████████▓▒░ ░▒▓████████▓▒░
#
#

# Type of constraints used (indices/real)
rcParams.update({"type_constraints": "indices"})
# default = "indices", accepts "incides" or "real"

# StudentT nu parameter (aka normality parameter or degress of freedom).
# As the number increases the distribution becomes closer and closer to a Normal distribution
rcParams.update({"StudentT_nu": 20})
# default = 20

# Continuous sampling is used by default. Discrete sampling is meant only for tests.
# Use discrete sampling instead by setting the value below to >0. Set to 0 for continuous sampling (default).
# Discrete sampling uses categorical variables and Metropolis-within-Gibbs step method
# number of elements in array is 2*N -1; factor of 2 adds one element between two existing ones; factor of 1 forces drawing grid vertices (~nearest neighbors)
rcParams.update({"inference_upsample": 0})
# default = 0 (continuous sampling), accepts integer values >= 0

# When using continuous sampling (default), you may use an upsampling factor. This is meant to force sampling the vertices
# Set to 0 for no upsampling. Factor of 2 adds one element between two existing ones; factor of 1 forces drawing grid vertices (~nearest neighbors)
rcParams.update({"linterp_upsample": 0})
# default = 0 (no upsampling), accepts interger values >= 0

# by default the code ignore model upper limits (identified with -inf)
rcParams.update({"ignore_model_upper_limits": True})
# default = True

# ========================================
# INFERENCE

#  ░▒▓███████▓▒░   ░▒▓██████▓▒░   ░▒▓██████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓████████▓▒░ ░▒▓███████▓▒░  ░▒▓███████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓███████▓▒░  ░▒▓████████▓▒░ ░▒▓█▓▒░        ░▒▓███████▓▒░  ░▒▓██████▓▒░   ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓███████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░  ░▒▓██████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓████████▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓███████▓▒░
#

# precision for all arrays & tensor library
# recommended: float32 (float64 will use more memory and significantly slow down the inference)
rcParams.update({"precision": "float32"})
# default = 'float32', accepts "float32" or "float64" (float16 prevents using C routines)

# Backend (CPU or GPU)
rcParams.update({"inference_backend": "cpu"})
# default = 'cpu', accepts "cpu" or "gpu"

if rcParams["inference_backend"] == "cpu":
    # CPU sampling

    #   ░▒▓██████▓▒░  ░▒▓███████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░        ░▒▓███████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░        ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░
    #   ░▒▓██████▓▒░  ░▒▓█▓▒░         ░▒▓██████▓▒░
    #
    #

    # Maximum number of cores to use (and thus jobs)
    rcParams.update({"ncores_max": 4})
    # default = 4
    # mgris uses either ncores_max or the number of cores - 1, whichever is the smallest

    # How many chains per core
    # more chains minimize the possibility of some fraction of chains that start/go haywire.
    # Makes it easier for convergence diagnostics and to randomize start values in single run
    # but a higher number of chains shouldn't be at the expense of the individual chain length
    rcParams.update({"njobs_per_core": 1})
    # default = 1

    # Minimum number of draws (including tuning) *per chain*
    rcParams.update({"nsamples_per_job_min": 100})
    # default = 100

    # Maximum number of draws *per chain* (can be overridden if using --nsamples flag)
    rcParams.update({"nsamples_per_job_max": 50000})
    # default = 50000

    # Total maximum number of draws (set for RAM and disk space)
    rcParams.update({"nsamples_max": 200000})
    # default = 200000

    # target ESS fraction for each stage in the SMC sampler
    # Determines the change of beta from stage to stage, i.e. indirectly the number of stages,
    # the higher the value of `threshold` the higher the number of stages
    rcParams.update({"smc_threshold": 0.85})
    # default = 0.85

    # correlation threshold (PyMC=4; replaces p_acc_rate)
    # determines the number of MCMC steps computed automatically in the IMH kernel
    # choose either a small number with fewer samples or a larger number with more samples
    # probably better to have smaller number of samples that mutate well
    rcParams.update({"smc_corr_threshold": 0.001})
    # default = 0.001

    # acceptance rate for SMC sampler (PyMC <=3.11)
    # determines the number of MCMC steps computed automatically in the IMH kernel
    rcParams.update({"smc_acc_rate": 0.95})
    # default = 0.95
    # 0.9-~0.99 is fine

    # code for zero-probability draws in log
    rcParams.update({"infcode": -np.inf})
    # default = -np.inf

else:
    # GPU sampling with Blackjax SMC

    #   ░▒▓██████▓▒░  ░▒▓███████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒▒▓███▓▒░ ░▒▓███████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░
    #  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░
    #   ░▒▓██████▓▒░  ░▒▓█▓▒░         ░▒▓██████▓▒░
    #
    #
    # number of XLA interfaces
    rcParams.update({"ncores_max": 1})
    # default = 1

    # number of jobs, each job running nparticles (i.e., independent chains) -- see below
    rcParams.update({"njobs": 4})
    # default = 4
    # >=2 to enable rhat diagnostic

    # minimum number of particles, i.e., independent chains per job
    rcParams.update({"nsamples_per_job_min": 100})
    # default = 100

    # maximum number of particles, i.e., independent chains per job
    rcParams.update({"nsamples_per_job_max": 100000})
    # default = 100000

    # total minimum number of particles, i.e., independent chains all jobs combined
    rcParams.update(
        {"nsamples_min": rcParams["njobs"] * rcParams["nsamples_per_job_min"]}
    )
    # default = automatic

    # total maximum number of particles, i.e., independent chains all jobs combined
    rcParams.update(
        {"nsamples_max": rcParams["njobs"] * rcParams["nsamples_per_job_max"]}
    )
    # default = automatic

    # kernel parameters
    # rcParams.update({"smc_kernel": "HMC"})  # accepts HMC or NUTS
    # for a given set of parameters below, NUTS is several times slower but may lead to better results.
    rcParams.update({"smc_kernel": "HMC"})
    # default = 'HMC', accepts "HMC" or "NUTS"

    # HMC kernel only
    rcParams.update({"smc_step_size": 0.001})
    # default = 0.001
    # prefer low values as number of parameters increases (e.g., with hierarchical)

    # HMC kernel only
    rcParams.update({"smc_integration_steps": 25})
    # default = 25
    # large values (~100) are nice but integrated distributions (LOC) may require less (~20-50)
    # drives a good part of the inference time, if changed, nsamples probably needs to change as well

    # the number of iterations represented in the diagnosed quantities
    # (that many iterations will be used even if actual number of iterations may be smaller)
    rcParams.update({"smc_iterations_to_diagnose": 100})
    # default = 100

    # target effective sampling size between SMC iterations, used to calculate the tempering exponent
    rcParams.update({"smc_target_essn": 0.75})
    # default = 0.75
    # prefer larger values as number of parameters increases (e.g., with hierarchical)

    # number of steps of the inner kernel when mutating particles
    rcParams.update({"smc_mcmc_steps": 100})
    # default = 100

    # code for zero-probability in log (blackjax doesn't seem to like -inf likelihood)
    rcParams.update({"infcode": -100})
    # default = -100

# ========================================
# DIAGNOSTICS

#  ░▒▓███████▓▒░  ░▒▓█▓▒░  ░▒▓██████▓▒░   ░▒▓██████▓▒░   ░▒▓███████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓████████▓▒░ ░▒▓█▓▒▒▓███▓▒░  ░▒▓██████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓███████▓▒░  ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░  ░▒▓██████▓▒░  ░▒▓███████▓▒░
#
#

# Posterior prediective check (PPC) samples
rcParams.update({"ppc_samples": 1e4})
# default = 1e4

# Target *total* draws for creating traces from inferred parameters for the processing step
# also used for the post-processing as a consequence
rcParams.update({"ndraws_proc": 7500})
# default = 7500

# Target draws *per chain* for the processing step
# also used for the post-processing as a consequence
rcParams.update({"min_ndraws_proc": 1000})
# default = 1000

# Fraction of draws to throw away for the processing step
# also used for the post-processing as a consequence
# mostly useful for walkers like NUTS
# doesn't hurt because processing and post-processing steps may be long
rcParams.update({"frac_ndraws_proc": 0.5})
# default = 0.5

# Probability of credible interval to be used throughout
# Interval that spans most of the distribution
rcParams.update({"ci": 0.94})
# default = 0.94

# p-value threshold for warnings
rcParams.update({"pvalue_threshold": [0.05, 1 - 0.05]})
# default = [0.05, 0.95]

# what is considered a significant enough correlation coefficient
# used only for corr_obs_params* and corr_params_obs* plots
rcParams.update({"corr_significant": 0.1})
# default = 0.1

# ========================================
# PLOTS

# number of columns for mosaic plots
rcParams.update({"plot_ncols": 3})
# default = 3

# ========================================
# ANY CUSTOM VARIABLE HERE
# custom variables can be used, e.g., in the context-specific rules
# they can be overridden in the input file
rcParams.update({"ebv_sigma": 1})
# default = 1

# ================================================================================
# ================================================================================
# ================================================================================
# The parameters below are not supposed to be modified
# ================================================================================
# ================================================================================
# ================================================================================

# ========================================
# PYMC version

#  ░▒▓███████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓██████████████▓▒░   ░▒▓██████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░
#  ░▒▓███████▓▒░   ░▒▓██████▓▒░  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░
#  ░▒▓█▓▒░           ░▒▓█▓▒░     ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░
#  ░▒▓█▓▒░           ░▒▓█▓▒░     ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░           ░▒▓█▓▒░     ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░  ░▒▓██████▓▒░
#
#
# THEANO/AESARA FLAGS
# compilation directories are located by default in /tmp (recommended for use with home computer or cluster)
# make sure to provide a user-specific name if several users run on same machine
# it is also possible to change this directory if one user runs several jobs on a singe computer or node by doing:
# export THEANO_FLAGS='floatX=float32,base_compiledir=/tmp/theano.NOBACKUP_job1'; python mgris_search.py -v input.txt
# export AESARA_FLAGS='floatX=float32,base_compiledir=/tmp/aesara.NOBACKUP_job1'; python mgris_search.py -v input.txt

# for simplicity, export every possibility depending on PyMC version
# do this before importing pm
for tensorlib in ("pytensor", "aesara", "theano"):
    rcParams.update(
        {
            f"{tensorlib.upper()}_FLAGS": f"floatX={rcParams['precision']},base_compiledir=/tmp/{tensorlib}.NOBACKUP"
        }
    )

    os.environ.get(f"{tensorlib.upper()}_FLAGS", rcParams[f"{tensorlib.upper()}_FLAGS"])
    if (
        os.getenv(f"{tensorlib.upper()}_FLAGS") is None
    ):  # can be needed if one tries to setup the env variable elsewhere
        os.environ[f"{tensorlib.upper()}_FLAGS"] = rcParams[
            f"{tensorlib.upper()}_FLAGS"
        ]

try:
    import pymc as pm
except:
    import pymc3 as pm

# alternative method: use a symlink (only for local use)
# # remove any previous compile directory in ~
# try:
#     shutil.rmtree(Path(f"~/.{tensorlib}").expanduser())
# except OSError as e:
#     pass

# # remove any previous link
# try:
#     os.unlink(Path(f"~/.{tensorlib}").expanduser())
# except OSError as e:
#     pass

# # remove any previous compile directory in /tmp
# try:
#     shutil.rmtree(f"/tmp/{tensorlib}.NOBACKUP")
# except OSError as e:
#     pass

# os.makedirs(f"/tmp/{tensorlib}.NOBACKUP", exist_ok=True)
# os.symlink(
#     f"/tmp/{tensorlib}.NOBACKUP",
#     Path(f"~/.{tensorlib}").expanduser(),
# )


rcParams.update({"pmversion": int(pm.__version__[0])})

if rcParams["pmversion"] == 5:
    tensorlib = "pytensor"
elif rcParams["pmversion"] == 4:
    tensorlib = "aesara"
elif rcParams["pmversion"] == 3:
    tensorlib = "theano"
rcParams.update({"tensorlib": tensorlib})


# ========================================
# MISC

#  ░▒▓██████████████▓▒░  ░▒▓█▓▒░  ░▒▓███████▓▒░  ░▒▓██████▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░  ░▒▓██████▓▒░  ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░ ░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░        ░▒▓█▓▒░ ░▒▓█▓▒░░▒▓█▓▒░
#  ░▒▓█▓▒░░▒▓█▓▒░░▒▓█▓▒░ ░▒▓█▓▒░ ░▒▓███████▓▒░   ░▒▓██████▓▒░
#
#

# Default colors for components in plots from mgris_process
rcParams.update({"sector_colors": [k for k in mcolors.TABLEAU_COLORS]})

# CI step
rcParams.update(
    {
        "ci_arr": 0.01
        * np.array(
            [
                0.7979,
                1,
                3,
                5,
                7.9655,
                10,
                15.8519,
                20,
                23.5823,
                30,
                38.2925,
                43,
                48.4308,
                51,
                54.6745,
                58,
                60.4675,
                64,
                68.2689,
                72,
                76,
                80,
                84,
                88,
                92,
                95.4499,
                99.73,
                99.9936,
                99.99994,
            ]
        )
    }
)

# ========================================
# TESTS/OBSOLETE

rcParams.update({"memmap": False})

rcParams.update({"propagate_round_to_trace": False})

# ========================================
# ASCII art:
# https://www.patorjk.com/software/taag/#p=display&h=0&v=2&c=bash&f=BlurVision%20ASCII&t=Model
# ========================================
