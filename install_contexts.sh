#!/bin/sh
#initialize and add submodule(s)
git submodule init
#optional: de/activate modules, depending on what you have access to
git config submodule.Contexts/mgris_bond.active true
git config submodule.Contexts/mgris_sfgx.active true
git config submodule.Contexts/mgris_loc.active false 
# or git submodule deinit -f mgris_loc to deactivate
#pull everything but large files
git submodule update --remote --force --depth 1
#pull large binary files
git submodule foreach git lfs pull
#if git-lfs is not installed you can always use wget or the gitlab repository to download the files but see below to install it.
