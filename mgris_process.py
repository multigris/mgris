#!/usr/bin/env python

# coding: utf-8

# ===================================================================
# ===================================================================
# ===================================================================
# IMPORTS
# --------------------------------------------------------------------
# system
import os
import glob
import argparse
import logging
from datetime import datetime
import pickle  # using dill for serialized data?
import random

# import dill as pickle #using dill for serialized data
# pickle.settings['recurse'] = True

# remove some warnings
# https://docs.python.org/3/library/warnings.html
# import warnings
# warnings.simplefilter('ignore', UserWarning)

from pathlib import Path
import sys

path_root = Path(__file__).parents[0]
sys.path.insert(0, str(path_root))

# global parameters
from rcparams import rcParams

# --------------------------------------------------------------------
# PyMC3 & arviz
if rcParams["pmversion"] >= 4:
    import pymc as pm
else:
    import pymc3 as pm

import arviz as az

# --------------------------------------------------------------------
# misc

import xarray

# from scipy import stats
import matplotlib.pyplot as plt
from copy import deepcopy

try:
    import jax.numpy_test as np
except:
    import numpy as np

# from random import sample as random_sample


# --------------------------------------------------------------------
# local libraries
import importlib
import Library.lib_mc
import Library.lib_main
import Library.lib_alt
import Library.lib_class
import Library.lib_plots

importlib.reload(Library.lib_mc)
importlib.reload(Library.lib_main)
importlib.reload(Library.lib_alt)
importlib.reload(Library.lib_class)
importlib.reload(Library.lib_plots)
from Library.lib_mc import *
from Library.lib_main import *
from Library.lib_alt import *
from Library.lib_class import *
from Library.lib_plots import *

import mgris_search


# --------------------------------------------------------------------
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        inputfile="",
        verbose=False,
        debug=False,
        clean=False,
        skip_plots=False,
        no_lims=False,
        ppc=False,
        fix_seed=False,
        filter_chains=False,
        force=False,
    ):
        args.inputfile = str(inputfile)
        args.verbose = bool(verbose)
        args.debug = bool(debug)
        args.clean = bool(clean)
        args.skip_plots = bool(skip_plots)
        args.no_lims = bool(no_lims)
        args.ppc = bool(ppc)
        args.fix_seed = bool(fix_seed)
        args.filter_chains = bool(filter_chains)
        args.force = bool(force)


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    # --------------------------------------------------------------------

    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # COMMAND-LINE SETTINGS

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS processing step")
        parser.add_argument("inputfile", type=str, help="name_of_input_file")
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        parser.add_argument(
            "--debug", "-d", help="Debugging information", action="store_true"
        )
        parser.add_argument(
            "--skip-plots",
            "-sp",
            help="Skip the diagnostics plots",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--ppc", "-ppc", help="posterior predictive check", action="store_true"
        )
        parser.add_argument(
            "--no-lims",
            "-nl",
            help="ignore limits (meant for tests)",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument(
            "--clean",
            "-c",
            help="remove temporary files for PPC of observables to predict",
            action="store_true",
        )
        parser.add_argument(
            "--fix-seed",
            "-fs",
            help="fix random seed for reproducible results",
            action="store_true",
        )
        parser.add_argument(
            "--filter-chains",
            "-fc",
            help="[experimental] ignore bad chains for results and plots",
            action="store_true",
        )
        parser.add_argument(
            "--force",
            "-f",
            help="Force the run even if context files differ",
            action="store_true",
        )
        args = parser.parse_args()

    verbose = args.verbose  # we'll use this a lot

    starttime = datetime.now()

    if args.fix_seed:
        np.random.seed(123456)
        random.seed(123456)

    global logger
    # start logging now (console + file)
    logname = "process"
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logger = setup_custom_logger(logname, loglevel=loglevel)

    # welcome and disclaimers
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("       ┍ M U L ┑")
    logging.info("       ┝ T I G ┥")
    logging.info("       ┕ R I S ┙")
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("     ┍            ┑")
    logging.info("     ┝ PROCESSING ┥")
    logging.info("     ┕            ┙")
    logging.info("\n")
    logging.info("\n")
    logging.info("\n")
    logging.info("Thanks for using MULTIGRIS.")
    logging.info(
        "Please make sure to check the README file, the notebooks, and the publication (https://ui.adsabs.harvard.edu/abs/2022A%26A...667A..34L/abstract)."
    )

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # LOADING EVERYTHING

    printsection("Reading inference data")

    input_f = args.inputfile
    if not FileExists(input_f):
        # try and find if there's only one input*.txt file
        f = glob.glob(args.inputfile + "/input*.txt")
        if len(f) == 1:
            input_f = f[0]
        elif len(f) > 1:
            raise OSError("More than one input file found: {}".format(f))
        else:
            raise OSError("No input file found, expected {}".format(input_f))

    input_params = readinputfile(
        input_f, root_directory, verbose=verbose
    )  # read also the post-processing input file

    output_directory = input_params["output_directory"]
    directory_models = input_params["directory_models"]
    hierarchical = input_params["hierarchical"]

    # possibly update output_directory
    if not FileExists(output_directory, is_dir=True):
        output_directory = "/".join(os.path.abspath(input_f).split("/")[:-1]) + "/"

    logging.info("\n")
    logging.info("\n")
    logging.info("\n")

    logging.info(
        """
    ┍========================================┑
    ┝=========== System summary =============┥
    ┝========================================┙
    ┝ Date/time:
    ┝ - {}
    ┕========================================┙
   """.format(
            starttime.strftime("%d/%m/%Y %H:%M:%S")
        )
    )

    logging.info("\n")
    logging.info("\n")
    logging.info("\n")

    logging.info(
        """
    ┍========================================┑
    ┝============= Run summary ==============┥
    ┝========================================┙
    ┝ - input file         : {}
    ┝ - output directory   : {}
    ┕========================================┙
    """.format(
            input_f, output_directory
        )
    )

    # ===================================================================
    # move output log
    movelogfile("process", output_directory + "/Logs/")

    # ===================================================================
    with open(output_directory + "input.pkl", "rb") as f:
        inp = pickle.load(f)

    # update
    inp["output_directory"] = output_directory
    inp["directory_models"] = directory_models

    observations = inp["observations"]
    type_constraints = inp["type_constraints"]
    # lines_to_infer = inp['lines_to_infer']
    # n_inf = inp['n_inf']
    no_lims = inp["no_lims"]
    order_of_magnitude = inp["order_of_magnitude"]
    order_of_magnitude_obs = inp["order_of_magnitude_obs"]
    true_params = inp["true_parameters"]
    n_comps = inp["n_comps"]

    use_scaling = inp["use_scaling"]
    if "use_scaling_specific" in inp.keys():  # backward compatibility
        use_scaling_specific = inp["use_scaling_specific"]
    else:
        use_scaling_specific = inp["use_scaling_uniform"]

    # backward compatibility
    if "plaw_params" not in inp.keys():
        inp["plaw_params"] = []
    if "normal_params" not in inp.keys():
        inp["normal_params"] = []
    if "smoothplaw_params" not in inp.keys():
        inp["smoothplaw_params"] = []
    if "brokenplaw_params" not in inp.keys():
        inp["brokenplaw_params"] = []
    if "doublenormal_params" not in inp.keys():  # backward compatibility
        inp["doublenormal_params"] = []

    # temporary: delta_log is obsolete
    if "delta_log" in dir(inp["observations"]):
        if inp["observations"].delta_log is not None:
            inp["observations"].delta = inp["observations"].delta_log

    s, nstab, sdev, maxv = check_nsigma(inp["observations"], verbose=True)
    # if verbose:
    logging.info(s)
    if sdev > 1 or maxv > 3:
        logging.error(
            " /!\\ Some tracers seem to deviate significantly as far as the detection level is concerned, this may cause a pathological posterior"
        )

    ci = inp["ci"]

    if not FileExists(inp["context"], is_dir=True):
        logging.error("/!\\ Context directory not found, trying working directory...")
        inp["context"] = inp["context"][inp["context"].index("Contexts/") :] + "/"
        # inp['directory_models'] = inp['context']
        inp["context"] = inp["directory_models"]

    # verifying whether context files have been changed or not
    verify_context(inp, args.force)

    # backward compatibility
    if "intensive_obs" not in inp.keys():
        inp["intensive_obs"] = []
    if "intensive_secondary_parameters_noweight" not in inp.keys():
        inp["intensive_secondary_parameters_noweight"] = []

    # list of context_specific parameters
    inp["context_specific_params"] = []
    if inp["active_rules"] != []:
        for rule in inp["active_rules"]:
            for p in inp["active_rules_details"][rule]["params"]:
                inp["context_specific_params"] += [
                    p,
                ]

    # --------------------------------------------------------------------
    # posterior predictive
    ppc_samples = inp["ppc_samples"]

    # ===================================================================
    logging.info("Making obs_grid...")

    grid = make_grid_wrapper(inp, verbose=verbose)

    # parse parameters to infer directly or that need hyperparameters
    inp = grid.parse_params(inp)

    inp["grid"] = grid  # just for add2trace

    (
        grid,
        obs_grid,
        mask_grid,
        mask_ul_grid,
        minvalue,
        completion_fraction,
        order_of_magnitude,
        order_of_magnitude_std,
    ) = make_obs_grid_wrapper(
        inp, grid, observations.obsnames_u, None, None, verbose=verbose
    )
    inp["obs_grid"] = obs_grid  # just for add2trace
    # inp["mask_grid"] = mask_grid  # just for add2trace

    # ===================================================================

    # will contain results to be pickled
    results_process_f = output_directory + "results_process.pkl"
    if FileExists(results_process_f):
        try:
            with open(results_process_f, "rb") as f:
                resdict = pickle.load(f)
        except:
            logging.warning(
                "Existing file {} seems to be unreadable, using empty dictionary".format(
                    results_process_f
                )
            )
            resdict = {}
    else:
        resdict = {}

    # ===================================================================

    logging.info("Loading multi-trace...")

    new_trace_f = output_directory + "trace_process.netCDF"
    ppc_f = output_directory + "ppc.netCDF"
    results_process_f = output_directory + "results_process.pkl"
    if args.clean:
        for f in (new_trace_f, ppc_f, results_process_f):
            if FileExists(f):
                logging.warning("Removing {}".format(f))
                os.remove(f)

    if FileExists(new_trace_f):
        trace = az.from_netcdf(new_trace_f)

    else:
        trace = az.from_netcdf(output_directory + "trace.netCDF")

        printsection("Converting to element-wise logp")

        kk = [k for k in trace.posterior.keys() if "likelihood_" in k]
        if len(kk) == 0:
            logging.warning(
                "Individual likelihoods are missing, remove -nil option in search script to keep them"
            )
        elif "log_likelihood" in dir(trace):
            trace.log_likelihood = trace.log_likelihood.expand_dims(
                {"x": len(kk)}, axis=2
            )
            trace.log_likelihood = trace.log_likelihood.assign_coords(
                {"x": np.arange(len(kk))}
            )
            tmp = np.zeros(trace.log_likelihood["log_likelihood"].shape)
            for i, k in enumerate(kk):
                tmp[:, :, i] = np.squeeze(np.array(trace.posterior[k]))
                del trace.posterior[k]
            trace.log_likelihood["log_likelihood"].data = tmp

        printsection("Processing parameter traces & identifying components")

        # we take only the last part for following steps
        # this doesn't make much sense for SMC to take the last nsamples but at least we reduce the number of samples for a faster processing time

        logging.info("Taking random subset")
        # .sel on posterior because we may have a trace.warmup_posterior (discard = False)

        # take last half (useful mostly for walkers like NUTS)
        if "frac_ndraws_proc" not in inp.keys():  # backward compatibility
            inp["frac_ndraws_proc"] = 0.5
        # inp["frac_ndraws_proc"] = 0
        trace.posterior = trace.posterior.sel(
            draw=np.arange(
                int(inp["frac_ndraws_proc"] * trace.posterior.dims["draw"]),
                trace.posterior.dims["draw"],
            )
        )
        # reindex
        trace.posterior = trace.posterior.assign_coords(
            {"draw": np.arange(trace.posterior.dims["draw"])}
        )
        # take full sample?
        # rndsample = trace.posterior.dims["draw"]
        # and also set inp["frac_ndraws_proc"] to 1
        # take random sample
        rndsample = np.min([trace.posterior.dims["draw"], inp["min_ndraws_proc"]])
        logging.info("Selecting a random sample of {} draws".format(rndsample))
        # rndsample = random_sample(
        #    set(np.arange(trace.posterior.dims["draw"])), rndsample
        # )
        # causing issues in python 3.9, replaced by:
        rndsample = np.random.default_rng().choice(
            np.arange(trace.posterior.dims["draw"]), rndsample, replace=False
        )
        trace.posterior = trace.posterior.sel(draw=rndsample)
        # reindex
        trace.posterior = trace.posterior.assign_coords(
            {"draw": np.arange(trace.posterior.dims["draw"])}
        )

        # same for log_likelihood array
        if "log_likelihood" in dir(trace):
            trace.log_likelihood = trace.log_likelihood.sel(
                draw=np.arange(
                    int(inp["frac_ndraws_proc"] * trace.log_likelihood.dims["draw"]),
                    trace.log_likelihood.dims["draw"],
                )
            )
            trace.log_likelihood = trace.log_likelihood.assign_coords(
                {"draw": np.arange(trace.log_likelihood.dims["draw"])}
            )
            trace.log_likelihood = trace.log_likelihood.sel(draw=rndsample)
            trace.log_likelihood = trace.log_likelihood.assign_coords(
                {"draw": np.arange(trace.log_likelihood.dims["draw"])}
            )

        # Replace -inf values?
        # values = inp['values']
        # values = checkinfinite(values)
        # inp['values'] = values

        # transform boolean into 1 or 0 for numpy routines
        if "mask" in trace.posterior.keys():
            trace.posterior["mask"] = (
                trace.posterior["mask"] * 1
            )  # cannot do *= because of same type rule

        inp["params_notinf"] = (
            grid.params.names_plaw
            + grid.params.names_smoothplaw
            + grid.params.names_brokenplaw
            + grid.params.names_normal
            + grid.params.names_doublenormal
        )

        # now we identify and sort the sectors a posteriori, using indices since even when sampling real values they are converted to indices anyway
        # we can do that even if there was a sorting used for the prior
        # since the re-distribution of components is not perfect, we worsen somewhat the global solution by improving the PDFs of parameters for individual components
        # so we make a new trace for each param
        # if (
        #     inp["sorting"] == "none"
        #     and not hierarchical
        #     # not needed for hierarchical
        #     and not (n_comps > 1 and inp["params_notinf"] != [])
        #     # causing issues with LOC*2
        # ):
        if (
            inp["sorting"] == "none"
            and not hierarchical
            # not needed for hierarchical
        ):
            logging.info("- Isolating and sorting sectors")
            trace = characterize_sectors(inp, trace, verbose=verbose)

        # indices have been modified so now we can
        # add parameters (indices or real) to trace
        # if type_constraints=='indices' and '{}_00'.format(grid.params.names[0]) not in trace.posterior.data_vars:
        # we do it anyway real or indices because the sectors may have been characterized

        with open(output_directory + "results_search.pkl", "rb") as f:
            resdict_search = pickle.load(f)
            if "prior_predictive" in resdict_search.keys():
                prior_predictive = resdict_search["prior_predictive"]
                if rcParams["pmversion"] >= 4:
                    prior_predictive = prior_predictive.prior
            else:
                prior_predictive = {}  # backward compatibility

        logging.info("- Convert indices to real values")
        for p in grid.params.names:
            for s in range(inp["n_comps"]):
                ps = f"{p}_{s}"
                if ps in inp["params_notinf"]:
                    # for p2 in (
                    #     f"lowerbound_{ps}",
                    #     f"upperbound_{ps}",
                    # ):  # backward compatibility
                    #     if p2 in trace.posterior.keys():
                    #         f = grid.params.interp_idx2vals(p)
                    #         trace.posterior["idx_" + p2] = deepcopy(trace.posterior[p2])
                    #         trace.posterior[p2].data = f(trace.posterior[p2].data)
                    for p2 in (f"idx_lowerbound_{ps}", f"idx_upperbound_{ps}"):
                        if p2 in trace.posterior.keys():
                            f = grid.params.interp_idx2vals(p)
                            trace.posterior[p2.replace("idx_", "")] = deepcopy(
                                trace.posterior[p2]
                            )
                            trace.posterior[p2.replace("idx_", "")].data = f(
                                trace.posterior[p2].data
                            )
                            if "w" in prior_predictive.keys():
                                prior_predictive[p2.replace("idx_", "")] = deepcopy(
                                    prior_predictive[p2]
                                )
                                prior_predictive[p2.replace("idx_", "")].data = f(
                                    prior_predictive[p2].data
                                )
                else:
                    f = grid.params.interp_idx2vals(p)
                    if inp["propagate_round_to_trace"]:
                        if p not in inp["linterp_params"]:
                            trace.posterior["idx_{}".format(ps)] = trace.posterior[
                                "idx_{}".format(ps)
                            ].round()
                        elif inp["linterp_upsample"] > 0:
                            trace.posterior["idx_{}".format(ps)] = (
                                trace.posterior["idx_{}".format(ps)]
                                * inp["linterp_upsample"]
                            ).round() / inp["linterp_upsample"]
                    trace.posterior[ps] = deepcopy(trace.posterior["idx_{}".format(ps)])
                    trace.posterior[ps].data = f(
                        trace.posterior["idx_{}".format(ps)].data
                    )
                    if "w" in prior_predictive.keys():
                        if rcParams["pmversion"] >= 4:
                            prior_predictive[ps] = (
                                prior_predictive["idx_{}".format(ps)].shape,
                                f(prior_predictive["idx_{}".format(ps)]),
                            )
                        else:
                            prior_predictive[ps] = f(
                                prior_predictive["idx_{}".format(ps)]
                            )
        resdict.update({"prior_predictive": prior_predictive})

        # if we use a continuous RV for ncomps for SMC, round it up here
        if "ncomps" in trace.posterior.keys():
            trace.posterior["ncomps"] = np.round(trace.posterior["ncomps"])

        # fill trace with new tracers that require a scaling
        logging.info("- Adding observables to trace")
        if hierarchical:
            trace.posterior["w"] = deepcopy(trace.posterior["scale"])
            trace.posterior["w"] *= 0
            trace.posterior["w"] += 1

        # if hierarchical:
        #     trace = add2trace_h(trace, inp, observations.names)
        # else:  # new wrapper for sectors or distributions
        #     trace = add2trace_wrapper(trace, inp, observations.names)
        # trace = add2trace_wrapper(trace, inp, observations.names)

        # may skip if forcing in inference
        skip = False
        if skip:
            refparam = get_refparam(inp)
            reftrace = deepcopy(trace.posterior[refparam])
            reftrace.data *= 0
            for s in range(n_comps):
                trace.posterior["w_{}".format(s)] = deepcopy(reftrace)
                trace.posterior["w_{}".format(s)].data = trace.posterior["w"].data[
                    :, :, s
                ]
                for n in observations.names:
                    trace.posterior[n] = trace.posterior[n][:, :, 0]
        else:
            trace = add2trace_wrapper(trace, inp, observations.names)

        # if inp["params_notinf"] == []:  # not plaw or normal
        #     if hierarchical:
        #         trace = add2trace_h(trace, inp, observations.names)
        #     else:
        #         trace = add2trace(trace, inp, observations.names)
        # else:
        #     # limited version for plaw or normal
        #     trace = add2trace_alt(trace, inp, observations.names)

        # calculate average values when using plaw distributions
        if grid.params.names_notinf != []:
            trace = calcavg(inp, trace)

        # get all results from now on in linear scale if needed
        s = []
        for o in observations.names:
            if observations.obs[o].obs_valuescale == "linear":
                s += [
                    o,
                ]
                trace.posterior[o] = 10 ** trace.posterior[o].astype("float64")
        if len(s) > 0:
            logging.warning(
                "Switching draw values to linear for {}".format(", ".join(s))
            )

        # save changes to avoid having to redo
        trace.to_netcdf(
            new_trace_f
        )  # need to write another file, permission issues otherwise

    # for pnsigma: linear scale if needed
    s = []
    for i, o in enumerate(observations.names):
        if observations.obs[o].obs_valuescale == "linear":
            s += [
                o,
            ]
            inp["observations"].values[i] = 10 ** inp["observations"].values[i]
    if len(s) > 0:
        logging.warning("Switching draw values to linear for {}".format(", ".join(s)))

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # RESULTS

    # switch back from now on in linear scale if needed
    # for o in inp['observations'].names:
    #    if 'obs_valuescale' in dir(inp['observations'].obs[o]):
    #        if inp['observations'].obs[o].obs_valuescale=='linear':
    #            inp['observations'].obs[o].value = 10**np.array(inp['observations'].obs[o].value)
    # inp['observations'].values = [inp['observations'].obs[o].value for o in inp['observations'].names]

    results = ""

    # Chains that are stuck in low probability density region
    if not hierarchical:
        goodchains, badchains = analyze_chains(trace)
        if len(badchains) > 0:
            results += logappend(
                "- /!\\ Some chains seem to be stuck in burn-in phase or in local peak (significantly lower likelihood over chain length): {}".format(
                    badchains
                ),
                error=True,
            )
            if args.filter_chains:
                trace.posterior = trace.posterior.sel(chain=goodchains)
                # reindex
                trace.posterior = trace.posterior.assign_coords(
                    {"chain": np.arange(trace.posterior.dims["chain"])}
                )
                results += logappend(
                    "- /!\\ These chains have been ignored for output but it is advised to run the model again with longer chains",
                    warning=True,
                )
        results += logappend("")

    # =========================
    # now print results on screen

    printsection("Summary")
    sut = az.summary(trace)

    with open(output_directory + "results_search.pkl", "rb") as f:
        resdict_search = pickle.load(f)
    # keep only primary params
    tmp = inp["sysunc"].keys() if inp["sysunc"] is not None else []
    todrop = np.array(
        [
            sut.iloc[i].name
            for i in range(len(sut))
            if "scale" not in sut.iloc[i].name
            and "idx_" not in sut.iloc[i].name
            and sut.iloc[i].name not in resdict_search["var_names"]
            and "w[" not in sut.iloc[i].name
            and sut.iloc[i].name not in tmp
        ]
    )
    sut = sut.drop(todrop)

    results += logappend(sut.to_string())
    results += logappend("")

    results += logappend("/!\\ Values can be =0 if ~0")
    results += logappend(az.summary(trace, var_names=observations.names).to_string())
    results += logappend("")

    results += logappend(
        """
- ESS informs if the chains are large enough (effective number of uncorrelated draws)
  - bulk: sampling efficiency in the bulk of the distribution (related to efficiency of mean and median estimates)
  - tail: minimum for 5% and 95% quantiles.
- rhat informs if the chains mix well
- MCSE estimates the error introduced by sampling and thus the level of precision of our estimates

"""
    )

    results += logappend("")
    # percentage of NaNs / replace NaNs
    for io, o in enumerate(observations.names):
        n = round(
            100
            * len(np.where(~np.isfinite(trace.posterior[o].data.flatten()))[0])
            / len(trace.posterior[o].data.flatten()),
            1,
        )
        if n > 0:
            results += logappend("...{}: NaNs = {}%".format(o, n), warning=True)

    # =========================
    # =========================
    # =========================
    # =========================

    printsection("Results")

    # =========================

    estimators = get_estimators(inp, trace.posterior, ci=ci)

    resdict.update({"estimators": estimators})

    justmedian = "ncomps" in trace.posterior.keys()
    kind = ["median", "hpdi"]
    altkind = "mean"  # will be between ()

    # =========================
    # Sectors and parameters
    results += logappend(
        """
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 ▒▒▒▒ PARAMETERS ▒▒▒▒▒
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

"""
    )

    # =========================
    if "ncomps" in trace.posterior.keys():
        results += logappend(
            """
- n components:
"""
        )
        results += logappend(
            estformat(
                inp,
                estimators,
                [
                    [
                        "ncomps",
                    ],
                ],
                kind=kind,
                justmedian=justmedian,
            )
        )
        results += logappend("   weights      :  ")
        for i in range(n_comps):
            tab = trace.posterior["ncomps"].data.flatten()
            results += logappend(str(np.round(len(tab[tab == i + 1]) / len(tab), 2)))
            if i < n_comps - 1:
                results += logappend(" / ")
        results += logappend("")

    if use_scaling is not None and 1 == 0:
        results += logappend(
            """
- Scaling factor [log]:"""
        )
        results += logappend(
            estformat(
                inp,
                estimators,
                [
                    [
                        "scale_eff",
                    ],
                ],
                kind=kind,
                justmedian=justmedian,
            )
        )

    # =========================

    if not hierarchical:
        results += logappend(
            "\n- Component mixing weights (sum of *means* = 1)"
        )  # .format([quants['w_s{}'.format(s)].data.astype(dtype) for s in range(n_sectors)])
        results += logappend(
            estformat(
                inp,
                estimators,
                [
                    ["w_{}".format(s) for s in range(n_comps)],
                ],
                kind=kind,
                justmedian=justmedian,
            )
        )  # default = mean & hpdi

    results += logappend(
        "\n- Parameters"
    )  # .format([quants['w_s{}'.format(s)].data.astype(dtype) for s in range(n_sectors)])
    for i, ps in enumerate(grid.params.names_inf):
        tmp = true_params[i] if true_params is not None else None
        results += logappend(
            estformat(
                inp,
                estimators,
                [
                    [
                        ps,
                    ],
                ],
                kind=kind,
                true_params=tmp,
                justmedian=justmedian,
            )
        )
        # results += estformat(estimators, [p], kind=['robust_mean', 'robust_stdev'])
        # results += estformat(estimators, [p], kind=['mu', 'lambda', 'tau'])

    allparams_notinf = []

    for i, ps in enumerate(grid.params.names_plaw):
        allparams_notinf += [
            [
                f"alpha_{ps}",
                f"lowerbound_{ps}",
                f"upperbound_{ps}",
                f"avg_{ps}",
            ],
        ]

    for i, ps in enumerate(grid.params.names_smoothplaw):
        allparams_notinf += [
            [
                f"alpha_{ps}",
                # f"lowercut_{ps}",
                # f"uppercut_{ps}",
                # f"lowerbound_{ps}",
                # f"upperbound_{ps}",
                # f"lowerfactor_{ps}",
                # f"upperfactor_{ps}",
                f"lowerbound_{ps}",
                f"upperbound_{ps}",
                f"lowerscale_{ps}",
                f"upperscale_{ps}",
                f"avg_{ps}",
            ],
        ]

    for i, ps in enumerate(grid.params.names_brokenplaw):
        allparams_notinf += [
            [
                f"alpha1_{ps}",
                f"alpha2_{ps}",
                f"pivot_{ps}",
                f"lowerbound_{ps}",
                f"upperbound_{ps}",
                f"avg_{ps}",
            ],
        ]

    for i, ps in enumerate(grid.params.names_normal):
        allparams_notinf += [
            [
                f"mu_{ps}",
                f"sigma_{ps}",
                f"lowerbound_{ps}",
                f"upperbound_{ps}",
                f"avg_{ps}",
            ],
        ]

    for i, ps in enumerate(grid.params.names_doublenormal):
        allparams_notinf += [
            [
                f"mu1_{ps}",
                f"sigma1_{ps}",
                f"mu2_{ps}",
                f"sigma2_{ps}",
                f"ratio21_{ps}",
                f"lowerbound_{ps}",
                f"upperbound_{ps}",
                f"avg_{ps}",
            ],
        ]

    if allparams_notinf != []:
        results += logappend(
            estformat(
                inp, estimators, allparams_notinf, kind=kind, justmedian=justmedian
            )
        )

    results += logappend("")

    # =========================
    # e.g., pmextra will be something like pm.TruncatedNormal('mu_Z', mu=-0.3, sigma=0.3, lower=-1.3, upper=0),
    pmextras = [re.findall("[\"'](.*?)[\"']", inp["pmextra"])]
    if pmextras not in ([], [[]]):
        results += logappend(
            """
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 ▒▒ HYPERPARAMETERS ▒▒
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    """
        )

        results += logappend(
            estformat(inp, estimators, pmextras, kind=kind, justmedian=justmedian)
        )

        results += logappend("")

    # =========================
    if inp["active_rules"] != []:
        results += logappend(
            """
- Parameters for active context-specific rules:
"""
        )

        for p in inp["context_specific_params"]:
            if len(trace.posterior[p].data.shape) > 2:
                tmp = deepcopy(
                    trace.posterior["{}_0".format(inp["grid"].params.names[0])]
                )
                for s in range(trace.posterior[p].data.shape[2]):
                    trace.posterior["{}_{}".format(p, s)] = deepcopy(tmp)
                    trace.posterior["{}_{}".format(p, s)].data = trace.posterior[
                        p
                    ].data[:, :, s]
                    estimators = get_estimators(inp, trace.posterior, ci=ci)
                    resdict.update({"estimators": estimators})
                    results += logappend(
                        estformat(
                            inp,
                            estimators,
                            [
                                [
                                    "{}_{}".format(p, s),
                                ],
                            ],
                            kind=kind,
                            justmedian=justmedian,
                        )
                    )
            else:
                results += logappend(
                    estformat(
                        inp,
                        estimators,
                        [
                            [
                                p,
                            ],
                        ],
                        kind=kind,
                        justmedian=justmedian,
                    )
                )

    # =========================
    if inp["sysunc"] is not None:
        results += logappend(
            """
- Systematic offsets [log]:
"""
        )

        for s in inp["sysunc"]:
            results += logappend(
                estformat(
                    inp,
                    estimators,
                    [
                        [
                            s,
                        ],
                    ],
                    kind=kind,
                    justmedian=justmedian,
                )
            )

    # =========================
    # observables

    if (
        no_lims and observations.mask_obs_lims != []
    ):  # remove all upper limits from the plots
        for o in observations.mask_obs_lims:
            observations.drop(o)
        inp["observations"] = observations

    results += logappend(
        """
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 ▒▒▒▒ OBSERVABLES ▒▒▒▒
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒


p-value near 50% ideally (but may be due to abnormally large HDI)
        <2.5  : overfit at the 95% level
        >97.5 : underfit/poor fit at the 95% level
"""
    )
    # 0-diff: % chance of model=obs

    results += logappend(
        "Credible intervals ({0}%; parameter falls within {0}% probability within this interval):".format(
            int(100 * ci)
        )
    )

    fieldlength = int(np.max([len(o) for o in observations.names]))

    h = f"\n   {'Quantity': <{fieldlength}} : "
    if true_params is not None:
        h += f" (true value)"
    h += f" ({altkind: <10}) {kind[0]: <10} {'+': <10}  {'-': <10} "
    h += f" | {'Obs.': <10}  {'+': <10}  {'-': <10}  (\u0394 = obs-mod; n\u03C3)          | {'p-value [%]': <6} \n"
    header = h + "  " + "_" * len(h)

    results += logappend(header)

    pvalues = {}
    results += logappend("")

    # stats.ttest_ind(trace,posterior[o], ppc, equal_var = False)
    # regroup observations from different sets
    for i, o in enumerate(observations.obsnames_u):
        l = [o2 for o2 in observations.names if o in o2]
        for i2, o2 in enumerate(l):
            pvalue = pvalue_calc(trace, observations, estimators, o2)
            pvalues.update({o2: pvalue})
            idx = None if hierarchical else i2
            results += logappend(
                estformat(
                    inp,
                    estimators,
                    [
                        [
                            o2,
                        ],
                    ],
                    kind=kind,
                    observations=observations,
                    pvalue=pvalue,
                    justmedian=justmedian,
                    idx=idx,
                )
            )
    results += logappend("")
    resdict.update({"pvalues": pvalues})

    # #=========================
    #    #explicitly inferred
    #    if n_inf>0:
    #       results += '''
    # _______________________________________________________________________________________
    # _______________________________________________________________________________________
    # Explicitly inferred

    # '''

    #       results += '\n'
    #       results += header
    #       for i in range(n_inf):
    #          results += estformat(inp, estimators, [lines_to_infer[i]], kind=kind, true_params=inp['inferred_predictions'][i], justmedian=justmedian)

    # =========================
    if use_scaling_specific is not None:
        results += logappend(
            """
_______________________________________________________________________________________
Additionnal scaling factor between 0 and 1 (PDR covering factor à la Cormier)
_______________________________________________________________________________________
"""
        )

        results += logappend(
            estformat(
                inp,
                estimators,
                [
                    [
                        "scale_specific",
                    ],
                ],
                kind=kind,
                justmedian=justmedian,
            )
        )
        pickle.dump(
            np.array(trace.posterior["scale_specific"].data),
            open(output_directory + "fcov_pdr.pkl", "wb"),
            protocol=4,
        )

    # #=========================
    # #reference model point
    # if n_sectors==1:
    #    #nearest point in grid
    #    tmp = [[np.argmin(abs(quants[p+'_s0'].data-Params_values[p]))] for p in params_name]
    #    #scaling
    #    if use_scaling==[None]:
    #       tmp2 = 0
    #       tmpscaling = ''
    #    else:
    #       tmp2 = quants['scale'].data
    #       tmpscaling = f' (including scaling: {tmp2: <5.2f})'
    #    #logging.info(tmp)
    #    results += '''
    # _______________________________________________________________________________________
    # Reference model point (nearest):

    # - Names          : {}
    # - Parameters     : {}
    # - Observed lines : {} (grid values)
    # - Observed lines : {}{}
    # - Inferred lines : {} (grid values)
    # - Inferred lines : {}{}\n'''.format([grid.params_name[i] for i in range(n_params)],
    #                                     [np.float32(float(grid[i][tmp])) for i in range(n_params)],
    #                                     [np.float32(float(obs_grid[i][tmp])) for i in range(n_obs)],
    #                                     [np.float32(tmp2+float(obs_grid[i][tmp])) for i in range(n_obs)], tmpscaling,
    #                                     [np.float32(float(obs_grid[i+n_obs][tmp])) for i in range(n_inf)],
    #                                     [np.float32(tmp2+float(obs_grid[i+n_obs][tmp])) for i in range(n_inf)], tmpscaling
    # )

    results += logappend(
        """

 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 ▒▒▒▒ LIKELIHOODS ▒▒▒▒
 ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

""",
    )

    with open(output_directory + "results_search.pkl", "rb") as f:
        tmp = pickle.load(f)

    loo = az.loo(trace)
    waic = az.waic(trace)
    if trace.log_likelihood["log_likelihood"].ndim == 2:
        logging.error("/!\\ Not an element-wise logp")
        resdict.update({"loo_elpd": np.nan})
        resdict.update({"waic_elpd": np.nan})
    else:
        if rcParams["pmversion"] >= 4 or "loo" not in dir(loo):
            resdict.update({"loo_elpd": loo.elpd_loo})
            resdict.update({"waic_elpd": waic.elpd_waic})
        else:
            resdict.update({"loo_elpd": loo.loo})
            resdict.update({"waic_elpd": waic.waic})

    s = """ Likelihood diagnostics

  LOO    | WAIC    | Marg. likelihood P(data, Model)
 ____________________________________________________________________________________________________
"""
    s += f" {resdict['loo_elpd']: <7,.3f} | {resdict['waic_elpd']: <7,.3f} |"

    if "log_marginal_likelihood" in tmp.keys():  # only for SMC for now
        s += f" {tmp['log_marginal_likelihood']: <5,.3}"
        resdict.update({"log_marginal_likelihood": tmp["log_marginal_likelihood"]})

    results += logappend(s)
    results += logappend("")

    # =========================
    if "mask" in trace.posterior.keys():
        if np.min(trace.posterior["mask"]) == 0:
            logging.critical(
                "/!\\ The trace includes some masked data where grid is invalid [press c to continue]"
            )
            breakpoint()

    results += logappend("")
    results += logappend("")

    # ===================================================================
    # global CI

    # first make combined trace
    nolims = True  # temporary
    k = 0  # because of nolims
    for o in observations.names:
        if nolims and observations.obs[o].limit:
            continue
        if k == 0:
            tmp = (
                observations.obs[o].value - trace.posterior[o]
            )  # dividing by stdev doesn't change anything
        else:
            tmp = xarray.concat(
                (tmp, observations.obs[o].value - trace.posterior[o]), dim="draw"
            )  # dividing by stdev doesn't change anything
        k += 1
    # reindex
    tmp = tmp.assign_coords({"draw": np.arange(len(tmp[0]))})
    trace2 = deepcopy(trace)
    trace2.posterior["combined"] = tmp
    if not args.skip_plots:
        os.makedirs(inp["output_directory"] + "Plots", exist_ok=True)
    estimators_combined = get_estimators(
        inp, trace2.posterior, names=("combined",), ci=ci
    )
    resdict.update({"estimators_combined": estimators_combined})

    # CI
    ci_val, ci_str = calc_ci(inp, estimators_combined["combined"])
    resdict.update({"ci_val_combined": ci_val})

    # global p-value
    pvalue = pvalue_calc(
        trace, observations, estimators, "combined", inp=inp
    )  # inp for order of magnitude obs
    resdict.update({"pvalue_combined": pvalue})
    tmp = f"- Global p-value : {100*pvalue: <5,.3f}"
    results += logappend(tmp)

    tmp = f"- Global CI: {ci_str}"
    results += logappend(tmp)

    # pnsigma
    pnsigma = 3 * [
        np.nan,
    ]
    for j, nsigma in enumerate((1, 2, 3)):
        tmp2 = np.array(
            [
                (
                    trace.posterior[o].data.flatten()
                    > (
                        inp["observations"].values[i]
                        - nsigma * inp["observations"].delta[i][1]
                    )
                )
                * (
                    trace.posterior[o].data.flatten()
                    < (
                        inp["observations"].values[i]
                        + nsigma * inp["observations"].delta[i][0]
                    )
                )
                for i, o in enumerate(inp["observations"].names)
                if o in inp["observations"].mask_obs_detected
                and o in trace.posterior.keys()
            ]
        )
        tmp2 = tmp2.flatten()
        pnsigma[j] = np.sum(tmp2) / len(tmp2)
    results += logappend(
        f"- % of posterior draws that agree within 1,2,3-sigma: {100*pnsigma[0]: <5,.2f}, {100*pnsigma[1]: <5,.2f}, {100*pnsigma[2]: <5,.2f}"
    )
    resdict.update({"pnsigma": pnsigma})

    # ==================================================================

    with open(output_directory + "results_search.pkl", "rb") as f:
        resdict_search = pickle.load(f)

    paramnames_forplots = [
        getp(ps)
        for ps in grid.params.names_inf
        if np.sum(["idx_{}".format(ps) in v for v in resdict_search["var_names"]])
    ]
    # the test above is necessary in case one the parameters is deterministic (i.e., fixed value)
    paramnames_forplots = unique(paramnames_forplots, keepsort=True)

    for ps in grid.params.names_notinf:
        tmp = [
            f"idx_lowerbound_{ps}",
            f"idx_upperbound_{ps}",
            # f"lowercut_{ps}",
            # f"uppercut_{ps}",
            # f"lowerfactor_{ps}",
            # f"upperfactor_{ps}",
            f"lowerscale_{ps}",
            f"upperscale_{ps}",
            f"alpha_{ps}",
            f"alpha1_{ps}",
            f"alpha2_{ps}",
            f"pivot_{ps}",
            f"mu_{ps}",
            f"sigma_{ps}",
            f"mu1_{ps}",
            f"sigma1_{ps}",
            f"mu2_{ps}",
            f"sigma2_{ps}",
            f"ratio21_{ps}",
        ]
        # print([pp.replace("idx_", "") for pp in tmp])
        # print(resdict_search["var_names"] + resdict_search["unobserved_RVs"])
        paramnames_forplots += [
            "_".join(pp.replace("idx_", "").split("_")[:-1])
            for pp in tmp
            if pp in resdict_search["var_names"] + resdict_search["unobserved_RVs"]
        ]
    params_forplots = []
    for s in range(inp["n_comps"]):
        for t in paramnames_forplots:
            params_forplots += [
                f"{t}_{s}",
            ]
    params_forplots = unique(params_forplots, keepsort=True)

    resdict.update({"params_forplots": params_forplots})
    resdict.update({"paramnames_forplots": paramnames_forplots})

    # ===================================================================
    # PPC for observed quantities
    if args.ppc:
        printsection("Posterior predictive sampling")
        if FileExists(ppc_f):
            ppc = az.from_netcdf(ppc_f)
        else:
            # posterior predictive check, do this once trace contains the observables
            # PPC sampling speed seems to scale exponentially with number of parameters *in log space*... choosing a large number of samples here costs a lot...
            # this works because we just loaded the context with the right definition of the Deterministic variable

            logging.info(
                "Running mgris_search to get model context with new tracers..."
            )

            # first remove handlers
            log = logging.getLogger()  # root logger
            for hdlr in log.handlers[:]:  # remove all old handlers
                log.removeHandler(hdlr)

            model = mgris_search.main(
                args=mgris_search.args(
                    inputfile=input_f,
                    nsamples_per_job=100,
                    return_model_context=True,
                    method_step=inp["method_step"],
                    order=inp["interp_order"],
                    verbose=False,
                )
            )

            # remove handlers
            log = logging.getLogger()  # root logger
            for hdlr in log.handlers[:]:  # remove all old handlers
                log.removeHandler(hdlr)

            # re-use process log
            logger = setup_custom_logger(
                "process", directory=output_directory, filemode="a", loglevel=loglevel
            )

            logging.info("====> Back to mgris_process")

            ppc = pm.fast_sample_posterior_predictive(
                trace, samples=10000, model=model, var_names=observations.names
            )
            ppc = az.convert_to_inference_data(ppc)
            az.to_netcdf(ppc, ppc_f)

        pnsigma = ppc_calc(ppc.posterior, inp)
        resdict.update({"pnsigma_ppc": pnsigma})

        for i in range(len(pnsigma)):
            tmp = f"- % of PPC draws that agree within {i+1}-sigma: {100*pnsigma[i]: <5,.2f}\n"
        logging.info(tmp)
        results += tmp
        logging.info("\n")

        # posterior-predictive p-value?
        # first make combined trace
        ppps = {}
        tmp = "PPPs:\n"
        for o in observations.names:
            data = ppc.posterior[o].data.flatten()
            mean = data.mean()
            std = data.std()
            obs = np.log10(observations.obs[o].value) - order_of_magnitude_obs
            diff = ((data - mean) / std) ** 2 - ((obs - mean) / std) ** 2
            ppp = len(np.where(diff <= 0)[0]) / len(diff)
            ppps.update({o: ppp})
            tmp += f"- {o} [%] = {100*ppps[o]: <5,.3f}\n"
        resdict.update({"ppps": ppps})

        # global ppp
        T_obs = np.sum(
            [
                (
                    (
                        np.log10(observations.obs[o].value)
                        - order_of_magnitude_obs
                        - ppc.posterior[o].data.flatten().mean()
                    )
                    / ppc.posterior[o].data.flatten().std()
                )
                ** 2
                for o in observations.names
            ],
            axis=0,
        )
        T_ppc = np.sum(
            [
                (
                    (
                        ppc.posterior[o].data.flatten()
                        - ppc.posterior[o].data.flatten().mean()
                    )
                    / ppc.posterior[o].data.flatten().std()
                )
                ** 2
                for o in observations.names
            ],
            axis=0,
        )
        ppp = len(np.where((T_ppc - T_obs) <= 0)[0]) / len(T_ppc)
        tmp += f"- global [%] = {100*ppp: <5,.3f}\n"
        resdict.update({"ppp": ppp})
        logging.info(tmp)
        results += tmp
        logging.info("\n")

        # fig, ax = plt.subplots(1, 1, figsize=(5, 5))
        # ax.hist(T_ppc, color="red", alpha=0.2)
        # ax.axvline(T_obs, color="blue")
        # fig.savefig(inp["output_directory"] + "/Plots/ppc_ppp.pdf")

        # prior_predictive = pm.sample_prior_predictive(samples=50, random_seed=RANDOM_SEED)
        # trace.add_groups(posterior_predictive=ppc.posterior)
        # az.plot_bpv(trace, kind="p_value")

    # ===================================================================
    # write results

    with open(inp["output_directory"] + "results.txt", "w") as f:
        f.write(results)

    with open(inp["output_directory"] + "results_process.pkl", "wb") as f:
        pickle.dump(resdict, f, protocol=4)
        pickle.dump(estimators, f, protocol=4)
        if verbose:
            logging.info(
                "Results values saved: {}\n".format(
                    inp["output_directory"] + "results_process.pkl"
                )
            )

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # CHECKS

    # ------------------------------------------------
    # check for edge effects, i.e., when the most likely value is the min/max of the parameter
    # this causes an issues because the sampler cannot explore around
    for ps in grid.params.names_inf:
        ips = "idx_{}".format(ps)
        p = getp(ps)
        hpdi2 = np.round(estimators[ips]["hpdi"], 2)
        # logging.info(grid.params_name[i])
        # logging.info(hpdi2)
        eps = 0.1  # this is for the indices so 0.1 is already pretty low
        # if (hpdi2[0]<l+eps) or (hpdi2[1]>r-eps):
        if (hpdi2[0] < 0.5) or (hpdi2[1] > len(grid.params.values[p]) - 1 - 0.5):
            logging.warning(
                "...{}: /!\\ Possible edge effect, HPD[CI={}] of indices: {} (you may wanna extend the range for this parameter in the models)".format(
                    ips, ci, hpdi2
                )
            )

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # MAKE PLOTS
    if not args.skip_plots:
        printsection("Make plots")
        # using trace_process
        diagnostic_plots(trace.posterior, inp, ci=ci, verbose=verbose)

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # CLEANING UP

    printsection("Cleaning up")

    call_gc()

    # ===================================================================
    # ===================================================================
    # ===================================================================

    logging.info("")

    endtime = datetime.now()

    logging.info(
        """
   ==========================================
   - Date/time         : {}
   - Duration          : {} min.
   ==========================================
   """.format(
            endtime.strftime("%d/%m/%Y %H:%M:%S"),
            round((endtime - starttime).total_seconds() / 60.0, 1),
        )
    )

    closelogfiles()


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    global logger
    # ===================================================================
    # logging console + file
    logger = setup_custom_logger("process")

    closelogfiles()
    main()
