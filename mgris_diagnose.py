import os
import argparse
import glob
import pickle
import logging
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plt
import arviz as az

# --------------------------------------------------------------------
# local libraries
import importlib
import Library.lib_mc
import Library.lib_main
import Library.lib_plots
import Library.lib_misc

importlib.reload(Library.lib_mc)
importlib.reload(Library.lib_main)
importlib.reload(Library.lib_plots)
importlib.reload(Library.lib_misc)
from Library.lib_mc import corr_xy_analyze, corr_xy
from Library.lib_main import (
    readinputfile,
    setup_custom_logger,
    movelogfile,
    closelogfiles,
    logappend,
    call_gc,
    printsection,
)
from Library.lib_plots import diagnostic_plots, corr_xy_plot
from Library.lib_misc import FileExists


# --------------------------------------------------------------------
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        inputfile="",
        skip_plots=False,
        verbose=False,
        debug=False,
    ):
        args.inputfile = str(inputfile)
        args.skip_plots = bool(skip_plots)
        args.verbose = bool(verbose)
        args.debug = bool(debug)


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS analysis step")
        parser.add_argument("inputfile", type=str, help="name_of_input_file")
        parser.add_argument(
            "--skip-plots",
            "-sp",
            help="Skip the diagnostics plots",
            action="store_true",
        )  # - in args are always replaced with _
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        parser.add_argument(
            "--debug", "-d", help="Debugging information", action="store_true"
        )
        args = parser.parse_args()

    verbose = args.verbose

    global logger
    # start logging now
    # logging console + file
    logname = "diagnose"
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logger = setup_custom_logger(logname, loglevel=loglevel)

    starttime = datetime.now()

    logging.info(
        """
    ==========================================
    System summary
    - Date/time         : {}
    ==========================================
    """.format(
            starttime.strftime("%d/%m/%Y %H:%M:%S")
        )
    )

    # =========================

    input_f = args.inputfile
    if not FileExists(input_f):
        # try and find if there's only one input*.txt file
        f = glob.glob(args.inputfile + "/input*.txt")
        if len(f) == 1:
            input_f = f[0]
        elif len(f) > 1:
            raise OSError("More than one input file found: {}".format(f))
        else:
            raise OSError("No input file found, expected {}".format(input_f))

    input_params = readinputfile(input_f, root_directory, verbose=False)
    output_directory = input_params["output_directory"]
    # possibly update output_directory
    if not FileExists(output_directory, is_dir=True):
        output_directory = "/".join(os.path.abspath(input_f).split("/")[:-1]) + "/"

    # ===================================================================
    # move output log
    movelogfile("diagnose", output_directory + "/Logs/")

    # ===================================================================
    with open(output_directory + "results_process.pkl", "rb") as f:
        resdict = pickle.load(f)
    with open(output_directory + "input.pkl", "rb") as f:
        inp = pickle.load(f)

    observations = inp["observations"]
    obs = observations.names
    params = resdict["params_forplots"]
    results = ""

    inp["output_directory"] = output_directory

    # =========================
    # =========================
    printsection("Verifications")

    results += logappend(
        """
- ESS informs if the chains are large enough (effective number of uncorrelated draws)
  - bulk: sampling efficiency in the bulk of the distribution (related to efficiency of mean and median estimates)
  - tail: minimum for 5% and 95% quantiles.
- rhat informs if the chains mix well
- MCSE estimates the error introduced by sampling and thus the level of precision of our estimates
    
    """
    )

    # ESS
    # For SMC the value of ESS seems to be overly optimistic when the model runs OK, but is very useful when the model has problems. i.e. if ESS is OK it does not mean too much, but if the ESS is low, then most likely than not SMC had troubles and you can not trust its results.
    # ESSs < 100 is not so good but this may be liberal and > 200 would be better. On the other hand chasing ESSs > 10000 may be a waste of computational resources

    # =========================
    printsection("Checking parameters", level=2)

    keys = []
    trace = az.from_netcdf(output_directory + "/trace.netCDF")
    for p in inp["params_name"]:
        for c in range(inp["n_comps"]):
            if f"idx_{p}_{c}" in trace.posterior.keys():
                if c > 0:
                    if (
                        trace.posterior[f"idx_{p}_{c}"].median()
                        == trace.posterior[f"idx_{p}_{c-1}"].median()
                    ):
                        continue
                keys += [
                    f"{p}_{c}",
                ]
    tmp = list(inp["sysunc"].keys()) if inp["sysunc"] is not None else []
    keys += [
        "scale",
    ] + tmp
    trace = az.from_netcdf(output_directory + "/trace_process.netCDF")
    sut = az.summary(trace.posterior, var_names=keys)
    results += logappend(sut.to_string())
    results += logappend("")

    results += logappend("Potential warnings:")

    if sut["ess_bulk"].min() < 12:
        results += logappend(
            "- /!\\ Some ESS are too small (ess_bulk<12) (https://arxiv.org/abs/1903.08008)",
            warning=True,
        )

    if sut["ess_bulk"].min() > 100:
        results += logappend(
            "- /!\\ min ESS is large (ess_bulk>100) and may be a waste of computational resources",
            warning=True,
        )

    if sut["r_hat"].max() > 1.01:
        results += logappend(
            "- /!\\ Some parameters have rhat>1.01 (https://arxiv.org/abs/1903.08008)",
            warning=True,
        )

    for k in keys:
        span = trace.posterior[k].max(axis=1) - trace.posterior[k].min(axis=1)
        spandev = span / span.std()
        if spandev.min() < 1:
            results += logappend(
                f"- /!\\ Not all chains seem to span the same range for parameter {k}",
                warning=True,
            )

    # =========================
    printsection("Checking observables", level=2)

    results += logappend(
        "\nChecking whether some observables dominate likelihood calculation [relative uncertainties]"
    )
    results += logappend(az.summary(trace, var_names=observations.names).to_string())
    results += logappend("")

    results += logappend(
        "- Checking rhat/ess_tail values and deviations \u0394 (val-mean)/std:"
    )
    results += logappend(
        "  (if rhat relatively small and ESS relatively large, observable may dominate the likelihood calculation)"
    )
    results += logappend("  - name \u0394(rhat) \u0394(ess)")

    rhats = az.rhat(trace.posterior)
    rhats = [float(rhats[o]) for o in observations.names]
    deviations_rhat = np.array([(r - np.mean(rhats)) / np.std(rhats) for r in rhats])
    # median_deviations_rhat = np.median(deviations_rhat)

    esss = az.ess(trace.posterior, method="tail")
    esss = [float(esss[o]) for o in observations.names]
    deviations_ess = np.array([(e - np.mean(esss)) / np.std(esss) for e in esss])
    # median_deviations_ess = np.median(deviations_ess)

    inds = np.argsort(deviations_ess * deviations_rhat)
    for i in inds:
        s = f"  - {observations.names[i]: <20}: {deviations_rhat[i]: <5,.1f} {deviations_ess[i]: <5,.1f} "
        if deviations_rhat[i] < -2 and deviations_ess[i] > 2:
            s += f"    /!\\ Tracer {observations.names[i]} seems to dominate the likelihood calculation"
        if deviations_rhat[i] > 2 and deviations_ess[i] < -2:
            s += "    /!\\ Likelihood seems to be dominated by other tracers, use prediction with caution"
        results += logappend(s, warning="!" in s)

    results += logappend(
        "\nChecking whether some tracers have too low uncertainties compared to grid [absolute uncertainties]"
    )
    results += logappend(
        " Uncertainty on prediction may be smaller than observed uncertainty but not larger"
    )
    results += logappend(
        " Value quoted is the ratio of the HDI range over the observed uncertainty. <1 is just low detection level, significantly >1 means the detection might be overestimated"
    )
    facs = []
    for i, o in enumerate(observations.names):
        hdi = az.hdi(trace.posterior[o], hdi_prob=inp["ci"], skipna=True)
        fac_model = float(hdi[o][1].values) - float(hdi[o][0].values)
        fac_obs = inp["observations"].delta[i][1] + inp["observations"].delta[i][0]
        facs += [
            fac_model / fac_obs,
        ]
    inds = np.argsort(facs)[::-1]
    for i in inds:
        if ~np.isfinite(facs[i]):
            continue
        warning = "" if facs[i] < 2.5 else "/!\\ "
        results += logappend(
            f"  - {observations.names[i]}: {facs[i]} {warning}", warning=warning != ""
        )

    # ESS
    # For SMC the value of ESS seems to be overly optimistic when the model runs OK, but is very useful when the model has problems. i.e. if ESS is OK it does not mean too much, but if the ESS is low, then most likely than not SMC had troubles and you can not trust its results.
    # ESSs < 100 is not so good but this may be liberal and > 200 would be better. On the other hand chasing ESSs > 10000 may be a waste of computational resources

    # logging.info('')
    # logging.info('Gelman-Rubin convergence test for multiple chains')
    # logging.info(az.rhat(trace))

    # =========================
    printsection("Checking correlations", level=1)

    results += logappend(
        """
_______________________________________________________________________________________
_______________________________________________________________________________________
CORRELATIONS

"""
    )
    marginalize = True

    results += logappend("")

    # results += logappend('- Correlation of each parameter vs. likelihood')
    # #for a given param get tracers that correlate best
    # r2, rescorr = corr_xy(inp, params, ['log_likelihood'], verbose=verbose, marginalize=marginalize, label='likelihood', normx=True) #normx because likelihood increase and decrease across best param value
    # results += logappend(r2)
    # resdict.update({'rescorr_parameters_likelihood': rescorr})

    obs = inp["observations"].obsnames_u
    for o in obs:
        if o not in trace.posterior.keys():
            ind = [n for n in inp["observations"].names if o in n][0]
            trace.posterior[o] = trace.posterior[ind]

    results += logappend("- Correlation of each tracer [model] vs. parameters")
    # for a given param get tracers that correlate best
    r2, rescorr = corr_xy(
        inp,
        obs,
        params,
        verbose=verbose,
        marginalize=marginalize,
        label="tracers",
        trace=trace,
    )
    results += logappend(r2)
    resdict.update({"rescorr_tracers": rescorr})

    # results += logappend('- Correlation of each tracer [model] vs. likelihood')
    # #for a given param get tracers that correlate best
    # r2, rescorr = corr_xy(inp, obs, ['log_likelihood'], verbose=verbose, marginalize=marginalize, label='likelihood', trace=trace, normx=True)
    # results += logappend(r2)
    # resdict.update({'rescorr_tracers_likelihood': rescorr})

    results += logappend("- Correlation of each parameter vs. tracers [model]")
    # for a given tracer get params that correlate best
    r2, rescorr = corr_xy(
        inp,
        params,
        obs,
        verbose=verbose,
        marginalize=marginalize,
        label="parameters",
        trace=trace,
    )
    results += logappend(r2)
    resdict.update({"rescorr_parameters": rescorr})

    # =========================================================================
    obs2 = [f"likelihood_{o}" for o in inp["observations"].names]
    if obs2[0] in trace.posterior.keys():  # backward compatibility
        results += logappend("- Correlation of each tracer [obs] vs. parameters")
        # for a given param get tracers that correlate best
        r2, rescorr = corr_xy(
            inp,
            obs2,
            params,
            verbose=verbose,
            marginalize=marginalize,
            label="tracers",
            # normy=True,
        )
        results += logappend(r2)
        resdict.update({"rescorr_tracers_obs": rescorr})

        # results += logappend('- Correlation of each tracer [obs] vs. likelihood')
        # #for a given param get tracers that correlate best
        # r2, rescorr = corr_xy(inp, obs2, ['log_likelihood'], verbose=verbose, marginalize=marginalize, label='likelihood')
        # results += logappend(r2)
        # resdict.update({'rescorr_tracers_likelihood_obs': rescorr})

        results += logappend("- Correlation of each parameter vs. tracers [obs]")
        # for a given tracer get params that correlate best
        r2, rescorr = corr_xy(
            inp,
            params,
            obs2,
            verbose=verbose,
            marginalize=marginalize,
            label="parameters",
            # normx=True,
        )
        results += logappend(r2)
        resdict.update({"rescorr_parameters_obs": rescorr})

    # ====================================

    if not args.skip_plots:
        diagnostic_plots(
            trace,
            inp,
            ci=inp["ci"],
            verbose=verbose,
            kind=("corr"),
            label="",
            resdict=resdict,
        )

    # ====================================
    r2, resdict = corr_xy_analyze(
        resdict, obs, obs2, params, inp=inp, skip_plots=args.skip_plots
    )
    results += r2

    # if not args.skip_plots:
    #     #make avg correlation plot
    #     corrparamavg = resdict['rescorr_parameters']['corr_param_avg']
    #     fig, ax = plt.subplots(1, 1, sharey='col', figsize=(0.75*len(params), 5))
    #     tmp = corrparamavg[0]
    #     ind = np.argsort(tmp)
    #     ax.scatter(np.arange(len(params)), np.array(tmp)[ind], color='black')
    #     ax.axhline(np.mean(tmp), color='black')
    #     ax.axhspan(np.mean(tmp)-np.std(tmp), np.mean(tmp)+np.std(tmp), color='black', alpha=0.2)
    #     ax.set_xticks(np.arange(len(params)))
    #     ax.set_xticklabels(np.array(params)[ind], rotation='vertical', fontsize=12)
    #     ax.set_ylabel('Average corr. coeff.')
    #     plt.tight_layout()
    #     fig.savefig(output_directory + '/Plots/correlation_parameters_average.pdf')
    #     plt.close('all')

    # ===================================================================
    # pp
    printsection("Post-processing parameters/tracers")

    if FileExists(output_directory + "results_post-process.pkl"):
        with open(output_directory + "results_post-process.pkl", "rb") as f:
            resdict_pp = pickle.load(f)

        if "obs_to_predict" in resdict_pp.keys():  # backward compatibility
            # for a given tracer get params that correlate best
            results += logappend("")
            results += logappend("- Correlation of each parameter vs. tracers")
            params_4corr = resdict["params_forplots"] + resdict_pp["params_to_predict"]
            obs_4corr = [
                r for r in inp["observations"].names if r in trace.posterior.keys()
            ] + resdict_pp["obs_to_predict"]
            r2, rescorr = corr_xy(
                inp,
                params_4corr,
                obs_4corr,
                verbose=verbose,
                marginalize=marginalize,
                post_process=True,
            )
            results += r2
            resdict_pp.update({"rescorr_parameters": rescorr})

            if not args.skip_plots:
                # for a given tracer get params that correlate best
                if len(resdict_pp["params_to_predict"]) > 1:
                    corr_xy_plot(
                        inp,
                        params_4corr,
                        obs_4corr,
                        post_process=True,
                        verbose=verbose,
                        label="parameters",
                        resdict=resdict_pp,
                    )
                plt.close("all")

    # ===================================================================
    # write results

    with open(output_directory + "results_diagnose.txt", "w") as f:
        f.write(results)

    # ===================================================================
    # ===================================================================
    # ===================================================================

    printsection("Cleaning up")

    call_gc()

    # ===================================================================
    # ===================================================================
    # ===================================================================

    logging.info("")

    endtime = datetime.now()

    logging.info(
        """
    ==========================================
    - Date/time         : {}
    - Duration          : {} min.
    ==========================================
    """.format(
            endtime.strftime("%d/%m/%Y %H:%M:%S"),
            round((endtime - starttime).total_seconds() / 60.0, 1),
        )
    )

    closelogfiles()


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    global logger
    logger = setup_custom_logger("diagnose")
    closelogfiles()
    main()
