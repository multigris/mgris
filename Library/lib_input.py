# convenience program to make input files.
# see also make_input_wrapper.py for example
# context input files are not generated automatically.

from datetime import datetime

from rcparams import rcParams

if rcParams["pmversion"] >= 4:
    import pymc as pm
else:  # pymc <3.11.4
    import pymc3 as pm

import os
import sys
from Library.lib_misc import getgitv


# ===========================================================================================================
# ===========================================================================================================
# ===========================================================================================================
def process_obs_sets(params):
    obs_sets = ""
    if "observation_sets" in params.keys():
        for i in range(len(params["observation_sets"])):
            params["BEGIN observations_SET{}".format(i)] = params["observation_sets"][i]
            if i == 0:
                params["BEGIN observations_SET{}".format(i)].description = params[
                    "BEGIN observations"
                ].description
            obs_sets += get(params, "BEGIN observations_SET{}".format(i))
            obs_sets += "\n\n"
    return obs_sets


# ===========================================================================================================
def get(params, param):
    p = params[param]

    # prefer to skip for clarity
    if p.value is None:
        return ""

    res = ""
    mandatory = "Mandatory" if p.mandatory else "Optional"

    res += "# {} ({})\n".format(p.title, mandatory)

    desc = "\n".join(["# {}".format(d) for d in p.description.split("\n")])
    desc = "\n".join(["# {}".format(d) for d in p.description.split("\n")])
    res += desc + "\n"

    if p.default is not None:
        res += "# Default: {}\n".format(p.default)

    res += "#.........................\n"
    if p.value is not None:
        cs = "# " if p.commented else ""

        if p.name == "sysunc":
            for i in range(len(p.value)):
                res += "{}{} {}\n".format(
                    cs, p.name, p.value[i]
                )  # value here is '(mean) sdev [labels...]'

        elif (
            p.name in ("BEGIN configuration", "BEGIN observations")
            or "BEGIN observations" in p.name
        ):
            tmp = p.value.split("\n")
            tmp = [t for t in tmp if t != ""]  # avoid empty lines
            label, scale, disabled, predict = "", "", "", ""
            if p.extras != {}:
                if "scale" in p.extras.keys():
                    scale = " " + p.extras["scale"].strip()
                if "name" in p.extras.keys():
                    label = " " + p.extras["name"].strip()
                if "disabled" in p.extras.keys():
                    disabled = " disabled"  # space is important
                if "predict" in p.extras.keys():
                    predict = " predict"  # space is important
            res += "{}{}{}{}{}{}\n".format(cs, p.name, label, scale, disabled, predict)
            for i in range(0, len(tmp)):
                res += "{}{}".format(cs, tmp[i])
                if i < len(tmp) - 1:
                    res += "\n"
            if res[-1] != "\n":
                res += "\n"

            if p.extras != {}:
                for k, val in p.extras.items():
                    if k in ("delta_ref", "delta_add", "scale_factor"):
                        res += "{}{} {}\n".format(cs, k, val)

            if res[-1] != "\n":
                res += "\n"
            res += "{}END".format(cs)

        elif p.name in ("select",):
            for val in p.value:
                res += "{}{} {}\n".format(cs, p.name, val)

        else:
            p.value = str(p.value).replace(
                "\n", ""
            )  # this is for large arrays in rcParams
            res += "{}{} {}".format(cs, p.name, p.value)
    else:
        res += "#{} ".format(p.name)

    res += "\n#.........................\n"

    res = res.replace("\n\n", "\n")

    return res


# ===========================================================================================================
# ===========================================================================================================
# ===========================================================================================================
class InputParameter:
    """Input parameter"""

    def __init__(
        self,
        name,
        default=None,
        value=None,
        description="",
        title="",
        commented=False,
        mandatory=False,
        extras={},
    ):
        self.name = name
        self.default = default
        self.value = value
        self.description = description
        self.title = title
        self.commented = commented
        self.mandatory = mandatory
        self.extras = extras
        return

    def set_name(self, name):
        self.name = name

    def set_value(self, value):
        self.value = value

    def comment(self):
        self.commented = True

    def uncomment(self):
        self.commented = False

    def optional(self):
        self.mandatory = False

    def mandatory(self):
        self.mandatory = True


# ===========================================================================================================
# ===========================================================================================================
# ===========================================================================================================
def get_params():
    params_arr = [
        InputParameter(
            "context",
            title="CONTEXT PATH",
            description="""- Path (relative or absolute) to context directory (i.e., where the context input file is located).
- If relative path, the MULTIGRIS root directory will be used.""",
        ),
        # -------------------------------
        InputParameter(
            "output",
            mandatory=True,
            default="Results/",
            title="OUTPUT PATH",
            description="""- Path (relative or absolute) to the output directory where results and plots are saved.
- If relative path, the MULTIGRIS root directory will be used.""",
        ),
        # -------------------------------
        InputParameter(
            "true_parameters",
            commented=True,
            title="true_parameters",
            description="""Test parameters (for tests only; only for single component).
- This will set parameter set values to get the 'observed' values from. 
- Observed values and potential lower/upper limits will be ignored if true_parameters is on.
- Use values like in grid (can use np.log10 if more convenient).""",
        ),
        # -------------------------------
        InputParameter(
            "USE configuration",
            title="PRE-DEFINED CONFIGURATION",
            description="""- Name of pre-defined configurations in the 'configurations.dat' file located in the context directory.""",
        ),
        # -------------------------------
        InputParameter(
            "BEGIN configuration",
            title="SPECIFIC CONFIGURATION CONSTRAINTS",
            description="""- Any specific parameter constraints to complement/replace pre-defined configuration.
- Any constraint already in pre-defined configuration will be overwritten.
- Each parameter is described by default as a single distribution (i.e., not transformed into a power-law, normal... distribution)
  with nearest neighbor grid interpolation. 

- Syntax: 
   BEGIN configuration <name (optional)>
   distrib <param> <type: single/plaw/smoothplaw/brokenplaw/normal/doublenormal> <interpolation (only for single): nearest/linear>
   <constraint>
   <constraint>
   ...
   END""",
            extras={},
        ),
        # -------------------------------
        InputParameter(
            "select",
            title="SELECT PARAMETER RANGE/VALUES",
            description="""- Hard constraints to select a subset of the grid.
- Values correspond to the model table raw values (i.e., before any conversion if requested).

- Syntax: 
   select <parameter> [<lower_value>,<upper_value>]

- [,10] means value <=10, [2,] value >=2,
- [2,10] means value >=2 and <=10 (included), 
- [2,2] means =2,
- Possible to use 'min' and 'max', i.e., [min,min], [max,max],
- Possible to use more than two values, i.e., [2,8,10] will select only these values.""",
        ),
        # -------------------------------
        InputParameter(
            "active_rules",
            title="CONTEXT-SPECIFIC RULES",
            description="""- List of rule names, to be defined in 'Contexts/<context>/Library/lib.py'.""",
        ),
        InputParameter(
            "BEGIN observations",
            mandatory=True,
            title="OBSERVATION SET",
            description="""- List of observations, potentially split in several sets.

- Syntax: 
   BEGIN observations <linear/log> <disabled> <predict> <name>
   <label> <value> <error> 
   <label> <value> <error+> <error->
   END

- Observation sets:
  - Possible to provide several observation sets. Each set can be disabled individually (see below). 
- Header keywords:
  - Disable a set without commenting it: 'BEGIN observations disabled'.
  - If the predict keyword is present, all observables from the set will be appended to obs_to_predict for post-processing.
  - An optional name can be given to each set which is appended to the label later on for results (default: SET1, SET2...). 
- Values:
  - Can be nan (will be ignored; prediction with mgris_post_process).
- Errors:
  - Can be nan (will be ignored; prediction with mgris_post_process).
  - Can be asymmetric (+ then -).
  - Set by default to delta_ref if no value provided.
  - Each set may use a delta_ref value, which is used for all observables that do not have an error.
  - Each set may use a delta_add value, with total error=delta+delta_add in log, or max([delta,delta_add*value]) in linear scale. 
- Limits:
   - Either single value indicated with < and > (***no space***), see rcparams for the definition of limits.
   - Or two values (measurement and - large - error bars).
   - Can be a range, starting with lower limit first (e.g., >30 <50).
- Scaling
   - Observations are extensive by default. Define intensive observations in the context input file. 
   - Observations (extensive and intensive) can be converted to linear/log scale ('BEGIN observations tolinear/tolog').
   - Observations are in linear scale by default. Force log scale with 'BEGIN observations log'. 
   - Each set may have a scale_factor value to scale all extensive observables (+ in log scale or * in linear scale). 
     If tolinear/tolog is used in the header, the original scale is used to apply the scaling factor.""",
            extras={},
        ),
        # -------------------------------
        # InputParameter('list_of_obs_aposteriori', description='''Aposteriori lines.
        # This is to run a model w/o some observables as constraints, to then compare predictions/observations for inferring a "PDR covering factor" à la Cormier+ (2019)
        # Specify lines that are already provided in observables.'''),
        # -------------------------------
        InputParameter(
            "use_scaling",
            mandatory=True,
            default="'all'",
            title="REFERENCE OBSERVABLE(S) FOR THE SCALING PRIOR",
            description="""- Models are scaled to observations using a single scaling random variable.
- Specify here the observable(s) that will be used to estimate the prior:
  - all observables ('all'),
  - a subset (['<label1>', '<label2>']),
  - one observable (['<label>']),
  - or none ('none'): observations are directly compared to the modeled values,
- If several sets are used, provide full label (i.e., label{<set_name>}).
- If a specific combination (e.g, sum) of observables is to be used, create the corresponding column in the context
  pre-processing.py file beforehand and propagate the observed value & uncertainty in the observations set. """,
        ),
        # -------------------------------
        InputParameter(
            "sysunc",
            title="SYSTEMATIC UNCERTAINTIES/OFFSETS",
            description="""- A common scaling factor (log) can be applied to groups of observables.

- Syntax
   sysunc label <sdev> ['<label1>','<label2>',... or else <setname>]
   sysunc label <mean> <sdev> ['<label1>','<label2>',... or else <setname>]
   (e.g., sysunc elemental_abundance_o 0.2 ['O351.8004m', 'O388.3323m'])

- Mean (optional) and sigma should be ***in log***. If mean is not provided, will assume 0. 
- If several observation sets are used, provide the full label (i.e., label{<set_name>}).
- It is also possible to simply provide the set name instead. """,
        ),
        # -------------------------------
        # Specify lines that are already provided in observables.'''),
        InputParameter(
            "use_scaling_specific",
            title="use_scaling_specific",
            description="""Additional scaling factor (for tests only).
List of observables that will be scaled by an additional scaling factor, uniform 
between 0 and 1. This is used for example to mimic a scaling factor à la Cormier+ 
(2019). If several observation sets are provided, the full label should be given 
(i.e., label{observation block name})""",
        ),
        # -------------------------------
        #               InputParameter('obs_notmeasured_to_infer', description='''Observables with no measured value, to be inferred.
        # These will not be used for the inference as they don't have observed values.
        # /!\ These will be calculated for each draw and as such they take memory. It is preferable to use obs_to_predict as a post-processing step.'''),
        # -------------------------------
        InputParameter(
            "obs_to_predict",
            title="NEW OBSERVABLES TO PREDICT",
            description="""- Observables not used for inference can be predicted with mgris_post_process.
- The observables can be in the main grid or in the post-processing grid.""",
        ),
        # -------------------------------
        InputParameter(
            "secondary_parameters",
            title="NEW PARAMETERS TO PREDICT",
            description="""- Parameters (not used for inference) can be predicted with mgris_post_process.
- The parameters can be in the main grid or in the post-processing grid.  """,
        ),
        # # -------------------------------
        # Obsolete
        #         InputParameter(
        #             "BEGIN distributions",
        #             title="DISTRIBUTION FOR EACH PARAMETER",
        #             description="""- Each parameter is by default described as a Normal distribution.
        # - Nearest neighbor interpolation is used by default.
        # - Specify here other distribution/interpolation methods.
        # - Syntax:
        #    BEGIN distributions
        #    <parameter> <0/nearest | 1/linear | plaw | brokenplaw | normal | doublenormal>
        #    END
        # - 0: nearest neightbor interpolation, 1: linear interpolation
        # - plaw: power-law distribution, brokenplaw: broken power-law distribution with pivot
        # - normal: normal distribution, doublenormal: two normal distributions.
        # - Use additional configuration constraints to parameterize the distributions.""",
        #             extras={},
        #         ),
        # -------------------------------
        # better to use distrib in configuration block
        InputParameter(
            "linterp_params",
            title="PARAMETERS TO BE LINEARLY INTERPOLATED",
            description="""- If list is empty and -o 1 flag is used in the command line, linear interpolation is
  used for all parameters. """,
        ),
        # -------------------------------
        InputParameter(
            "plaw_params",
            title="PARAMETERS THAT WILL USE A POWER-LAW DISTRIBUTION",
            description=""" """,
        ),
        # -------------------------------
        InputParameter(
            "normal_params",
            title="PARAMETERS THAT WILL USE A NORMAL DISTRIBUTION",
            description=""" """,
        ),
        # -------------------------------
        InputParameter(
            "discrete_params",
            title="PARAMETERS WITH DISCRETE SAMPLING (use with caution)",
            description="""- If list is empty and inference_upsample is >0 in rcParams, 
  then discrete sampling will be used for all parameters. """,
        ),
        # -------------------------------
        InputParameter(
            "pmextra",
            commented=True,
            title="PMEXTRA",
            description="""New random variables that may used with literal priors (e.g., for hierarchical methods)""",
        ),
        # -------------------------------
        InputParameter(
            "hierarchical",
            commented=True,
            default="'True'",
            title="HIERARCHICAL",
            description="""Each observation set corresponds to a specific set of parameters.""",
        ),
        # -------------------------------
    ]

    for p in rcParams.keys():
        params_arr += [
            InputParameter(
                p,
                value=rcParams[p],
                title="rcParams global parameter",
                description=f"""Global parameter {p}""",
                commented=True,
            ),
        ]

    params = {}
    for p in params_arr:
        params.update({p.name: p})

    return params


# ===========================================================================================================
# ===========================================================================================================
# ===========================================================================================================
def make_input(params, filename):
    # parsing git repository commit
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/../"
    gitv, gitv_remote, gitv_date, gitv_submodules = getgitv(root_directory)

    header = """#####################################################
#  ▗▄▄▄▖▗▖  ▗▖▗▄▄▖ ▗▖ ▗▖▗▄▄▄▖    ▗▄▄▄▖▗▄▄▄▖▗▖   ▗▄▄▄▖
#    █  ▐▛▚▖▐▌▐▌ ▐▌▐▌ ▐▌  █      ▐▌     █  ▐▌   ▐▌   
#    █  ▐▌ ▝▜▌▐▛▀▘ ▐▌ ▐▌  █      ▐▛▀▀▘  █  ▐▌   ▐▛▀▀▘
#  ▗▄█▄▖▐▌  ▐▌▐▌   ▝▚▄▞▘  █      ▐▌   ▗▄█▄▖▐▙▄▄▖▐▙▄▄▖    
#####################################################

# Generated automatically
# Date: {}
    """.format(
        datetime.now().strftime("%a %d %B %Y - %H:%M:%S")
    )

    sysv = sys.version.replace("\n", "")

    header += f"""

#------------------------------------------------------------------
#  ▗▖  ▗▖▗▄▄▄▖▗▄▄▖  ▗▄▄▖▗▄▄▄▖ ▗▄▖ ▗▖  ▗▖ ▗▄▄▖
#  ▐▌  ▐▌▐▌   ▐▌ ▐▌▐▌     █  ▐▌ ▐▌▐▛▚▖▐▌▐▌   
#  ▐▌  ▐▌▐▛▀▀▘▐▛▀▚▖ ▝▀▚▖  █  ▐▌ ▐▌▐▌ ▝▜▌ ▝▀▚▖
#   ▝▚▞▘ ▐▙▄▄▖▐▌ ▐▌▗▄▄▞▘▗▄█▄▖▝▚▄▞▘▐▌  ▐▌▗▄▄▞▘
#------------------------------------------------------------------

# Python
#  - version      : {sysv}
#  - bin          : {sys.executable}
#  - prefix       : {sys.exec_prefix}

# PyMC
#  - version      : {pm.__version__}

# mgris
#  - git commit   : {gitv}
#  - (date)       : {gitv_date}
#  - (submodules) : {gitv_submodules}

#------------------------------------------------------------------




"""

    # ===========================================================================================================

    rcparams = "\n\n".join([get(params, p) for p in rcParams.keys()])
    # "\n".join([get(params, p) for p in rcParams.keys() if not params[p].commented]),

    # ===========================================================================================================

    # transform obs blocks if necessary
    obs_sets = process_obs_sets(params)

    body = """
#---------------------------------------------------------------------
#   ▗▄▖ ▗▖ ▗▖▗▄▄▄▖▗▄▄▖ ▗▖ ▗▖▗▄▄▄▖
#  ▐▌ ▐▌▐▌ ▐▌  █  ▐▌ ▐▌▐▌ ▐▌  █  
#  ▐▌ ▐▌▐▌ ▐▌  █  ▐▛▀▘ ▐▌ ▐▌  █  
#  ▝▚▄▞▘▝▚▄▞▘  █  ▐▌   ▝▚▄▞▘  █  
#---------------------------------------------------------------------

{}




#---------------------------------------------------------------------
#   ▗▄▄▖ ▗▄▖ ▗▖  ▗▖▗▄▄▄▖▗▄▄▄▖▗▖  ▗▖▗▄▄▄▖
#  ▐▌   ▐▌ ▐▌▐▛▚▖▐▌  █  ▐▌    ▝▚▞▘   █  
#  ▐▌   ▐▌ ▐▌▐▌ ▝▜▌  █  ▐▛▀▀▘  ▐▌    █  
#  ▝▚▄▄▖▝▚▄▞▘▐▌  ▐▌  █  ▐▙▄▄▖▗▞▘▝▚▖  █  
#   - Retrieves context-specific input file, including grid file path, 
#     parameters, pre-defined configurations etc...
#   - Default is always one component. If more than one component, 
#     use either a pre-defined configuration from the 
#     configurations.dat file or specify configuration manually below. 
#---------------------------------------------------------------------

{}

{}

{}

{}

{}

{}

{}

{}




#---------------------------------------------------------------------
#   ▗▄▖ ▗▄▄▖  ▗▄▄▖▗▄▄▄▖▗▄▄▖ ▗▖  ▗▖ ▗▄▖▗▄▄▄▖▗▄▄▄▖ ▗▄▖ ▗▖  ▗▖ ▗▄▄▖
#  ▐▌ ▐▌▐▌ ▐▌▐▌   ▐▌   ▐▌ ▐▌▐▌  ▐▌▐▌ ▐▌ █    █  ▐▌ ▐▌▐▛▚▖▐▌▐▌   
#  ▐▌ ▐▌▐▛▀▚▖ ▝▀▚▖▐▛▀▀▘▐▛▀▚▖▐▌  ▐▌▐▛▀▜▌ █    █  ▐▌ ▐▌▐▌ ▝▜▌ ▝▀▚▖
#  ▝▚▄▞▘▐▙▄▞▘▗▄▄▞▘▐▙▄▄▖▐▌ ▐▌ ▝▚▞▘ ▐▌ ▐▌ █  ▗▄█▄▖▝▚▄▞▘▐▌  ▐▌▗▄▄▞▘
#   - Values, uncertainties, and scaling.
#---------------------------------------------------------------------

{}

{}

{}




#---------------------------------------------------------------------
#  ▗▄▄▖ ▗▄▄▖ ▗▄▄▄▖▗▄▄▄ ▗▄▄▄▖ ▗▄▄▖▗▄▄▄▖▗▄▄▄▖ ▗▄▖ ▗▖  ▗▖ ▗▄▄▖
#  ▐▌ ▐▌▐▌ ▐▌▐▌   ▐▌  █  █  ▐▌     █    █  ▐▌ ▐▌▐▛▚▖▐▌▐▌   
#  ▐▛▀▘ ▐▛▀▚▖▐▛▀▀▘▐▌  █  █  ▐▌     █    █  ▐▌ ▐▌▐▌ ▝▜▌ ▝▀▚▖
#  ▐▌   ▐▌ ▐▌▐▙▄▄▖▐▙▄▄▀▗▄█▄▖▝▚▄▄▖  █  ▗▄█▄▖▝▚▄▞▘▐▌  ▐▌▗▄▄▞▘
#   - Unobserved observables
#   - Secondary parameters (not used for inference)
#---------------------------------------------------------------------

{}

{}




#---------------------------------------------------------------------
#   ▗▄▖ ▗▖  ▗▖▗▄▄▄▖▗▄▄▖ ▗▄▄▖ ▗▄▄▄▖▗▄▄▄ ▗▄▄▄▖ ▗▄▄▖
#  ▐▌ ▐▌▐▌  ▐▌▐▌   ▐▌ ▐▌▐▌ ▐▌  █  ▐▌  █▐▌   ▐▌   
#  ▐▌ ▐▌▐▌  ▐▌▐▛▀▀▘▐▛▀▚▖▐▛▀▚▖  █  ▐▌  █▐▛▀▀▘ ▝▀▚▖
#  ▝▚▄▞▘ ▝▚▞▘ ▐▙▄▄▖▐▌ ▐▌▐▌ ▐▌▗▄█▄▖▐▙▄▄▀▐▙▄▄▖▗▄▄▞▘
#   - Any rcParams parameter can be overridden locally.
#---------------------------------------------------------------------

{}




""".format(
        get(params, "output"),
        get(params, "context"),
        get(params, "USE configuration"),
        get(params, "BEGIN configuration"),
        get(params, "pmextra"),
        # get(params, 'linterp_params'), get(params, 'plaw_params'), get(params, 'normal_params'), get(params, 'discrete_params')
        get(params, "discrete_params"),
        get(params, "select"),
        get(params, "active_rules"),
        get(params, "hierarchical"),
        get(params, "BEGIN observations") if obs_sets == "" else obs_sets,
        get(params, "use_scaling"),
        get(params, "sysunc"),
        get(params, "obs_to_predict"),
        get(params, "secondary_parameters"),
        rcparams,
    )

    footer = """###################
#  ▗▄▄▄▖▗▖  ▗▖▗▄▄▄ 
#  ▐▌   ▐▛▚▖▐▌▐▌  █
#  ▐▛▀▀▘▐▌ ▝▜▌▐▌  █
#  ▐▙▄▄▖▐▌  ▐▌▐▙▄▄▀
###################
    """.format(
        datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    )

    with open(filename, "w") as f:
        f.writelines(header)
        f.writelines(body)
        f.writelines(footer)
