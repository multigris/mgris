import numpy as np
import gc

import pandas as pd

# from astropy.table import Table
import logging

# logger = logging.getLogger('root') #https://stackoverflow.com/questions/7621897/python-logging-module-globally
import arviz as az

from Library.lib_misc import *
from Library.lib_mc import *
from Library.lib_class import ModelGrid

from tqdm import tqdm
import shutil
import tempfile
from copy import deepcopy, copy
import tempfile
import difflib
import re

# matplotlib produces way too many warnings...
import warnings

warnings.filterwarnings("ignore")

from rcparams import rcParams

try:
    # we will keep numpy for types
    from jax.scipy.ndimage import map_coordinates
    from jax.numpy import meshgrid

    # making sure that device is visible
    jax.default_backend()
    jax.devices()
except:
    from scipy.ndimage import map_coordinates
    from numpy import meshgrid

# need to keep this for types
import numpy as np

try:
    import jax.numpy_test as jnp
except:
    import numpy as jnp

from numpy import nan  # useful to get eval(nan)=numpy.nan


# -------------------------------------------------------------------
class CustomFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    # format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"
    format = "%(message)s"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset,
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


# -------------------------------------------------------------------
def setup_custom_logger(name, directory=None, filemode="w", loglevel=logging.INFO):
    if (
        directory is None
    ):  # this is to avoid writing in ./ where we may not have permission
        directory = tempfile.mkdtemp()
    logging.basicConfig(
        level=loglevel,
        format="[⏲ %(asctime)s]\t%(levelname)8s\t%(module)15s: \t%(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        filename="{}/output_{}.txt".format(directory, name),
        filemode=filemode,
    )
    # Until here logs only to file
    # print('{}/output_{}.txt'.format(directory, name))

    # define a new Handler to log to console as well
    console = logging.StreamHandler()

    # optional, set the logging level
    console.setLevel(loglevel)

    # set a format which is the same for console use
    # formatter = logging.Formatter(fmt='%(message)s')
    # tell the handler to use this format
    # console.setFormatter(formatter)

    # tell the handler to use this format
    console.setFormatter(CustomFormatter())

    # logging.debug("debug message")
    # logging.info("info message")
    # logging.warning("warning message")
    # logging.error("error message")
    # logging.critical("critical message")

    # add the handler to the root logger
    logging.getLogger("").addHandler(console)
    # logger.addHandler(handler)

    logger = logging.getLogger(name)
    return logger


# -------------------------------------------------------------------
def closelogfiles():
    log = logging.getLogger()  # root logger
    for handler in log.handlers[:]:  # the [:] is important
        handler.close()
        log.removeHandler(handler)
    # logging.shutdown() #commented because in a notebook we may need to run logging again


# -------------------------------------------------------------------
def movelogfile(logname, directory):
    outfile = "output_{}.txt".format(logname)

    os.makedirs(directory, exist_ok=True)

    # first remove handlers
    log = logging.getLogger()  # root logger
    orig = log.handlers[0].baseFilename

    for hdlr in log.handlers[:]:  # remove all old handlers #the [:] is important
        log.removeHandler(hdlr)

    # then move log file (os.rename works only if moving in same filesystem)
    try:
        shutil.copy(orig, "{}/{}".format(directory, outfile))
    except:
        msg = "Error while copying {} to {}".format(
            orig, "{}/{}".format(directory, outfile)
        )
        logging.critical(msg)
        raise Exception(msg)

    os.remove(orig)

    # finally relaunch setup
    setup_custom_logger(
        logname, directory=directory, filemode="a"
    )  # logger.addHandler(logging.FileHandler('{}/{}'.format(output_directory, logname), mode='a'))


# -------------------------------------------------------------------
def logappend(s, debug=False, warning=False, error=False, critical=False):
    if critical:
        logging.critical(s)
    elif error:
        logging.error(s)
    elif warning:
        logging.warning(s)
    elif debug:
        logging.debug(s)
    else:
        logging.info(s)
    return s + "\n"


# -------------------------------------------------------------------
def readinputfile_standalone(input_f, root_directory, verbose=False):
    res = readinputfile(input_f, root_directory, verbose=verbose)
    closelogfiles()
    return res


# -------------------------------------------------------------------
def readinputfile(input_f, root_directory, verbose=False):
    # input_f = root_directory + '/' + input_f  #The input itself is not in the code folder, only in the Contexts input files, the input directory is an absolute path

    if not FileExists(input_f):
        raise OSError("File not found", input_f)

    res = {}

    # readparam function defaults to None except if default keyword is set
    with open(input_f, "r") as f:
        input_file_lines = f.readlines()

        # remove comments and empty lines, and carriage returns
        input_file_lines = list(
            np.array(
                [
                    l.replace("\n", "").strip()
                    for l in input_file_lines
                    if l[0] != "#" and l != "\n"
                ]
            )
        )

        # get context name first to get general input file (and configurations.dat) if needed
        context = readparam(input_file_lines, ["context"], verbose=verbose)
        if context[0] != "/":
            context = root_directory + context
        if context[-1] == "/":
            context = context[:-1]

        res.update()
        if context is None:
            logging.error(
                "/!\\ Context is not provided, assuming that the input file contains all the information..."
            )
            context = root_directory
        else:
            # if verbose:
            #     logging.warning(
            #         "/!\\ Context was provided, using context general input file: {}".format(
            #             context + "/context_input.txt"
            #         )
            #     )
            with open(context + "/context_input.txt", "r") as f:
                generalinput = f.readlines()
                input_file_lines = list(generalinput) + input_file_lines
        res.update({"input_file_lines": input_file_lines})
        res.update({"context": context})
        res.update(
            {"contextshort": context[context.index("Contexts/") :].replace("/", ".")}
        )

        # are we using any pre-defined configuration?
        # if so, merge block with input file
        use = [
            l for l in input_file_lines if "USE configuration" in l and l[0] != "#"
        ]  # remove commented lines
        if use != []:
            # logging.info(use[0])
            # nconf = use[0].split(' ')[2].strip()
            nconf = use[0].split(" ")[2].strip()  # pre-defined configuration number
            res.update({"configuration": nconf})
            if verbose:
                logging.info("-- Using pre-defined configuration {}".format(nconf))
            with open(
                context + "/configurations.dat", "r"
            ) as f2:  # read pre-defined configuration block
                f2lines = f2.readlines()
                f2lines = np.array(
                    [l.replace("\n", "") for l in f2lines if l[0] != "#"]
                )  # remove commented lines
                start = np.where(f2lines == "BEGIN configuration {}".format(nconf))[0]
                if len(start) == 0:
                    panic(
                        f"Configuration not found... {nconf}, check/update {context}/configurations.dat"
                    )
                start = start[
                    0
                ]  # include BEGIN configuration line for readconfiguration
                end = (
                    start + np.where(f2lines[start:] == "END")[0][0] + 1
                )  # include END configuration line for readconfiguration
                input_file_lines += list(f2lines[start:end])

        # read distributions
        # preferred method is through distrib keyword in conf block

        # backward compatibility:
        # parameters to interpolate linearly
        linterp_params = readparam(
            input_file_lines, ["linterp_params"], verbose=verbose, default=[[]]
        )
        # parameters to use as power-law distribution
        plaw_params = readparam(
            input_file_lines, ["plaw_params"], verbose=verbose, default=[[]]
        )
        smoothplaw_params = readparam(
            input_file_lines, ["smoothplaw_params"], verbose=verbose, default=[[]]
        )
        brokenplaw_params = readparam(
            input_file_lines, ["brokenplaw_params"], verbose=verbose, default=[[]]
        )
        # parameters to use as normal distribution
        normal_params = readparam(
            input_file_lines, ["normal_params"], verbose=verbose, default=[[]]
        )
        doublenormal_params = readparam(
            input_file_lines, ["doublenormal_params"], verbose=verbose, default=[[]]
        )
        if (
            linterp_params
            + normal_params
            + plaw_params
            + smoothplaw_params
            + brokenplaw_params
            + doublenormal_params
            != []
        ) or np.sum(["BEGIN distribution" in t for t in input_file_lines]) > 0:
            panic(
                "Distributions need to be gathered under the configuration block with as, e.g., distrib <parameter> plaw"
            )

        # parse distrib here!
        alldistribs = [t for t in input_file_lines if t[0:8] == "distrib "]
        (
            linterp_params,
            plaw_params,
            brokenplaw_params,
            smoothplaw_params,
            normal_params,
            doublenormal_params,
        ) = ([], [], [], [], [], [])
        for d in alldistribs:
            tmp = d.replace(" (", "_").replace(")", "")
            tmp = tmp.split(" ")
            distrib = tmp[2]
            param = tmp[1]
            if distrib == "plaw":
                plaw_params += [
                    param,
                ]
            elif distrib == "smoothplaw":
                smoothplaw_params += [
                    param,
                ]
            elif distrib == "brokenplaw":
                brokenplaw_params += [
                    param,
                ]
            elif distrib == "normal":
                normal_params += [
                    param,
                ]
            elif distrib == "doublenormal":
                doublenormal_params += [
                    param,
                ]
            elif distrib == "single":
                if len(tmp) <= 3:
                    logging.warning(
                        f"Assuming nearest neighbor interpolation for parameter: {param}"
                    )
                else:
                    extra = tmp[3]
                    if extra == "linear":
                        linterp_params += [
                            param,
                        ]

        for ps in linterp_params:
            if "_" in ps:
                logging.warning(
                    f"/!\\ {ps}: the same interpolation method will be forced for all components!"
                )
        # same interpolation methods for all components
        linterp_params = [getp(ps) for ps in linterp_params]

        tmp = list(
            set(
                [
                    getp(p)
                    for p in plaw_params
                    + smoothplaw_params
                    + brokenplaw_params
                    + normal_params
                    + doublenormal_params
                ]
            )
        )
        if len(list(set(tmp) & set(linterp_params))) > 0:
            panic(
                "Same parameter cannot be linearly interpolated *and* used within a distribution"
            )

        res.update({"linterp_params": linterp_params})
        res.update({"plaw_params": plaw_params})
        res.update({"smoothplaw_params": smoothplaw_params})
        res.update({"brokenplaw_params": brokenplaw_params})
        res.update({"normal_params": normal_params})
        res.update({"doublenormal_params": doublenormal_params})

        # directories
        directory_models = root_directory + readparam(
            input_file_lines, ["directory_models"], default=[""], verbose=verbose
        )
        output_directory = readparam(
            input_file_lines, ["output"], default=["Results/"], verbose=verbose
        )

        res.update({"directory_models": directory_models})
        res.update({"output_directory": output_directory})

        # grid file & format
        post_processing_file = readparam(
            input_file_lines, ["post_processing_file"], default=[None], verbose=verbose
        )
        # grid_file, post_processing_file, delimiter_f = readparam(input_file_lines, ['grid_file',
        #                                                                            'post_processing_file',
        #                                                                            'delimiter'], default=[None,
        #                                                                                                   None,
        #                                                                                                   '\t'], verbose=verbose)
        # res.update({'grid_file': grid_file}) #expected in Grids/
        res.update({"post_processing_file": post_processing_file})
        # res.update({'delimiter_f': delimiter_f})

        # params & observables
        params_name = readparam(
            input_file_lines, ["primary_parameters"], verbose=verbose
        )
        params_name_add = readparam(
            input_file_lines, ["primary_parameters_add"], default=[[]], verbose=verbose
        )
        res.update({"params_name": params_name + params_name_add})

        # hierarchical?
        hierarchical = readparam(
            input_file_lines, ["hierarchical"], default=[False], verbose=verbose
        )
        res.update({"hierarchical": hierarchical})

        obs_to_predict_disabled = (
            []
        )  # obs blocks to ignore (disabled) but include in obs_to_predict
        # start = np.where(np.array(input_file_lines)=='BEGIN observations')[0]
        start = [
            i for i, l in enumerate(input_file_lines) if l[0:18] == "BEGIN observations"
        ]
        if len(start) == 0:
            raise Exception("/!\\ Observation block not found")
        else:
            # read several blocks
            observation_sets = {}
            nblocks = len(start)
            for i, s in enumerate(start):
                delta_ref, delta_add = None, None
                obstolog = ("tolog" in input_file_lines[s]) or (
                    "aslog" in input_file_lines[s]
                )  # aslog obsolete
                obstolinear = "tolinear" in input_file_lines[s] or (
                    "aslinear" in input_file_lines[s]
                )  # aslinear obsolete

                obstitle = set(input_file_lines[s].split(" ")) - set(
                    [
                        "BEGIN",
                        "observations",
                        "linear",
                        "log",
                        "tolinear",
                        "tolog",
                        "disabled",
                        "predict",
                    ]
                )
                if len(obstitle) == 0:
                    obstitle = "SET" + str(i + 1)
                else:
                    obstitle = "".join(obstitle).replace("_", "")

                # obs_valuescale = 'log' if 'log' in input_file_lines[start] else 'linear' #default is linear
                obs_valuescale = (
                    # "linear" if "linear" in input_file_lines[s] else "log"
                    "log"
                    if "log" in input_file_lines[s]
                    else "linear"
                )  # default is log #tolinear means it will be linear eventually

                # default scale_factor
                scale_factor = 0.0 if obs_valuescale == "log" else 1.0

                observations_dict = {}
                end = s + np.where(np.array(input_file_lines)[s:] == "END")[0][0] + 1
                for j in range(s + 1, end - 1):
                    tmp = input_file_lines[j].strip().split(" ")
                    if tmp[0] == "scale_factor":
                        scale_factor = eval(tmp[1])
                    elif tmp[0] == "delta_ref":
                        delta_ref = eval(tmp[1])
                    elif tmp[0] == "delta_add":
                        delta_add = eval(tmp[1])
                    else:
                        observations_dict.update({tmp[0]: [tmp, delta_ref]})

                if "predict" in input_file_lines[s]:
                    obs_to_predict_disabled += [o for o in observations_dict.keys()]

                # if 'disabled' in input_file_lines[s]:
                #   continue

                observation_sets.update(
                    {
                        obstitle: {
                            "obstolog": obstolog,
                            "obstolinear": obstolinear,
                            "obs_valuescale": obs_valuescale,
                            "scale_factor": scale_factor,
                            "delta_ref": delta_ref,
                            "delta_add": delta_add,
                            "observations_dict": observations_dict,
                            "disabled": "disabled" in input_file_lines[s],
                            "predict": "predict" in input_file_lines[s],
                        }
                    }
                )
        res.update({"observation_sets": observation_sets})

        tolog, tolinear = readparam(
            input_file_lines, ["tolog", "tolinear"], default=[[], []], verbose=verbose
        )

        res.update({"tolog": tolog})
        res.update({"tolinear": tolinear})

        # aposteriori lines (optional; this is to run a model w/o constraints, then compare predictions/observations for inferring a "PDR covering factor" à la Cormier+ 2019)
        # aposteriori lines are part of observables at first, with observed values, and deltas, they will be redistributed afterward (i.e., removed from observed and added to inferred)
        obs_notmeasured_to_infer, obs_to_predict, params_to_predict = readparam(
            input_file_lines,
            ["obs_notmeasured_to_infer", "obs_to_predict", "secondary_parameters"],
            default=[None, [], []],
            verbose=verbose,
        )

        res.update({"obs_notmeasured_to_infer": obs_notmeasured_to_infer})
        obs_to_predict += [o for o in obs_to_predict_disabled]
        obs_to_predict = list(set(obs_to_predict))
        res.update({"obs_to_predict": obs_to_predict})
        tmp = readparam(
            input_file_lines, ["params_to_predict"], verbose=verbose, default=[[]]
        )
        if len(tmp) > 0:
            logging.error(
                "/!\\ 'params_to_predict' is obsolete, please use 'secondary_parameters' instead"
            )
            params_to_predict = tmp
        params_to_predict = list(set(params_to_predict))
        res.update({"params_to_predict": params_to_predict})

        # =====================================================================================================================
        # with default values

        # scaling factor, optional
        use_scaling = readparam(
            input_file_lines, ["use_scaling"], default=["all"], verbose=verbose
        )
        res.update({"use_scaling": str(use_scaling).replace("'", "")})

        # sectors, optional, can come from pre-defined configuration file
        # full configuration will be read later on just before model
        n_comps = readparam(input_file_lines, ["n_comps"], verbose=verbose, default=1)
        n_comps = int(n_comps)
        res.update({"n_comps": n_comps})

        n_comps_vary = readparam(
            input_file_lines, ["n_comps_vary"], verbose=verbose, default="False"
        )
        res.update({"n_comps_vary": bool(eval(n_comps_vary))})

        groups = readparam(input_file_lines, ["groups"], verbose=verbose, default=None)
        if groups is None:
            groups = [
                [i for i in range(n_comps)],
            ]
        if set([s for g in groups for s in g]) != set(np.arange(n_comps)):
            panic("The group configuration does not include all the components!")

        res.update({"groups": groups})

        sorting = readparam(
            input_file_lines, ["sorting"], verbose=verbose, default="none"
        )
        res.update({"sorting": sorting})

        # group_norm = readparam(input_file_lines, ['group_norm'], verbose=verbose, default=[None])
        # res.update({'group_norm': group_norm})

        # force some observables to be ignore in likelihood during inference
        obs_skiplikelihood = readparam(
            input_file_lines, ["obs_skiplikelihood"], verbose=verbose, default=[[]]
        )
        res.update({"obs_skiplikelihood": obs_skiplikelihood})

        # all obs are expected to be extensive, here we get the intensive ones
        intensive_obs = readparam(
            input_file_lines, ["intensive_obs"], verbose=verbose, default=[[]]
        )
        res.update({"intensive_obs": intensive_obs})

        intensive_secondary_parameters = readparam(
            input_file_lines,
            ["intensive_secondary_parameters"],
            verbose=verbose,
            default=[[]],
        )
        res.update({"intensive_secondary_parameters": intensive_secondary_parameters})

        intensive_secondary_parameters_noweight = readparam(
            input_file_lines,
            ["intensive_secondary_parameters_noweight"],
            verbose=verbose,
            default=[[]],
        )
        res.update(
            {
                "intensive_secondary_parameters_noweight": intensive_secondary_parameters_noweight
            }
        )

        extensive_secondary_parameters = readparam(
            input_file_lines,
            ["extensive_secondary_parameters"],
            verbose=verbose,
            default=[[]],
        )
        res.update({"extensive_secondary_parameters": extensive_secondary_parameters})
        if (
            len(intensive_secondary_parameters) == 0
            and len(extensive_secondary_parameters) == 0
            and res["post_processing_file"] is not None
        ):
            logging.error(
                "/!\\ scale_norm is obsolete, please use 'intensive_secondary_parameters' or 'extensive_secondary_parameters' instead"
            )
            scale_norm = readparam(
                input_file_lines, ["scale_norm"], verbose=verbose, default=[[]]
            )
            extensive_secondary_parameters = scale_norm
            res.update(
                {"extensive_secondary_parameters": extensive_secondary_parameters}
            )
            res.update({"intensive_secondary_parameters": []})
        elif (
            len(intensive_secondary_parameters) != 0
            and len(extensive_secondary_parameters) == 0
        ):
            res.update(
                {"intensive_secondary_parameters": intensive_secondary_parameters}
            )
            res.update(
                {
                    "extensive_secondary_parameters": [
                        p
                        for p in params_to_predict
                        if p not in intensive_secondary_parameters
                    ]
                }
            )
        elif (
            len(extensive_secondary_parameters) != 0
            and len(intensive_secondary_parameters) == 0
        ):
            res.update(
                {"extensive_secondary_parameters": extensive_secondary_parameters}
            )
            res.update(
                {
                    "intensive_secondary_parameters": [
                        p
                        for p in params_to_predict
                        if p not in extensive_secondary_parameters
                    ]
                }
            )
        elif res["post_processing_file"] is not None:
            panic(
                "Please set either 'extensive_secondary_parameters' or 'intensive_secondary_parameters', not both."
            )

        # context-specific rules, as a list
        rules = readparam(
            input_file_lines, ["active_rules"], verbose=verbose, default=[[]]
        )
        res.update({"active_rules": rules})

        # parameters to sample with discrete values
        discrete_params = readparam(
            input_file_lines, ["discrete_params"], verbose=verbose, default=[[]]
        )
        res.update({"discrete_params": discrete_params})

        # new model parameters to be used in literal priors
        pmextra = readparam(
            input_file_lines,
            ["pmextra"],
            verbose=verbose,
            default="",
        )
        res.update({"pmextra": pmextra})

        # =====================================================================================================================
        # no default values
        nodefault_params = [
            "use_scaling_specific",  # list of lines to be scaled by an additionnal scaling factor, uniform between 0 and 1 (useful to compute f_cov 'a la Cormier')
            "true_parameters",
            "select",
            "sysunc",
        ]

        for param in nodefault_params:
            tmp = readparam(input_file_lines, [param], verbose=verbose)
            res.update({param: tmp})

        # temporary
        tmp_range = readparam(input_file_lines, ["range"], verbose=verbose)
        if tmp_range is not None:
            logging.error(
                "/!\\ 'range' parameter name is obsolete, use 'select' instead"
            )
            res.update({"select": tmp_range})

        # =====================================================================================================================
        # possible rcparams overrides

        for param in rcParams.keys():
            tmp = readparam(
                input_file_lines, [param], verbose=verbose, default=[rcParams[param]]
            )
            if param == "precision" and rcParams["precision"] != tmp:
                panic(
                    "Precision cannot be overridden. Please change the rcparams.py file instead."
                )
            try:
                res.update({param: eval(tmp)})
            except:
                res.update({param: tmp})

        return res


# -------------------------------------------------------------------
def readparam(input_file_lines, input_param_names, verbose=False, default=None):
    """
    Read parameters from input file
    """
    file_lines = np.array([l.strip().replace("\n", "") for l in input_file_lines])

    res = []

    for input_param_name in input_param_names:
        tmpall = [
            " ".join(line.split(" ")[1:])
            for line in file_lines
            if line[0 : len(input_param_name)] == input_param_name
            and input_param_name + " " in line
        ]  # +'\t' in line and line[0]!='#' and line[0]!=' '] # dash is used for commented lines

        if (
            len(tmpall) > 1
            and "select" not in input_param_name
            and "range" not in input_param_name
            and "sysunc" not in input_param_name
        ):
            logging.critical(tmpall)
            # logging.warning('/!\\ Found more than one instance of parameter {}. Keep only one and remove/comment the other ones. '.format(input_param_name))
            raise Exception(
                "/!\\ Found more than one instance of parameter {} in input + context file. Keep only one and remove/comment the other ones. ".format(
                    input_param_name
                )
            )

        if tmpall == []:
            if verbose:
                logging.debug(
                    "/!\\ Parameter: {} = not found in input + context file...".format(
                        input_param_name
                    )
                )
            res += [
                None,
            ]
        else:
            tmp = tmpall[0]  # should be only one, except for sysunc

            # eval lists?
            if tmp[0] == "[":
                # if input_param_name in ('parameters', 'true_parameters', 'list_of_obs', 'list_of_obs_aposteriori', 'obs_notmeasured_to_infer', 'obs_to_predict', 'params_to_predict', 'use_scaling', 'covering_factor', 'tolog', 'aslinear', 'post_processing_file', 'use_scaling_specific', 'config', 'group_norm', 'active_rules'):
                # tmp = tmp.replace(']', ',]') #in case there's a single element
                tmp = eval(tmp)
            elif input_param_name in ("delta_ref",):
                tmp = float(tmp)

            # append / if needed
            if input_param_name == "output":
                if tmp[-1] != "/":
                    tmp += "/"

            # replace upper/lower bounds for ranges
            if "select" in input_param_name or "range" in input_param_name:
                param = [l.replace(", ", ",").split(" ")[0] for l in tmpall]
                param_select = [
                    eval(
                        l.replace(", ", ",")
                        .split(" ")[1]
                        .replace("[,", "[-np.inf,")
                        .replace(",]", ",np.inf]")
                        .replace("min", "-np.inf")
                        .replace("max", "np.inf")
                    )
                    for l in tmpall
                ]
                tmp = {}
                for i, p in enumerate(param):
                    tmp.update({p: param_select[i]})

            # systematic uncertainties as tmpall = [['label sdev list of observables'], ['label sdev list of observables']...]
            if "sysunc" in input_param_name:
                try:
                    labels = [l.split(" ")[0] for l in tmpall]
                    vals = [l.split(" ")[1] for l in tmpall]
                    sdevs = [l.split(" ")[2] for l in tmpall]
                    lists = [" ".join(l.split(" ")[3:]) for l in tmpall]
                    tmp = {}
                    for i, l in enumerate(labels):
                        tmp.update(
                            {l: [float(vals[i]), float(sdevs[i]), eval(lists[i])]}
                        )
                except:
                    labels = [l.split(" ")[0] for l in tmpall]
                    sdevs = [l.split(" ")[1] for l in tmpall]
                    lists = [" ".join(l.split(" ")[2:]) for l in tmpall]
                    tmp = {}
                    for i, l in enumerate(labels):
                        tmp.update({l: [0.0, float(sdevs[i]), eval(lists[i])]})

            if verbose:
                logging.info(f"- {input_param_name: <50} : {tmp}")
            res += [
                tmp,
            ]

    # sets default if None
    if default is not None:
        if type(default) is not list:
            default = [default]

        # print('====')
        # print(input_param_names, input_param_name, res)
        for i, r in enumerate(res):
            if res[i] is None and default[i] is not None:
                if verbose:
                    logging.info(
                        f"- {input_param_names[i]: <50} : {default[i]} (default)"
                    )
                res[i] = default[i]

    if len(res) == 1:
        res = res[0]

    return res


# -------------------------------------------------------------------
def upsample1d(arr, factor, inp):
    """
    Upsample linearly 1D array between existing points
    """
    return jnp.concatenate(
        [
            np.array(
                [
                    jnp.linspace(arr[i], arr[i + 1], factor + 1)[:-1]
                    for i, z in enumerate(arr)
                    if i < len(arr) - 1
                ]
            ).flatten(),
            jnp.array([arr[-1]]),
        ]
    ).astype(inp["precision"])


# -------------------------------------------------------------------
def make_obs_grid(coords, list_of_lines, grid, fill=True, interpolate=False):
    """
    Get the observable (i.e. parse grid). If case=='luminosity', unit is erg/s, if case=='intensity' unit is erg/s/cm2 (the observed distance is required).
    """
    values = grid.values

    n_lines = len(list_of_lines)

    # coords = jnp.array(coords)

    # initialize as nans, otherwise if 0 and log10 becomes -inf and not recognized by np.*nan* tests
    # obs = np.zeros(shape=(n_lines, len(coords[0])))
    # mask = np.ones(shape=(n_lines, len(coords[0])), dtype=bool) #True by default

    coords = pd.DataFrame(jnp.array(coords).T, columns=grid.params.names)
    # pd.merge(coords, values, on=inp['grid'].params.names, how='left')

    # having some issues merging with float64 precision
    values[grid.params.names] = values[grid.params.names].astype(np.float32)
    coords[grid.params.names] = coords[grid.params.names].astype(np.float32)

    # f = round(100*len(values)/len(coords), 1) #f should alays be <=100 because of safety check for duplicates in mgris_search
    # logging.info('Completion % of grid in context table: {}'.format(f))

    # pandas merge is nice and fast but requires a ****load of memory
    # eiter use files:
    # df.to_csv("final.csv", mode="a", index=False, header=False)
    # vals = pd.read_csv('Results/final_grid.csv', chunksize=1000, sep=inp['delimiter_f'])
    # or:

    # we may need to split in chunk if table is huge, change the number of chunks here
    n_chunks = 1
    chunks = np.array_split(coords, n_chunks)
    for i, chunk in tqdm(enumerate(chunks), total=len(chunks)):
        df = (lambda x: pd.merge(x, values, on=grid.params.names, how="left"))(
            chunk
        )  # having some issues merging with float64 precision
        if interpolate:
            df = df.interpolate(
                method="linear",
                limit_area="inside",
                limit=2,  # only interpolate up to 2 NaNs
            )  # try to interpolate nans...? --> do it in pre-processing first
        obs = df if i == 0 else obs.append(df)
    # order should be grid.params.names

    # by default non-matching rows in merged table are filled with nans
    # mask = obs[list_of_lines].values == False #will return False (non-masked data)

    obs = obs[list_of_lines].values.T
    return obs


# -------------------------------------------------------------------
def call_gc():
    logging.info("Garbage collection thresholds : {}".format(gc.get_threshold()))
    collected = gc.collect()
    logging.info("Garbage collector: collected {}".format("%d objects." % collected))


# -------------------------------------------------------------------
def meshgrid_simple(*xi, **kwargs):
    """
    Return coordinate matrices from coordinate vectors.
    Make N-D coordinate arrays for vectorized evaluations of
    N-D scalar/vector fields over N-D grids, given
    one-dimensional coordinate arrays x1, x2,..., xn.
    .. versionchanged:: 1.9
       1-D and 0-D cases are allowed.
    Parameters
    ----------
    x1, x2,..., xn : array_like
        1-D arrays representing the coordinates of a grid.
    indexing : {'xy', 'ij'}, optional
        Cartesian ('xy', default) or matrix ('ij') indexing of output.
        See Notes for more details.
        .. versionadded:: 1.7.0
    sparse : bool, optional
        If True a sparse grid is returned in order to conserve memory.
        Default is False.
        .. versionadded:: 1.7.0
    copy : bool, optional
        If False, a view into the original arrays are returned in order to
        conserve memory.  Default is True.  Please note that
        ``sparse=False, copy=False`` will likely return non-contiguous
        arrays.  Furthermore, more than one element of a broadcast array
        may refer to a single memory location.  If you need to write to the
        arrays, make copies first.
        .. versionadded:: 1.7.0
    Returns
    -------
    X1, X2,..., XN : ndarray
        For vectors `x1`, `x2`,..., 'xn' with lengths ``Ni=len(xi)`` ,
        return ``(N1, N2, N3,...Nn)`` shaped arrays if indexing='ij'
        or ``(N2, N1, N3,...Nn)`` shaped arrays if indexing='xy'
        with the elements of `xi` repeated to fill the matrix along
        the first dimension for `x1`, the second for `x2` and so on.
    Notes
    -----
    This function supports both indexing conventions through the indexing
    keyword argument.  Giving the string 'ij' returns a meshgrid with
    matrix indexing, while 'xy' returns a meshgrid with Cartesian indexing.
    In the 2-D case with inputs of length M and N, the outputs are of shape
    (N, M) for 'xy' indexing and (M, N) for 'ij' indexing.  In the 3-D case
    with inputs of length M, N and P, outputs are of shape (N, M, P) for
    'xy' indexing and (M, N, P) for 'ij' indexing.  The difference is
    illustrated by the following code snippet::
        xv, yv = np.meshgrid(x, y, sparse=False, indexing='ij')
        for i in range(nx):
            for j in range(ny):
                # treat xv[i,j], yv[i,j]
        xv, yv = np.meshgrid(x, y, sparse=False, indexing='xy')
        for i in range(nx):
            for j in range(ny):
                # treat xv[j,i], yv[j,i]
    In the 1-D and 0-D case, the indexing and sparse keywords have no effect.
    See Also
    --------
    index_tricks.mgrid : Construct a multi-dimensional "meshgrid"
                     using indexing notation.
    index_tricks.ogrid : Construct an open multi-dimensional "meshgrid"
                     using indexing notation.
    Examples
    --------
    >>> nx, ny = (3, 2)
    >>> x = np.linspace(0, 1, nx)
    >>> y = np.linspace(0, 1, ny)
    >>> xv, yv = np.meshgrid(x, y)
    >>> xv
    array([[0. , 0.5, 1. ],
           [0. , 0.5, 1. ]])
    >>> yv
    array([[0.,  0.,  0.],
           [1.,  1.,  1.]])
    >>> xv, yv = np.meshgrid(x, y, sparse=True)  # make sparse output arrays
    >>> xv
    array([[0. ,  0.5,  1. ]])
    >>> yv
    array([[0.],
           [1.]])
    `meshgrid` is very useful to evaluate functions on a grid.
    >>> import matplotlib.pyplot as plt
    >>> x = np.arange(-5, 5, 0.1)
    >>> y = np.arange(-5, 5, 0.1)
    >>> xx, yy = np.meshgrid(x, y, sparse=True)
    >>> z = np.sin(xx**2 + yy**2) / (xx**2 + yy**2)
    >>> h = plt.contourf(x,y,z)
    >>> plt.show()
    """
    ndim = len(xi)

    copy_ = kwargs.pop("copy", True)
    sparse = True  # kwargs.pop('sparse', False)
    indexing = kwargs.pop("indexing", "xy")

    if kwargs:
        raise TypeError(
            "meshgrid() got an unexpected keyword argument '%s'" % (list(kwargs)[0],)
        )

    if indexing not in ["xy", "ij"]:
        raise ValueError("Valid values for `indexing` are 'xy' and 'ij'.")

    s0 = (1,) * ndim
    output = [
        np.asanyarray(x.astype(np.float16), dtype=np.float16).reshape(
            s0[:i] + (-1,) + s0[i + 1 :]
        )
        for i, x in enumerate(xi)
    ]

    if indexing == "xy" and ndim > 1:
        # switch first and second axis
        output[0].shape = (1, -1) + s0[2:]
        output[1].shape = (-1, 1) + s0[2:]

    if not sparse:
        # Return the full N-D matrix (not only the 1-D vector)
        output = np.broadcast_arrays(*output, subok=True)

    if copy_:
        output = [x.copy() for x in output]

    return output


# -------------------------------------------------------------------
def make_obs_grid_wrapper(
    inp,
    grid,
    labels,
    refine_force_memory,
    refine,
    verbose=False,
    obs_to_predict=[],
    params_to_predict=[],
):
    printsection("Building parameter grid")

    param_grid = jnp.array(
        meshgrid(*[grid.params.values[p] for p in grid.params.names], indexing="ij"),
        dtype=inp["precision"],
    )  # force matrix indexing i,j (default is x,y)
    logging.debug("Data type   : {}".format(param_grid.dtype))
    logging.debug(
        "Shape       : n_params x {} is a repeated {}D-array. Here: {}".format(
            tuple([len(grid.params.values[p]) for p in grid.params.names]),
            grid.params.n,
            np.array(param_grid).shape,
        )
    )
    logging.debug("Size        : {} elements".format(param_grid.size))

    tmp = 1e-6 * param_grid.size * param_grid.itemsize
    logging.debug("Memory size : {} Mb".format(round(tmp, 1)))

    if tmp > 1e3 * inp["maxgridmemsize"] and not refine_force_memory:
        raise MemoryError(
            "/!\\ Memory for 32-bit array is more than {}Gb... Force the code to go ahead anyway using -f flag".format(
                inp["maxgridmemsize"]
            )
        )  # 32-bit because not using griddata when not refining

    # =============================
    printsection("Building observable grid")

    # tmpfile = output_directory + 'final_grid.pkl'
    # if FileExists(tmpfile):
    #     logging.info('Loading complete grid, remove if needed: {}'.format(tmp))
    #     with open(tmpfile, 'rb') as f:
    #         obs_grid = pickle.load(f)
    # else:
    logging.info("Creating complete grid...")
    tmp = tuple([g.flatten() for g in param_grid])  # explain why we do this

    completion_fraction = round(
        100 * len(grid.values) / len(pd.DataFrame(jnp.array(tmp).T)), 1
    )  # f should alays be <=100 because of safety check for duplicates in mgris_search
    logging.info(
        "Completion % of grid in context table: {} ({} / {})".format(
            completion_fraction, len(grid.values), len(pd.DataFrame(jnp.array(tmp).T))
        )
    )

    obs_grid = np.array(
        make_obs_grid(tmp, labels, grid),
        dtype=inp["precision"],
    )  # remember that obs_finegrid also has to be filled

    # reshape
    logging.info("Reshaping...")
    obs_grid = obs_grid.reshape(
        (len(labels),) + tuple([len(grid.params.values[p]) for p in grid.params.names])
    ).astype(inp["precision"])

    # re-mask and fill masked values
    # obs_grid = np.ma.masked_invalid(obs_grid)
    # with open(tmpfile, 'wb') as f:
    #     pickle.dump(obs_grid, f, protocol=4)
    # logging.info('Complete grid saved in {}'.format(tmp))

    logging.debug("Data type   : {}".format(obs_grid.dtype))
    logging.debug("Shape       : {} ((n_obs + n_inf) x grid)".format(obs_grid.shape))
    logging.debug("Size        : {} elements".format(obs_grid.size))

    tmp = 1e-6 * obs_grid.size * obs_grid.itemsize
    logging.debug("Memory size : {} Mb".format(round(tmp, 1)))

    if tmp > 1e3 * inp["maxgridmemsize"] and not refine_force_memory:
        raise MemoryError(
            "/!\\ Memory for 32-bit array is more than {}Gb... Force the code to go ahead anyway using -rfm or change rcParams flag".format(
                inp["maxgridmemsize"]
            )
        )  # 32-bit because not using griddata when not refining

    # ----------------------------------------------------
    # keep this, this is if we wanna interpolate the nans in the grid.
    # normally this is masked and PyMC3 takes care of it
    # but maybe there are some issues with automatic imputation?
    # this is slow, so we should probably save the result and not
    # redo it each time
    # printsection('Replacing invalid values')
    # grid_arr = tuple([grid[j].flatten() for j in range(n_params)])
    # tmp = []
    # for i in range(n_obs+n_inf):
    #   x = obs_grid[i].flatten()
    #   tmp += [griddata(tuple([g[~x.mask] for g in grid_arr]), x[~x.mask], tuple([g[x.mask] for g in grid_arr]).astype(dtype), method='linear'), ]
    # tmp = np.array(tmp, dtype=dtype) #upsampled grid #nearest, linear, cubic
    # tmp = np.array(tmp).reshape((n_obs+n_inf,)+tuple([len(Params_valuesfine[p]) for p in params_name]))
    # tmp = np.ma.masked_invalid(tmp)
    # tmp[tmp.mask] = order_of_magnitude #like in get_masked_observable, a fill value is needed
    # ----------------------------------------------------

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # MODEL PREPARATION

    if refine:
        printsection("Building fine grid")

        # memory issues? https://stackoverflow.com/questions/57507832/unable-to-allocate-array-with-shape-and-data-type
        # sparse = True makes a crash
        # using meshgrid_simple which is the same as meshgrid with 32-bit precision all the way doesn't change anything
        # eventually we need to do something else than meshgrid, takes too much memory

        gridfine = deepcopy(grid)
        logging.info("...refining individual parameter arrays")
        for p in grid.params.names:
            gridfine.params.values[p] = interp1d(
                np.arange(len(grid.params.values[p])),
                grid.params.values[p],
                fill_value="extrapolate",
            )(grid.params.upsample(refine, inp, p=p))

        # ---------------------------------------------------
        # keep this for now
        # griddata is too slow. Our grid is structured so we can use RegularGridInterpolator
        # finegrid = np.array( meshgrid_simple( *[upsample1d(Params_values[p], upsample_factor, inp) for p in params_name], indexing='ij' ) ).astype(np.float16)
        # for p in params_name:
        #   Params_valuesfine.update({p: upsample1d(Params_values[p], upsample_factor, inp)})
        # grid_arr = tuple([np.ma.masked_array(grid[j], mask=obs_grid[j].mask).flatten() for j in range(n_params)])
        # obs_finegrid = np.array([griddata(grid_arr, obs_grid[i].flatten(), tuple(finegrid), method='linear').astype(dtype) for i in range(n_obs+n_inf)], dtype=dtype) #upsampled grid #nearest, linear, cubic
        # or
        # obs_finegrid = np.array([NearestNDInterpolator(grid_arr, obs_grid[i].flatten())(tuple(finegrid)).astype('float16') for i in range(n_obs+n_inf)]).astype(np.float16)
        # obs_finegrid = np.array([LinearNDInterpolator(grid_arr, obs_grid[i].flatten())(tuple(finegrid)).astype('float16') for i in range(n_obs+n_inf)]).astype(np.float16)
        # ---------------------------------------------------

        # ---------------------------------------------------
        # keep this for now
        # Interpolation on a regular grid in arbitrary dimensions
        # Although some models are thrwon away because of NaNs (checkallnans function), obs_grid is completed in get_masked_observable
        # finegrid = np.array( meshgrid_simple( *[upsample1d(Params_values[p], upsample_factor, inp) for p in params_name], indexing='ij' ) ).astype(np.float16)
        # for p in params_name:
        #   Params_valuesfine.update({p: upsample1d(Params_values[p], upsample_factor, inp)})
        # interpfunc = [RegularGridInterpolator(tuple([Params_values[p] for p in params_name]), obs_grid[i].astype('float16'), method='linear') for i in range(n_obs+n_inf)]
        # obs_finegrid = np.array([interpfunc[i](finegrid.T.reshape(-1, n_params)).astype('float16') for i in range(n_obs+n_inf)]).astype(np.float16)
        # ---------------------------------------------------

        # Interpolation on indices (several times faster than RGI)
        logging.info("...calculating upsampled parameter index meshgrid")
        # from dask.array import meshgrid as mg
        # param_grid_idx_upsample = mg( *grid.params.upsample(refine, inp), indexing='ij' )
        # param_grid_upsample = mg( *[interp1d(np.arange(len(grid.params.values[p])), grid.params.values[p], fill_value='extrapolate')(grid.params.upsample(refine, inp, p=p)) for p in grid.params.names], indexing='ij' )

        param_grid_idx_upsample = jnp.array(
            meshgrid(*grid.params.upsample(refine, inp), indexing="ij")
        ).astype(np.float16)
        # import dask.array as da
        # param_grid_idx_upsample = da.meshgrid( *grid.params.upsample(refine, inp), indexing='ij')

        logging.info("...calculating upsampled parameter meshgrid")
        param_grid_upsample = jnp.array(
            meshgrid(
                *[
                    interp1d(
                        np.arange(len(grid.params.values[p])),
                        grid.params.values[p],
                        fill_value="extrapolate",
                    )(grid.params.upsample(refine, inp, p=p))
                    for p in grid.params.names
                ],
                indexing="ij",
            )
        ).astype(np.float16)
        # param_grid_upsample = da.meshgrid( *[interp1d(np.arange(len(grid.params.values[p])), grid.params.values[p], fill_value='extrapolate')(grid.params.upsample(refine, inp, p=p)) for p in grid.params.names], indexing='ij')

        logging.info(
            "...reshaping  upsampled parameter meshgrid"
        )  # this is a large array... memmap? dask?
        tmp = jnp.array(
            [
                c
                for c in param_grid_idx_upsample.reshape(
                    grid.params.n,
                    jnp.prod(
                        [len(gridfine.params.values[p]) for p in grid.params.names]
                    ),
                )
            ],
            dtype=inp["precision"],
        )

        # interpolate
        logging.info("...interpolating new observable grid")

        if inp["memmap"]:
            fname = tempfile.NamedTemporaryFile()
            logging.debug("Temporary memmap file: {}".format(fname.name))
            f = np.memmap(
                fname.name,
                dtype=np.float32,
                mode="w+",
                shape=(len(labels), tmp.shape[1]),
            )
            for i in range(len(labels)):
                f[i] = map_coordinates(
                    obs_grid.astype(inp["precision"])[i], tmp, order=1
                )  # prefilter=False,
            # fname = tempfile.NamedTemporaryFile()

            logging.info("...reshaping new observable grid")
            obs_grid = f.reshape(
                (len(labels),)
                + tuple([len(gridfine.params.values[p]) for p in grid.params.names])
            ).astype(inp["precision"])
            # still <class 'numpy.memmap'>
        else:
            obs_grid = jnp.array(
                [
                    map_coordinates(obs_grid.astype(inp["precision"])[i], tmp, order=1)
                    for i in range(len(labels))
                ],
                dtype=inp["precision"],
            ).astype(
                inp["precision"]
            )  # prefilter=False,   #32bits is the minimum map_coordinates can do because of C code
            # reshape as (n_obs+n_inf, n_params, [len(p) for p in params])

            logging.info("...reshaping new observable grid")
            obs_grid = obs_grid.reshape(
                (len(labels),)
                + tuple([len(gridfine.params.values[p]) for p in grid.params.names])
            ).astype(inp["precision"])
            obs_grid = np.array(obs_grid).astype(inp["precision"])

        # re-mask ->> memmap will convert to an array...
        # obs_grid = np.ma.masked_invalid(obs_grid)
        # masked values will be eventually converted as float ==> 0.

        if verbose:
            logging.debug(type(obs_grid))

        logging.info("...done")
        # replace values
        param_grid = deepcopy(param_grid_upsample)
        # regenerate values (useful for chi2 method for instance)
        logging.info("...generating individual parameter values")
        gridfine.generate_values(
            param_grid, obs_grid, inp["observations"].names, inp=inp
        )
        grid = gridfine
        for p in grid.params.values:
            logging.info("{} : {}".format(p, grid.params.values[p]))

        logging.debug("New parameter grid:")
        logging.debug("Data type   : {}".format(param_grid.dtype))
        logging.debug("Shape       : {}".format(np.array(param_grid).shape))
        logging.debug("Size        : {} elements".format(param_grid.size))
        logging.debug(
            "Memory size : {} Mb".format(
                round(1e-6 * param_grid.size * param_grid.itemsize, 1)
            )
        )
        logging.debug("")
        logging.debug("New observable grid:")
        logging.debug("Data type   : {}".format(obs_grid.dtype))
        logging.debug("Shape       : {}".format(np.array(obs_grid).shape))
        logging.debug("Size        : {} elements".format(obs_grid.size))
        logging.debug(
            "Memory size : {} Mb".format(
                round(1e-6 * obs_grid.size * obs_grid.itemsize, 1)
            )
        )

        # make some room before model
        del (
            param_grid_upsample,
            param_grid_idx_upsample,
        )  # , Params_valuesfine, param_grid, tmp# obs_finegrid,

    else:  # not refined
        if inp["memmap"]:
            fname = tempfile.NamedTemporaryFile()
            logging.debug("Temporary memmap file: {}".format(fname.name))
            f = np.memmap(fname.name, dtype=np.float32, mode="w+", shape=obs_grid.shape)
            for i in range(len(obs_grid)):
                f[i] = obs_grid[i]
            del obs_grid
            obs_grid = f

    del param_grid

    # ===================================================================
    # ===================================================================
    # ===================================================================
    # grid and normalizing to avoid big numbers

    # first replace inf values
    # finite for calculations
    # the mask is there to find out whether we interpolate with inf
    # do inf/min before nan since we could replace nan

    # order_of_magnitude = float(int(np.nanmedian(obs_grid[np.isfinite(obs_grid)]))) #this is for later when we need to combine big numbers
    # obs_grid -= order_of_magnitude #normalize obs_grid to obs values; this is to avoid having precision issues with big numbers
    # we scale everything down to avoid large numbers
    # minvalue = np.min(obs_grid[np.isfinite(obs_grid)]) #minvalue = order_of_magnitude

    all_secondary_parameters = (
        inp["intensive_secondary_parameters"] + inp["extensive_secondary_parameters"]
    )

    if "order_of_magnitude" in inp.keys():
        order_of_magnitude = inp["order_of_magnitude"]
        order_of_magnitude_std = inp["order_of_magnitude_std"]
    else:
        # using only extensive obs
        order_of_magnitude = float(
            int(
                np.nanmedian(
                    [
                        o
                        for i, o in enumerate(
                            obs_grid[
                                0 : len(inp["observations"].obsnames_u)
                            ]  # -1 not needed
                        )
                        if inp["observations"].obsnames_u[i]
                        not in inp["intensive_obs"]
                        + all_secondary_parameters  # in case a parameter is used for priors
                    ]
                )
            )
        )
        order_of_magnitude_std = float(
            MADAM(
                [
                    o
                    for i, o in enumerate(
                        obs_grid[
                            0 : len(inp["observations"].obsnames_u)
                        ]  # -1 not needed
                    )
                    if inp["observations"].obsnames_u[i]
                    not in inp["intensive_obs"]
                    + all_secondary_parameters  # in case a parameter is used for priors
                ]
            )
        )

    all_labels = inp["observations"].obsnames_u + obs_to_predict + params_to_predict

    for i in range(len(obs_grid)):
        if all_labels[i] not in inp["intensive_obs"] + all_secondary_parameters:
            obs_grid[
                i
            ] -= order_of_magnitude  # normalize obs_grid to obs values; this is to avoid having precision issues with big numbers

    # min value for all extensive observables
    tmp = jnp.array(
        [
            jnp.array(o).flatten()
            for i, o in enumerate(obs_grid)
            if all_labels[i] not in inp["intensive_obs"] + all_secondary_parameters
        ]
    ).flatten()
    minvalue = jnp.min(tmp[jnp.isfinite(tmp)])

    # with nans to keep track of unwanted regions (esp. for linear interp)
    # mask will be either 0 (for simplicity for mask interpolation) or NaN
    mask_grid = deepcopy(obs_grid).astype("int16") * 0 + 1  # interp will become float

    # mask for model ULs for extensive obs (identified with inf values)
    mask_ul_grid = (
        deepcopy(obs_grid).astype("int16") * 0 + 1
    )  # interp will become float

    # mask = 1: finite
    # mask = 0: infinite (invalid data that will be skipped for inference)

    # rows in model grid with all NaNs or model ULs have been removed in the pre-processing
    # but make_obs_grid completes the grid and fill them with NaNs

    # mask parameter sets for which there are *only* NaNs
    mask_allnans = jnp.all(
        jnp.isnan(obs_grid[0 : len(inp["observations"].obsnames_u) - 1]), axis=0
    )

    frac = 100 * jnp.sum(mask_allnans) / jnp.sum(mask_allnans >= 0)
    if frac > 0:
        logging.warning(f" % parameter sets with only invalid data: {frac: .1f}")
        for i in range(len(obs_grid)):
            mask_grid[i][mask_allnans] = 0
            obs_grid[i][
                mask_allnans
            ] = 0  # /!\\ 2x check value here; 0 now, before 100; in principle value doesn't matter much since it is flagged for inference

    frac = 100 * jnp.sum(jnp.isnan(obs_grid)) / len(obs_grid.flatten())
    if frac > 0:
        logging.warning(f" % remaining invalid data: {frac: .1f}")
        for i in range(len(obs_grid)):
            frac2 = 100 * jnp.sum(jnp.isnan(obs_grid[i])) / len(obs_grid[i].flatten())
            if frac2 > 0:
                logging.warning(f"   - % for {all_labels[i]}: {frac: .1f}")

    # mask parameter sets for which there are *only* model ULs
    # if rcParams["ignore_model_upper_limits"]:
    #     mask_allul = np.all(
    #         obs_grid == -np.inf, axis=0
    #     )  # np.sum(obs_grid, axis=0) == -np.inf
    #     for i, o in enumerate(obs_grid):
    #         mask_grid[i][mask_allul] = 0
    #     print(np.sum(mask_allul))

    # =========================

    # mask_grid[~np.isnan(obs_grid)] = 1
    # mask_grid[np.isnan(obs_grid)] = 0
    # # store.create_dataset('mask_grid', data=mask_grid, compression="gzip", compression_opts=9)
    # # value below doesn't matter at all because NaNs will be taken care of with potential with mask, but make sure to provide same value in post_process script when building new_obs_grid, especially for linterp
    # obs_grid[np.isnan(obs_grid)] = -100 # inp['nan_code']
    # or anything, will be replaced with nans in post-processing

    # =========================
    # Now we can replace all NaNs with minvalue
    # the only NaNs that were not masked are probably model ULs

    # we already masked sets with only NaNs (invalid data)
    # the remaining NaNs are probably minvalues but we can also avoid them
    if rcParams["gridnanisnan"]:
        mask_grid[np.isnan(obs_grid)] = 0
        obs_grid[np.isnan(obs_grid)] = (
            0  # /!\\ 2x check value here; 0 now, before 100; in principle value doesn't matter much since it is flagged for inference
        )
    else:
        # we don't mask for inference
        for i in range(len(obs_grid)):
            if all_labels[i] not in inp["intensive_obs"] + all_secondary_parameters:
                obs_grid[i][np.isnan(obs_grid[i])] = minvalue
            else:  # minvalue for intensive params is difficult to know, assuming very low value
                obs_grid[i][
                    np.isnan(obs_grid[i])
                ] = -20  # /!\\ 2x check value here; -20 now, before -100

    # =========================

    # mask_ul_grid[~np.isinf(obs_grid)] = 1 #*~np.isnan(obs_grid)
    # mask_ul_grid[np.isinf(obs_grid)] = 0
    # model ULs should be replaced with min value to propagate UL even through interpolation, but following line can be before or after mask to decide whether mask will include them or not for further treatment
    # obs_grid[np.isinf(obs_grid)] = minvalue
    for i in range(len(obs_grid)):
        # print(i, inp['observations'].obsnames_u[i])
        if (
            all_labels[i]
            not in inp["intensive_obs"] + inp["intensive_secondary_parameters"]
        ):  # this is a 'normal' tracer for which minvalue makes sense
            mask_ul_grid[i][~np.isinf(obs_grid[i])] = 1  # *~np.isnan(obs_grid)
            mask_ul_grid[i][np.isinf(obs_grid[i])] = 0
            obs_grid[i][np.isinf(obs_grid[i])] = minvalue
        else:  # intensive
            ind = np.where(np.isinf(obs_grid[i]))[0]
            if len(ind) > 0:
                if all_labels[i] in inp["intensive_obs"]:
                    panic(
                        f"Intensive observable [{all_labels[i]}] has infinite values, please take care of them in the pre-processing script"
                    )
                elif all_labels[i] in inp["intensive_secondary_parameters"]:
                    panic(
                        f"Intensive secondary parameter [{all_labels[i]}] has infinite values, please take care of them in the pre-processing script"
                    )

            # obs_grid[i][np.isinf(obs_grid[i])] = -100

    # tmp = np.round(
    #     100 * len(np.where(mask_grid.flatten() == 0)[0]) / len(mask_grid.flatten()), 2,
    # )
    # if tmp > 0:
    #     logging.warning("Fraction of nans (invalid data) in grid: {}%".format(tmp))

    tmp = np.round(
        100
        * len(np.where(mask_ul_grid.flatten() == 0)[0])
        / len(mask_ul_grid.flatten()),
        2,
    )
    if tmp > 0:
        logging.warning(f"Fraction of infs (model upper limits) in grid: {tmp: .2f}%")

    return (
        grid,
        obs_grid,
        mask_grid,
        mask_ul_grid,
        minvalue,
        completion_fraction,
        order_of_magnitude,
        order_of_magnitude_std,
    )


# -------------------------------------------------------------------
def make_grid_wrapper(inp, add=[], verbose=False):
    # values = ascii.read(directory_models+grid_file, format=format_f, delimiter=delimiter_f)
    # read values and possibly pre-process them (convert, combine columns etc...)
    # add is for common column for post-processing merge

    observations = inp["observations"]
    # lines_to_infer = []#inp['lines_to_infer']
    if "range" in inp.keys():
        logging.error("/!\\ 'range' parameter name is obsolete, use 'select' instead")
        inp["select"] = inp["range"]
    selects = inp["select"]
    tolog = inp["tolog"]
    tolinear = (
        inp["tolinear"] if "tolinear" in inp.keys() else []
    )  # backward compatibility
    directory_models = inp["directory_models"]
    grid_file = directory_models + "Grids/model_grid.fth"
    params_name = inp["params_name"]

    # for pp tests: don't add again an obs already used.
    add = [a for a in add if a not in inp["observations"].names]

    tmp_filename = directory_models + "Grids/list_observables.dat"
    if not FileExists(tmp_filename):
        panic(
            "File not found {}\nHave you run the pre-processing script located below from the mgris folder?\n{}".format(
                tmp_filename, directory_models + "/pre-processing.py"
            )
        )
    obslist = list(np.loadtxt(tmp_filename, dtype="U"))

    if inp["post_processing_file"] is not None:
        post_processing_file, commoncolumn = inp["post_processing_file"]
        pplist = list(
            np.atleast_1d(
                np.loadtxt(
                    directory_models + "Grids/list_post-processing.dat", dtype="U"
                )
            )
        )
        for o in add + inp["observations"].obsnames_u:
            if o not in obslist + [commoncolumn]:
                logging.warning(f"Observable {o} not found in main model table")
                if o not in pplist:
                    panic(f"Observable {o} not found in post-processing table either!")
                else:
                    logging.info(f"Observable {o} found in post-processing table")

    others = [o for o in observations.obsnames_u if o not in obslist]
    if len(others) > 0 and inp["post_processing_file"] is not None:  # check in pp table
        grid = ModelGrid(params=params_name)
        # here it says from_ascii but in fact it can read feather files
        grid.from_ascii(
            directory_models + "Grids/model_grid.fth",
            inp,
            add=[o for o in add if o in obslist]
            + [
                commoncolumn,
            ]
            + [o for o in observations.obsnames_u if o in obslist]
            + params_name,
        )  # context needed for pre-processing
        grid.mergewith(
            directory_models + "Grids/model_grid_post_processing.fth",
            commoncolumn,
            add=others,
            inp=inp,
        )
        # grid.values = grid.values.drop(columns=commoncolumn)
        # for o in others:
        #    if o in inp['extensive_secondary_parameters']:
        #       inp['observations'].obs[o].extensive = True
        #    else:
        #       inp['observations'].obs[o].extensive = False
    else:
        grid = ModelGrid(params=params_name)
        # here it says from_ascii but in fact it can read feather files
        grid.from_ascii(
            directory_models + "Grids/model_grid.fth",
            inp,
            add=add + observations.obsnames_u + params_name,
        )  # context needed for pre-processing
        # for o in observations.obsnames_u: #default is all extensive
        #    if o in inp['intensive_obs']:
        #       inp['observations'].obs[o].extensive = False
        #    else:
        #       inp['observations'].obs[o].extensive = True

    grid.get_param_values(inp=inp, names=params_name)

    # then take only relevant tracers + parameters (normally already done in readtable with fth file reading)
    grid.values = grid.values[
        observations.obsnames_u + grid.params.names + add + others
    ]

    n_models_init = grid.n_models()  # will be changed after when we throw away NaNs

    # fill parameter columns from model name (temporary, to be moved in grid generating script)
    # for p in params_name:
    #   values[p] = np.array([parse_model_name(values['model_name'][i], xray=xray)[p] for i in range(n_models_init)], dtype=dtype)

    # keep only some models?
    grid.checkhardconstraints(selects)

    # temporary, convert some parameters to log
    # true_params = grid.checkconversions(tolog=tolog, true_params)
    grid.checkconversions(tolog=tolog, tolinear=tolinear)

    # check whether all values are not finite for some parameter values here
    # this might cause some issues and even if not it saves some memory...
    # first get unique values
    grid.get_param_values(inp=inp)  # auto-fill params based on unique parameter values

    # then check nans missing parameter set, missing parameter values...
    grid.checkallnans(observations.obsnames_u, verbose=verbose)

    # breakpoint()
    # if inp['ignore_model_upper_limits']:
    #   grid.checkinfinite(observations.obsnames_u, verbose=verbose)

    # remaining nans will be considered as model upper limits -> inf code
    # they will be masked anyway, so we don't have to do that
    # for t in observations.obsnames_u:
    #    grid.values[t][~np.isfinite(grid.values[t])] = -np.inf
    # -inf values will be replaced with model upper limit / min value in make_obs_grid

    # //!\\\ The code crashes if one range is of size only one
    # print(grid.params.names)
    for p in grid.params.names:
        if len(grid.params.values[p]) == 1:
            logging.warning(
                "/!\\ Only one value for parameter {} = {}, removing corresponding column...".format(
                    p, grid.params.values[p]
                )
            )
            grid.values = grid.values.drop(columns=p)
            grid.params.drop(p)
            # inp['grid'] = grid
            logging.warning("-- Updated parameter names: {}".format(grid.params.names))
            # Params_values[p] = np.repeat(Params_values[p][0], 2)

    # remove potential duplicate columns
    # (e.g., when adding a post-processing obs/param in the input file)
    grid.values = grid.values.loc[:, ~grid.values.columns.duplicated()].copy()

    # safety check
    grid.checkduplicates()

    # update parameter values (if some NaN models were discarded)
    grid.get_param_values(inp=inp)  # auto-fill params based on unique parameter values
    # inp['grid'] = grid

    # number of models
    n_models = grid.n_models()

    if n_models < n_models_init:
        logging.info("")
        logging.warning(
            "Only a subset of the model grid will be used ({}/{} models = {}%)".format(
                n_models, n_models_init, np.round(100 * n_models / n_models_init, 1)
            )
        )
        logging.info("- Effective parameter values: ")
        for p in grid.params.names:
            tmp = ", ".join(grid.params.values[p].astype(str))
            logging.info(f"-- {p: <26} : ({len(grid.params.values[p])}) {tmp}")

    # saving final grid
    # if not args.return_model_context:
    #  grid.to_csv(inp['output_directory']+'/final_grid.csv', delimiter_f=delimiter_f)
    #  if verbose:
    #      logging.info('Final grid saved: {}'.format(inp['output_directory']+'/final_grid.csv'))

    # if sum(config)>1 and true_params is not None: #test with true_params only for single sector for now
    #    panic('Test of true parameters only available for one group and one sector')

    grid.values = grid.values.reset_index()  # re-add index

    return grid


# -------------------------------------------------------------------
def merge_grids_wrapper(inp, obs=[], params=[], verbose=False):
    post_processing_file, commoncolumn = inp["post_processing_file"]

    # drop o.names that are in obs_to_predict?
    grid = make_grid_wrapper(
        inp,
        verbose=verbose,
        add=[
            commoncolumn,
        ]
        + obs,
    )

    # remove duplicate columns (e.g., if we add pp tracers that are already in model table)
    grid.values = grid.values.loc[:, ~grid.values.columns.duplicated()]

    # grid = make_grid_wrapper(inp, verbose=verbose, add=[commoncolumn,]+[o for o in obs_to_predict if o not in inp['observations'].names]) #if we wanna calculate tracers already in inp['observations'].names
    # also consider pp table
    add = [p for p in params if p not in grid.values.keys()]
    grid.mergewith(
        inp["directory_models"] + "Grids/model_grid_post_processing.fth",
        commoncolumn,
        add=add,
        inp=inp,
    )

    # Replace -inf values by min-5?
    # grid.checkinfinite()

    # keep only some models? is it necessary here?
    # grid.checkhardconstraints(ranges)

    # temporary, convert some parameters to log
    # no need to update true_params
    tolinear = (
        inp["tolinear"] if "linear" in inp.keys() else []
    )  # backward compatibility
    grid.checkconversions(tolog=inp["tolog"], tolinear=tolinear, checkinf=False)

    # some post-processing parameters may be converted to linear
    # grid.checkaslinear(aslinear, None)

    # then check nans
    grid.checkallnans(obs + params, checkinf=False, verbose=verbose)

    # only relevant quantities
    # if params_to_predict is None:
    #   params_to_predict = []
    if params == [] and obs == []:
        panic("Nothing to predict...")

    # just keep what we need
    needed = list(set(obs + params + grid.params.names))
    grid.values = grid.values[needed]

    return grid


# -------------------------------------------------------------------
def check_nsigma(observations, verbose=True):
    nsigma = 10 ** np.array(observations.values) / np.array(observations.delta).mean(
        axis=1
    )
    nstab = {}
    for i, o in enumerate(observations.names):
        if observations.obs[o].limit:
            continue
        tmp = nsigma[i] / np.nanmean([ns for j, ns in enumerate(nsigma) if j != i])
        if ~np.isfinite(tmp):
            continue
        nstab.update({o: tmp})
    tmp = [nstab[o] for o in nstab]
    if tmp == []:
        logging.warning("/!\\ Unable to diagnose detection levels")
        return np.nan, [], np.nan, np.nan
    else:
        nstab = dict(sorted(nstab.items(), key=lambda x: -x[1]))
        s = "List of tracers whose detection level deviates from the mean detection level (values=nsigma/mean(nsigma); ideally most values should be around 1, or global sdev as low as possible):\n"
        for o in nstab:
            s += f" - {o: <26} : {nstab[o]}\n"
        sdev = np.std(tmp)
        maxv = np.max(tmp)
        s += f" -- sdev all deviations: {sdev}"
        return s, nstab, sdev, maxv


# -------------------------------------------------------------------
def read_data(f, columns=None, delimiter=None, chunk=False):
    """
    Reading file for pre-processing
    """
    if not FileExists(f):
        print("/!\\ File does not exist: {}".format(f))
        return

    if os.path.splitext(f)[1] in (".txt", ".csv"):  # ASCII file
        print("Reading original txt file... {}".format(f))

        mem_gb = os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_PHYS_PAGES") / (1024.0**3)
        fs_gb = os.path.getsize(f) / 1e9
        print(
            "Available memory: {}Gb, file size: {}Gb".format(
                np.round(mem_gb, 2), np.round(fs_gb, 2)
            )
        )

        if chunk:
            chunksize = 1e5
            k = 0
            for chunk in pd.read_csv(f, chunksize=chunksize, sep=delimiter):
                if k == 0:
                    data = chunk
                else:
                    data = pd.concat([data, chunk])
                    k += 1
            print(f"Read {k} chunks")
            del chunk
        else:
            data = pd.read_csv(f, sep=delimiter)

        data.reset_index(inplace=True, drop=True)

        return data.astype(rcParams["precision"])

    elif os.path.splitext(f)[1] == ".fth":  # binary feather file
        print("Reading original Feather file... {}".format(f))

        if chunk:
            for k, batch in enumerate(read_feather_in_chunks(f)):
                if k == 0:
                    if columns is None:
                        data = batch
                    else:
                        data = batch[columns]
                else:
                    if columns is None:
                        data = data.append(batch)
                    else:
                        data = data.append(batch[columns])
            print(f"Read {k+1} chunks")
            del chunk
        else:
            # reading all rows at once...
            data = pd.read_feather(f, columns=columns)

        data.reset_index(inplace=True, drop=True)

        return data

    else:
        print("Extension not recognized: {}".format(os.path.splitext(f)[1]))
        exit()


# -------------------------------------------------------------------
def check_duplicates(data, columns=None):
    duplicates = [
        d for d in data.duplicated(keep="first", subset=columns) if d
    ]  # keep=False means we save all duplicated rows, not only first or last
    if duplicates != []:
        print(
            f"/!\\ Table contains {len(duplicates)} duplicates on {len(data)} rows..."
        )
        choice = inputRL(
            "Do you wanna remove them?\n[Y: remove / n: keep and rely on hard constraints in input file to disentangle duplicates)]\n[Y/n]: ",
            prefill="Y",
        )
        if choice.strip() == "Y":
            data = data.drop_duplicates(keep="first", subset=columns)
            data = data.reset_index()
        else:
            print("Keeping...")

    return data


# -------------------------------------------------------------------
def get_iset(o, n_sets):
    if n_sets == 1:
        iset = 0
    else:
        if "SET" not in o:  # when doing tests with only one point
            iset = 0
        else:
            iset = int(o[o.index("SET") + 3 : -1]) - 1
    return iset


# -------------------------------------------------------------------
def prefilter(lines):
    return [
        re.sub(r"[ \t]+", r" ", l.strip()) for l in lines if l.strip() not in ("", "\n")
    ]


# -----


def verify_context(inp, force=False):
    # verifying whether context files have been changed or not

    if "context_input" not in inp.keys() or "context_pre_processing" not in inp.keys():
        logging.warning(
            "Cannot verify consistency of context files, probably an old file is used"
        )
        return

    error = False
    with open(inp["context"] + "/context_input.txt") as f:
        context_input = f.readlines()
        present = prefilter(context_input)
        orig = prefilter(inp["context_input"])
    if present != orig:
        logging.warning(
            "Context input file is different compared to the version used with mgris_search"
        )
        for line in difflib.unified_diff(
            orig,
            present,
            fromfile="saved from mgris_search",
            tofile="to be used with mgris_process",
            lineterm="",
            n=0,
        ):
            print(line)
            error = True

    with open(inp["context"] + "/pre-processing.py") as f:
        context_pre_processing = f.readlines()
        present = prefilter(context_pre_processing)
        orig = prefilter(inp["context_pre_processing"])
    if present != orig:
        logging.warning(
            "Context pre-processing script is different compared to the version used with mgris_search"
        )
        for line in difflib.unified_diff(
            orig,
            present,
            fromfile="saved from mgris_search",
            tofile="to be used with mgris_process",
            lineterm="",
            n=0,
        ):
            print(line)
        error = True

    if error and not force:
        panic("Refusing to continue but you can use the flag -f to force")


# --------------------------------------
def calcavg(inp, trace):
    """
    Calculates average values for plaw, normal... distributions
    """

    for ps in inp["plaw_params"]:
        x, weight = distrib_plaw(
            [
                trace.posterior[f"lowerbound_{ps}"].to_numpy(),
                trace.posterior[f"upperbound_{ps}"].to_numpy(),
            ],
            trace.posterior[f"alpha_{ps}"].to_numpy(),
        )
        tmp = np.sum((10**weight) * x, axis=0) / np.sum(10**weight, axis=0)
        trace.posterior[f"avg_{ps}"] = trace.posterior[f"lowerbound_{ps}"] * 0.0
        trace.posterior[f"avg_{ps}"].data = tmp  # np.log10(tmp)

    for ps in inp["smoothplaw_params"]:
        x, weight = distrib_smoothplaw(
            [
                trace.posterior[f"lowerbound_{ps}"].to_numpy(),
                trace.posterior[f"upperbound_{ps}"].to_numpy(),
            ],
            trace.posterior[f"alpha_{ps}"].to_numpy(),
            [
                trace.posterior[f"lowerscale_{ps}"].to_numpy(),
                trace.posterior[f"upperscale_{ps}"].to_numpy(),
            ],
        )
        tmp = np.sum((10**weight) * x, axis=0) / np.sum(10**weight, axis=0)
        trace.posterior[f"avg_{ps}"] = trace.posterior[f"lowerbound_{ps}"] * 0.0
        trace.posterior[f"avg_{ps}"].data = tmp  # np.log10(tmp)

    for ps in inp["brokenplaw_params"]:
        x, weight = distrib_brokenplaw(
            [
                trace.posterior[f"lowerbound_{ps}"].to_numpy(),
                trace.posterior[f"upperbound_{ps}"].to_numpy(),
            ],
            [
                trace.posterior[f"alpha1_{ps}"].to_numpy(),
                trace.posterior[f"alpha2_{ps}"].to_numpy(),
            ],
            trace.posterior[f"pivot_{ps}"].to_numpy(),
        )
        tmp = np.sum((10**weight) * x, axis=0) / np.sum(10**weight, axis=0)
        trace.posterior[f"avg_{ps}"] = trace.posterior[f"lowerbound_{ps}"] * 0.0
        trace.posterior[f"avg_{ps}"].data = tmp  # np.log10(tmp)

    for ps in inp["normal_params"]:
        x, weight = distrib_normal(
            [
                trace.posterior[f"lowerbound_{ps}"].to_numpy(),
                trace.posterior[f"upperbound_{ps}"].to_numpy(),
            ],
            trace.posterior[f"mu_{ps}"].to_numpy(),
            trace.posterior[f"sigma_{ps}"].to_numpy(),
        )
        tmp = np.sum((10**weight) * x, axis=0) / np.sum(10**weight, axis=0)
        trace.posterior[f"avg_{ps}"] = trace.posterior[f"lowerbound_{ps}"] * 0.0
        trace.posterior[f"avg_{ps}"].data = tmp  # np.log10(tmp)

    for ps in inp["doublenormal_params"]:
        x, weight = distrib_doublenormal(
            [
                trace.posterior[f"lowerbound_{ps}"].to_numpy(),
                trace.posterior[f"upperbound_{ps}"].to_numpy(),
            ],
            [
                trace.posterior[f"mu1_{ps}"].to_numpy(),
                trace.posterior[f"mu2_{ps}"].to_numpy(),
            ],
            [
                trace.posterior[f"sigma1_{ps}"].to_numpy(),
                trace.posterior[f"sigma2_{ps}"].to_numpy(),
            ],
            trace.posterior[f"ratio21_{ps}"].to_numpy(),
        )
        tmp = np.sum((10**weight) * x, axis=0) / np.sum(10**weight, axis=0)
        trace.posterior[f"avg_{ps}"] = trace.posterior[f"lowerbound_{ps}"] * 0.0
        trace.posterior[f"avg_{ps}"].data = tmp  # np.log10(tmp)

    return trace


# --------------------------------------


def getp(ps):
    "from <param>_<sector> get only <param>"
    if re.search(".*_[0-9]", ps) is None:
        return ps
    else:
        return "_".join(ps.split("_")[:-1])


# --------------------------------------
