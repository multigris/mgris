import numpy as np
from Library.lib_misc import FileExists, GetUniques, panic  # , unique
from scipy.interpolate import interp1d
import re
from math import log10

# global parameters
from rcparams import rcParams

if rcParams["pmversion"] == 5:
    import pytensor.tensor as tt
elif rcParams["pmversion"] == 4:
    import aesara.tensor as tt
elif rcParams["pmversion"] == 3:
    import theano.tensor as tt

import logging

import pandas as pd
from copy import deepcopy

try:
    import jax.numpy as jnp
except:
    import numpy as np


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# Observations --> ObservationSet #1 --> Observations #1
#              -->                   --> Observations #2
#              -->                   --> Observations #3
#              --> ObservationSet #2 --> Observations #4

# class Observations is a group of several of class ObservationSet
# class ObservationSet is a dictionnary of several single observed quantities of class Observation
#
# masks are handled with ObservationSet
# value, error, limit, scale... are handled with Observation
# -------------------------------------------------------------------
# -------------------------------------------------------------------


# -------------------------------------------------------------------
class Observation:
    """Observation"""

    # -------------------------------------------
    def __init__(
        self,
        name,
        value=None,
        arr=None,
        delta=None,
        delta_type=None,
        upper=False,
        lower=False,
        limit=False,
        delta_ref=None,
    ):
        if delta is not None and delta_type is None:
            raise Exception("/!\\ Please provide delta_type")

        # default
        self.name = name
        self.arr = arr
        self.upper = upper
        self.lower = lower
        self.value = value
        self.delta = delta
        self.delta_type = delta_type
        self.delta_ref = delta_ref
        self.limit = limit

        if (
            self.value is None and self.arr is not None
        ):  # parse arr if given to get value and delta
            self.parse_arr()

        if self.upper or self.lower:  # at least one limit
            self.limit = True

        # if not self.limit and self.delta is None and self.delta_ref is None:
        #    raise Exception('/!\\ No uncertainty entered (in observation block or as delta_ref)') #unc not needed for limits

        if not self.limit and self.delta is None:  # use delta_ref default
            if self.delta_ref is None and np.isfinite(self.value):
                panic("delta_ref was not provided")
            # if 'nan' in self.delta_ref and 'np.nan' not in delta_ref:
            #    self.delta_ref = self.delta_ref.replace('nan', 'np.nan')
            if np.isfinite(self.value):
                self.delta = np.repeat(float(self.delta_ref), 2)
            else:
                self.delta = [np.nan, np.nan]

        if self.limit and self.delta is None:  # for UL and LL by default no uncertainty
            self.delta = [1.0, 1.0]  # the value for limits doesn't matter

        if self.delta is not None:
            self.update_delta()

        return

    # -------------------------------------------
    def parse_arr(self):
        if (
            self.arr[1] == "<"
            or self.arr[1] == ">"
            or self.arr[1] == "<="
            or self.arr[1] == ">="
        ):
            print(self.arr)
            panic("No space is allowed between < (or >) and value")
        # value
        self.value = eval(
            self.arr[1]
            .replace("inf", " np.inf")
            .replace("nan", " np.nan")
            .replace(">", "")
            .replace("<", "")
            .strip()
        )
        # limits
        if ">" in self.arr[1]:
            self.lower = True
        if "<" in self.arr[1]:  # or '-' in self.string_value:
            self.upper = True
        # other args
        if len(self.arr) > 2:
            # both UL and LL?
            if ">" in self.arr[2]:
                self.lower = True
                self.arr[2] = self.arr[2].replace(
                    ">", ""
                )  # delta[0] will be LL in mgris_search
            elif "<" in self.arr[2]:
                self.upper = True
                self.arr[2] = self.arr[2].replace(
                    "<", ""
                )  # delta[0] will be UL in mgris_search
            # delta
            if len(self.arr) == 4:
                if "nan" in self.arr[3] and "np.nan" not in self.arr[3]:
                    self.arr[3] = self.arr[3].replace("nan", "np.nan")
                if "nan" in self.arr[2] and "np.nan" not in self.arr[2]:
                    self.arr[2] = self.arr[2].replace("nan", "np.nan")
                self.delta = np.array(
                    [eval(self.arr[3]), eval(self.arr[2])]
                )  # (a)symmetric EB, convert to -/+
            elif len(self.arr) == 3:
                if "nan" in self.arr[2] and "np.nan" not in self.arr[2]:
                    self.arr[2] = self.arr[2].replace("nan", "np.nan")
                self.delta = np.repeat(eval(self.arr[2]), 2)  # symmetric EB
            else:
                self.delta = None  # default: no uncertainty entered, will look for delta_ref later

    # -------------------------------------------
    def update_delta(self):
        if self.delta_type == "absolute":  # first convert to relative
            self.delta /= 10**self.value
        # self.delta = self.delta
        # self.delta = max(abs(log10(1+self.delta[1])), abs(log10(1-self.delta[0]))) #for now symetric

    # -------------------------------------------
    def tolog(self):
        # self.delta = [log10(self.value)-np.log10(self.value-self.delta[0]), log10(self.value+self.delta[1])-log10(self.value)]
        if not self.upper and not self.lower:  # we don't wanna change delta[0] if range
            # self.delta = np.repeat(np.nanmax([log10(self.value)-log10(self.value-self.delta[0]), log10(self.value+self.delta[1])-log10(self.value)]), 2)
            # up and low
            self.delta = [
                log10(self.value + self.delta[0]) - log10(self.value),
                log10(self.value) - log10(self.value - self.delta[1]),
            ]
        # self.delta_log = self.delta
        self.value = log10(self.value)

    # -------------------------------------------
    def tolinear(self):
        # self.delta = [log10(self.value)-log10(self.value-self.delta[0]), log10(self.value+self.delta[1])-log10(self.value)]
        if not self.upper and not self.lower:  # we don't wanna change delta[0] if range
            # self.delta = np.repeat(np.nanmax([log10(self.value)-log10(self.value-self.delta[0]), log10(self.value+self.delta[1])-log10(self.value)]), 2)
            # up and low
            self.delta = [
                10 ** (self.value + self.delta[0]) - 10**self.value,
                10**self.value - 10 ** (self.value - self.delta[1]),
            ]
        # self.delta_log = self.delta
        self.value = 10 ** (self.value)

    # -------------------------------------------


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------


# -------------------------------------------------------------------
class ObservationSet:
    """Observations"""

    # -------------------------------------------
    def __init__(self, names=None, values=None, delta=None, delta_type=None):
        if delta is not None and delta_type is None:
            raise Exception("/!\\ Please provide delta_type")
        self.names = names
        self.ignored = []
        self.delta = delta
        self.delta_type = delta_type
        if self.delta is not None:
            if len(self.delta) == 1:
                self.delta = np.repeat(self.delta[0], len(self.names))
            if len(self.delta) != len(self.names):
                raise Exception(
                    "/!\\ delta and observed not the same number of elements"
                )
        self.values = values
        if self.values is not None:
            self.obs = {
                n: Observation(
                    n,
                    value=self.values[i],
                    delta=self.delta[i],
                    delta_type=self.delta_type,
                )
                for i, n in enumerate(self.names)
            }  #
            self.update()
        elif self.names is not None:
            self.obs = {n: Observation(n) for i, n in enumerate(self.names)}
        else:
            self.obs = {}
        return

    # -------------------------------------------
    def tolog(self):
        for o in self.names:
            self.obs[o].tolog()
        self.update()

    # -------------------------------------------
    def tolinear(self):
        for o in self.names:
            self.obs[o].tolinear()
        self.update()

    # -------------------------------------------
    def update(self):
        self.names = list(self.obs.keys())

        # update obsnames as well
        try:
            self.obsnames = [self.obs[o].obsname for o in self.obs]
            self.obsnames_u = []  # this is to keep the same order
            for o in self.obsnames:
                if o not in self.obsnames_u:
                    self.obsnames_u += [
                        o,
                    ]
        except:
            None

        ##convenience
        # self.string_values = [self.obs[o].string_value for o in self.obs]
        self.values = [self.obs[o].value for o in self.obs]
        self.delta = [self.obs[o].delta for o in self.obs]
        self.n = len(self.obs)

    # -------------------------------------------
    def n_obs(self):
        self.update()  # just in case...
        return len(self.names)

    # -------------------------------------------
    def add(self, o):
        self.obs.update({o.name: o})
        self.update()

    # -------------------------------------------
    def drop(self, o):
        del self.obs[o]
        self.update()

    # -------------------------------------------
    def update_delta(self):
        for o in self.names:
            self.obs[o].update_delta()

    # -------------------------------------------
    def getidx(self, o):  # get index value for obs_grid
        return np.where(np.array(self.obsnames_u) == self.getobsname(o))[0][0]

    # -------------------------------------------------------------------
    def getobsname(self, o):
        if "{" in o:
            return o[0 : o.index("{")]
        else:
            return o

    # -------------------------------------------
    def ignore_missing(self, verbose=False):
        """Observations that are NaNs can be inferred but by default they're ignored"""
        if len(self.mask_obs_nan) > 0:
            logging.warning(
                "- Some observables do not have values and/or error bars, ignoring them (can be forced with --force_missing)..."
            )
            for o in self.mask_obs_nan:
                self.drop(o)
                self.ignored += [
                    o,
                ]
            logging.info("  - Ignored         : {}".format(self.ignored))

            self.mask_obs_nan = []
            logging.info("- New observables : {}".format(self.names))
            logging.info("- New observed    : {}".format(self.values))
            logging.info("- New delta       : {}".format(self.delta))
            logging.info("- New n_obs       : {}".format(self.n))

            # redo masks
            if verbose:
                logging.info("Updating masks")
                self.set_masks()

    # -------------------------------------------
    def set_masks(self):
        # ideally we wanna use a delta for the upper/lower limits (e.g., corresponding to RMS). If not there, replace with some value to make sure we do use the limits
        self.mask_obs_nan = [
            o
            for o in self.names
            if (
                ~np.isfinite(self.obs[o].value)
                | ~np.isfinite(self.obs[o].delta[0])
                | ~np.isfinite(self.obs[o].delta[1])
            )
            and not self.obs[o].limit
        ]  # NaN values of unmeasured lines --> can be inferred explicitly

        self.mask_obs_no_eb_lims = [
            o
            for o in self.names
            if (
                ~np.isfinite(self.obs[o].delta[0])
                and ~np.isfinite(self.obs[o].delta[1])
            )
            or (self.obs[o].delta[0] == 0 and self.obs[o].delta[1] == 0)
            and self.obs[o].limit
        ]
        # if len(self.mask_obs_no_eb_lims)>0 and obs_valuescale=='log':
        #     for o in self.mask_obs_no_eb_lims:
        #         #val = np.array([np.nanmean([self.obs[o].delta[0] for o in self.names]), np.nanmean([self.obs[o].delta[1] for o in self.names])])
        #         #val = np.repeat(0, 2)
        #         logging.warning('/!\\ Delta for log limit {} was not set, using reference delta'.format(o))
        #         if self.obs[o].delta_ref is None:
        #             panic('delta_ref was not provided')
        #         val = np.repeat(self.obs[o].delta_ref, 2)
        #         self.obs[o].delta = val
        #         #self.obs[o].delta = np.array([0, 0]) #check mu/sigma dist 0
        #     self.update() #updates delta array
        #     logging.info('delta = {}'.format(self.delta))

        # lower limits
        self.mask_obs_lo_lims = [o for o in self.names if self.obs[o].lower]

        # upper limits
        self.mask_obs_up_lims = [
            o for o in self.names if self.obs[o].upper
        ]  # negative values

        # combined mask for lower/upper limits
        self.mask_obs_lims = self.mask_obs_lo_lims + self.mask_obs_up_lims

        # all good / detections (we're not including the scaling reference if there is a single one)
        self.mask_obs_detected = [
            o
            for o in self.names
            if not self.obs[o].limit and np.isfinite(self.obs[o].value)
        ]

        self.mask_obs_extensive = [o for o in self.names if self.obs[o].extensive]
        self.mask_obs_intensive = [
            o for o in self.names if o not in self.mask_obs_extensive
        ]
        tmp = set(self.mask_obs_detected).difference(self.mask_obs_intensive)
        self.mask_obs_detected_extensive = [
            o for o in self.names if o in tmp
        ]  # this is to keep the same order

    # -------------------------------------------
    def compute_oom(self):
        """Calculates the order of magnitude for the ObservationSet.
        Only for extensive values!
        Also produces the offset values wrt the order of magnitude for the inference
        """
        self.order_of_magnitude_obs = np.mean(
            np.array([self.obs[o].value for o in self.mask_obs_detected_extensive])
        )
        self.order_of_magnitude_obs_std = np.std(
            np.array([self.obs[o].value for o in self.mask_obs_detected_extensive])
        )
        for o in self.obs:
            self.obs[o].value_offset = self.obs[o].value
            if self.obs[o].extensive:
                self.obs[o].value_offset -= self.order_of_magnitude_obs

    # -------------------------------------------


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------


# -------------------------------------------------------------------
class Observations:
    """Observations"""

    # -------------------------------------------
    def __init__(self, sets_raw, input_params, verbose=False):
        self.sets_raw = sets_raw
        # convert to observation sets
        self.sets = {}
        self.nblocks = len(sets_raw)

        for s in sets_raw:
            observations = ObservationSet()
            observations.obstolog = sets_raw[s]["obstolog"]
            observations.obstolinear = sets_raw[s]["obstolinear"]
            observations.obs_valuescale = sets_raw[s]["obs_valuescale"]
            observations.scale_factor = sets_raw[s]["scale_factor"]
            observations.delta_ref = sets_raw[s]["delta_ref"]
            observations.delta_add = sets_raw[s]["delta_add"]

            for o in sets_raw[s]["observations_dict"]:
                logging.info(
                    f"Processing label: {o}: {sets_raw[s]['observations_dict'][o]}"
                )
                obs = Observation(
                    o,
                    arr=sets_raw[s]["observations_dict"][o][0],
                    delta_type="relative",
                    delta_ref=observations.delta_ref,
                )

                obs.extensive = (
                    o not in input_params["intensive_obs"]
                    and o not in input_params["intensive_secondary_parameters"]
                )

                # some conversions #Conversions of *grid* observables done in lib_class/check_conversions
                if (
                    observations.obstolinear or observations.obs_valuescale == "log"
                ):  # obs block is in log scale but we wanna apply a scaling factor
                    # first apply scaling factor anyway
                    if obs.extensive:
                        obs.value += (
                            observations.scale_factor
                        )  # default is 0 if obs_valuescale is log
                        if observations.delta_add is not None:
                            obs.delta = [
                                d + float(observations.delta_add) for d in obs.delta
                            ]
                    # convert value *and delta* to linear but we'll use the log of the value for the inference
                    if observations.obstolinear:
                        obs.tolinear()
                        # all calculations are made in log until inference where we switch to linear #just update values, not delta!
                        obs.value = log10(obs.value)

                elif (
                    observations.obs_valuescale == "linear"
                ):  # obs block is in linear scale; there's no tolog anymore
                    # first apply scaling factor anyway
                    if obs.extensive:
                        obs.value *= (
                            observations.scale_factor
                        )  # default is 1 if obs_valuescale is linear
                        obs.delta = [
                            d * observations.scale_factor for d in obs.delta
                        ]  # default is 1 if obs_valuescale is linear
                        if observations.delta_add is not None:
                            obs.delta = [
                                np.max([d, float(observations.delta_add) * obs.value])
                                for d in obs.delta
                            ]
                    # all calculations are made in log until inference where we switch to linear #just update values, not delta!
                    obs.value = log10(obs.value)

                # I don't think it's needed anymore
                # elif observations.obs_valuescale=='log': #obs block is in log scale
                #     #first apply scaling factor anyway
                #     if obs.extensive:
                #         obs.value += observations.scale_factor #default is 0 if obs_valuescale is log
                #         if observations.delta_add is not None:
                #             obs.delta = [d+observations.delta_add for d in obs.delta]
                #     obs.tolog()

                else:  # this shouldn't happen
                    panic("Need to clarify scale of observed fluxes...")

                # print(o, obs.value, obs.delta)
                # will be used later
                obs.obs_valuescale = observations.obs_valuescale

                observations.add(obs)

                if self.nblocks > 1:
                    observations.obs[o + "{" + s + "}"] = observations.obs[o]
                    observations.obs[o + "{" + s + "}"].obsname = (
                        o  # keep old name for list observable
                    )
                    observations.obs[o + "{" + s + "}"].name = o + "{" + s + "}"
                    del observations.obs[o]
                else:
                    observations.obs[o].obsname = o

                observations.update()

            self.sets.update({s: observations})

        self.flatten()  # produces an ObservationSet; backward compatibility for code < hierarchical

    # -------------------------------------------
    def flatten(self):
        """Concatenates all obs in order to switch back to ObservationSet for inference"""
        self.oset = ObservationSet()
        for s in self.sets:
            for o in self.sets[s].obs:
                self.oset.add(self.sets[s].obs[o])
        self.oset.update()

    # -------------------------------------------
    def order_of_magnitude_obs(self):
        """Array of orders of magnitude for each observation set"""
        return [self.sets[s].order_of_magnitude_obs for s in self.sets]

    # -------------------------------------------
    def order_of_magnitude_obs_std(self):
        """Array of standard deviation of orders of magnitude for each observation set"""
        return [self.sets[s].order_of_magnitude_obs_std for s in self.sets]

    # -------------------------------------------
    # def getnames(self, verbose=False):
    #     #list of unique obs names
    #     self.names = []
    #     for s in self.sets:
    #         for o in self.sets[s].obs:
    #             self.names += [self.sets[s].obs[o].name,]
    #     self.names = set(self.names)

    # ======================================


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------


# -------------------------------------------------------------------
class ModelGrid:
    """Grid (parameters and values)"""

    # -------------------------------------------
    def __init__(self, values=None, params=None):
        self.values = values
        self.params = params
        if type(params) is not ParameterSet:
            self.params = ParameterSet(names=params)
        return

    # -------------------------------------------
    def from_ascii(self, filename, inp=None, add=[], delimiter_f=None):
        if inp is None:
            precision = rcParams["precision"]
        else:
            precision = inp["precision"]
        self.values = (
            self.readtable(filename, inp=inp, add=add, delimiter_f=delimiter_f)
            .select_dtypes(include=["floating"])
            .astype(precision)
        )  # exclude strings and force precision

    # -------------------------------------------------------------------
    def readtable(self, filename, inp=None, add=None, delimiter_f=None):  # inp=None,
        # filename = filename[filename.index('Contexts/'):] #using grid file from where code is launched
        if inp is None:
            precision = rcParams["precision"]
        else:
            precision = inp["precision"]
        fthfile = filename.replace(".txt", ".fth").replace(".csv", ".fth")
        if FileExists(fthfile):
            logging.info(
                "Reading Feather file... {} (run pre-processing script if the latter needs to be updated)".format(
                    fthfile
                )
            )

            columns = []
            if add is not None:
                columns += add
            # if inp is not None:
            #    columns += inp['params_name']
            #    columns += inp['observations'].names
            if columns != []:
                columns = GetUniques(columns)
            try:
                values = pd.read_feather(fthfile, columns=columns)
            except:
                if inp is not None:
                    panic(
                        f"Some columns were not found. Please use exact labels from {inp['context']}/Grids/list_observables.dat"
                    )
                else:
                    panic(
                        "Some columns were not found. Please use the exact labels from the context model grid."
                    )
        else:
            logging.critical("Not found: {}".format(fthfile))
            panic(
                "Please run pre-processing script to create Feather binary file (and other mandatory files)"
            )
            # logging.info('Reading ascii file... {} (run pre-processing script to create Feather binary file for optimal performance)'.format(filename))

            # values = pd.read_csv(filename, delimiter_f)

        # reset to index = 0
        values = values.reset_index()

        # try to convert str to float when that's possible #can be very long if columns are not pre-selected with add/columns...!
        # for t in values.keys():
        #    try:
        #       values[t] = values[t].astype(precision)
        #    except:
        #       logging.warning('Failed to convert to {}'.format(precision))

        try:
            values = values.astype(precision)
        except:
            logging.error("Failed to convert to {}".format(precision))

        # pre-processing is now done manually
        # if inp is not None:
        #    #pre-processing? e.g., make new column as sum of two, replacing nans with minimum value...
        #    context = inp['context']
        #    if FileExists(context + '/pre-processing.py'):
        #       logging.info('Executing {}'.format(context + '/pre-processing.py'))
        #       exec(open(context + '/pre-processing.py', "rb").read())

        return values

    # -------------------------------------------
    def to_csv(self, filename, delimiter_f=None):
        self.values.to_csv(filename, sep=delimiter_f, index=False, float_format="%.3f")

    # -------------------------------------------
    def n_models(self):
        return len(self.values)

    # -------------------------------------------
    def get_param_values(self, inp=None, names=None):
        if inp is None:
            precision = rcParams["precision"]
        else:
            precision = inp["precision"]
        # self.params.set_values({p:np.array(unique(self.values[p], sort=True), dtype=precision) for p in self.params.names})
        self.params.set_values(
            {
                p: np.array(sorted(self.values[p].unique()), dtype=precision)
                for p in self.params.names
            }
        )

    # -------------------------------------------
    def mergewith(self, othergrid_filename, commoncolumn, add=[], inp=None):
        othergrid = ModelGrid(params=self.params)
        othergrid.from_ascii(
            othergrid_filename,
            inp,
            add=[
                commoncolumn,
            ]
            + self.params.names
            + add,
        )
        # drop the parameters columns
        othergrid.values = othergrid.values.drop(columns=self.params.names)
        if (
            commoncolumn not in self.values.keys()
            or commoncolumn not in othergrid.values.keys()
        ):
            raise Exception(
                "/!\\ Common column name not found: {}".format(commoncolumn)
            )
        self.values = (
            self.values.drop(columns="index")
            .set_index(commoncolumn)
            .join(
                othergrid.values.drop(columns="index").set_index(commoncolumn),
                lsuffix="_",
            )
            .reset_index()
        )  # lsuffix is because some params may be dropped (e.g., with range, so they stick around unfortunately)
        # values = join(values, values_pp, 'model_name')

    # -------------------------------------------
    def checkhardconstraints(self, ranges):
        # keep only some models?
        if ranges is not None:
            for p in ranges:
                if ranges[p] == [-np.inf, -np.inf]:
                    self.values = self.values[self.values[p] == np.min(self.values[p])]
                elif ranges[p] == [np.inf, np.inf]:
                    self.values = self.values[self.values[p] == np.max(self.values[p])]
                elif len(ranges[p]) > 2:
                    self.values = self.values.iloc[
                        [
                            i
                            for i, v in enumerate(
                                self.values[p].values.astype("float32")
                            )
                            if v in ranges[p]
                        ]
                    ]
                else:
                    self.values = self.values[
                        (self.values[p].values.astype("float32") >= ranges[p][0])
                        & (self.values[p].values.astype("float32") <= ranges[p][1])
                    ]
                logging.info(
                    "- Hard constraint for {}: {} (number of models left: {})".format(
                        p, ranges[p], len(self.values)
                    )
                )
                # logging.info("-- (number of models left: {})".format(len(self.values)))

    # -------------------------------------------
    def checkconversions(self, tolog=[], tolinear=[], checkinf=True):
        # backward compatibility
        if tolog is None:
            tolog = []
        if tolinear is None:
            tolinear = []

        for p in tolog:
            if p in self.values.keys():
                n_inf1 = np.sum(~np.isfinite(self.values[p]))
                self.values[p] = log10(self.values[p])
                n_inf2 = np.sum(~np.isfinite(self.values[p]))
                if n_inf2 > n_inf1 and checkinf:
                    panic(
                        [
                            "Some infinite values have been introduced by log conversion",
                            "It is advised to deal with this in the context pre-processing script",
                            "e.g., either by forcing them to -inf (model upper limits), any other value, or dropping them",
                        ]
                    )
                self.values[p][self.values[p] == -np.inf] = (
                    np.min(self.values[p][np.isfinite(self.values[p])]) - 5
                )  # temporary for quantities to be convert in log that are 0

        for p in tolinear:
            if p in self.values.keys():
                self.values[p] = 10 ** (self.values[p])

        if "obs" in tolog:  # this is not obsinput!
            logging.info("Converting *grid* observables to log")
            for tmp in self.values.keys():
                if tmp not in self.params.names and self.values[tmp].dtype in (
                    "float64",
                    "float32",
                    "float16",
                ):  # selecting only floats because some columns might be labels
                    self.values[tmp] = np.log10(np.array(self.values[tmp]))

        if "obs" in tolinear:  # this is not obsinput!
            logging.info("Converting *grid* observables to linear")
            for tmp in self.values.keys():
                if tmp not in self.params.names and self.values[tmp].dtype in (
                    "float64",
                    "float32",
                    "float16",
                ):  # selecting only floats because some columns might be labels
                    self.values[tmp] = 10 ** (self.values[tmp])

    # -------------------------------------------
    def checkduplicates(self):
        duplicates = [
            i
            for i, c in enumerate(self.values[self.params.names].duplicated(keep=False))
            if c
        ]  # keep=False means we save all duplicated rows, not only first or last
        if duplicates != []:
            # pd.set_option('display.max_rows', len(duplicates)+1)
            logging.critical(
                "/!\\ Parameter sets contain {}% duplicates".format(
                    round(100.0 * len(duplicates) / len(self.values), 1)
                )
            )
            logging.critical("/!\\ Sorted duplicates:")
            logging.critical(
                self.values.iloc[duplicates].sort_values(self.params.names)
            )
            raise Exception(
                "/!\\ Parameter sets contain duplicates, either use hard constraints to disentangle or take care of them with pre-processing script"
            )

    # -------------------------------------------
    def checkallnans(self, full_observables, checkinf=True, verbose=False):
        """
        check if models returns nans for some parameter values
        """
        # step 1: check if all models returns nans for given parameter values
        # if verbose:
        #    logging.info("Checking NaN values for parameter...")
        for p in self.params.values.keys():
            # if verbose:
            #    logging.info("...{}".format(p))
            for val in self.params.values[p]:
                if checkinf:
                    if (~np.isfinite(np.array(self.values[p]))).all():
                        logging.error(
                            "/!\\ All NaNs for parameter {}, value {}".format(p, val)
                        )
                        self.values = self.values[self.values[p] != val]
                else:
                    if (np.isnan(self.values[p])).all():
                        logging.error(
                            "/!\\ All NaNs for parameter {}, value {}".format(p, val)
                        )
                        self.values = self.values[~self.values[p].isna()]

        # step 2: check if some models (given set of parameter) return all nans
        toremove = []
        for i, r in enumerate(np.array(self.values[full_observables])):
            if checkinf:
                if (~np.isfinite(r)).all():
                    # logging.info('Row {}: all nans'.format(r))
                    toremove += [
                        i,
                    ]
            else:
                if (np.isnan(r)).all():
                    # logging.info('Row {}: all nans'.format(i))
                    toremove += [
                        i,
                    ]
        # I'd rather removing them afterward
        if len(toremove) > 0:
            logging.warning(
                "/!\\ Removing {} models due to (all) nan values".format(len(toremove))
            )
            self.values = self.values.drop(
                index=self.values.index[toremove]
            )  # self.values.drop(toremove)

    # -------------------------------------------
    def checkinfinite(self, full_observables, verbose=False):
        toremove = []
        for i, r in enumerate(np.array(self.values[full_observables])):
            if (~np.isfinite(r)).any():
                toremove += [
                    i,
                ]
        if len(toremove) > 0:
            logging.warning(
                "/!\\ Removing {} models due to at least one infinite value".format(
                    len(toremove)
                )
            )
            self.values = self.values.drop(
                index=self.values.index[toremove]
            )  # self.values.drop(toremove)

    # -------------------------------------------
    # def checkinfinite(self):
    #     for p in self.values.keys():
    #        if p!='#model_name':
    #           self.values[p] = [f for f in self.values[p]]
    #           self.values[p][self.values[p]==-np.inf] = np.min(self.values[p][np.isfinite(self.values[p])])-5
    # -------------------------------------------
    def generate_values(self, param_grid, obs_grid, obs_names, inp=None):
        if inp is None:
            precision = rcParams["precision"]
        else:
            precision = inp["precision"]
        tmp_params = (
            jnp.array(param_grid)
            .reshape(
                len(self.params.names),
                np.prod(
                    np.array([len(self.params.values[p]) for p in self.params.names])
                ),
            )
            .astype(precision)
            .T
        )
        tmp_obs = (
            jnp.array(obs_grid)
            .reshape(
                len(obs_names),
                np.prod(
                    np.array([len(self.params.values[p]) for p in self.params.names])
                ),
            )
            .astype(precision)
            .T
        )
        tmp = jnp.array(
            [
                np.concatenate((tmp_obs[i], tmp_params[i]))
                for i in range(len(tmp_params))
            ]
        ).astype(precision)

        self.values = pd.DataFrame(data=tmp, columns=obs_names + self.params.names)

        # remove all nans
        self.checkallnans(obs_names)

        del tmp, tmp_params, tmp_obs

    # -------------------------------------------
    def parse_params(self, inp):
        # parse parameters to infer directly or that need hyperparameters
        self.params.names_inf = []
        self.params.names_plaw = sorted(inp["plaw_params"])
        self.params.names_smoothplaw = sorted(inp["smoothplaw_params"])
        self.params.names_brokenplaw = sorted(inp["brokenplaw_params"])
        self.params.names_normal = sorted(inp["normal_params"])
        self.params.names_doublenormal = sorted(inp["doublenormal_params"])
        for p in sorted(self.params.names):
            # if p appears, apply to all components
            if p in inp["plaw_params"]:
                self.params.names_plaw += [f"{p}_{s}" for s in range(inp["n_comps"])]
                self.params.names_plaw = list(set(self.params.names_plaw) - set([p]))

            elif p in inp["smoothplaw_params"]:
                self.params.names_smoothplaw += [
                    f"{p}_{s}" for s in range(inp["n_comps"])
                ]
                self.params.names_smoothplaw = list(
                    set(self.params.names_smoothplaw) - set([p])
                )

            elif p in inp["brokenplaw_params"]:
                self.params.names_brokenplaw += [
                    f"{p}_{s}" for s in range(inp["n_comps"])
                ]
                self.params.names_brokenplaw = list(
                    set(self.params.names_brokenplaw) - set([p])
                )

            elif p in inp["normal_params"]:
                self.params.names_normal += [f"{p}_{s}" for s in range(inp["n_comps"])]
                self.params.names_normal = list(
                    set(self.params.names_normal) - set([p])
                )

            elif p in inp["doublenormal_params"]:
                self.params.names_doublenormal += [
                    f"{p}_{s}" for s in range(inp["n_comps"])
                ]
                self.params.names_doublenormal = list(
                    set(self.params.names_doublenormal) - set([p])
                )

            else:
                for s in range(inp["n_comps"]):
                    if (
                        f"{p}_{s}"
                        not in inp["plaw_params"]
                        + inp["smoothplaw_params"]
                        + inp["brokenplaw_params"]
                        + inp["normal_params"]
                        + inp["doublenormal_params"]
                    ):
                        self.params.names_inf += [
                            f"{p}_{s}",
                        ]

        self.params.names_inf_p = sorted(
            list(set([getp(ps) for ps in self.params.names_inf]))
        )
        self.params.names_notinf = (
            self.params.names_plaw
            + self.params.names_smoothplaw
            + self.params.names_brokenplaw
            + self.params.names_normal
            + self.params.names_doublenormal
        )

        # update and return inp
        inp["plaw_params"] = self.params.names_plaw
        inp["smoothplaw_params"] = self.params.names_smoothplaw
        inp["brokenplaw_params"] = self.params.names_brokenplaw
        inp["normal_params"] = self.params.names_normal
        inp["doublenormal_params"] = self.params.names_doublenormal

        return inp


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------


# -------------------------------------------------------------------
class ParameterSet:
    """Grid parameter set"""

    # -------------------------------------------
    def __init__(self, names=[], values=None):
        self.names = names
        if values is None:
            values = {n: [] for n in names}
        self.values = values
        self.update()
        return

    # -------------------------------------------
    def set_values(self, values):
        self.values = values

    # -------------------------------------------
    def drop(self, p):
        del self.values[p]
        self.update()

    # -------------------------------------------
    def update(self):
        self.names = list(self.values.keys())
        self.n = len(self.names)

    # -------------------------------------------
    def n_params(self):
        self.update()  # just in case...
        return len(self.names)

    # -------------------------------------------
    def upsample_memtest(self, upsample_factor):
        return self.n * np.prod(
            [upsample_factor * (len(self.values[p]) - 1) + 1 for p in self.values]
        )

    # -------------------------------------------
    def upsample(self, upsample_factor, inp, p=None):
        if p is not None:
            return upsample1d(np.arange(len(self.values[p])), upsample_factor, inp)
        else:
            return [
                upsample1d(np.arange(len(self.values[p])), upsample_factor, inp)
                for p in self.names
            ]

    # -------------------------------------------
    def interp_idx2vals(self, p):
        return interp1d(
            np.arange(len(self.values[p])), self.values[p], fill_value="extrapolate"
        )

    # -------------------------------------------
    def interp_vals2idx(self, p):
        return interp1d(
            self.values[p], np.arange(len(self.values[p])), fill_value="extrapolate"
        )

    # -------------------------------------------
    def interp_t_vals2idx(self, p, x):

        tmp = tt.as_tensor_variable(self.values[p])
        # i = tt.extra_ops.searchsorted(tmp, x) - 1  # searchsorted returns floor + 1
        # simpler alternative (searchsorted op not yet implemented for jaxify)
        i = tt.sum(((tmp - x) < 0))
        i = tt.clip(i, 0, tmp.shape[0] - 2)
        return tt.clip(
            i + (x - tmp[i]) / (tmp[i + 1] - tmp[i]), 0, len(self.values[p]) - 1
        )

    # -------------------------------------------
    def interp_t_idx2vals(self, p, i):
        tmp = tt.as_tensor_variable(self.values[p])
        lower = tt.cast(tt.clip(tt.floor(i), 0, len(self.values[p]) - 2), "int16")
        itmp = tt.clip(i, 0, len(self.values[p]) - 1)  # comment to extrapolate
        return (lower + 1 - itmp) * tmp[lower] + (itmp - lower) * tmp[lower + 1]


# -------------------------------------------------------------------
# import pymc3 as pm
# import theano.tensor as tt
# class InvOrdered(pm.distributions.transforms.ElemwiseTransform):
#     name = "ordered"

#     def backward(self, y):
#         x = tt.zeros(y.shape)
#         x = tt.inc_subtensor(x[..., 0], y[..., 0])
#         x = tt.inc_subtensor(x[..., 1:], tt.exp(y[..., 1:]))
#         return -tt.cumsum(x, axis=-1)

#     def forward(self, x):
#         y = tt.zeros(x.shape)
#         y = tt.inc_subtensor(y[..., 0], x[..., 0])
#         y = tt.inc_subtensor(y[..., 1:], tt.log(-x[..., 1:] + x[..., :-1]))
#         return y

#     def forward_val(self, x, point=None):
#         y = np.zeros_like(x)
#         y[..., 0] = x[..., 0]
#         y[..., 1:] = np.log(-x[..., 1:] + x[..., :-1])
#         return y

#     def jacobian_det(self, y):
#         return tt.sum(y[..., 1:], axis=-1)
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------


# -----------------------------------------------------------------
class DelayedKeyboardInterrupt(object):
    def __enter__(self):
        self.signal_received = False
        self.old_handler = signal.getsignal(signal.SIGINT)
        signal.signal(signal.SIGINT, self.handler)

    def handler(self, sig, frame):
        self.signal_received = (sig, frame)
        logging.debug("SIGINT received. Delaying KeyboardInterrupt.")

    def __exit__(self, type, value, traceback):
        signal.signal(signal.SIGINT, self.old_handler)
        if self.signal_received:
            self.old_handler(*self.signal_received)


# --------------------------------------


def getp(ps):
    "from <param>_<sector> get only <param>"
    if re.search(".*_[0-9]", ps) is None:
        return ps
    else:
        return "_".join(ps.split("_")[:-1])
