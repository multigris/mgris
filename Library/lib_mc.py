import os
from shutil import move
import sys
import re

import pandas as pd
import logging
from copy import deepcopy
from tqdm import tqdm
import gc

from itertools import permutations, combinations
import more_itertools

# from astropy import units as u
# from scipy import interpolate
from math import floor, ceil, comb
from Library.lib_misc import *

# from Library.lib_main import logappend
# from Library.lib_plots import *

import scipy.stats as st
from scipy.stats import spearmanr, pearsonr

try:
    from scipy.stats import median_absolute_deviation
except:
    from scipy.stats import median_abs_deviation

import arviz as az
import xarray

# global parameters
from rcparams import rcParams

if rcParams["pmversion"] == 5:
    import pymc as pm
    import pytensor.tensor as tt
elif rcParams["pmversion"] == 4:
    import pymc as pm
    import aesara.tensor as tt
elif rcParams["pmversion"] == 3:
    import pymc3 as pm
    import theano.tensor as tt

    # from theano.tensor.shared_randomstreams import RandomStreams

import numpy as np

try:
    import jax.numpy_test as jnp
except:
    import numpy as jnp

# from Library.lib_plots import plot_hist
import matplotlib.pyplot as plt
import seaborn as sns

# try:
#    import cPickle as pickle
# except ImportError:
#    import pickle

# matplotlib produces way too many warnings...
import warnings

warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
def process_line_general(inp, line, params, res, verbose=False):
    """
    Parse priors for power-law/normal parameters
    """
    line = [l.strip() for l in line]

    # parameter
    p = line[0].split("\t")[0]

    set_params, set_params_replace = None, None
    constraints, constraints_replace = None, None
    value, sdev, pmin, pmax = None, None, None, None

    operation = line[1]

    if "lowerbound_" in p or "upperbound_" in p:
        # for these we need indices

        # p2 = p.split("_")[1]
        p2 = getp("_".join(p.split("_")[1:]))

        # set right parameter name for priors
        p = "idx_" + p

        if operation == "==":
            equation = "".join(line[2:])  # no need to replace the ()
            # replace with idxs
            equation = re.sub(
                "([a-z,A-Z]*)_([a-z,A-Z]*)_([0-9]*)",
                "grid.params.interp_t_idx2vals('\\2', model['idx_\\1_\\2_\\3'])",
                # "model['\\1_\\2_\\3']",
                equation,
            )
            equation = f"grid.params.interp_t_vals2idx('{p2}', {equation})"
            # equation = equation.replace("*", "+").replace(
            #     "/", "-"
            # )  # forcing log, temporary
            res["set_params_replace"] += [
                p,
            ]
            constraints_replace = {p: "pm.Deterministic('{}', {})".format(p, equation)}
            res["constraints_replace"].update(constraints_replace)
            # logging.info(constraints_replace[p])
            return res
        else:
            # for these we have real values
            value = params.interp_vals2idx(p2)(float(line[2]))
            if len(line) > 3:
                sdev = np.mean(
                    abs(
                        np.array(
                            [
                                params.interp_vals2idx(p2)(
                                    float(line[2]) + float(line[3])
                                ),
                                params.interp_vals2idx(p2)(
                                    float(line[2]) - float(line[3])
                                ),
                            ]
                        )
                        - value
                    )
                )
            if len(line) > 4:
                pmin = params.interp_vals2idx(p2)(float(line[4]))
            if len(line) > 5:
                pmax = params.interp_vals2idx(p2)(float(line[5]))
        # in main program we take care of bounds

    else:
        if operation == "==":
            equation = "".join(line[2:])
            # not boundaries: replace with vals
            equation = re.sub(
                "([a-z,A-Z]*)_([a-z,A-Z]*)_([0-9]*)",
                "model['\\1_\\2_\\3']",
                equation,
            )
            # equation = equation.replace("*", "+").replace(
            #     "/", "-"
            # )  # forcing log, temporary
            res["set_params_replace"] += [
                p,
            ]
            constraints_replace = {p: "pm.Deterministic('{}', {})".format(p, equation)}
            res["constraints_replace"].update(constraints_replace)
            # logging.info(constraints_replace[p])
            return res
        else:
            # for these we have real values
            value = float(line[2])
            if len(line) > 3:
                sdev = float(line[3])
            if len(line) > 4:
                pmin = float(line[4])
            if len(line) > 5:
                pmax = float(line[5])

    # =================================
    # go back to parsing classic priors

    # normal distribution
    if operation == "=" and value is not None and sdev is not None:
        if pmin is None and pmax is None:
            # if inp["type_constraints"] == "indices":
            #     pmin = "0"
            #     pmax = "len(grid.params.values[p])-1"
            # else:
            #     pmin = "np.min(grid.params.values[p])"
            #     pmax = "np.max(grid.params.values[p])"
            set_params_replace = p
            # constraints_replace = {
            #     p: "pm.TruncatedNormal('{}', mu={}, sigma={}, lower={}, upper={}, testval={})".format(
            #         p, value, sdev, pmin, pmax, value
            #     )
            # }
            constraints_replace = {
                p: "pm.Normal('{}', mu={}, sigma={}, testval={}, dtype='{}')".format(
                    p, value, sdev, value, inp["precision"]
                )
            }
        else:
            set_params_replace = p
            if value < pmax and value > pmin:
                testval = value
            elif value == pmin:
                testval = value + sdev
            elif value == pmax:
                testval = value - sdev
            else:
                testval = 0.5 * (pmin + pmax)
            constraints_replace = {
                p: "pm.TruncatedNormal('{}', mu={}, sigma={}, lower={}, upper={}, testval={}, dtype='{}')".format(
                    p, value, sdev, pmin, pmax, testval, inp["precision"]
                )
            }

    # fixed value
    if operation == "=" and value is not None and sdev is None:
        set_params_replace = p
        constraints_replace = {
            p: "pm.Deterministic('{}', theano.shared({}))".format(p, value)
        }

    rv = "model['{}']".format(p)

    # wrt other value
    op = (
        operation.replace(">=", "ge")
        .replace("<=", "le")
        .replace(">", "gt")
        .replace("<", "lt")
    )
    if operation in (">", ">=", "<", "<=") and value is not None:
        set_params = p
        constraints = {
            p: [
                "pm.Potential('prior_{}', tt.switch(tt.{}({}, {}), 0, infcode))".format(
                    p, op, rv, value
                ),
            ]
        }

    if verbose:
        logging.debug(
            "set_params={} set_params_replace={} constraints={} constraints_replace={}".format(
                set_params, set_params_replace, constraints, constraints_replace
            )
        )

    # update res
    if set_params is not None:
        res["set_params"] += [
            set_params,
        ]

    if set_params_replace is not None:
        res["set_params_replace"] += [
            set_params_replace,
        ]

    if constraints is not None:
        res["constraints"].update(constraints)

    if constraints_replace is not None:
        res["constraints_replace"].update(constraints_replace)

    return res


# -------------------------------------------------------------------
def process_line_sectors(inp, line, params, res, verbose=False):
    """
    Parse priors for indices/real values
    """

    n_comps = inp["n_comps"]
    groups = inp["groups"]
    line = [l.strip() for l in line]

    # parameter
    p = line[0].split("\t")[0]
    if p in ("n_comps", "select", "groups", "#"):
        return res

    # no sectors for these
    if (
        (
            inp["plaw_params"] != []
            and ("alpha_" in p or "lowerbound_" in p or "upperbound_" in p)
        )
        or (
            inp["smoothplaw_params"] != []
            and (
                "alpha_" in p
                # or "lowercut_" in p
                # or "uppercut_" in p
                # or "lowerbound_" in p
                # or "upperbound_" in p
                # or "lowerfactor_" in p
                # or "upperfactor_" in p
                or "lowerbound_" in p
                or "upperbound_" in p
                or "lowerscale_" in p
                or "upperscale_" in p
            )
        )
        or (
            inp["brokenplaw_params"] != []
            and (
                "alpha1_" in p
                or "alpha2_" in p
                or "pivot_" in p
                or "lowerbound_" in p
                or "upperbound_" in p
            )
        )
        or (
            inp["normal_params"] != []
            and (
                "mu_" in p or "sigma_" in p or "lowerbound_" in p or "upperbound_" in p
            )
        )
        or (
            inp["doublenormal_params"] != []
            and (
                "mu1_" in p
                or "sigma1_" in p
                or "mu2_" in p
                or "sigma2_" in p
                or "ratio21_" in p
                or "lowerbound_" in p
                or "upperbound_" in p
            )
        )
    ):
        logging.info(
            f"\nParameter {p} follows a distribution, switching to alternative prior parsing function"
        )
        logging.info(line)
        res = process_line_general(inp, line, params, res, verbose=False)
        return res  # stops here

    # from now on we have sectors to care about (params_inf)

    allparams = params.names + ["scale", "scale_eff", "w"]

    if p not in allparams:
        logging.warning(
            f"/!\\ Parameter {p} not found in grid, maybe it was dropped with hard constraints?"
        )
        return res

    if verbose:
        logging.info("")
        logging.info("Processing:")
        logging.info(line)

    set_params, set_params_replace = None, None
    constraints, constraints_replace = None, None
    constraints_chi2 = None
    sculpted = False
    real_value, sdev, pmin, pmax = None, None, None, None

    # operations for params that are global (not per sector)
    if p in ("scale", "scale_eff"):
        # operation type
        operation = line[1]

        ps = p
        rv = "model['{}']".format(ps)

        idx_value = float(line[2])
        real_value = idx_value
        if len(line) > 3:
            idx_sdev = float(line[3])
            sdev = idx_sdev
        # if len(line) > 4:
        #     idx_pmin = float(line[4])
        #     pmin = idx_pmin
        # if len(line) > 5:
        #     idx_pmax = float(line[5])
        #     pmax = idx_pmax

        s, s2 = 0, 0  # for chi2 string

    else:
        # operation type
        operation = line[2]

        # expand sectors?
        if "(" not in line[1] and ")" not in line[1]:
            panic("/!\\ Have you set the sector number in the prior between ()?")

        if line[1][1:][0] == "*":
            if len(line) > 3:  # duplicate line for each sector
                for s in range(n_comps):
                    newline = " ".join(line)
                    newline = newline.replace("*", str(s)).split(" ")
                    if verbose:
                        logging.info("New call")
                    res = process_line_sectors(
                        inp, newline, params, res, verbose=verbose
                    )
            else:  # age (*) = ---> age (1) = (0), age (2) = (0) etc...
                for s in range(1, n_comps):
                    newline = " ".join(line + ["(0)"]).replace("*", str(s)).split(" ")
                    if verbose:
                        logging.info("New call")
                    res = process_line_sectors(
                        inp, newline, params, res, verbose=verbose
                    )
            return res

        if p == "w":
            # get sector
            s = int(line[1].replace("(", "").replace(")", ""))
            ps = "{}_{}".format(p, s)
            rv = "model['{}'][{}]".format(p, s)

            # parse extra arguments
            s2 = None
            if len(line) > 3 and operation != "==":
                if "(" in line[3]:  # wrt other sector
                    s2 = line[3].replace("(", "").replace(")", "")
                    ps2 = "{}_{}".format(p, s2)
                    rv2 = "model['{}'][{}]".format(p, s2)
                    idx_value = np.nan
                else:  # value
                    real_value = float(line[3])
                    idx_value = real_value
                    if len(line) > 4:
                        sdev = float(line[4])
                        idx_sdev = sdev
                    else:
                        idx_sdev = sdev
        else:
            # get sector
            s = int(line[1].replace("(", "").replace(")", ""))
            ps = (
                "idx_{}_{}".format(p, s)
                if inp["type_constraints"] == "indices"
                else "{}_{}".format(p, s)
            )
            rv = "model['{}']".format(ps)

            # parse extra arguments
            if inp["type_constraints"] == "indices":
                s2 = None
                if len(line) > 3 and operation != "==":
                    if "(" in line[3]:  # wrt other sector
                        s2 = line[3].replace("(", "").replace(")", "")
                        ps2 = "idx_{}_{}".format(p, s2)
                        rv2 = "model['{}']".format(ps2)
                        idx_value = 0.5 * len(params.values[p])  # weakly informative
                    else:  # value
                        try:  # mu is a real number
                            real_value = float(line[3])
                            idx_value = params.interp_vals2idx(p)(real_value)
                            if len(line) > 4:
                                sdev = float(line[4])
                                idx_sdev = np.mean(
                                    abs(
                                        np.array(
                                            [
                                                params.interp_vals2idx(p)(
                                                    real_value + sdev
                                                ),
                                                params.interp_vals2idx(p)(
                                                    real_value - sdev
                                                ),
                                            ]
                                        )
                                        - idx_value
                                    )
                                )
                            else:
                                # weakly informative
                                idx_sdev = 0.5 * len(params.values[p])
                            if len(line) > 5:
                                pmin = float(line[5])
                                idx_pmin = max([0, params.interp_vals2idx(p)(pmin)])
                            # else:
                            #    pmin = "np.min(grid.params.values[p])"
                            #    idx_pmin = "0"
                            if len(line) > 6:
                                pmax = float(line[6])
                                idx_pmax = min(
                                    [
                                        len(params.values[p]) - 1,
                                        params.interp_vals2idx(p)(pmax),
                                    ]
                                )
                            # else:
                            #    pmax = "np.max(grid.params.values[p])"
                            #    idx_pmax = "len(grid.params.values[p])-1"
                        except:  # mu is a model parameter
                            logging.warning(f"Found hyperparameter {line[3]}")
                            real_value, sdev = 0, 0  # test real_value==None later
                            idx_value = f"grid.params.interp_t_vals2idx('{p}', model['{line[3]}'])"
                            # temporary: cannot be a parameter yet, choosing weakly informative prior around mean
                            idx_sdev = 0.5 * len(params.values[p])
            else:
                real_value, sdev = None, None
                s2 = None
                if len(line) > 3 and operation != "==":
                    if "(" in line[3]:  # wrt other sector
                        s2 = line[3].replace("(", "").replace(")", "")
                        ps2 = "{}_{}".format(p, s2)
                        rv2 = "model['{}']".format(ps2)
                    else:  # value
                        try:  # mu is a real number
                            real_value = float(line[3])
                            if len(line) > 4:
                                sdev = float(line[4])
                            if len(line) > 5:
                                pmin = float(line[5])
                            if len(line) > 6:
                                pmax = float(line[6])
                        except:  # mu is a model parameter
                            logging.warning(f"Found hyperparameter {line[3]}")
                            real_value = f"model['{line[3]}']"
                            # temporary: cannot be a parameter yet, choosing weakly informative prior around mean

    if verbose:
        logging.debug(
            "p={}, s={} val={} sdev={} s2={}".format(p, s, real_value, sdev, s2)
        )

    # ---------------
    # boundaries
    testval = None
    if pmin is not None and pmax is not None:
        if inp["type_constraints"] == "indices":
            boundaries = f", lower={idx_pmin}, upper={idx_pmax}"
            testval = f"0.5*(idx_pmin+idx_pmax)"
        else:
            boundaries = f", lower={pmin}, upper={pmax}"
            testval = f"0.5*(pmin+pmax)"
    else:
        if inp["type_constraints"] == "indices":
            boundaries = ", lower=0, upper=len(grid.params.values[p])-1"
            testval = f"0.5*(len(grid.params.values[p])-1)"
        else:
            boundaries = ", lower=np.min(grid.params.values[p]), upper=np.max(grid.params.values[p])"
            testval = f"np.mean(grid.params.values[p])"
    if "w_" in ps:
        boundaries = ", lower=0, upper=1"
        testval = "0.5"
    if ps in ("scale", "scale_eff"):
        boundaries = ""

    # ==================================
    # operations
    # first, constraints that will replace the default TruncatedNormal
    # literal equation
    if operation == "==":
        set_params_replace = ps
        # equation = "".join(line[3:]).replace("(", "_").replace(",", "").replace(")", "")
        equation = "".join(line[3:])
        equation = re.sub(r"\(([0-9])\)", "_\\1", equation)
        logging.info(equation)

        if inp["type_constraints"] == "indices":
            equation = re.sub(
                r"([a-z,A-Z]*)_([0-9]*)",
                "grid.params.interp_t_idx2vals('\\1', model['idx_\\1_\\2'])",
                equation,
            )
        else:
            equation = re.sub(
                "([a-z,A-Z]*)_([0-9]*)",
                "model['\\1_\\2']",
                equation,
            )
        # equation = equation.replace("*", "+").replace(
        #    "/", "-"
        # )  # forcing log, temporary
        if inp["type_constraints"] == "indices":  # convert back to idx
            constraints_replace = {
                ps: "pm.Deterministic('{}', grid.params.interp_t_vals2idx('{}', {}))".format(
                    ps, p, equation
                )
            }
        else:
            constraints_replace = {
                ps: "pm.Deterministic('{}', {})".format(ps, equation)
            }

        constraints_chi2 = [None]
        sculpted = True  # priors should make it possible to distinguish and identify sectors, so prior on mixing weights could be relaxed

    # normal distribution
    if operation == "=" and real_value is not None and sdev is not None:
        set_params_replace = ps
        if p in inp["discrete_params"]:  # inp['inference_idx_discrete']:
            new = inp["inference_upsample"] * len(params.values[p]) - 1
            fac = (len(params.values[p]) - 1) / (new - 1)
            parr = 10 ** np.array(
                [
                    pm.Normal.dist(
                        mu=idx_value / fac, sd=idx_sdev / fac, dtype=inp["precision"]
                    )
                    .logp(i)
                    .eval()
                    for i in np.arange(new)
                ]
            )  # np.ones(new)
            constraints_replace = {
                ps: "pm.Deterministic('{}', tt.min([len(grid.params.values[p])-1., {}*pm.Categorical('cat_{}', p={}, testval=round({}/{}))]))".format(
                    ps, fac, ps, arr2str(parr), idx_value, fac
                )
            }
        else:
            if testval is None:
                testval = idx_value
            constraints_replace = {
                ps: "pm.TruncatedNormal('{}', mu={}, sigma={}{}, testval={}, dtype='{}')".format(
                    ps, idx_value, idx_sdev, boundaries, testval, inp["precision"]
                )
            }
        constraints_chi2 = [None]
        sculpted = True  # priors should make it possible to distinguish and identify sectors, so prior on mixing weights could be relaxed

    # same as other sector
    if operation == "=" and real_value is None:
        set_params_replace = ps
        constraints_replace = {ps: "pm.Deterministic('{}',  {})".format(ps, rv2)}
        # constraints_replace = {ps: "X{}".format(ps2)}
        constraints_chi2 = [
            p,
            s,
            "|" + operation,
            s2,
        ]  # | is to know it's wrt other sector
        sculpted = False

    # fixed value --> better to use range in input file or to use Normal distribution!
    if operation == "=" and real_value is not None and sdev is None:
        set_params_replace = ps
        if inp["type_constraints"] == "indices":
            constraints_replace = {
                ps: "pm.Deterministic('{}', theano.shared({}))".format(ps, idx_value)
            }
        else:
            constraints_replace = {
                ps: "pm.Deterministic('{}', theano.shared({}))".format(ps, real_value)
            }
        # constraints_replace = {ps: "Y{}".format(idx_value)}
        constraints_chi2 = [p, s, operation, real_value]
        sculpted = True  # priors should make it possible to distinguish and identify sectors, so prior on mixing weights could be relaxed

    # ==================================
    # then simple ones with Potential

    op = (
        operation.replace(">=", "ge")
        .replace("<=", "le")
        .replace(">", "gt")
        .replace("<", "lt")
    )

    # wrt other sector
    if operation in (">", ">=", "<", "<=") and real_value is None:
        set_params = ps
        constraints = {
            ps: [
                "pm.Potential('prior_{}', tt.switch(tt.{}({}, {}), 0, infcode))".format(
                    ps, op, rv, rv2
                ),
            ]
        }
        constraints_chi2 = [
            p,
            s,
            "|" + operation,
            s2,
        ]  # | is to know it's wrt other sector
        sculpted = True  # priors should make it possible to distinguish and identify sectors, so prior on mixing weights could be relaxed

    # wrt other value
    if operation in (">", ">=", "<", "<=") and real_value is not None:
        set_params = ps
        constraints = {
            ps: [
                "pm.Potential('prior_{}', tt.switch(tt.{}({}, {}), 0, infcode))".format(
                    ps, op, rv, idx_value
                ),
            ]
        }
        constraints_chi2 = [p, s, operation, real_value]
        sculpted = True  # priors should make it possible to distinguish and identify sectors, so prior on mixing weights could be relaxed

    if verbose:
        logging.debug(
            "set_params={} set_params_replace={} constraints={} constraints_replace={} constraints_chi2={}".format(
                set_params,
                set_params_replace,
                constraints,
                constraints_replace,
                constraints_chi2,
            )
        )

    # update res
    if set_params is not None:
        res["set_params"] += [
            set_params,
        ]

    if set_params_replace is not None:
        res["set_params_replace"] += [
            set_params_replace,
        ]

    if constraints_chi2 is not None:
        res["constraints_chi2"] += [
            constraints_chi2,
        ]

    if constraints is not None:
        res["constraints"].update(constraints)

    if constraints_replace is not None:
        res["constraints_replace"].update(constraints_replace)

    res["sculpted"] += sculpted
    return res


# -------------------------------------------------------------------
def parse_configuration(inp, lines, params, verbose=False):
    """
    Parse configuration found in readconfiguration
    """
    # no need to read again n_sectors

    # first we get the first line appearing because between the pre-defined configuration and the input configuration there could be duplicates
    # the pre-defined configuration block is appended to input file so any
    # custom configuration line will precede
    # lines = unique(lines)
    # choosing first instance:
    tmp = []
    for l in lines:
        if (
            (l.split(" ")[0:2] not in [t.split(" ")[0:2] for t in tmp])
            and (l.split(" ")[0] != "distrib")
            and l.strip() != ""
        ):
            tmp += [
                l,
            ]
    lines = tmp

    res = {
        "set_params": [],
        "constraints": {},
        "set_params_replace": [],
        "constraints_replace": {},
        "constraints_chi2": [],
        "sculpted": False,
    }

    # test if constraint on all parameters
    for i in np.arange(0, len(lines)):
        if (
            "all" in lines[i] and "=" in lines[i]
        ):  # all is set ==> all parameters will be affected by the constraint
            tmp = lines[i].split(" ")
            for p in params.names:
                lines += [
                    "{} {} {}".format(p, tmp[1], tmp[2]),
                ]
            del lines[i]

    # go through all constraints
    for i in np.arange(0, len(lines)):
        line = lines[i].split(" ")
        if "(" in line[0] and ")" in line[0]:
            panic(
                "A space seem to be missing in the prior definition [<parameter name><space>(<component>)<space><equation or value>]"
            )
        res = process_line_sectors(inp, line, params, res, verbose=verbose)
        # if inp['type_constraints']=='indices':
        #     res = process_line_idx(inp, line, params, res, verbose=verbose)
        # else:
        #     res = process_line_real(inp, line, params, res, verbose=verbose)

    if verbose:
        logging.debug(res)

    return res


# -------------------------------------------------------------------
def readconfiguration(inp, input_file_lines, params, verbose=False):
    """
    Read configuration blocks from input file + pre-defined configuration file
    """

    # read *all* configuration blocks
    tmp = []
    start = [
        i + 1 for i, l in enumerate(input_file_lines) if "BEGIN configuration" in l
    ]
    if len(start) > 0:
        for startn in start:
            logging.info(
                "Configuration block found: {}...".format(input_file_lines[startn - 1])
            )
            end = startn + np.where(np.array(input_file_lines[startn:]) == "END")[0][0]
            tmp += list(input_file_lines[startn:end])

    if tmp == []:
        return None
    else:
        return parse_configuration(inp, tmp, params, verbose=verbose)


# -------------------------------------------------------------------
def mbsize(n_elements, precision):
    """
    Size in Mb
    """
    return n_elements * 1e-6 * int(precision) / 8


# -------------------------------------------------------------------
def calc_ci(inp, est, obs=0):
    """
    Calculate confidence interval
    """

    tmp1 = [
        1 - i
        for i in est["hpdi_all"].keys()
        if np.sign(est["hpdi_all"][i].to_array().data[0][0] - obs)
        == np.sign(est["hpdi_all"][i].to_array().data[0][1] - obs)
    ]
    # tmp1 = [1-i for i in est['hpdi_all'].keys() if 0<est['hpdi_all'][i].to_array().data[0][0]-obs or 0>est['hpdi_all'][i].to_array().data[0][1]-obs]
    if tmp1 == []:
        tmp = np.mean([100 * inp["ci_arr"][-1], 100])
    else:
        tmp = np.mean([100 * tmp1[np.argmin(tmp1)], 100 * tmp1[np.argmin(tmp1) - 1]])
        # tmp = 100*tmp1[np.argmin(tmp1)]
    return tmp, f"{tmp: <5,.2f} | {100-tmp: <5,.2f}"


# -------------------------------------------------------------------
def estformat(
    inp,
    estimators,
    namesets,
    kind=["median", "hpdi"],
    justmedian=False,
    true_params=None,
    observations=None,
    pvalue=None,
    idx=None,
):
    """
    Format result for terminal and result text file
    """

    # kind = ['mean', 'hpdi']
    # kind = ['robust_mean', 'robust_stdev']
    # kind = ['mu', 'lambda], 'tau']

    if pvalue is None:
        pvalue = np.nan

    altkind = "mean" if kind[0] == "median" else "median"
    s = ""
    if observations is not None:
        fieldlength = int(np.max([len(o) for o in observations.names]))
    else:
        fieldlength = 16

    for j, names in enumerate(namesets):
        s += "   " + 62 * "."
        s += "\n"
        for i, n in enumerate(names):
            s += f"   {n: <{fieldlength}} : "

            best = float(estimators[n][kind[0]])
            alt = float(estimators[n][altkind])
            interval = estimators[n][kind[1]]

            if "single_normalizing" in inp.keys():
                if observations is not None and inp["single_normalizing"] != "":
                    norm = float(estimators[inp["single_normalizing"]][kind[0]])
                    # increase ci until 0 is in range
                    # redundance with obs block below
                    if (
                        n not in observations.obs
                    ):  # that's for disabled obs sets with predict kw
                        ind = np.where(np.array(observations.obsnames) == n)[0]
                        if len(ind) == 0:
                            continue
                        if idx is None:
                            tmpobs = observations.obs[observations.names[ind[0]]]
                        else:
                            tmpobs = observations.obs[observations.names[ind[idx]]]
                    else:
                        tmpobs = observations.obs[n]
                    if tmpobs.obs_valuescale == "linear":
                        best /= norm
                        alt /= norm
                        interval /= norm
                    else:
                        best -= norm
                        alt -= norm
                        interval -= norm

            if "robust_stdev" in kind:
                interval = np.array([-interval[0], interval[0]])
                interval += best
            elif "lambda" in kind:
                interval = np.array(
                    [-interval[0], interval[0] * estimators[n][kind[2]][0]]
                )  # -lambda / +lambda*tau
                interval += best

            # if observations is not None and inp['obs_valuescale']=='linear':
            #    interval = 10**best*10**(interval-best)
            #    best = 10**best
            #    med = 10**med

            if true_params is not None:
                s += f" (true={true_params: <5,.2e})"

            if justmedian:
                s += f" {alt: <5,.2e}"
            else:
                # skip model columns if several obs for the same tracer
                if observations is None or idx is None:
                    s += f" ({alt: <9,.2e}) {best: <9,.2e} +{interval[1]-best: <9,.2e} -{best-interval[0]: <9,.2e}"
                else:
                    if inp["sysunc"] is not None:
                        sysunc = []
                        for s1 in inp["sysunc"]:
                            for s2 in inp["sysunc"][s1][2]:
                                sysunc += [
                                    s2,
                                ]
                    else:
                        sysunc = []

                    if idx == 0 or n in sysunc:
                        s += f" ({alt: <9,.2e}) {best: <9,.2e} +{interval[1]-best: <9,.2e} -{best-interval[0]: <9,.2e}"
                    else:
                        s2 = ""
                        s += f"  {s2:9}  {s2:9}  {s2:9}  {s2:9}"

            if observations is not None:
                # increase ci until 0 is in range
                # redudant with mod block above
                if (
                    n not in observations.obs
                ):  # that's for disabled obs sets with predict kw
                    ind = np.where(np.array(observations.obsnames) == n)[0]
                    if len(ind) == 0:
                        continue
                    if idx is None:
                        tmpobs = observations.obs[observations.names[ind[0]]]
                    else:
                        tmpobs = observations.obs[observations.names[ind[idx]]]
                    pvalue = np.nan
                else:
                    tmpobs = observations.obs[n]

                # ci_val, ci_str = calc_ci(inp, estimators[n], obs=tmpobs.value)

                # s += f' | Obs. {tmp_lim}{observations.obs[n].value: <5,.2f} +{observations.obs[n].delta[1]: <5,.2f} -{observations.obs[n].delta[0]: <5,.2f} (\u0394 {tmp_lim} {observations.obs[n].value-best: <5,.2f}) | p-value={pvalue: <5,.3e} ({pvalue2pc(pvalue): <5,.2f}) {pvalue_flag} | {ci_str}' #> to right-justify, < to left-justify

                if "obs_valuescale" in dir(tmpobs):
                    if tmpobs.obs_valuescale == "linear":
                        # print(n, '->linear')
                        tmpobs.value = 10**tmpobs.value

                if ~np.isfinite(tmpobs.delta[0]):
                    tmpobs.delta[0] = 0
                if ~np.isfinite(tmpobs.delta[1]):
                    tmpobs.delta[1] = 0

                if tmpobs.delta[0] == 0:
                    nsigma = 0  # np.inf
                else:
                    nsigma = (
                        (tmpobs.value - best) / tmpobs.delta[0]
                        if tmpobs.value - best < 0
                        else (tmpobs.value - best) / tmpobs.delta[1]
                    )
                    # --- Delta0
                    #  |
                    #  |
                    #  *obs
                    #  |
                    #  |
                    # --- Delta1

                pvalue_flag = " "
                if tmpobs.upper:
                    tmp_lim = "<"
                    pvalue_flag = "*" if pvalue < inp["pvalue_threshold"][0] else " "
                    tmpobs.delta = [0, 0]
                    nsigma = best / tmpobs.value
                elif tmpobs.lower:
                    tmp_lim = ">"
                    pvalue_flag = "*" if pvalue > inp["pvalue_threshold"][1] else " "
                    tmpobs.delta = [0, 0]
                    nsigma = 0
                else:
                    tmp_lim = "="
                    if (
                        pvalue < inp["pvalue_threshold"][0]
                        or pvalue > inp["pvalue_threshold"][1]
                    ):
                        pvalue_flag = "*"

                if "single_normalizing" in inp.keys():
                    if inp["single_normalizing"] != "":
                        norm = float(estimators[inp["single_normalizing"]][kind[0]])
                        if tmpobs.obs_valuescale == "linear":
                            tmpobs.value /= norm
                            tmpobs.delta = np.array(tmpobs.delta) / norm
                        else:
                            tmpobs.value -= norm
                            tmpobs.delta = np.array(tmpobs.delta) / norm

                s += f" | {tmp_lim}{tmpobs.value: <9,.2e} +{tmpobs.delta[0]: <9,.2e} -{tmpobs.delta[1]: <9,.2e} (\u0394 {tmp_lim} {tmpobs.value-best: <9,.2e}; {nsigma: <6,.2f}\u03C3) | {100*pvalue: <6,.3f} {pvalue_flag} "  # > to right-justify, < to left-justify

            # add \n when we have a list, not needed if single element
            # if i < len(names) - 1 and len(names) > 1:
            #    s += "\n"

            if len(names) > 1:
                s += "\n"

    return s


# -------------------------------------------------------------------
def symlog(x):
    """
    Log, including for <0
    """

    return [np.sign(xx) * np.log10(abs(xx)) for xx in x]


# -------------------------------------------------------------------
def ppc_calc(ppc, inp, success=True):
    """
    Posterior predictive check
    """

    norm_hist = True
    # cmap = sns.set_palette("husl", inp['n_obs'])
    from matplotlib import cm

    cm_ss = np.linspace(0, 1, inp["observations"].n)
    colors = [
        cm.tab20(x) for x in cm_ss
    ]  # qualitative cmap: https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html

    fig, ax = plt.subplots(1, 1, figsize=(8, 8))

    # calculating z-scores
    # logging.info(ppc.keys())

    # in p.sample_posterior, we can choose y_ parameters (Student T) or directly the parameters called observables[o], but it's not the same thing. Here we test the deviation from a Student-T distribution

    # sig = [np.sqrt(np.sum((ppc[o]-np.mean(ppc[o]))**2)/(len(ppc[o])-1)) for i,o in enumerate(inp['observables']) if i in inp['mask_obs_detected']]
    # mean = [np.mean(ppc[o]) for i,o in enumerate(inp['observables']) if i in inp['mask_obs_detected']]
    tmp = np.array(
        [
            (
                10 ** ppc[o].data[0] * 10 ** inp["order_of_magnitude_obs"][0]
                - inp["observations"].values[i]
            )
            / np.mean(inp["observations"].delta[i])
            # (
            #     np.sqrt(
            #         np.sum((10 ** ppc[o].data[0] - np.mean(10 ** ppc[o].data[0])) ** 2)
            #         / (len(ppc[o].data[0]) - 1)
            #     )
            # )
            for i, o in enumerate(inp["observations"].names)
            if o in inp["observations"].mask_obs_detected and o in ppc.keys()
        ]
    )
    if len(tmp) == 0:
        print("Pb...")
        return

    # keep commented code of we wanna do an offset to 0
    # tmp = np.array([ppc[o]/(np.sum([(ppc[o]-inp['observed'][i])**2 for i,o in enumerate(inp['observables']) if i in inp['mask_obs_detected']])/(len(ppc[o])-1)) for i,o in enumerate(inp['observables']) if i in inp['mask_obs_detected']])

    tmplabels = [
        o
        for o in inp["observations"].names
        if o in inp["observations"].mask_obs_detected
    ]
    # individual KDEs
    bins = np.arange(-10, 10, 0.2)
    for i, t in enumerate(tmp):
        sns.distplot(
            t,
            norm_hist=norm_hist,
            hist=False,
            kde=True,
            bins=bins,
            ax=ax,
            kde_kws={"alpha": 1},
            color=colors[i],
            label=tmplabels[i],
        )  # , kde_kws={'bw': 1},
        sns.distplot(
            t,
            norm_hist=norm_hist,
            hist=False,
            kde=True,
            bins=bins,
            ax=ax,
            kde_kws={"alpha": 0.1, "shade": True},
            color=colors[i],
        )  # , kde_kws={'bw': 1},
        # print(abs(t)<150)
        # sns.distplot(symlog(t[abs(t)<150]), norm_hist=norm_hist, hist=False, kde=True, ax=ax2, kde_kws={'alpha': 1}, color=colors[i], label=tmplabels[i])
        # sns.distplot(symlog(t[abs(t)<150]), norm_hist=norm_hist, hist=False, kde=True, ax=ax2, kde_kws={'alpha': 0.1, 'shade': True}, color=colors[i])

    # # global
    if success:
        tmp2 = tmp.flatten()
    else:
        tmp2 = np.max(tmp, axis=0)

    sns.distplot(
        tmp2,
        norm_hist=norm_hist,
        kde=False,
        bins=bins,
        ax=ax,
        color="gray",
        hist_kws={"alpha": 0.5},
    )
    ax.set(title="Posterior deviation", xlabel="Dev. [sigma]", ylabel="Frequency")

    ax.axvline(0, color="black")
    ax.set_xlim([bins.min(), bins.max()])

    ax.legend(loc="upper left")

    # inset
    ax2 = fig.add_axes([0.65, 0.68, 0.24, 0.18])
    ax2.set_xlim([-2, 2])
    sns.distplot(
        symlog(tmp2),
        norm_hist=norm_hist,
        kde=False,
        bins=np.arange(-2, 2, 0.1),
        ax=ax2,
        color="gray",
        hist_kws={"alpha": 0.5},
    )
    # ax2.set_yscale('log')
    ax2.axvline(0, color="black")
    # plt.ylim(1e-3, plt.ylim()[1])
    # ax2.legend(loc='upper left', bbox_to_anchor=(-2, 1)) #lower left corner of legend

    plotdir = inp["output_directory"] + "/Plots/"
    os.makedirs(plotdir, exist_ok=True)
    fig.savefig(plotdir + "ppc.pdf")

    tmp = [len(tmp2[abs(tmp2) < nsigma]) / len(tmp2) for nsigma in (1, 2, 3)]

    return tmp


# -------------------------------------------------------------------
def linearsum(ws, vals, dtype=rcParams["precision"]):
    """
    Simple linear sum making sure we get the right type at the end
    """

    return np.sum([ws[s] * np.float64(vals[s]) for s in np.arange(len(vals))]).astype(
        dtype
    )


# -------------------------------------------------------------------
def logsum_simple(logs, dtype=rcParams["precision"]):
    """
    Simple log sum avoiding large numbers making sure we get the right type at the end
    """

    if len(logs) == 2:
        tmp = np.logaddexp(logs[0] * np.log(10), logs[1] * np.log(10)) / np.log(10)
        return tmp.astype(rcParams["precision"])
    else:
        tmp = np.logaddexp(logs[0] * np.log(10), logs[1] * np.log(10)) / np.log(10)
        for i in range(2, len(logs)):
            tmp = np.logaddexp(tmp * np.log(10), logs[i] * np.log(10)) / np.log(10)
        return tmp.astype(rcParams["precision"])
        # return np.log10(
        #     np.nansum(10 ** (logs - np.nanmedian(logs)), axis=0)
        # ) + np.nanmedian(logs).astype(dtype)


# -------------------------------------------------------------------
def logsum(ws, logs, dtype=rcParams["precision"]):
    """
    Simple log sum making sure we get the right type at the end
    """

    return np.log10(
        np.sum(
            [ws[s] * np.power(10.0, np.float64(logs[s])) for s in np.arange(len(logs))],
            axis=0,
        )
    ).astype(dtype)


# -------------------------------------------------------------------
# def linearsum_t(ws, logs, dtype='float32'):
#    #return tt.log10(ws[0]) + logs[0] + tt.log10( 1 + tt.sum([ws[s]/ws[0] * tt.pow(10, logs[s] - logs[0]) for s in np.arange(1, len(ws))], dtype=dtype) )
#    return tt.sum([ws[s]*tt.pow(10, tt.cast(logs[s], 'float64')) for s in np.arange(len(ws))], dtype=dtype)


# -------------------------------------------------------------------
def logsum_t(ws, logs, dtype=rcParams["precision"]):
    """
    Log sum with tensors
    """
    return tt.cast(
        tt.log10(
            tt.sum(
                [
                    ws[s] * tt.pow(10.0, tt.cast(logs[s], "float64"))
                    for s in np.arange(len(logs))
                ]
            )
        ),
        dtype,
    )


# # -------------------------------------------------------------------
# # linear
# def make_interp_f2(inp, verbose=False):
#     params = inp["grid"].params

#     s = "def interp_f(inpi, coords_in):\n"

#     s += "   cell = np.floor(coords_in).astype('int16')\n"
#     s += "   cellup = np.min([2+cell, {}], axis=0).astype('int16')\n".format(
#         [len(params.values[p]) for p in params.names]
#     )
#     s += "   cellround = np.round(coords_in).astype('int16')\n"

#     s += "   tmp = inpi[\n"

#     # obs_grid same order as grid.params.names?
#     for i, p in enumerate(params.names):
#         if p in inp["linterp_params"]:
#             s += f"   cell[{i}]:cellup[{i}]"  # it can be a large array because we don't set the  upper index but that's simpler for now. AT the end we use indices 0 and 1 so it's fine
#         else:
#             s += f"   cellround[{i}]:cellround[{i}]+1"

#         if i < params.n - 1:
#             s += ",\n"
#         else:
#             s += "]\n\n"

#     s += "   dx = coords_in - cell\n"
#     for i, p in enumerate(params.names):
#         if p in inp["linterp_params"]:
#             if inp["linterp_upsample"] > 0:
#                 s += f"   dx[{i}] = {inp['linterp_upsample']}**-1*round(dx[{i}]*{inp['linterp_upsample']})\n"  # using inp here because we wanna use the same setting as search
#             if i < params.n - 1:
#                 s += f"   tmp = tmp[0, :]*(1-dx[{i}]) + tmp[cellup[{i}]-cell[{i}]-1, :]*dx[{i}]\n\n"
#             else:
#                 s += f"   tmp = tmp[0]*(1-dx[{i}]) + tmp[cellup[{i}]-cell[{i}]-1]*dx[{i}]\n\n"
#         else:
#             if i < params.n - 1:
#                 s += f"   tmp = tmp[0, :]\n"
#             else:
#                 s += f"   tmp = tmp[0]\n\n"

#     s += "   return tmp"

#     if verbose:
#         logging.debug(s)

#     exec("global interp_f\n" + s)

#     return interp_f


# -------------------------------------------------------------------
# linear
def interp_func(tmp_i, coords_in, inp=None, verbose=False):  # tmp_m
    params = inp["grid"].params

    # 1st step: nearest neighbor interpolations (squeezing)
    slices = []
    refshape = []
    for i, p in enumerate(params.names):
        if p in inp["linterp_params"]:
            slices += [
                slice(None),
            ]
            refshape += [
                len(params.values[p]),
            ]
        else:
            slices += [
                np.round(coords_in[i]).astype("int16"),
            ]
            refshape += [
                1,
            ]
    slices = tuple(slices)
    tmp = tmp_i[slices].reshape(refshape)
    # tmp_m = tmp_m[slices].reshape(refshape)

    # 2nd step: linear interpolations (operation on two elements along one dimension, for all relevant parameters)
    for i, p in enumerate(params.names):
        if p in inp["linterp_params"]:
            # swapping axes to be able to do tmp[0] for this parameter
            tmp = tmp.swapaxes(i, 0)
            # tmp_m = tmp_m.swapaxes(i, 0)
            cell = np.floor(coords_in[i]).astype("int16")
            cellup = np.min(np.array([1 + cell, len(params.values[p]) - 1])).astype(
                "int16"
            )
            dx = coords_in[i] - cell
            if inp["linterp_upsample"] > 0:
                dx = {inp["linterp_upsample"]} ** -1 * round(
                    dx * {inp["linterp_upsample"]}
                )

            # tmp = tmp_m[[cell]] * tmp[[cell]] * (1 - dx * tmp_m[[cellup]]) + tmp_m[
            #    [cellup]
            # ] * tmp[[cellup]] * (dx * tmp_m[[cell]] + (1 - dx) * (1 - tmp_m[[cellup]]))
            # tmp_m = (tmp_m[[cell]] + tmp_m[[cellup]]) >= 1
            # tmp_m = tmp_m.swapaxes(0, i)
            tmp = tmp[[cell]] * (1 - dx) + tmp[[cellup]] * dx
            tmp = tmp.swapaxes(0, i)

    return tmp.squeeze()


# -------------------------------------------------------------------
# nearest
def nearest_func(tmp_i, idxs_in, inp=None):
    return tmp_i[tuple([int(round(x)) for x in idxs_in])]


# -------------------------------------------------------------------
# from https://github.com/aloctavodia/BAP/blob/master/first_edition/code/Chp1/hpd.py
import scipy.stats.kde as kde


def hpd_grid(sample, alpha=0.05, roundto=2):
    """Calculate highest posterior density (HPD) of array for given alpha.
    The HPD is the minimum width Bayesian credible interval (BCI).
    The function works for multimodal distributions, returning more than one mode
    Parameters
    ----------

    sample : Numpy array or python list
        An array containing MCMC samples
    alpha : float
        Desired probability of type I error (defaults to 0.05)
    roundto: integer
        Number of digits after the decimal point for the results
    Returns
    ----------
    hpd: array with the lower

    """
    sample = np.asarray(sample)
    sample = sample[~np.isnan(sample)]
    # get upper and lower bounds
    l = np.min(sample)
    u = np.max(sample)
    density = kde.gaussian_kde(sample)
    x = np.linspace(l, u, 2000)
    y = density.evaluate(x)
    # y = density.evaluate(x, l, u) waitting for PR to be accepted
    xy_zipped = zip(x, y / np.sum(y))
    xy = sorted(xy_zipped, key=lambda x: x[1], reverse=True)
    xy_cum_sum = 0
    hdv = []
    for val in xy:
        xy_cum_sum += val[1]
        hdv.append(val[0])
        if xy_cum_sum >= (1 - alpha):
            break
    hdv.sort()
    diff = (u - l) / 20  # differences of 5%
    hpd = []
    hpd.append(round(min(hdv), roundto))
    for i in range(1, len(hdv)):
        if hdv[i] - hdv[i - 1] >= diff:
            hpd.append(round(hdv[i - 1], roundto))
            hpd.append(round(hdv[i], roundto))
    hpd.append(round(max(hdv), roundto))
    ite = iter(hpd)
    hpd = list(zip(ite, ite))
    modes = []
    for value in hpd:
        x_hpd = x[(x > value[0]) & (x < value[1])]
        y_hpd = y[(x > value[0]) & (x < value[1])]
        modes.append(round(x_hpd[np.argmax(y_hpd)], roundto))
    return hpd, x, y, modes


# -------------------------------------------------------------------
# from F. Galliano


# Bowley (1920) skewness
def skew_Bowley(X, alpha=25):
    """
    Computes the robust Bowley (1920) skewness based on percentiles.
    It uses the three percentiles: alpha, 50%, 100%-alpha.
    By default alpha=25%, but it can be changed (e.g. 1% or 0.1%).
    """
    Q1, Q2, Q3 = np.percentile(X, [alpha, 50, 100 - alpha])
    return (Q3 + Q1 - 2 * Q2) / (Q3 - Q1)


# -------------------------------------------------------------------
# from F. Galliano


# Groneveld & Meeden (1984) skewness
def skew_GM84(X):
    """
    Computes the robust Groneveld & Meeden (1984) skewness.
    It is a generalization of the Bowley (1920) estimator, integrating over
    alpha.
    """
    Nq = 100
    percinf = np.linspace(0.01, 49, Nq)
    perc = np.concatenate((percinf, [50], 100 - percinf))
    Q = np.array(list(np.percentile(X, perc)))
    Q1 = Q[:Nq]
    Q2 = Q[Nq]
    Q3 = Q[Nq + 1 :]
    # return( np.trapz(percinf,Q3+Q1-2*Q2) / np.trapz(percinf,Q3-Q1) )
    return np.trapz(x=percinf, y=Q3 + Q1 - 2 * Q2) / np.trapz(x=percinf, y=Q3 - Q1)


# -------------------------------------------------------------------
# from F. Galliano
# MAD
def MADAM(
    X, coeff=1 / 0.6744897501960817, median=None
):  # or from scipy.stats import median_abs_deviation
    """
    Computes the median absolute deviation about the median (MADAM).
    """
    if median is None:
        median = np.nanmedian(np.array(X))
    return coeff * np.nanmedian(np.abs(np.array(X) - median))


# -------------------------------------------------------------------
# from F. Galliano
def robust_moments(
    X,
    Y=None,
    skew=False,
    median=None,
    MAD=None,
    Nitermax=100,
    accuracy=1.0e-5,
    Msigma="lax75",
):
    """
    ROBUST MOMENT ESTIMATION METHODS

    By default, it uses M-estimates, with Tukey's bisquare weight.

    If two variables are entered (X and Y), it computes the moment for each of
    them, as well as their correlation. In this case, the median and the MAD if
    entered, must be 2 element arrays.

    For the variance, there are several formula. By default, we use the one from
    Lax (1975; 'lax75'), which is the best. Alternatively, one can also test the
    one from Beers et al. (1990; 'beers90'), or the naive weighted sum ('naive').
    """

    # Number of variables
    Nvar = 1 if Y is None else 2
    mean = np.zeros(Nvar)
    variance = np.zeros(Nvar)
    stdev = np.zeros(Nvar)
    skewness = np.zeros(Nvar)

    for xy in np.arange(Nvar):
        var = X if xy == 0 else Y

        # 2) M-estimates
        # ---------------
        # (see Beers et al. 1990, or
        # https://docs.astropy.org/en/stable/stats/robust.html)

        # Try to not recompute medians if it is already known
        if median is None:
            med = np.nanmedian(var)
        else:
            if Nvar == 1:
                med = median
            else:
                med = median[xy]
        if MAD is None:
            mad = MADAM(var, median=median)
        else:
            if Nvar == 1:
                mad = MAD
            else:
                mad = MAD[xy]
        Nitermax = np.max([Nitermax, 1])

        # Tukey's biweight M-estimators
        location, scale = med, mad
        Niter = 0
        meanOK, stdevOK = False, False
        while not meanOK and not stdevOK and Niter < Nitermax:
            ui = (var - location) / scale

            # Mean
            c = 6
            mask = np.abs(ui / c) < 1
            weight = (1 - (ui / c) ** 2) ** 2
            mean[xy] = np.sum(var[mask] * weight[mask]) / np.sum(weight[mask])

            # Std-dev
            c = 9
            mask = np.abs(ui / c) < 1
            n = np.sum(mask)
            if Msigma == "beers90":
                # Formula found in Beers et al. (1990), citing the book
                # of Mosteller & Tukey (1977)
                variance[xy] = (
                    n
                    * np.sum(
                        (var[mask] - location) ** 2 * (1 - (ui[mask] / c) ** 2) ** 4
                    )
                    / (
                        np.sum(
                            (1 - (ui[mask] / c) ** 2) * (1 - 5 * (ui[mask] / c) ** 2)
                        )
                    )
                    ** 2
                )
            elif Msigma == "lax75":
                # Formula found in the book of Mosteller & Tukey (1977),
                # citing Lax (1975)
                variance[xy] = (
                    n
                    * np.sum(
                        (var[mask] - location) ** 2 * (1 - (ui[mask] / c) ** 2) ** 4
                    )
                    / (
                        np.sum(
                            (1 - (ui[mask] / c) ** 2) * (1 - 5 * (ui[mask] / c) ** 2)
                        )
                        * (
                            -1
                            + np.sum(
                                (1 - (ui[mask] / c) ** 2)
                                * (1 - 5 * (ui[mask] / c) ** 2)
                            )
                        )
                    )
                )
            elif Msigma == "naive":
                # Simple weighted moment
                variance[xy] = np.sum(
                    (var[mask] - location) ** 2 * weight[mask]
                ) / np.sum(weight[mask])
            stdev[xy] = np.sqrt(variance[xy])

            # Skewness
            if skew:
                # somehow not working well
                c = 9
                mask = np.abs(ui / c) < 1
                weight = (1 - (ui / c) ** 2) ** 2
                skewness[xy] = np.sum(ui[mask] ** 3 * weight[mask]) / np.sum(
                    weight[mask]
                )

            # Update
            meanOK = np.abs(mean[xy] - location) <= accuracy * 2 * np.abs(
                mean[xy] + location
            )
            stdevOK = np.abs(stdev[xy] - scale) <= accuracy * 2 * np.abs(
                stdev[xy] + scale
            )
            location = mean[xy]
            scale = stdev[xy]
            Niter += 1

    covariance, rho = None, None

    # 4) Correlation coefficient
    # ---------------------------
    # M-estimator
    if Nvar == 2:
        c = 9
        ui = (X - mean[0]) / stdev[0] / c
        vi = (Y - mean[1]) / stdev[1] / c
        maskX = np.abs(ui) < 1
        maskY = np.abs(vi) < 1
        mask = maskX * maskY
        n = np.sum(mask)
        covariance = (
            n
            * np.sum(
                (X[mask] - mean[0])
                * (1 - ui[mask] ** 2) ** 2
                * (Y[mask] - mean[1])
                * (1 - vi[mask] ** 2) ** 2
            )
            / np.sum((1 - ui[mask] ** 2) * (1 - 5 * ui[mask] ** 2))
            / np.sum((1 - vi[mask] ** 2) * (1 - 5 * vi[mask] ** 2))
        )
        rho = covariance / stdev[0] / stdev[1]

    res = {
        "robust_mean": mean,
        "robust_variance": variance,
        "robust_stdev": stdev,
        "robust_covariance": covariance,
        "robust_skewness": skewness,
        "median": med,
        "rho": rho,
    }

    # override
    if Y is None:
        res["robust_skewness"] = [skew_GM84(X)]
    else:
        res["robust_skewness"] = [skew_GM84(X), skew_GM84(Y)]

    return res


# -------------------------------------------------------------------
def get_estimators(inp, posterior, names=None, ci=None):
    """
    Calculate estimators (mean, median etc...)
    """

    if names is None:
        names = posterior.keys()

    if ci is None:
        ci = inp["ci"]

    hpdi = az.hdi(posterior, hdi_prob=ci, skipna=True)

    means = posterior.mean(skipna=True)
    medians = posterior.median(skipna=True)
    res = {}
    for n in names:
        hpdi_all = {
            i: az.hdi(posterior[n], hdi_prob=i, skipna=True) for i in inp["ci_arr"]
        }

        r = robust_moments(posterior[n].data, skew=True)
        mu, lambd, tau = convert_to_splitnorm(
            mean=r["robust_mean"],
            stdev=r["robust_stdev"],
            skewness=r["robust_skewness"],
        )

        r["robust_mean"] = float(r["robust_mean"][0])
        r["robust_stdev"] = float(r["robust_stdev"][0])

        r.update({"mu": mu})
        r.update({"lambda": lambd})
        r.update({"tau": tau})

        r.update({"stdev": posterior[n].data.std()})
        r.update({"mad": MADAM(posterior[n].data)})

        r.update({"mean": means[n].data})
        r.update({"hpdi": hpdi[n].data})
        r.update({"hpdi_all": hpdi_all})
        r.update({"ci": ci})
        r.update({"median": medians[n].data})

        res.update({n: r})

    return res


# -------------------------------------------------------------------
def convert_to_splitnorm(mean=None, stdev=None, skewness=None):
    # Compute tau numerically: tau=1.E2 ==> skewness=0.99
    # mu = mode of the splitnormal
    taugrid = np.logspace(-2, 2, 10000)
    bgrid = (np.pi - 2) / np.pi * (taugrid - 1.0) ** 2 + taugrid
    skewgrid = (
        bgrid ** (-1.5)
        * np.sqrt(2 / np.pi)
        * (taugrid - 1.0)
        * ((4 / np.pi - 1.0) * (taugrid - 1.0) ** 2 + taugrid)
    )

    # Loop on the indices
    N = len(mean)
    mu = np.zeros(N)
    lambd = np.zeros(N)
    tau = np.zeros(N)
    for i in np.arange(N):
        tau[i] = np.exp(np.interp(skewness[i], skewgrid, np.log(taugrid)))
        b = (np.pi - 2) / np.pi * (tau[i] - 1.0) ** 2 + tau[i]
        lambd[i] = stdev[i] / np.sqrt(b)
        mu[i] = mean[i] - np.sqrt(2.0 / np.pi) * lambd[i] * (tau[i] - 1.0)

    return mu, lambd, tau


# -------------------------------------------------------------------
def convert_from_splitnorm(mu=None, lambd=None, tau=None):
    N = len(mu)
    mean = mu + np.sqrt(2.0 / np.pi) * lambd * (tau - 1.0)
    b = (np.pi - 2) / np.pi * (tau - 1.0) ** 2 + tau
    stdev = np.sqrt(b) * lambd
    skewness = (
        b ** (-1.5)
        * np.sqrt(2 / np.pi)
        * (tau - 1.0)
        * ((4 / np.pi - 1.0) * (tau - 1.0) ** 2 + tau)
    )

    return mean, stdev, skewness


# -------------------------------------------------------------------
# https://discourse.pymc.io/t/attributeerror-cant-pickle-local-object-mixture-comp-dist-random-wrapper-locals-wrapped-random/4619/7
class MixHelper:
    """
    To get a Mixture in PyMC3 avoiding potential issues with pickle
    """

    def __init__(self, K, w, ps):
        self.K = K
        self.ps = ps
        self.w = w

    def __call__(self, pt):
        logps = []
        for k in range(self.K):
            logp = self.ps[k].logp(pt).sum() + tt.log(self.w[k])
            logps.append(logp)
        return pm.logsumexp(logps)


# -------------------------------------------------------------------
def pvalue_calc(trace_orig, observations, estimators, name, ndraw=None, inp=None):
    kind = ["median", "mad"]
    trace = deepcopy(trace_orig)
    if ndraw is None:
        ndraw = trace.posterior.dims["draw"]
    trace = trace.posterior.sel(draw=np.arange(ndraw))

    # following equation depends on metric, here we use the chi**2
    # For each trace element calculate: Sum_obs ((trace-model_mean)/model_sigma)**2 - Sum_obs ((observed-model_mean)/model_sigma)**2
    # then get pvalue from above array

    # overfitting? It's improbable that it'fits so well

    if name != "combined":  # individual tracers
        # diff = trace[name].data.flatten()-observations.obs[name].value
        data = trace[name].data.flatten()
        mean = estimators[name][kind[0]]  # np.mean(data)
        std = estimators[name][kind[1]]  # np.std(data)
        diff = ((data - mean) / std) ** 2 - (
            (10 ** observations.obs[name].value - mean) / std
        ) ** 2
        # p-values for ULs?
        if observations.obs[name].upper:
            pvalue = len(np.where(diff <= 0)[0]) / len(diff)
            pvalue /= 2.0
        elif observations.obs[name].lower:
            pvalue = len(np.where(diff >= 0)[0]) / len(diff)
            pvalue /= 2.0
        else:
            pvalue = len(np.where(diff <= 0)[0]) / len(diff)

    else:
        # for combined we do trace - observed also but we normalize each observation using the mean & stdev and we calculate one value for all observables for each trace element

        # indok = [
        #     o
        #     for o in observations.names
        #     if pvalue_calc(trace_orig, observations, estimators, o) >= 0.5
        # ]
        # # print(indok)
        # if len(indok) == 0:
        #     pvalue = np.nan
        # else:
        # detections and soft lower/upper limits (i.e., non-significant detections)
        T_mod = np.sum(
            [
                (
                    (trace[o].data.flatten() - estimators[o][kind[0]])
                    / estimators[o][kind[1]]
                )
                ** 2
                for o in observations.names
                if not observations.obs[o].limit
            ],
            axis=0,
        )
        T_obs = np.sum(
            [
                (
                    (observations.obs[o].value - estimators[o][kind[0]])
                    / estimators[o][kind[1]]
                )
                ** 2
                # for o in indok
                for o in observations.names
                if not observations.obs[o].limit
            ]
        )
        # hard upper limits (i.e., not necessarily from a measurement with errors)
        T_mod_UL = np.atleast_1d(
            np.sum(
                [
                    (
                        (trace[o].data.flatten() - estimators[o][kind[0]])
                        / estimators[o][kind[1]]
                    )
                    ** 2
                    for o in observations.names
                    if observations.obs[o].upper
                ],
                axis=0,
            )
        )
        T_obs_UL = np.sum(
            [
                (
                    (observations.obs[o].value - estimators[o][kind[0]])
                    / estimators[o][kind[1]]
                )
                ** 2
                for o in observations.names
                if observations.obs[o].upper
            ]
        )
        # hard lower limits (i.e., not necessarily from a measurement with errors)
        T_mod_LL = np.atleast_1d(
            np.sum(
                [
                    (
                        (trace[o].data.flatten() - estimators[o][kind[0]])
                        / estimators[o][kind[1]]
                    )
                    ** 2
                    for o in observations.names
                    if observations.obs[o].lower
                ],
                axis=0,
            )
        )
        T_obs_LL = np.sum(
            [
                (
                    (observations.obs[o].value - estimators[o][kind[0]])
                    / estimators[o][kind[1]]
                )
                ** 2
                for o in observations.names
                if observations.obs[o].lower
            ]
        )
        pvalue = (
            len(np.where((T_mod - T_obs) <= 0)[0])
            + 0.5 * len(np.where((T_mod_UL - T_obs_UL) <= 0)[0])
            + 0.5 * len(np.where((T_mod_LL - T_obs_LL) >= 0)[0])
        ) / (len(T_mod) + len(T_mod_UL) + len(T_mod_LL))

    del trace_orig, trace

    return pvalue


# #-------------------------------------------------------------------
# def drawrng():
#     '''Draw a random number with theano between 0 and 1
#     '''
#     #from theano import function
#     #f = function([], RandomStreams().uniform(size=(1,))[0])
#     return RandomStreams().uniform(size=(1,))[0]


# -------------------------------------------------------------------
def mergetrace(trace, params):
    """
    Merge components in single array for combined trace plots
    """

    trace2 = deepcopy(trace)
    for p in params:
        trace2[p] = deepcopy(trace2["w"])
        for s in range(len(trace2["w"].data[0, 0, :])):
            if "{}_{}".format(p, s) in trace.keys():  # in case several comps LOC & not
                trace2[p].data[:, :, s] = trace["{}_{}".format(p, s)].data
                del trace2["{}_{}".format(p, s)]
    return trace2


# # -------------------------------------------------------------------
# class Callback:
#     """
#     Callback function for sampler (called each draw)
#     """

#     def __init__(self, every=500, max_rhat=1.05):
#         self.every = every
#         self.max_rhat = max_rhat
#         self.traces = {}

#     def __call__(self, trace, draw):
#         # if draw.tuning:
#         #    return

#         self.traces[draw.chain] = trace  # identify chain trace belongs to

#         # every draw
#         multitrace = pm.backends.base.MultiTrace(list(self.traces.values()))
#         tmp = multitrace.get_values("w_0").T
#         self.wmean = np.array(
#             [np.mean(tmp[i][tmp[i] != 0]) for i in range(len(tmp.T[0]))]
#         )
#         if len(trace) % self.every == 0:
#             print(self.wmean)

#         # every self.every draw
#         # if len(trace) % self.every == 0:
#         #    multitrace = pm.backends.base.MultiTrace(list(self.traces.values()))
#         #    tmp = multitrace.get_values('w_0').T
#         #    self.wmean = np.array([np.mean(tmp[i][tmp[i]!=0]) for i in range(len(tmp.T[0]))])
#         #    breakpoint()

#         # if trace.get_values['mask'][-np.min([self.every,len(trace.get_values['mask'])]):].min()==0:
#         #    print('Invalid data was drawn in chain {}'.format(draw.chain)) #might not accepted!
#         # if az.rhat(multitrace).to_array().max() < self.max_rhat:
#         #    raise KeyboardInterrupt


# -------------------------------------------------------------------
def dispersion_metric(parr):
    return parr.std(axis=(1, 2)).mean()  # normalized by mean


def characterize_sectors_old(inp, trace, verbose=False):
    """
    Function to re-distribute the draws in each chain in order to isolate components
    This doesn't work yet for draws that switch across *groups*
    Current workaround would be to add a prior on the group scaling
    """

    n = inp["n_comps"]  # number of components

    if n == 1:
        logging.info("Nothing to sort")
        return trace

    # possible permutations between components
    perm = list(permutations(np.arange(n)))

    # parameters that have different values between components
    plist = [
        p
        for p in inp["grid"].params.names
        if np.array(
            [trace.posterior["idx_{}_{}".format(p, s)].values for s in range(n)]
        )
        .mean(axis=(1, 2))
        .std()
        > 1.0e-5
    ]

    # make table to work with
    parr = [
        np.array([trace.posterior["idx_{}_{}".format(p, s)].values for s in range(n)])
        for p in plist
    ]
    for i, p in enumerate(plist):
        parr[i] = np.einsum("kij->ijk", parr[i])  # same dims as w
    for p in inp["context_specific_params"]:
        parr += [
            trace.posterior[p].values,
        ]
    parr += [
        trace.posterior["w"].values,
    ]
    parr = np.array(parr)

    # update plist with w
    for p in inp["context_specific_params"]:
        plist += [
            p,
        ]
    plist += [
        "w",
    ]

    if verbose:
        logging.debug(plist)
        logging.debug(parr.shape)

    logging.debug(
        "Initial value = {} ".format(dispersion_metric(parr[:-1, :, :, :]))
    )  # independently on p, c, and s #we sort w but don't use w for the metric

    # ===================================
    # switch individual chains (mostly useful for NUTS)
    # iterations
    res, last = 0, 1e8
    iiter = 1
    maxiter = 10
    while res < last and iiter < maxiter:
        for c in tqdm(range(parr.shape[1])):
            tmp = []
            for s in perm:
                parr2 = deepcopy(parr)
                parr2[:, c, :, :] = np.einsum("kij->ijk", parr2[:, c, :, s])
                tmp += [
                    dispersion_metric(parr2[:-1, :, :, s]),
                ]
            # choose best permutation
            parr[:, c, :, :] = np.einsum(
                "kij->ijk", parr[:, c, :, perm[np.array(tmp).argmin()]]
            )

        if iiter > 1:
            last = res
        res = parr[:-1, :, :, :].std(axis=(1, 2)).mean()

        logging.info("(chains) Iteration {} = {}".format(iiter, res))
        iiter += 1

    # ===================================
    # switch individual draws (mostly useful for SMC)
    # iterations
    res, last = 0, 1e8
    iiter = 1
    maxiter = 10
    while res < last and iiter < maxiter:
        for c in tqdm(range(parr.shape[1])):
            for d in range(parr.shape[2]):
                tmp = []
                for s in perm:
                    parr2 = deepcopy(parr)
                    parr2[:, c, d, :] = parr2[:, c, d, s]
                    tmp += [
                        dispersion_metric(parr2[:, :, :, s]),
                    ]
                # choose best permutation
                parr[:, c, d, :] = parr[:, c, d, perm[np.array(tmp).argmin()]]

        if iiter > 1:
            last = res
        res = dispersion_metric(parr[:, :, :, :])

        logging.info("(draws) Iteration {} = {}".format(iiter, res))
        iiter += 1

    # ===================================
    # sort w
    parr = parr[
        :,
        :,
        :,
        np.argsort(
            np.median(
                parr[len(parr) - 1, :, :, :],
                axis=(
                    0,
                    1,
                ),
            )
        )[::-1],
    ]

    # update traces
    for i, p in enumerate(plist):
        if verbose:
            logging.debug(p)
            logging.debug([np.mean(parr[i, :, :, s].flatten()) for s in range(n)])
            logging.debug([np.std(parr[i, :, :, s].flatten()) for s in range(n)])
        for s in range(n):
            if p == "w":
                trace.posterior["w"].values[:, :, s] = parr[i, :, :, s]
                # trace.posterior['w_{}{}'.format(g,s)].values = parr[i,:,:,s] #that's for the notebook tests, this is eventually added in add2trace
            elif p in inp["context_specific_params"]:
                trace.posterior[p].values[:, :, s] = parr[i, :, :, s]
            else:
                trace.posterior["idx_{}_{}".format(p, s)].values = parr[i, :, :, s]

    return trace


def characterize_sectors(inp, trace, verbose=False):
    """
    Function to re-distribute the draws in each chain in order to isolate components
    This doesn't work yet for draws that switch across *groups*
    Current workaround would be to add a prior on the group scaling
    """

    n = inp["n_comps"]  # number of components

    if n == 1:
        logging.info("Nothing to sort")
        return trace

    # possible permutations between components
    perm = list(permutations(np.arange(n)))

    # list of parameters that will be considered
    allparams = []
    tmp = list(set([getp(ps) for ps in inp["grid"].params.names_inf]))
    for p in tmp:
        keep = True
        for s in range(n):
            if f"idx_{p}_{s}" not in trace.posterior.keys():
                keep = False
        if keep:
            allparams += [
                f"idx_{p}",
            ]
        else:
            logging.warning(
                "Parameter distributions are not the same between components, no need to sort. "
            )
            return trace
    tmp = list(set([getp(ps) for ps in inp["grid"].params.names_plaw]))
    for p in tmp:
        keep = True
        for s in range(n):
            if f"alpha_{p}_{s}" not in trace.posterior.keys():
                keep = False
        if keep:
            allparams += [
                f"alpha_{p}",
                f"idx_lowerbound_{p}",
                f"idx_upperbound_{p}",
            ]
        else:
            logging.warning(
                "Parameter distributions are not the same between components, no need to sort. "
            )
            return trace
    tmp = list(set([getp(ps) for ps in inp["grid"].params.names_smoothplaw]))
    for p in tmp:
        keep = True
        for s in range(n):
            if f"lowerscale_{p}_{s}" not in trace.posterior.keys():
                keep = False
        if keep:
            allparams += [
                f"alpha_{p}",
                f"lowerscale_{p}",
                f"upperscale_{p}",
                f"idx_lowerbound_{p}",
                f"idx_upperbound_{p}",
            ]
        else:
            logging.warning(
                "Parameter distributions are not the same between components, no need to sort. "
            )
            return trace
    tmp = list(set([getp(ps) for ps in inp["grid"].params.names_brokenplaw]))
    for p in tmp:
        keep = True
        for s in range(n):
            if f"pivot_{p}_{s}" not in trace.posterior.keys():
                keep = False
        if keep:
            allparams += [
                f"alpha1_{p}",
                f"alpha2_{p}",
                f"pivot_{p}",
                f"idx_lowerbound_{p}",
                f"idx_upperbound_{p}",
            ]
        else:
            logging.warning(
                "Parameter distributions are not the same between components, no need to sort. "
            )
            return trace
    tmp = list(set([getp(ps) for ps in inp["grid"].params.names_normal]))
    for p in tmp:
        keep = True
        for s in range(n):
            if f"mu_{p}_{s}" not in trace.posterior.keys():
                keep = False
        if keep:
            allparams += [
                f"mu_{p}",
                f"sigma_{p}",
                f"idx_lowerbound_{p}",
                f"idx_upperbound_{p}",
            ]
        else:
            logging.warning(
                "Parameter distributions are not the same between components, no need to sort. "
            )
            return trace
    tmp = list(set([getp(ps) for ps in inp["grid"].params.names_doublenormal]))
    for p in tmp:
        keep = True
        for s in range(n):
            if f"mu1_{p}_{s}" not in trace.posterior.keys():
                keep = False
        if keep:
            allparams += [
                f"mu1_{p}",
                f"mu2_{p}",
                f"sigma1_{p}",
                f"sigma2_{p}",
                f"ratio21_{p}",
                f"idx_lowerbound_{p}",
                f"idx_upperbound_{p}",
            ]
        else:
            logging.warning(
                "Parameter distributions are not the same between components, no need to sort. "
            )
            return trace

    # allparams = []
    # # if "names_plaw" in dir(inp["grid"].params):  # backward compatibility
    # for p in inp["grid"].params.names:
    #     if p in inp["grid"].params.names_plaw:
    #         allparams += [
    #             f"alpha_{p}",
    #         ]
    #     elif p in inp["grid"].params.names_smoothplaw:
    #         allparams += [
    #             f"alpha_{p}",
    #             f"lowerscale_{p}",
    #             f"upperscale_{p}",
    #         ]
    #     elif p in inp["grid"].params.names_brokenplaw:
    #         allparams += [
    #             f"alpha1_{p}",
    #             f"alpha2_{p}",
    #             f"pivot_{p}",
    #         ]
    #     elif p in inp["grid"].params.names_normal:
    #         allparams += [f"mu_{p}", f"sigma_{p}"]
    #     elif p in inp["grid"].params.names_doublenormal:
    #         allparams += [
    #             f"mu1_{p}",
    #             f"sigma1_{p}",
    #             f"mu2_{p}",
    #             f"sigma2_{p}",
    #             f"ratio21_{p}",
    #         ]
    #     else:
    #         allparams += [f"idx_{p}"]
    if verbose:
        logging.info("All parameters to consider: {}".format(allparams))

    # parameters that have different values between components
    plist = [
        p
        for p in allparams
        if np.array([trace.posterior[p + f"_{s}"].values for s in range(n)])
        .mean(axis=(1, 2))
        .std()
        > 1.0e-5
    ]
    if verbose and plist != allparams:
        logging.info(
            "Parameters with different values between components: {}".format(plist)
        )

    # make table to work with
    parr = [
        np.array([trace.posterior[p + f"_{s}"].values for s in range(n)]) for p in plist
    ]
    for i, p in enumerate(plist):
        parr[i] = np.einsum("kij->ijk", parr[i])  # same dims as w

    if "context_specific_params" in inp.keys():
        for p in inp["context_specific_params"]:
            if len(trace.posterior[p].values.shape) == 2:
                # only one for all sectors
                tmp = np.repeat(
                    trace.posterior[p].values[:, :, None], inp["n_comps"], axis=2
                )
            else:
                # per sector
                tmp = trace.posterior[p].values
            parr += [
                tmp,
            ]
    parr += [
        trace.posterior["w"].values,
    ]
    parr = np.array(parr)

    # update plist
    if "context_specific_params" in inp.keys():
        for p in inp["context_specific_params"]:
            plist += [
                p,
            ]

    # update plist with w
    plist += [
        "w",
    ]

    if verbose:
        logging.info("Final list of parameters: {}".format(plist))

    if verbose:
        logging.debug(parr.shape)
        logging.debug(
            "Initial value = {} ".format(dispersion_metric(parr[:-1, :, :, :]))
        )  # independently on p, c, and s #we sort w but don't use w for the metric

    # ===================================
    # switch individual chains (mostly useful for NUTS)
    # iterations
    res, last = 0, 1e8
    iiter = 1
    maxiter = 10
    while res < last and iiter < maxiter:
        for c in tqdm(range(parr.shape[1])):
            tmp = []
            for s in perm:
                parr2 = deepcopy(parr)
                parr2[:, c, :, :] = np.einsum("kij->ijk", parr2[:, c, :, s])
                tmp += [
                    dispersion_metric(parr2[:-1, :, :, s]),
                ]
            # choose best permutation
            parr[:, c, :, :] = np.einsum(
                "kij->ijk", parr[:, c, :, perm[np.array(tmp).argmin()]]
            )

        if iiter > 1:
            last = res
        res = parr[:-1, :, :, :].std(axis=(1, 2)).mean()

        logging.info("(chains) Iteration {} = {}".format(iiter, res))
        iiter += 1

    # ===================================
    # switch individual draws (mostly useful for SMC)
    # iterations
    res, last = 0, 1e8
    iiter = 1
    maxiter = 10
    while res < last and iiter < maxiter:
        for c in tqdm(range(parr.shape[1])):
            for d in range(parr.shape[2]):
                tmp = []
                for s in perm:
                    parr2 = deepcopy(parr)
                    parr2[:, c, d, :] = parr2[:, c, d, s]
                    tmp += [
                        dispersion_metric(parr2[:, :, :, s]),
                    ]
                # choose best permutation
                parr[:, c, d, :] = parr[:, c, d, perm[np.array(tmp).argmin()]]

        if iiter > 1:
            last = res
        res = dispersion_metric(parr[:, :, :, :])

        logging.info("(draws) Iteration {} = {}".format(iiter, res))
        iiter += 1

    # ===================================
    # sort w
    parr = parr[
        :,
        :,
        :,
        np.argsort(
            np.median(
                parr[len(parr) - 1, :, :, :],
                axis=(
                    0,
                    1,
                ),
            )
        )[::-1],
    ]

    # update traces
    for i, p in enumerate(plist):
        if verbose:
            logging.debug(p)
            logging.debug([np.mean(parr[i, :, :, s].flatten()) for s in range(n)])
            logging.debug([np.std(parr[i, :, :, s].flatten()) for s in range(n)])
        for s in range(n):
            if p == "w":
                trace.posterior["w"].values[:, :, s] = parr[i, :, :, s]
                # trace.posterior['w_{}{}'.format(g,s)].values = parr[i,:,:,s] #that's for the notebook tests, this is eventually added in add2trace
            elif p in inp["context_specific_params"]:
                if len(trace.posterior[p].values.shape) == 2:
                    # only one for all sectors
                    trace.posterior[p].values = parr[i, :, :, 0]
                else:
                    # per sector
                    trace.posterior[p].values[:, :, s] = parr[i, :, :, s]
            else:
                trace.posterior["{}_{}".format(p, s)].values = parr[i, :, :, s]

    return trace


# -------------------------------------------------------------------
def t_test(sample, mu):
    mean = np.mean(sample)
    var = np.var(
        sample, ddof=1
    )  ### “Delta Degrees of Freedom”: the divisor used in the calculation is N - ddof, where N represents the number of elements. By default ddof is zero.
    sem = (var / len(sample)) ** 0.5
    t = abs(mu - mean) / sem
    df = len(sample) - 1
    p = 2 * (1 - st.t.cdf(t, df))  ###
    return (t, p)


# -------------------------------------------------------------------
def combine_vars(trace, var_names, label):
    """
    Concatenate traces of different variables
    """

    posterior = trace.posterior if "posterior" in dir(trace) else trace
    tmp = deepcopy(posterior[var_names[0]])
    k = 1
    for i, name in enumerate(var_names):
        if name not in posterior.keys():
            continue  # happens if obs = inf or nan
        if i > 0:
            k += 1
            tmp = xarray.concat([tmp, posterior[name]], dim="chain").assign_coords(
                {"chain": np.arange(k * posterior.dims["chain"])}
            )
    tmp.name = label
    return az.InferenceData(
        posterior=tmp.to_dataset()
    )  # new trace because different number of chains


# -------------------------------------------------------------------
def distrib_plaw(boundaries, alpha, x=[]):
    if x == []:
        x = np.linspace(boundaries[0], boundaries[1], 100)
    weight = (1 + alpha) * x
    return x, weight


def distrib_brokenplaw(boundaries, alphas, pivot, x=[]):
    if x == []:
        x = np.linspace(boundaries[0], boundaries[1], 100)
    weight = (1 + alphas[0]) * (x - pivot) * (x <= pivot) + (1 + alphas[1]) * (
        x - pivot
    ) * (x > pivot)
    return x, weight


# def distrib_smoothplaw(boundaries, alpha, cuts, factors, x=[]):
# to allow an exponential decline of x dex over y: factor = np.log10(np.log10(x)/y)
#     if x == []:
#         x = np.linspace(boundaries[0], boundaries[1], 100)
#     weight = (
#         (1 + alpha) * x
#         - 10 ** (10 ** factors[0] * (cuts[0] - x))
#         - 10 ** (10 ** factors[1] * (x - cuts[1]))
#     )
#     return x, weight


# def distrib_smoothplaw(cuts, alpha, scales, x=[]):
#     if x == []:
#         x = np.linspace(cuts[0], cuts[1], 100)
#     weight = (
#         (1 + alpha) * x
#         - abs(10 ** scales[0]) / (x - cuts[0])
#         - abs(10 ** scales[1]) / (cuts[1] - x)
#     )
#     return x, weight


def distrib_smoothplaw(cuts, alpha, scales, x=[]):
    if x == []:
        x = np.linspace(cuts[0], cuts[1], 100)
    weight = (1 + alpha) * x
    corr = -np.exp(-(10 ** scales[1]) * (cuts[1] - x))
    corr += -np.exp(-(10 ** scales[0]) * (x - cuts[0]))
    corr = np.maximum(corr, -10)
    weight += corr
    return x, weight


def distrib_normal(boundaries, mu, sigma, x=[]):
    if x == []:
        x = np.linspace(boundaries[0], boundaries[1], 100)
    weight = 0.4342944819032518 * (-((mu - x) ** 2) / (2.0 * sigma**2))
    return x, weight


def distrib_dnormal(boundaries, mus, sigmas, ratio21, x=[]):
    if x == []:
        x = np.linspace(boundaries[0], boundaries[1], 100)
    weight = np.log10(
        np.exp(-((mus[0] - x) ** 2) / (2.0 * sigmas[0] ** 2))
        + 10**ratio21 * np.exp(-((mus[1] - x) ** 2) / (2.0 * sigmas[1] ** 2))
    )
    return x, weight


# -------------------------------------------------------------------
def analyze_chains(trace):
    nsigma = 1
    tmp = np.array([t.mean() for t in trace.posterior["log_likelihood"].data])
    tmp2 = nsigma * np.array([t.std() for t in trace.posterior["log_likelihood"].data])
    badchains = [
        i
        for i in np.arange(trace.posterior.dims["chain"])
        if (tmp[i] + tmp2[i]) < (tmp[np.argmax(tmp)] - tmp2[np.argmax(tmp)])
    ]
    goodchains = [
        i
        for i in np.arange(trace.posterior.dims["chain"])
        if (tmp[i] + tmp2[i]) > (tmp[np.argmax(tmp)] - tmp2[np.argmax(tmp)])
    ]
    return goodchains, badchains


# -------------------------------------------------------------------
def corr_xy(
    inp,
    xnames,
    ynames,
    post_process=False,
    method="spearman",
    verbose=False,
    marginalize=True,
    label="",
    resdict=None,
    trace=None,
    normx=False,
    normy=False,
):
    """
    The resulting products (tab, corrcomp) are then used in corr_xy_plot
    """
    from Library.lib_main import logappend

    d = inp["output_directory"]
    results = ""

    if post_process:
        t = az.from_netcdf(d + "/trace_post-process.netCDF")
    else:
        if trace is None:
            t = az.from_netcdf(d + "/trace_process.netCDF")
        else:
            t = trace

    n_x = len(xnames)
    n_y = len(ynames)

    # for chain in range(t.posterior.dims['chain']):
    tc = deepcopy(t)  # .sel(chain=3)
    # deepcopy because we may take log and trace and used as input in next call in mgris_diagnose

    for o in inp["observations"].obsnames_u:
        if o in tc.posterior.keys():  # test necessary in case we use post-process
            tc.posterior[o] = np.log10(tc.posterior[o])

    tab = np.zeros([n_x, n_y])
    # calculate corr. coeff. tab
    valsx, valsy = [], []
    # t = t.sel(chain=1)
    for i, xx in enumerate(xnames):
        results += logappend("  - {}/{}: {}".format(i + 1, len(xnames), xx))
        for j, yy in enumerate(ynames):
            # results += logappend("- {}/{}".format(j + 1, len(ynames)))

            if normx:
                tc.posterior[xx] = abs(
                    tc.posterior[xx] - float(tc.posterior[xx].median())
                )
            if normy:
                tc.posterior[yy] = abs(
                    tc.posterior[yy] - float(tc.posterior[yy].median())
                )

            valsx += [
                float(tc.posterior[xx].median()),
            ]
            valsy += [
                float(tc.posterior[yy].median()),
            ]
            x = tc.posterior[xx].data.flatten()
            y = tc.posterior[yy].data.flatten()

            # marginalize = True
            if marginalize:
                # remove middle xx% because it forces a ~ zero correlation
                # perc = 0.25  # remove perc % "extreme" values
                # ind = np.where(
                #     (y < np.quantile(y, 0.5 * (1 - perc)))
                #     | (y > np.quantile(y, 1 - 0.5 * (1 - perc)))
                # )[0]
                # if ind == []:  # failed
                #     x = x[ind]
                #     y = y[ind]
                # else:
                ind = [
                    True,
                ] * len(x)

                # mean of the deviation in sigma for all other ys
                tmp = [
                    np.abs(
                        tc.posterior[y2].data.flatten()[ind]
                        - np.nanmean(tc.posterior[y2].data.flatten()[ind])
                    )
                    / np.nanstd(tc.posterior[y2].data.flatten()[ind])
                    for y2 in ynames
                    if y2 != yy
                ]
                tmp = np.nanmean(
                    tmp,
                    axis=0,
                )  # needs to be the smallest possible

                tmp2 = np.abs(
                    tc.posterior[yy].data.flatten()[ind]
                    - np.nanmean(tc.posterior[yy].data.flatten()[ind])
                ) / np.nanstd(
                    tc.posterior[yy].data.flatten()[ind]
                )  # needs to be the largest possible

                # if yy == "C2157.636m" and xx == "cut_0":
                #     breakpoint()

                # take only some percentage where the other ys don't vary much and where y varies
                perc = 0.05
                ind = np.where((tmp / tmp2) < np.quantile((tmp / tmp2), perc))[0]
                x = x[ind]
                y = y[ind]

            # if xx=='C2157.636m' and yy=='cut_0':
            #     fig, axs = plt.subplots(1, len(ynames), figsize=(2 * len(ynames), 3))
            #     for i, y2 in enumerate(ynames):
            #         axs[i].scatter(
            #             tc.posterior[xx].data.flatten(),
            #             tc.posterior[y2].data.flatten(),
            #             alpha=np.min([1, 100 / len(tc.posterior[xx].data.flatten())]),
            #         )
            #         axs[i].scatter(
            #             x,
            #             tc.posterior[y2].data.flatten()[ind],
            #             alpha=np.min([1, 100 / len(ind)]),
            #             label=y2,
            #         )
            #         axs[i].legend()
            #     fig.savefig("try.pdf")

            if method == "spearman":
                corr, _ = spearmanr(x, y)
            elif method == "pearson":
                corr, _ = pearsonr(x, y)

            tab[i, j] = corr

    # 2D correlation map
    s = int(0.5 * len(xnames))
    corrcomp_max = np.zeros([len(xnames), len(xnames)])
    corrcomp_mean = np.zeros([len(xnames), len(xnames)])
    for i in range(len(xnames)):
        for i2 in range(len(xnames)):
            corrcomp_max[i, i2] = 1 - 0.5 * np.nanmax(np.abs(tab[i, :] - tab[i2, :]))
            if ~np.isfinite(corrcomp_max[i, i2]):
                corrcomp_max[i, i2] = 0.0
            corrcomp_mean[i, i2] = 1 - 0.5 * np.nanmean(
                np.abs(tab[i, :] - tab[i2, :])
            )  # mean or max?? if mean, change rcParams['corr_significant'] to a lower value
            if ~np.isfinite(corrcomp_mean[i, i2]):
                corrcomp_mean[i, i2] = 0.0

    # if label=='likelihood':
    #    tab = 0.5*(1+np.array(tab)) # for the likelihood -1 means the tracer doesn't constrain the model
    if (normx or normy) and label != "likelihood":
        tab = np.abs(tab)

    rescorr = {
        "tabcorr": tab,
        "corrcomp_max": corrcomp_max,
        "corrcomp_mean": corrcomp_mean,
        "valsx": valsx,
        "valsy": valsy,
    }

    return results, rescorr


# -------------------------------------------------------------------
def combeval2(tab, chunk):
    return np.nanmin(
        [np.nanmax(tab[:, c], axis=1) - np.nanmin(tab[:, c], axis=1) for c in chunk],
        axis=1,
    )


# -------------------------------------------------------------------
def combeval(tab, chunk):
    # tmp = np.array([[np.max(np.abs(tab[i,c])) for i in range(len(params))] for c in chunk])
    # tmp2 = np.array([[np.max(tab[i,c])*np.abs(np.min(tab[i,c])) for i in range(len(params))] for c in chunk])
    # tmp = [np.max(tab[:,c], axis=1)*np.abs(np.min(tab[:,c], axis=1)) for c in chunk]
    # tmp = np.array([np.mean(np.abs(tab[:,c]), axis=1)*np.mean(np.mean(np.abs(tab[:,c]), axis=0)) for c in chunk])

    # tmp = [np.nanmean(np.abs(tab[:,c]), axis=1) for c in chunk]
    # val = np.nanmean(tmp, axis=1)/np.std(tmp, axis=1)
    # val = np.nanmean(tmp, axis=1)/(np.nanmax(tmp, axis=1)-np.nanmin(tmp, axis=1))

    # Z test
    # val = [np.abs(np.nanmean(np.where(tab[5,c]>0, tab[5,c], np.nan)))*np.abs(np.nanmean(np.where(tab[5,c]<0, tab[5,c], np.nan))) for c in chunk]

    # take mean of all >0 values * mean of all <0 values for each param, then metric is the highest mean possible for all params (best constraints) with the smallest sdev possible (uniform weights for all params)
    # tmp = np.array([np.abs(np.nanmean(np.where(tab[:,c]>0, tab[:,c], np.nan), axis=1))*np.abs(np.nanmean(np.where(tab[:,c]<0, tab[:,c], np.nan), axis=1)) for c in chunk])

    # we wanna maximize the spread between tracers for each parameter. If the correlation coefficients are very different it means the tracers constrain well together and independently the parameter
    # tmp = np.array([np.nanstd(tab[:,c], axis=1)*np.nanmean(np.abs(tab[:,c]), axis=1) for c in chunk])
    # tmp = np.array([np.max(np.abs(tab[:,c]), axis=1) for c in chunk])
    # tmp = np.array([np.nanmax(tab[:,c], axis=1)-np.nanmin(tab[:,c], axis=1) for c in chunk])

    # Z test
    # val = np.array([np.nanstd(tab[5,c])*np.nanmean(np.abs(tab[5,c])) for c in chunk])

    # this is the quantity we wanna maximize
    # val = np.mean(tmp, axis=1)/np.std(tmp, axis=1)

    tmp1 = np.array(
        [
            np.abs(
                np.mean(tab[:, c], where=tab[:, c] > 0, axis=1)
                / np.std(tab[:, c], where=tab[:, c] > 0, axis=1)
            )
            for c in chunk
        ]
    )
    tmp2 = np.array(
        [
            np.abs(
                np.mean(tab[:, c], where=tab[:, c] < 0, axis=1)
                / np.std(tab[:, c], where=tab[:, c] < 0, axis=1)
            )
            for c in chunk
        ]
    )
    val = np.nanmean(
        [
            np.mean(tmp1, axis=1) / np.std(tmp1, axis=1),
            np.mean(tmp2, axis=1) / np.std(tmp2, axis=1),
        ],
        axis=0,
    )

    return val


# -------------------------------------------------------------------
def corr_xy_analyze(resdict, obs, obs2, params, inp=None, skip_plots=False):
    from Library.lib_main import logappend

    results = ""
    obs2 = [s.replace("likelihood_", "") for s in obs2]

    observations = inp["observations"]
    for o in observations.names:
        if observations.obs[o].obs_valuescale == "linear":
            observations.obs[o].value = 10 ** observations.obs[o].value

    # ===================================
    results += logappend("\n- Checking any useless tracers/parameters")
    useless_tracers = []
    for i, xx in enumerate(obs):
        if (
            np.nanmax(np.abs(resdict["rescorr_tracers"]["tabcorr"][i, :]))
            < rcParams["corr_significant"]
        ):
            useless_tracers += [
                xx,
            ]
            results += logappend(
                f"  - Tracer {xx} does not appear to constrain any parameter...",
                warning=True,
            )

    useless_parameters = []
    for i, xx in enumerate(params):
        if (
            np.nanmax(np.abs(resdict["rescorr_parameters"]["tabcorr"][i, :]))
            < rcParams["corr_significant"]
        ):
            useless_parameters += [
                xx,
            ]
            results += logappend(
                f"  - Parameter {xx} does not appear to be significantly constrained by any tracer...",
                warning=True,
            )

    # ===================================
    # results += logappend('\n- Sorted list of significant model parameters:')
    # c_parameters = [np.round(t[0],2) for t in resdict['rescorr_parameters_likelihood']['tabcorr']]
    # ind = np.argsort(c_parameters)[::-1]
    # for i in ind:
    #     results += logappend(f'  - {params[i]}: {c_parameters[i]}', warning=True)
    # c_parameters_dict = {params[i]: c_parameters[i] for i in range(len(params))}

    # ===================================

    threshold_significance = 0.0
    # ===================================

    tmp = [
        np.nanmax(np.abs(resdict["rescorr_parameters"]["tabcorr"][i, :]))
        for i in range(resdict["rescorr_parameters"]["tabcorr"].shape[0])
    ]
    resdict["rescorr_parameters"].update(
        {"corr_param_avg": [tmp, np.mean(tmp), np.std(tmp)]}
    )

    return results, resdict


# -------------------------------------------------------------------
def subgrid_distrib_trace_norm(tmp_o, s, grid, inp, trace, chain, draw):  # tmp_m,
    # here we do only obs
    # functions could be optimized for numpy (compared to tensors) but we keep the same
    # steps for clarity

    # 1st step: nearest neighbor interpolations (squeezing)
    trace2 = trace.posterior.sel(chain=chain, draw=draw)
    slices = [
        slice(None),
    ]
    refshape = [
        tmp_o.shape[0],
    ]
    for p in grid.params.names:
        ps = f"{p}_{s}"
        if ps in grid.params.names_notinf or p in inp["linterp_params"]:
            slices += [
                slice(None),
            ]
            refshape += [
                len(grid.params.values[p]),
            ]
        else:
            slices += [
                int(np.round(trace2[f"idx_{ps}"].data)),
            ]
            refshape += [
                1,
            ]
    slices = tuple(slices)
    tmp_o = tmp_o[slices].reshape(refshape)
    # tmp_m = tmp_m[slices].reshape(refshape)

    # 2nd step: linear interpolations (operation on two elements along one dimension, for all relevant parameters)
    for i, p in enumerate(grid.params.names):
        ps = f"{p}_{s}"
        if p in inp["linterp_params"]:
            # swapping axies to be able to do tmp[0] for this parameter
            tmp_o = tmp_o.swapaxes(i + 1, 0)
            # tmp_m = tmp_m.swapaxes(i + 1, 0)
            cell = int(np.floor(trace2[f"idx_{ps}"].data))
            cellup = int(
                np.min(
                    [
                        1 + cell,
                        len(grid.params.values[p]) - 1,
                    ]
                )
            )
            dx = trace2[f"idx_{ps}"].data - np.floor(trace2[f"idx_{ps}"].data)
            if inp["linterp_upsample"] > 0:
                dx = {inp["linterp_upsample"]} ** -1 * round(
                    dx * {inp["linterp_upsample"]}
                )

            # tmp_o = tmp_m[[cell]] * tmp_o[[cell]] * (1 - dx * tmp_m[[cellup]]) + tmp_m[
            #     [cellup]
            # ] * tmp_o[[cellup]] * (
            #     dx * tmp_m[[cell]] + (1 - dx) * (1 - tmp_m[[cellup]])
            # )
            tmp_o = tmp_o[[cell]] * (1 - dx) + tmp_o[[cellup]] * dx
            tmp_o = tmp_o.swapaxes(0, i + 1)

    # 3rd step: distributions
    for i, p in enumerate(grid.params.names):
        ps = f"{p}_{s}"
        if ps not in grid.params.names_notinf:
            continue
        newshape = tuple(
            [1]
            + [1] * i
            + [len(grid.params.values[p])]
            + [1] * (len(grid.params.names) - i - 1)
        )
        pgrid = grid.params.values[p].reshape(newshape)
        dgrid = np.append(
            abs(grid.params.values[p][:-1] - np.roll(grid.params.values[p], -1)[:-1]),
            abs((grid.params.values[p] - np.roll(grid.params.values[p], -1))[-2]),
        ).reshape(newshape)
        # for all params
        bmask = np.select(
            [
                (pgrid >= float(trace2[f"lowerbound_{ps}"].data))
                * (pgrid <= float(trace2[f"upperbound_{ps}"].data))
            ],
            [0],
            -np.inf,
        )
        if ps in inp["plaw_params"]:
            # print(ps, "plaw", inp["plaw_params"])
            weight = (1 + trace2[f"alpha_{ps}"].data) * pgrid
        elif ps in inp["smoothplaw_params"]:
            weight = (1 + trace2[f"alpha_{ps}"].data) * pgrid
            corr = -np.exp(
                -(10 ** trace2[f"lowerscale_{ps}"].data)
                * (pgrid - trace2[f"lowerbound_{ps}"].data)
            ) - np.exp(
                -(10 ** trace2[f"upperscale_{ps}"].data)
                * (trace2[f"upperbound_{ps}"].data - pgrid)
            )
            # trying to avoid floating errors
            corr = np.maximum(corr, -10)
            weight += corr
        elif ps in inp["brokenplaw_params"]:
            # print(ps, inp["brokenplaw_params"])
            weight = (
                (1 + trace2[f"alpha1_{ps}"].data)
                * (pgrid - trace2[f"pivot_{ps}"].data)
                * (pgrid <= trace2[f"pivot_{ps}"].data)
            ) + (
                (1 + trace2[f"alpha2_{ps}"].data)
                * (pgrid - trace2[f"pivot_{ps}"].data)
                * (pgrid > trace2[f"pivot_{ps}"].data)
            )
        elif ps in inp["normal_params"]:
            # print(ps, inp["normal_params"])
            weight = 0.4342944819032518 * (
                -((trace2[f"mu_{ps}"].data - pgrid) ** 2)
                / (2.0 * trace2[f"sigma_{ps}"].data ** 2)
            )
        elif ps in inp["doublenormal_params"]:
            # print(ps, inp["doublenormal_params"])
            weight = np.log10(
                np.exp(
                    -((trace2[f"mu1_{ps}"].data - pgrid) ** 2)
                    / (2.0 * trace2[f"sigma1_{ps}"].data ** 2)
                )
                + 10 ** trace2[f"ratio21_{ps}"].data
                * np.exp(
                    -((trace2[f"mu2_{ps}"].data - pgrid) ** 2)
                    / (2.0 * trace2[f"sigma2_{ps}"].data ** 2)
                )
            )
        else:
            logging.panic(f"Parameter {ps} not recognized")
        # account for irregular grid, need this *before normalizing*
        weight += np.log10(dgrid)  # need this *before normalizing*
        # normalize weights using only values within boundaries
        # max is in case boundaries are too close or prior on lower<upper not strict because of infcode. In this case though, bmask will be such that tmp_m will be 0. We just need to make sure it's negative enough wrt expected weights
        weight -= np.max([-100, np.log10(np.sum(10 ** (weight + bmask)))])
        # apply weights
        tmp_o += weight + bmask  # apply weights and boundary mask

    if len(grid.params.names_notinf) > 0:
        # operations along all axes except 0 (obs)
        axis = tuple([i + 1 for i in range(tmp_o.ndim - 1)])  # (1,2...)

        tmp_o = (np.log10(np.sum(10 ** tmp_o.astype("float64"), axis=axis))).astype(
            inp["precision"]
        )

    return tmp_o.squeeze()


# -------------------------------------------------------------------
def applyweights(ndim, i, p, s, grid, trace, weight):
    ps = f"{p}_{s}"
    pgrid = grid.params.values[p]
    dgrid = np.append(
        abs(grid.params.values[p][:-1] - np.roll(grid.params.values[p], -1)[:-1]),
        abs((grid.params.values[p] - np.roll(grid.params.values[p], -1))[-2]),
    )
    # for all params
    bmask = np.select(
        [
            (pgrid >= float(trace[f"lowerbound_{ps}"].data))
            * (pgrid <= float(trace[f"upperbound_{ps}"].data))
        ],
        [0],
        -np.inf,
    )
    weight *= pgrid
    weight += np.log10(dgrid)
    weight -= np.max([-100, np.log10(np.sum(10 ** (weight + bmask)))])
    dims = list(set(np.arange(ndim)) - set([i + 1]))
    weight = np.expand_dims(weight, dims)
    bmask = np.expand_dims(bmask, dims)
    return weight + bmask


def subgrid_distrib_trace_norm_new(
    tmp_o,
    s,
    grid,
    inp,
    trace,
    squeezeaxes,
    linterpaxes,
    plawaxes,
    smoothplawaxes,
    brokenplawaxes,
    normalaxes,
    doublenormalaxes,
):  # tmp_m,
    # here we do only obs
    # functions are optimized for numpy (compared to tensors)

    # 1st step: nearest neighbor interpolations (squeezing)
    for i in squeezeaxes[s]:
        p = grid.params.names[i]
        ps = f"{p}_{s}"
        cell = np.round(trace[f"idx_{ps}"]).data.astype("int16")
        tmp_o = np.take(
            tmp_o,
            [cell],
            axis=i + 1,
        )

    # 2nd step: linear interpolations (operation on two elements along one dimension, for all relevant parameters)
    for i in linterpaxes[s]:
        p = grid.params.names[i]
        ps = f"{p}_{s}"
        dx = trace[f"idx_{ps}"].data - np.floor(trace[f"idx_{ps}"].data)
        if inp["linterp_upsample"] > 0:
            dx = {inp["linterp_upsample"]} ** -1 * round(dx * {inp["linterp_upsample"]})
        cell = int(np.floor(trace[f"idx_{ps}"].data))
        cellup = int(
            np.min(
                [
                    1 + cell,
                    len(grid.params.values[p]) - 1,
                ]
            )
        )
        tmp_o = (
            np.take(
                tmp_o,
                [cell],
                axis=i + 1,
            )
            * (1 - dx)
            + np.take(
                tmp_o,
                [cellup],
                axis=i + 1,
            )
            * dx
        )
        # tmp_o = tmp_m[[cell]] * tmp_o[[cell]] * (1 - dx * tmp_m[[cellup]]) + tmp_m[
        #     [cellup]
        # ] * tmp_o[[cellup]] * (
        #     dx * tmp_m[[cell]] + (1 - dx) * (1 - tmp_m[[cellup]])
        # )

    # 3rd step: distributions
    for i in plawaxes[s]:
        p = grid.params.names[i]
        ps = f"{p}_{s}"
        tmp_o += applyweights(
            tmp_o.ndim, i, p, s, grid, trace, 1 + trace[f"alpha_{ps}"].data
        )

    for i in smoothplawaxes[s]:
        p = grid.params.names[i]
        ps = f"{p}_{s}"
        weight = (1 + trace[f"alpha_{ps}"].data) * grid.params.values[p]
        corr = -np.exp(
            -(10 ** trace[f"lowerscale_{ps}"].data)
            * (grid.params.values[p] - trace[f"lowerbound_{ps}"].data)
        ) - np.exp(
            -(10 ** trace[f"upperscale_{ps}"].data)
            * (trace[f"upperbound_{ps}"].data - grid.params.values[p])
        )
        # trying to avoid floating errors
        corr = np.maximum(corr, -10)
        weight += corr
        tmp_o += applyweights(tmp_o.ndim, i, p, s, grid, trace, weight)

    for i in brokenplawaxes[s]:
        p = grid.params.names[i]
        ps = f"{p}_{s}"
        weight = (
            (1 + trace[f"alpha1_{ps}"].data)
            * (grid.params.values[p] - trace[f"pivot_{ps}"].data)
            * (grid.params.values[p] <= trace[f"pivot_{ps}"].data)
        ) + (
            (1 + trace[f"alpha2_{ps}"].data)
            * (grid.params.values[p] - trace[f"pivot_{ps}"].data)
            * (grid.params.values[p] > trace[f"pivot_{ps}"].data)
        )
        tmp_o += applyweights(tmp_o.ndim, i, p, s, grid, trace, weight)

    for i in normalaxes[s]:
        p = grid.params.names[i]
        ps = f"{p}_{s}"
        weight = 0.4342944819032518 * (
            -((trace[f"mu_{ps}"].data - grid.params.values[p]) ** 2)
            / (2.0 * trace[f"sigma_{ps}"].data ** 2)
        )
        tmp_o += applyweights(tmp_o.ndim, i, p, s, grid, trace, weight)

    for i in doublenormalaxes[s]:
        p = grid.params.names[i]
        ps = f"{p}_{s}"
        weight = np.log10(
            np.exp(
                -((trace[f"mu1_{ps}"].data - grid.params.values[p]) ** 2)
                / (2.0 * trace[f"sigma1_{ps}"].data ** 2)
            )
            + 10 ** trace[f"ratio21_{ps}"].data
            * np.exp(
                -((trace[f"mu2_{ps}"].data - grid.params.values[p]) ** 2)
                / (2.0 * trace[f"sigma2_{ps}"].data ** 2)
            )
        )
        tmp_o += applyweights(tmp_o.ndim, i, p, s, grid, trace, weight)

    if len(grid.params.names_notinf) > 0:
        # operations along all axes except 0 (obs)
        axis = tuple([i + 1 for i in range(tmp_o.ndim - 1)])  # (1,2...)

        tmp_o = (np.log10(np.sum(10 ** tmp_o.astype("float64"), axis=axis))).astype(
            inp["precision"]
        )

    return tmp_o


# -------------------------------------------------------------------
def add2trace_sectors(
    inp,
    labels_obs,
    labels_params,
    scaling,
    pp,
    scale_context,
    scale_sysunc,
    scale_specific,
    reftrace,
    trace,
    verbose=False,
):
    nchains = trace.posterior.dims["chain"]
    ndraws = trace.posterior.dims["draw"]
    n_comps = inp["n_comps"]
    groups = inp["groups"]
    interp_order = inp["interp_order"]

    # make functions available
    if interp_order == 0 or inp["linterp_params"] == []:
        interp_f = nearest_func
    else:  # either explicit order 1 or auto
        interp_f = interp_func  # make_interp_f(inp, verbose=verbose)
        # read exs string from input?
    logging.debug("General interpolation function : {}".format(interp_f))

    # inp['obs_grid'][obs_grid.mask] = order_of_magnitude #that's if we wanna do like in mgris_search

    logging.info("")
    for io, o in enumerate(labels_obs + labels_params):
        if pp:
            jo = io  # if we wanna predict a tracer that was used for the inference
        else:
            jo = (
                inp["observations"].getidx(o) if o in inp["observations"].names else io
            )  # for a tracer in two sets; no need to get right idx for pp

        if inp["hierarchical"]:
            iobj = inp["observations"].obs[o].iobj

        kind = "parameter" if o in labels_params else "observable"

        # default = 0
        scale_context_tmp = np.zeros([n_comps, nchains, ndraws])
        if o in scale_context.keys():
            scale_context_tmp = scale_context[o]

        scale_tmp = deepcopy(trace.posterior["scale"])
        if kind == "parameter":
            scale_tmp -= inp["order_of_magnitude"]

        extensive_or_intensive = "extensive" if scaling[io] else "intensive"
        logging.info(
            "\nAdding to trace : {} ({}; {})".format(o, kind, extensive_or_intensive)
        )

        # if o in group_norm:
        #    logging.info('Using group normalization')

        # choose the way we do the linear combination of components
        sum_f = logsum

        # pre-fill component/group contributions
        if inp["grid"].params.names_inf != []:
            # component contributions
            trace.posterior["f_s_{}".format(o)] = deepcopy(
                trace.posterior["w"] * np.nan
            )

            # group contributions
            for g in range(len(groups)):
                trace.posterior["f_g{}_{}".format(g, o)] = deepcopy(reftrace * np.nan)

            # component contributions in each group
            for g in range(len(groups)):
                trace.posterior["f_g{}_s_{}".format(g, o)] = deepcopy(
                    trace.posterior["w"] * np.nan
                )

        # main loop
        logging.info("Iterating through chains...")
        for it in tqdm(range(nchains)):
            # pre-calculate params
            tmp_params = np.array(
                [
                    [
                        trace.posterior["idx_{}_{}".format(p, s)].data[it]
                        for p in inp["grid"].params.names
                    ]
                    for s in range(n_comps)
                ]
            )

            # pre-calculate weights
            tmp_w = np.array(
                [trace.posterior["w_{}".format(s)].data[it] for s in range(n_comps)]
            )

            if sum(np.isnan(tmp_params.flatten())) > 0:
                logging.critical("/!\\ NaNs in chain {}".format(it))
                breakpoint()
                # panic()

            if inp["use_scaling_specific"] is None:  # default
                if inp["hierarchical"]:
                    trace.posterior[o].data[it] = (
                        scale_context_tmp[iobj, it, :]
                        + scaling[io] * scale_tmp.data[it, :, iobj]
                        + np.array(
                            [
                                interp_f(
                                    inp["obs_grid"][jo],
                                    # inp["mask_grid"][jo],
                                    tmp_params[iobj, :, i],
                                    inp=inp,
                                )
                                for i in range(ndraws)
                            ]
                        )
                    )
                else:
                    trace.posterior[o].data[it] = [
                        float(
                            sum_f(
                                tmp_w[:, i],
                                [
                                    scale_context_tmp[s, it, i]
                                    + interp_f(
                                        inp["obs_grid"][jo],
                                        # inp["mask_grid"][jo],
                                        tmp_params[s, :, i],
                                        inp=inp,
                                    )
                                    for s in range(n_comps)
                                ],
                            )
                            + scaling[io] * scale_tmp.data[it, i]
                        )
                        for i in range(ndraws)
                    ]
            else:  # only for first sector (tests only)
                trace.posterior[o].data[it] = [
                    float(
                        sum_f(
                            [tmp_w[0, i]],
                            [
                                scale_context_tmp[0, it, i]
                                + interp_f(
                                    inp["obs_grid"][jo],
                                    # inp["mask_grid"][jo],
                                    tmp_params[0, :, i],
                                    inp=inp,
                                )
                            ],
                        )
                        + scaling[io] * scale_tmp.data[it, i]
                    )
                    for i in range(ndraws)
                ]

            # extensive?
            if scaling[io]:
                if inp["hierarchical"]:
                    trace.posterior[o].data[it, :] += inp["order_of_magnitude_obs"][
                        iobj
                    ]
                else:
                    trace.posterior[o].data[it, :] += inp["order_of_magnitude_obs"]
                if o in labels_obs:
                    trace.posterior[o].data[it, :] += (
                        scale_sysunc[io, it] + scale_specific[io, it]
                    )  # + scale_context_tmp[it] # (now possible per sector, moving to sum_f)

            # sector contributions
            # pre-calculate params
            tmp_params = np.array(
                [
                    [
                        trace.posterior["idx_{}_{}".format(p, s)].data[it]
                        for p in inp["grid"].params.names
                    ]
                    for s in range(n_comps)
                ]
            )
            # pre-calculate weights
            tmp_w = np.array(
                [trace.posterior["w_{}".format(s)].data[it] for s in range(n_comps)]
            )
            # for some intensive secondary parameters, we may not wanna apply the mixing weight
            if o in inp["intensive_secondary_parameters_noweight"]:
                tmp_w *= 0
                tmp_w += 1

            for s in range(n_comps):
                trace.posterior["f_s_{}".format(o)].data[it, :, s] = np.array(
                    [
                        sum_f(
                            [tmp_w[s, i]],
                            [
                                scale_context_tmp[s, it, i]
                                + interp_f(
                                    inp["obs_grid"][jo],
                                    # inp["mask_grid"][jo],
                                    tmp_params[s, :, i],
                                    inp=inp,
                                )
                            ],
                        )
                        for i in range(ndraws)
                    ]
                )
                # extensive?
                if scaling[io]:  # fraction (log)
                    trace.posterior["f_s_{}".format(o)].data[it, :, s] -= np.array(
                        [
                            sum_f(
                                tmp_w[:, i],
                                [
                                    scale_context_tmp[s, it, i]
                                    + interp_f(
                                        inp["obs_grid"][jo],
                                        # inp["mask_grid"][jo],
                                        tmp_params[s, :, i],
                                        inp=inp,
                                    )
                                    for s in range(n_comps)
                                ],
                            )
                            for i in range(ndraws)
                        ]
                    )
                # else just the value (log) in the component

            # group contributions
            for g in range(len(groups)):
                trace.posterior["f_g{}_{}".format(g, o)].data[it, :] = np.log10(
                    np.sum(
                        [
                            10 ** trace.posterior["f_s_{}".format(o)].data[it, :, s]
                            for s in groups[g]
                        ],
                        axis=(0),
                    )
                )
                # component contribution in each group
                for s in range(n_comps):
                    trace.posterior["f_g{}_s_{}".format(g, o)].data[it, :, s] = (
                        trace.posterior["f_s_{}".format(o)].data[it, :, s]
                        - trace.posterior["f_g{}_{}".format(g, o)].data[it, :]
                    )

    trace.posterior["scale_eff"] = deepcopy(trace.posterior["scale"])

    if inp["hierarchical"]:
        for iobj in range(inp["n_obj"]):
            try:  # backward compatibility
                tmp = inp["order_of_magnitude_obs"][iobj]  # HH
            except:
                tmp = inp["order_of_magnitude_obs"]
            trace.posterior["scale_eff"].data[:, :, iobj] += (
                tmp - inp["order_of_magnitude"]
            )
    else:
        try:  # backward compatibility
            tmp = inp["order_of_magnitude_obs"][0]  # obs set 0 (iobj)
        except:
            tmp = inp["order_of_magnitude_obs"]
        trace.posterior["scale_eff"].data += tmp - inp["order_of_magnitude"]

    return trace


# # -------------------------------------------------------------------
# def add2trace_plawnormal_tryfast(
#     inp,
#     labels_obs,
#     labels_params,
#     scaling,
#     pp,
#     scale_context,
#     scale_sysunc,
#     scale_specific,
#     reftrace,
#     trace,
#     verbose=False,
# ):
#     nchains = trace.posterior.dims["chain"]
#     ndraws = trace.posterior.dims["draw"]
#     n_comps = inp["n_comps"]

#     grid = inp["grid"]

#     # =======================

#     # quick summary of extensive / intensive
#     for io, o in enumerate(labels_obs + labels_params):
#         kind = "parameter" if o in labels_params else "observable"

#         extensive_or_intensive = "extensive" if scaling[io] else "intensive"
#         logging.info(
#             "Adding to trace: {} ({}; {})".format(o, kind, extensive_or_intensive)
#         )

#     # main loop
#     logging.info("\nIterating through chains...")
#     for it in range(nchains):
#         logging.info(f"\nChain {it+1}/{nchains}")
#         # pre-calculate
#         tmp_w = np.array(
#             [trace.posterior["w_{}".format(s)].data[it] for s in range(n_comps)]
#         )

#         scale_context_tmp = {}
#         scale_sysunc_specific = {}
#         for io, o in enumerate(labels_obs + labels_params):
#             if pp:
#                 jo = io  # if we wanna predict a tracer that was used for the inference
#             else:
#                 jo = (
#                     inp["observations"].getidx(o)
#                     if o in inp["observations"].names
#                     else io
#                 )
#             scale_context_tmp.update(
#                 {
#                     o: scale_context[o]
#                     if o in scale_context.keys()
#                     else scale_context[list(scale_context.keys()[0])] * 0
#                 }
#             )
#             scale_sysunc_specific.update(
#                 {
#                     o: scale_sysunc[io] + scale_specific[io]
#                     if o in labels_obs
#                     else scale_sysunc[0] * 0
#                 }
#             )

#         if inp[
#             "hierarchical"
#         ]:  # faster to replicate loops to avoid if statements for each draw
#             for i in tqdm(range(ndraws)):
#                 allres = []
#                 for n in range(n_comps):
#                     allres += [
#                         subgrid_distrib_trace_norm(
#                             deepcopy(inp["obs_grid"]),
#                             # deepcopy(inp["mask_grid"]),
#                             n,
#                             grid,
#                             inp,
#                             trace,
#                             it,
#                             i,
#                         ),
#                     ]
#                 allres = np.array(allres)

#                 for io, o in enumerate(labels_obs + labels_params):
#                     if pp:
#                         jo = io  # if we wanna predict a tracer that was used for the inference
#                     else:
#                         jo = (
#                             inp["observations"].getidx(o)
#                             if o in inp["observations"].names
#                             else io
#                         )

#                     iobj = inp["observations"].obs[o].iobj

#                     trace.posterior[o].data[it, i] = float(
#                         allres[iobj, jo]
#                         + scale_context_tmp[o][0, it, i]
#                         + scaling[io] * float(trace.posterior["scale"][it, i, iobj])
#                         + inp["order_of_magnitude_obs"][iobj]
#                         + scale_sysunc_specific[o][it, i]
#                         - (o in labels_params) * inp["order_of_magnitude"]
#                     )
#         else:  # not hierarchical
#             for i in tqdm(range(ndraws)):
#                 allres = []
#                 for n in range(n_comps):
#                     allres += [
#                         subgrid_distrib_trace_norm(
#                             deepcopy(inp["obs_grid"]),
#                             # deepcopy(inp["mask_grid"]),
#                             n,
#                             grid,
#                             inp,
#                             trace,
#                             it,
#                             i,
#                         ),
#                     ]
#                 allres = np.array(allres)

#                 for io, o in enumerate(labels_obs + labels_params):
#                     if pp:
#                         jo = io  # if we wanna predict a tracer that was used for the inference
#                     else:
#                         jo = (
#                             inp["observations"].getidx(o)
#                             if o in inp["observations"].names
#                             else io
#                         )

#                     trace.posterior[o].data[it, i] = float(
#                         logsum(
#                             tmp_w[:, i],
#                             allres[:, jo]
#                             + 0  # scale_context_tmp[o][0, it, i]
#                             + scaling[io] * float(trace.posterior["scale"][it, i])
#                             + inp["order_of_magnitude_obs"]
#                             + 0  # scale_sysunc_specific[o][it, i]
#                             - (o in labels_params) * inp["order_of_magnitude"],
#                         )
#                     )

#     trace.posterior["scale_eff"] = deepcopy(trace.posterior["scale"])

#     if inp["hierarchical"]:
#         for iobj in range(inp["n_obj"]):
#             try:  # backward compatibility
#                 tmp = inp["order_of_magnitude_obs"][iobj]  # HH
#             except:
#                 tmp = inp["order_of_magnitude_obs"]
#             trace.posterior["scale_eff"].data[:, :, iobj] += (
#                 tmp - inp["order_of_magnitude"]
#             )
#     else:
#         try:  # backward compatibility
#             tmp = inp["order_of_magnitude_obs"][0]
#         except:
#             tmp = inp["order_of_magnitude_obs"]
#         trace.posterior["scale_eff"].data += tmp - inp["order_of_magnitude"]

#     return trace


# -------------------------------------------------------------------
def add2trace_plawnormal(
    inp,
    labels_obs,
    labels_params,
    scaling,
    pp,
    scale_context,
    scale_sysunc,
    scale_specific,
    reftrace,
    trace,
    verbose=False,
):
    nchains = trace.posterior.dims["chain"]
    ndraws = trace.posterior.dims["draw"]
    n_comps = inp["n_comps"]

    grid = inp["grid"]

    # =======================

    # quick summary of extensive / intensive
    for io, o in enumerate(labels_obs + labels_params):
        kind = "parameter" if o in labels_params else "observable"

        extensive_or_intensive = "extensive" if scaling[io] else "intensive"
        logging.info(
            "Adding to trace: {} ({}; {})".format(o, kind, extensive_or_intensive)
        )

    # main loop
    logging.info("\nIterating through chains...")
    for it in range(nchains):
        logging.info(f"\nChain {it+1}/{nchains}")
        # pre-calculate
        tmp_w = np.array(
            [trace.posterior["w_{}".format(s)].data[it] for s in range(n_comps)]
        )

        for i in tqdm(range(ndraws)):
            allres = []
            for n in range(n_comps):
                allres += [
                    np.atleast_1d(
                        subgrid_distrib_trace_norm(
                            deepcopy(inp["obs_grid"]),
                            # deepcopy(inp["mask_grid"]),
                            n,
                            grid,
                            inp,
                            trace,
                            it,
                            i,
                        )
                    ),
                ]
            allres = np.array(allres)

            for io, o in enumerate(labels_obs + labels_params):
                if pp:
                    jo = io  # if we wanna predict a tracer that was used for the inference
                else:
                    jo = (
                        inp["observations"].getidx(o)
                        if o in inp["observations"].names
                        else io
                    )

                if inp["hierarchical"]:
                    iobj = inp["observations"].obs[o].iobj

                tmp2 = (
                    scale_sysunc[io, it, i] + scale_specific[io, it, i]
                    if o in labels_obs
                    else 0
                )

                scale_context_tmp = 0

                if inp["hierarchical"]:
                    if o in scale_context.keys():
                        scale_context_tmp = scale_context[o][iobj, it, i]

                    trace.posterior[o].data[it, i] = float(
                        allres[iobj, jo]
                        + scale_context_tmp
                        + scaling[io] * float(trace.posterior["scale"][it, i, iobj])
                        + inp["order_of_magnitude_obs"][iobj]
                        + tmp2
                        - (o in labels_params) * inp["order_of_magnitude"]
                    )
                else:
                    if o in scale_context.keys():
                        scale_context_tmp = scale_context[o][0, it, i]

                    trace.posterior[o].data[it, i] = float(
                        logsum(
                            tmp_w[:, i],
                            allres[:, jo]
                            + scale_context_tmp
                            + scaling[io] * float(trace.posterior["scale"][it, i])
                            + inp["order_of_magnitude_obs"]
                            + tmp2
                            - (o in labels_params) * inp["order_of_magnitude"],
                        )
                    )

    trace.posterior["scale_eff"] = deepcopy(trace.posterior["scale"])

    if inp["hierarchical"]:
        for iobj in range(inp["n_obj"]):
            try:  # backward compatibility
                tmp = inp["order_of_magnitude_obs"][iobj]  # HH
            except:
                tmp = inp["order_of_magnitude_obs"]
            trace.posterior["scale_eff"].data[:, :, iobj] += (
                tmp - inp["order_of_magnitude"]
            )
    else:
        try:  # backward compatibility
            tmp = inp["order_of_magnitude_obs"][0]
        except:
            tmp = inp["order_of_magnitude_obs"]
        trace.posterior["scale_eff"].data += tmp - inp["order_of_magnitude"]

    return trace


# -------------------------------------------------------------------
def add2trace_plawnormal_new(
    inp,
    labels_obs,
    labels_params,
    scaling,
    pp,
    scale_context,
    scale_sysunc,
    scale_specific,
    reftrace,
    trace,
    verbose=False,
):
    nchains = trace.posterior.dims["chain"]
    ndraws = trace.posterior.dims["draw"]
    n_comps = inp["n_comps"]

    grid = inp["grid"]

    # =======================

    # quick summary of extensive / intensive
    for io, o in enumerate(labels_obs + labels_params):
        kind = "parameter" if o in labels_params else "observable"

        extensive_or_intensive = "extensive" if scaling[io] else "intensive"
        logging.info(
            "Adding to trace: {} ({}; {})".format(o, kind, extensive_or_intensive)
        )

    squeezeaxes = {
        s: [
            i
            for i, p in enumerate(grid.params.names)
            if f"{p}_{s}" not in grid.params.names_notinf
            and p not in inp["linterp_params"]
        ]
        for s in range(n_comps)
    }
    linterpaxes = {
        s: [
            i
            for i, p in enumerate(grid.params.names)
            if f"{p}_{s}" in inp["linterp_params"]
        ]
        for s in range(n_comps)
    }
    plawaxes = {
        s: [
            i
            for i, p in enumerate(grid.params.names)
            if f"{p}_{s}" in inp["plaw_params"]
        ]
        for s in range(n_comps)
    }
    smoothplawaxes = {
        s: [
            i
            for i, p in enumerate(grid.params.names)
            if f"{p}_{s}" in inp["smoothplaw_params"]
        ]
        for s in range(n_comps)
    }
    brokenplawaxes = {
        s: [
            i
            for i, p in enumerate(grid.params.names)
            if f"{p}_{s}" in inp["brokenplaw_params"]
        ]
        for s in range(n_comps)
    }
    normalaxes = {
        s: [
            i
            for i, p in enumerate(grid.params.names)
            if f"{p}_{s}" in inp["normal_params"]
        ]
        for s in range(n_comps)
    }
    doublenormalaxes = {
        s: [
            i
            for i, p in enumerate(grid.params.names)
            if f"{p}_{s}" in inp["doublenormal_params"]
        ]
        for s in range(n_comps)
    }

    # main loop
    logging.info("\nIterating through chains...")
    for it in range(nchains):
        logging.info(f"\nChain {it+1}/{nchains}")
        # pre-calculate
        tmp_w = np.array(
            [trace.posterior["w_{}".format(s)].data[it] for s in range(n_comps)]
        )
        for i in tqdm(range(ndraws)):
            allres = []
            trace2 = trace.posterior.sel(chain=it, draw=i)
            for n in range(n_comps):
                allres += [
                    subgrid_distrib_trace_norm_new(
                        deepcopy(inp["obs_grid"]),
                        # deepcopy(inp["mask_grid"]),
                        n,
                        grid,
                        inp,
                        trace2,
                        squeezeaxes,
                        linterpaxes,
                        plawaxes,
                        smoothplawaxes,
                        brokenplawaxes,
                        normalaxes,
                        doublenormalaxes,
                    ),
                ]
            allres = np.array(allres)

            for io, o in enumerate(labels_obs + labels_params):
                if pp:
                    jo = io  # if we wanna predict a tracer that was used for the inference
                else:
                    jo = (
                        inp["observations"].getidx(o)
                        if o in inp["observations"].names
                        else io
                    )

                if inp["hierarchical"]:
                    iobj = inp["observations"].obs[o].iobj

                tmp2 = (
                    scale_sysunc[io, it, i] + scale_specific[io, it, i]
                    if o in labels_obs
                    else 0
                )

                scale_context_tmp = 0

                if inp["hierarchical"]:
                    if o in scale_context.keys():
                        scale_context_tmp = scale_context[o][iobj, it, i]

                    trace.posterior[o].data[it, i] = float(
                        allres[iobj, jo]
                        + scale_context_tmp
                        + scaling[io] * float(trace.posterior["scale"][it, i, iobj])
                        + inp["order_of_magnitude_obs"][iobj]
                        + tmp2
                        - (o in labels_params) * inp["order_of_magnitude"]
                    )
                else:
                    if o in scale_context.keys():
                        scale_context_tmp = scale_context[o][0, it, i]

                    trace.posterior[o].data[it, i] = float(
                        logsum(
                            tmp_w[:, i],
                            allres[:, jo]
                            + scale_context_tmp
                            + scaling[io] * float(trace.posterior["scale"][it, i])
                            + inp["order_of_magnitude_obs"]
                            + tmp2
                            - (o in labels_params) * inp["order_of_magnitude"],
                        )
                    )

    trace.posterior["scale_eff"] = deepcopy(trace.posterior["scale"])

    if inp["hierarchical"]:
        for iobj in range(inp["n_obj"]):
            try:  # backward compatibility
                tmp = inp["order_of_magnitude_obs"][iobj]  # HH
            except:
                tmp = inp["order_of_magnitude_obs"]
            trace.posterior["scale_eff"].data[:, :, iobj] += (
                tmp - inp["order_of_magnitude"]
            )
    else:
        try:  # backward compatibility
            tmp = inp["order_of_magnitude_obs"][0]
        except:
            tmp = inp["order_of_magnitude_obs"]
        trace.posterior["scale_eff"].data += tmp - inp["order_of_magnitude"]

    return trace


# -------------------------------------------------------------------
def get_refparam(inp):
    """
    To make a temporary array for trace to fill later
    """
    params = inp["grid"].params
    if params.names_inf != []:
        refparam = "idx_{}".format(params.names_inf[0])
    else:
        if params.names_plaw != []:
            refparam = "alpha_{}".format(params.names_plaw[0])
        elif params.names_smoothplaw != []:
            refparam = "alpha_{}".format(params.names_smoothplaw[0])
        elif params.names_brokenplaw != []:
            refparam = "alpha1_{}".format(params.names_brokenplaw[0])
        elif params.names_normal != []:
            refparam = "mu_{}".format(params.names_normal[0])
        elif params.names_doublenormal != []:
            refparam = "mu1_{}".format(params.names_doublenormal[0])

    return refparam


# -------------------------------------------------------------------
def add2trace_wrapper(
    trace, inp, labels_obs, labels_params=[], scaling_params=[], pp=False, verbose=False
):
    """
    Add observable values to trace using indices and interpolation
    Can also add new parameters fro post-processing table
    """

    n_comps = inp["n_comps"]
    nchains = trace.posterior.dims["chain"]
    ndraws = trace.posterior.dims["draw"]
    sysunc = inp["sysunc"]

    # backward compatibility
    if sysunc is not None:
        for s in sysunc:
            if len(sysunc[s]) == 2:
                sysunc[s] = [0.0, sysunc[s][0], sysunc[s][1]]
    if "use_scaling_specific" not in inp.keys():
        inp["use_scaling_specific"] = inp["use_scaling_uniform"]

    # will be used in specific add2trace kernel for each method (sectors, distributions...)
    scaling = [
        (
            inp["observations"].getobsname(o) not in inp["intensive_obs"]
            and inp["observations"].getobsname(o)
            not in inp["intensive_secondary_parameters"]
        )
        for o in labels_obs
    ] + scaling_params

    # will be used several times by kernel
    refparam = get_refparam(inp)
    reftrace = deepcopy(trace.posterior[refparam])
    reftrace.data *= 0

    # add w_s parameters for conveniency (e.g., for get_estimators)
    for s in range(n_comps):
        trace.posterior["w_{}".format(s)] = deepcopy(reftrace)
        trace.posterior["w_{}".format(s)].data = trace.posterior["w"].data[:, :, s]
    # or trace.posterior['w_{}'.format(s)] = trace.posterior['w'].sel(w_dim_0=s)

    # make scale available if doesn't exist (tests or if use_scaling is [None])
    if "scale" not in trace.posterior.data_vars:
        trace.posterior["scale"] = deepcopy(reftrace)
        trace.posterior["scale"].data *= 0

    # pre-process for all obs
    # systematic uncertainties / nuisance variables
    scale_sysunc = np.zeros([len(labels_obs), nchains, ndraws])
    if sysunc is not None:
        for s in sysunc:
            for io, o in enumerate(labels_obs):
                if (
                    o in sysunc[s][2]
                    or inp["observations"].getobsname(o) in sysunc[s][2]
                ):
                    scale_sysunc[io] += trace.posterior[s].data

    # pre-process for all obs
    scale_specific = np.zeros([len(labels_obs), nchains, ndraws])  # default
    # override if observation in use_scaling_specific
    if inp["use_scaling_specific"] is not None:
        for io, o in enumerate(labels_obs):
            if (
                o in inp["use_scaling_specific"]
                or inp["observations"].getobsname(o) in inp["use_scaling_specific"]
            ):
                logging.info("Using scale_specific constraint for {}".format(o))
                scale_specific[io] = trace.posterior["scale_specific"].data

    # pre-process for all obs
    # context-specific scaling
    scale_context = {
        o: np.zeros([n_comps, nchains, ndraws]) for o in labels_obs
    }  # default

    # may need to update for post-processing
    if pp and len(inp["active_rules"]) > 0:
        inp_bu = deepcopy(inp)  # will need to revert
        inp["observations"].names = labels_obs
        inp["observations"].values = np.zeros(len(labels_obs))
        context_specific_rules = vars(
            __import__(
                "{}.Library.lib".format(inp["contextshort"].replace("/", ".")),
                globals(),
                locals(),
                ["lib"],
            )
        )["context_specific_rules"]
        inp["active_rules"] = context_specific_rules(inp, verbose=verbose)
        for i, rule in enumerate(inp["active_rules"]):
            if rule in inp["active_rules"]:
                logging.info("Activating context-specific rule : {}".format(rule))
                logging.info("----------------------------------")
                inp["active_rules_details"].update({rule: inp["active_rules"][rule]})

    for rule in inp["active_rules"]:
        for io, o in enumerate(labels_obs):
            cs_args = inp["active_rules_details"][rule]["cs_args"]  # used in eval

            sc = eval(
                inp["active_rules_details"][rule]["scale_context_add2trace"],
                locals(),
                globals(),
            )
            if o in sc.keys():
                if type(sc[o]) == list:  # per comp
                    scale_context[o] = np.log10(
                        np.array([sc[o][s].data for s in range(n_comps)])
                    )
                else:  # same for all comps, forcing list with one comp
                    scale_context[o] = np.log10(
                        np.array([sc[o].data for s in range(n_comps)])
                    )
            else:
                scale_context[o] = np.zeros([n_comps, nchains, ndraws])  # default

    if pp and len(inp["active_rules"]) > 0:
        inp = inp_bu

    # pre-fill trace
    logging.info("")
    for o in labels_obs + labels_params:
        trace.posterior[o] = deepcopy(reftrace)

    # process trace
    # no power-law/normal distribution: using faster functions
    func = (
        add2trace_sectors
        if inp["grid"].params.names_notinf == []
        else add2trace_plawnormal
    )
    # add2trace_plawnormal does not treat yet intensive parameters
    trace = func(
        inp,
        labels_obs,
        labels_params,
        scaling,
        pp,
        scale_context,
        scale_sysunc,
        scale_specific,
        reftrace,
        trace,
        verbose=verbose,
    )

    return trace


# --------------------------------------
def limitsamples(nsamples_per_job, inp):
    if nsamples_per_job > inp["nsamples_per_job_max"]:
        nsamples_per_job = int(inp["nsamples_per_job_max"])
        logging.warning(
            "/!\\ Forcing samples per job to maximum allowed ({}; can be changed in rcParams)".format(
                int(inp["nsamples_per_job_max"])
            )
        )

    if nsamples_per_job < inp["nsamples_per_job_min"]:
        nsamples_per_job = int(inp["nsamples_per_job_min"])
        logging.warning(
            "/!\\ Forcing samples per job to minimum allowed ({}; can be changed in rcParams)".format(
                int(inp["nsamples_per_job_min"])
            )
        )

    return int(nsamples_per_job)


# --------------------------------------


def getp(ps):
    "from <param>_<sector> get only <param>"
    if re.search(".*_[0-9]", ps) is None:
        return ps
    else:
        return "_".join(ps.split("_")[:-1])
