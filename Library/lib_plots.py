import numpy as np
import os
from copy import deepcopy
import logging

import matplotlib

# matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

# plt.rcParams.update({"text.usetex": True})
import corner
import seaborn as sns
from matplotlib.ticker import AutoMinorLocator

import arviz as az

az.rcparams.rcParams["plot.max_subplots"] = 200

import xarray

import pickle

import re

from Library.lib_mc import *


# -------------------------------------------------------------------
def diagnostic_plots(
    trace, inp, ci=None, verbose=False, kind=("all"), label="", resdict=None
):  # trace is actually trace.posterior
    ext = "png" if label == "raw" else "pdf"

    if ci is None:
        ci = inp["ci"]

    if label != "":
        label = "_" + label

    if kind == ("all"):
        kind = (
            "context_specific_plots",
            "fracplot",
            "matchplot",
            "trace_params",
            "trace_observables",
            "trace_inferred",
            # "violin_observables",
            # "violin_norm_observables",
            # "violin_params",
            #'boxplot_observables',
            #'boxplot_norm_observables',
            #'boxplot_params',
            "posterior_params",
            "posterior_observables",
            "corner",
            "autocorr",
            "KDE",
            #'corr'
            #'forest',
            #'ridge',
            #'colored',
        )

    logging.info("\n")
    logging.info("The following plots will be done: {}\n".format(kind))

    n_comps = inp["n_comps"]
    # https://docs.pymc.io/api/plots.html
    plt.close("all")  # close all remaining plots if any in ipython

    plotdir = inp["output_directory"] + "/Plots/"
    os.makedirs(plotdir, exist_ok=True)
    logging.info("Output directory : {}\n".format(plotdir))

    # ======================================
    with open(inp["output_directory"] + "results_search.pkl", "rb") as f:
        resdict_search = pickle.load(f)
    with open(inp["output_directory"] + "results_process.pkl", "rb") as f:
        resdict_process = pickle.load(f)

    # ======================================
    # temporary, to be moved or as an option
    if "context_specific_plots" in kind:
        try:  # backward compatibility
            context_specific_plots = vars(
                __import__(
                    "{}.Library.lib".format(inp["contextshort"].replace("/", ".")),
                    globals(),
                    locals(),
                    ["lib"],
                )
            )["context_specific_plots"]
        except:  # old one, not allowing contexts within subdirs
            context_specific_plots = vars(
                __import__(
                    "Contexts.{}.Library.lib".format(inp["contextshort"]),
                    globals(),
                    locals(),
                    ["lib"],
                )
            )["context_specific_plots"]
        context_specific_plots(trace, inp, verbose=verbose)

    # if 'SFGX' in inp['context'] or 'BOND' in inp['context']:
    #    if 'SFGX' in inp['context']:
    #       #from Contexts.SFGX.Library.lib import sectorplot, spectrumplot
    #       from Contexts.SFGX.Library.lib import context_specific_plots
    #    elif 'BOND' in inp['context']:
    #       #from Contexts.BOND.Library.lib import sectorplot, spectrumplot
    #       from Contexts.BOND.Library.lib import context_specific_plots

    #    context_specific_plots(trace, inp, verbose=verbose)

    # ======================================
    # ======================================
    # ======================================
    # MATCH and FRAC PLOTS
    # ======================================
    # ======================================
    # ======================================

    # ======================================
    # plot to show obs vs. model match
    if "matchplot" in kind:
        logging.info("- match plots")
        matchplot(trace, inp)
        matchplot_disk(trace, inp)

    if "fracplot" in kind and inp["grid"].params.names_notinf == []:
        logging.info("- fraction plots")
        for f in ("fracplot_groups", "fracplot_groups", "fracplot_both"):
            f2 = inp["output_directory"] + "/Plots/{}.pdf".format(f)
            if FileExists(f2):
                os.remove(f2)
        fracplot(trace, inp, compare="groups")
        fracplot(trace, inp, compare="components")
        fracplot(trace, inp, compare="both")

    # ======================================

    params_forplots = resdict_process["params_forplots"]
    paramnames_forplots = resdict_process["paramnames_forplots"]

    if verbose:
        logging.info("  - Parameters  : {}".format(params_forplots))
        logging.info("  - Parameters  : {}".format(paramnames_forplots))
        logging.info("  - Observables : {}".format(inp["observations"].names))
        # logging.info(' - To infer : {}'.format(inp['lines_to_infer']))

    # ======================================
    # ======================================
    # ======================================
    # SOME TRANSFORMATIONS AND NEW PARAMS
    # ======================================
    # ======================================
    # ======================================

    # add scale_eff to prior predictive
    if "prior_predictive" in resdict_process.keys():
        if "w" in resdict_process["prior_predictive"].keys():
            # end of backward compatibility tests
            resdict_process["prior_predictive"]["scale_eff"] = deepcopy(
                resdict_process["prior_predictive"]["scale"]
            )
            # only 1 chain for prior predictive sampling
            if inp["n_obj"] == 1:
                # test needed only because of shape=(1,) not working with blackjax
                resdict_process["prior_predictive"]["scale_eff"][0, :] += (
                    inp["order_of_magnitude_obs"][0] - inp["order_of_magnitude"]
                )
            else:
                for iobj in range(inp["n_obj"]):
                    resdict_process["prior_predictive"]["scale_eff"][0, :, iobj] += (
                        inp["order_of_magnitude_obs"][iobj] - inp["order_of_magnitude"]
                    )

    # add deltabounds and bounds
    if "grid" in inp.keys():  # for mgris_diagnose
        if inp["grid"].params.names_notinf != []:
            for ps in inp["grid"].params.names_notinf:
                p = getp(ps)
                trace[f"deltabound_{ps}"] = trace[f"upperbound_{ps}"].copy()
                trace[f"deltabound_{ps}"] = (
                    trace[f"upperbound_{ps}"] - trace[f"lowerbound_{ps}"]
                )
                if f"deltabound_{p}" not in paramnames_forplots:
                    paramnames_forplots += [
                        f"deltabound_{p}",
                    ]
                if f"deltabound_{p}" not in params_forplots:
                    params_forplots += [
                        f"deltabound_{ps}",
                    ]
                if "prior_predictive" in resdict_process.keys():
                    if "w" in resdict_process["prior_predictive"].keys():
                        # end of backward compatibility tests
                        resdict_process["prior_predictive"][f"deltabound_{ps}"] = (
                            resdict_process["prior_predictive"][f"upperbound_{ps}"]
                            - resdict_process["prior_predictive"][f"lowerbound_{ps}"]
                        )

    # if inp["grid"].params.names_notinf != []:
    #     for p in inp["grid"].params.names_notinf:
    #         for s in range(inp["n_comps"]):
    #             print(p, s)
    #             trace[f"bothbounds_{p}_{s}"] = trace[f"deltabound_{p}_{s}"].copy()
    #             trace[f"bothbounds_{p}_{s}"].data = trace[
    #                 f"upperbound_{p}_{s}"
    #             ].data.copy()
    #             trace[f"bothbounds_{p}_{s}"].data.resize(
    #                 trace[f"bothbounds_{p}_{s}"].data.shape[0],
    #                 trace[f"bothbounds_{p}_{s}"].data.shape[1],
    #                 2,
    #             )
    #             trace[f"bothbounds_{p}_{s}"].data[:, :, 0] = trace[
    #                 f"lowerbound_{p}_{s}"
    #             ].data
    #             trace[f"bothbounds_{p}_{s}"].data[:, :, 1] = trace[
    #                 f"upperbound_{p}_{s}"
    #             ].data
    #             paramnames_forplots += [
    #                 f"bothbounds_{p}",
    #             ]
    #             params_forplots += [
    #                 f"bothbounds_{p}_{s}",
    #             ]

    # ======================================
    # ======================================
    # ======================================
    # CORR PLOTS
    # ======================================
    # ======================================
    # ======================================

    if "corr" in kind:
        addobs = []
        obs = inp["observations"].obsnames_u + addobs

        logging.info("- correlation of each tracer vs. parameters")
        # for a given param get tracers that correlate best
        corr_xy_plot(
            inp,
            obs,
            params_forplots,
            verbose=verbose,
            marginalize=True,
            label="tracers",
            resdict=resdict,
        )

        logging.info("- correlation of each parameter vs. tracers")
        # for a given tracer get params that correlate best
        corr_xy_plot(
            inp,
            params_forplots,
            obs,
            verbose=verbose,
            marginalize=True,
            label="parameters",
            resdict=resdict,
        )

        obs2 = [f"likelihood_{o}" for o in inp["observations"].names]
        corr_xy_plot(
            inp,
            obs2,
            params_forplots,
            verbose=verbose,
            marginalize=True,
            label="tracers_obs",
            resdict=resdict,
        )
        corr_xy_plot(
            inp,
            params_forplots,
            obs2,
            verbose=verbose,
            marginalize=True,
            label="parameters_obs",
            resdict=resdict,
        )

    # ======================================
    # ======================================
    # ======================================
    # TRACE PLOTS
    # ======================================
    # ======================================
    # ======================================

    # ======================================
    if "trace_params" in kind:
        logging.info("- trace (parameters)")

        alpha_plot_trace = 0.5

        if n_comps == 1:  # only one component
            tmp = params_forplots

            for p in inp["context_specific_params"]:
                tmp += [
                    p,
                ]

            tmp += ["scale_eff", "log_likelihood"]
            # if tmp[0] not in trace.keys(): #in case we want the full trace
            #    tmp = ['idx_{}_0'.format(p) for p in inp['grid'].params.names if 'idx_{}_0'.format(p) in resdict_search['unobserved_RVs']]
            #    tmp += ['scale','log_likelihood']

            fig1 = az.plot_trace(
                trace,
                var_names=tmp,
                divergences="bottom",
                compact=True,
                plot_kwargs={"alpha": alpha_plot_trace},
                trace_kwargs={"alpha": alpha_plot_trace},
                hist_kwargs={"alpha": alpha_plot_trace},
                rug_kwargs={"alpha": alpha_plot_trace},
                kind="rank_vlines",
            )
            plt.tight_layout()
            fig2 = az.plot_trace(
                trace,
                var_names=tmp,
                combined=True,
                divergences="bottom",
                compact=True,
                plot_kwargs={"alpha": alpha_plot_trace},
                trace_kwargs={"alpha": alpha_plot_trace},
                hist_kwargs={"alpha": alpha_plot_trace},
                rug_kwargs={"alpha": alpha_plot_trace},
                fill_kwargs={"alpha": alpha_plot_trace},
            )
            # confidence intervals
            for i, k in enumerate(tmp):
                if k == "log_likelihood":
                    continue
                hdi = az.hdi(trace[k], hdi_prob=ci).to_array().data[0]
                if len(hdi) == 1:  # scale, scale_eff
                    for h in hdi:
                        fig2[i, 0].axvspan(h[0], h[1], color="tab:blue", alpha=0.2)
                else:
                    fig2[i, 0].axvspan(hdi[0], hdi[1], color="tab:blue", alpha=0.2)

            if "prior_predictive" in resdict_process.keys():
                if "w" in resdict_process["prior_predictive"].keys():
                    # end of backward compatibility tests
                    for i, k in enumerate(tmp):
                        # for s in range(n_comps):
                        if k == "log_likelihood":
                            continue
                        if k in resdict_process["prior_predictive"].keys():
                            sns.kdeplot(
                                resdict_process["prior_predictive"][k][0],
                                color="black",
                                alpha=0.1,
                                ax=fig2[i, 0],
                                fill=True,
                                zorder=-20,  # behind
                                lw=0,
                            )

            plt.tight_layout()
        else:  # several components
            tmp = paramnames_forplots
            tmp3 = ["scale_eff", "log_likelihood"]
            # if tmp[0]+'_0' not in trace.keys(): #in case we want the full trace #_0 is because we use p above
            #    tmp = ['idx_{}'.format(p) for p in inp['grid'].params.names if 'idx_{}_0'.format(p) in resdict_search['unobserved_RVs']]
            #    tmp3 = ['scale','log_likelihood']
            tmp2 = deepcopy(tmp)
            if "ncomps" in resdict_search["unobserved_RVs"]:
                tmp2 += [
                    "ncomps",
                ]
            for p in inp["context_specific_params"]:
                tmp2 += [
                    p,
                ]
            fig1 = az.plot_trace(
                mergetrace(trace, tmp),
                var_names=tmp2
                + [
                    "w",
                ]
                + tmp3,
                divergences="bottom",
                compact=True,
                plot_kwargs={"alpha": alpha_plot_trace},
                trace_kwargs={"alpha": alpha_plot_trace},
                hist_kwargs={"alpha": alpha_plot_trace},
                rug_kwargs={"alpha": alpha_plot_trace},
            )
            plt.tight_layout()
            fig2 = az.plot_trace(
                mergetrace(trace, tmp),
                var_names=tmp2
                + [
                    "w",
                ]
                + tmp3,
                combined=True,
                divergences="bottom",
                compact=True,
                plot_kwargs={"alpha": alpha_plot_trace},
                trace_kwargs={"alpha": alpha_plot_trace},
                hist_kwargs={"alpha": alpha_plot_trace},
                rug_kwargs={"alpha": alpha_plot_trace},
                fill_kwargs={"alpha": alpha_plot_trace},
            )
            # confidence intervals
            for i, k in enumerate(
                tmp2
                + [
                    "w",
                ]
                + tmp3
            ):
                if k in ("log_likelihood", "ncomps"):
                    continue
                if k in ("scale", "scale_eff"):
                    hdi = az.hdi(trace[k], hdi_prob=ci).to_array().data[0]
                    if trace[k].ndim == 2:  # scale shape=(1,) blackjax
                        fig2[i, 0].axvspan(hdi[0], hdi[1], color="tab:blue", alpha=0.2)
                    else:
                        for h in hdi:
                            fig2[i, 0].axvspan(h[0], h[1], color="tab:blue", alpha=0.2)
                else:
                    for s in range(n_comps):
                        if (
                            f"{k}_{s}" in trace.keys()
                        ):  # test if several comps with LOC & not
                            hdi = (
                                az.hdi(trace[f"{k}_{s}"], hdi_prob=ci)
                                .to_array()
                                .data[0]
                            )
                            fig2[i, 0].axvspan(
                                hdi[0],
                                hdi[1],
                                color=rcParams["sector_colors"][
                                    min(s, len(rcParams["sector_colors"]) - 1)
                                ],
                                alpha=0.2,
                            )

            if "prior_predictive" in resdict_process.keys():
                if "w" in resdict_process["prior_predictive"].keys():
                    # end of backward compatibility tests
                    for i, k in enumerate(
                        tmp2
                        + [
                            "w",
                        ]
                        + tmp3
                    ):
                        if k == "log_likelihood":
                            continue
                        for s in range(n_comps):
                            if f"{k}_{s}" in resdict_process["prior_predictive"].keys():
                                sns.kdeplot(
                                    resdict_process["prior_predictive"][f"{k}_{s}"][0],
                                    color="black",
                                    alpha=0.1,
                                    ax=fig2[i, 0],
                                    fill=True,
                                    zorder=-20,  # behind
                                    lw=0,
                                )
                            elif k == "w":
                                sns.kdeplot(
                                    resdict_process["prior_predictive"]["w"][:, s][0],
                                    color="black",
                                    alpha=0.1,
                                    ax=fig2[i, 0],
                                    fill=True,
                                    zorder=-20,  # behind
                                    lw=0,
                                )
            plt.tight_layout()
        fig1[0, 0].get_figure().savefig(
            plotdir + "Trace_params{}.{}".format(label, ext)
        )
        fig2[0, 0].get_figure().savefig(
            plotdir + "Trace_params_combined{}.{}".format(label, ext)
        )
        plt.close("all")

    # ======================================
    if "trace_observables" in kind:
        logging.info("- trace (observables)")

        # observed values and errors
        lines = []
        for i, o in enumerate(inp["observations"].names):
            lines += [
                (
                    o,
                    {},
                    [
                        inp["observations"].values[i],
                        inp["observations"].values[i] - inp["observations"].delta[i][0],
                        inp["observations"].values[i] + inp["observations"].delta[i][0],
                    ],
                ),
            ]

        fig = az.plot_trace(
            trace,
            var_names=inp["observations"].names,
            lines=lines,
            divergences="bottom",
            compact=True,
            plot_kwargs={"alpha": alpha_plot_trace},
            trace_kwargs={"alpha": alpha_plot_trace},
            hist_kwargs={"alpha": alpha_plot_trace},
            rug_kwargs={"alpha": alpha_plot_trace},
        )
        plt.tight_layout()
        fig[0, 0].get_figure().savefig(plotdir + "Trace_observables.{}".format(ext))

        fig = az.plot_trace(
            trace,
            var_names=inp["observations"].names,
            lines=lines,
            combined=True,
            divergences="bottom",
            compact=True,
            plot_kwargs={"alpha": alpha_plot_trace},
            trace_kwargs={"alpha": alpha_plot_trace},
            hist_kwargs={"alpha": alpha_plot_trace},
            rug_kwargs={"alpha": alpha_plot_trace},
            fill_kwargs={"alpha": alpha_plot_trace},
        )
        # confidence intervals
        for i, k in enumerate(inp["observations"].names):
            if i < 0.5 * az.rcparams.rcParams["plot.max_subplots"]:
                hdi = az.hdi(trace[k], hdi_prob=ci).to_array().data[0]
                fig[i, 0].axvspan(hdi[0], hdi[1], color="tab:blue", alpha=0.2)
                xlow = np.linspace(
                    fig[i, 0].get_xlim()[0], inp["observations"].values[i], 100
                )
                glow = fig[i, 0].get_ylim()[1] * np.exp(
                    -((xlow - inp["observations"].values[i]) ** 2)
                    / (2 * inp["observations"].delta[i][0] ** 2)
                )
                xup = np.linspace(
                    inp["observations"].values[i], fig[i, 0].get_xlim()[1], 100
                )
                gup = fig[i, 0].get_ylim()[1] * np.exp(
                    -((xup - inp["observations"].values[i]) ** 2)
                    / (2 * inp["observations"].delta[i][1] ** 2)
                )
                fig[i, 0].plot(xlow, glow, color="black")
                fig[i, 0].plot(xup, gup, color="black")
            else:
                logging.warning("Too many observations, skipping...")
        plt.tight_layout()
        fig[0, 0].get_figure().savefig(
            plotdir + "Trace_observables_combined.{}".format(ext)
        )

        # labels = []
        # for i,o in enumerate(inp['observations'].names):
        #   labels += ['f_g_{}'.format(o)]
        # fig = az.plot_trace(trace, var_names=labels, combined=True, divergences='bottom', compact=True)
        # fig[0, 0].get_figure().savefig(plotdir + "Trace_observables_frac_combined.pdf")

        plt.close("all")

    # #======================================
    # if 'trace_inferred' in kind:
    #    if inp['n_inf']>0:
    #        logging.info('- trace (inferred)')
    #        fig = az.plot_trace(trace,
    #                            var_names=inp['lines_to_infer'],
    #                            divergences='bottom',
    #                            compact=True,
    #                            plot_kwargs={'alpha': alpha_plot_trace},
    #                            trace_kwargs={'alpha': alpha_plot_trace},
    #                            hist_kwargs={'alpha': alpha_plot_trace},
    #                            rug_kwargs={'alpha': alpha_plot_trace})
    #        fig[0, 0].get_figure().savefig(plotdir + "Trace_lines_to_infer.{}".format(ext))
    #        fig = az.plot_trace(trace,
    #                            var_names=inp['lines_to_infer'],
    #                            combined=True,
    #                            divergences='bottom',
    #                            compact=True,
    #                            plot_kwargs={'alpha': alpha_plot_trace},
    #                            trace_kwargs={'alpha': alpha_plot_trace},
    #                            hist_kwargs={'alpha': alpha_plot_trace},
    #                            rug_kwargs={'alpha': alpha_plot_trace},
    #                            fill_kwargs={'alpha': alpha_plot_trace})
    #        fig[0, 0].get_figure().savefig(plotdir + "Trace_lines_to_infer_combined.{}".format(ext))
    #        plt.close('all')

    # ======================================
    # ======================================
    # ======================================
    # BOX PLOTS
    # ======================================
    # ======================================
    # ======================================
    if "boxplot_observables" in kind:
        logging.info("- boxplot (observables)")

        fig = boxplot(
            [
                trace,
            ]
            * 2,
            inp["observations"].names,
            ref=inp["observations"].values,
        )
        fig.subplots_adjust(top=0.97)
        fig.suptitle("Observables", y=0.99)
        fig.savefig("{}/boxplot_obs.pdf".format(plotdir))
        plt.close("all")

    # ======================================
    # ======================================
    # ======================================
    # VIOLIN PLOTS
    # ======================================
    # ======================================
    # ======================================

    # ======================================
    if "violin_observables" in kind:
        logging.info("- violin (observables)")

        trace2 = deepcopy(trace)
        # for ip, p in enumerate(inp['observations'].names):
        #    if p not in trace.keys():
        #       continue #happens if obs = inf or nan
        #    trace2[p] -= inp['observations'].values[ip]
        fig = az.plot_violin(
            trace2, var_names=inp["observations"].names, sharey=False, sharex=True
        )
        tmp = [fig.flatten()[i].title.get_text() for i in range(len(fig.flatten()))]
        for i, o in enumerate(tmp):
            if o == "":
                continue  # empty figure to make mosaic
            fig.flatten()[i].axhline(
                inp["observations"].values[
                    np.where(np.array(inp["observations"].names) == o)[0][0]
                ],
                color="black",
            )
        fig.flatten()[0].get_figure().savefig("{}/Violin_obs.{}".format(plotdir, ext))
        plt.close("all")
        del trace2

    # ======================================
    if "violin_norm_observables" in kind:
        logging.info("- violin/norm (observables)")

        trace2 = deepcopy(trace)
        for ip, p in enumerate(inp["observations"].names):
            trace2[p] -= inp["observations"].values[ip]
        var_names = [l for l in inp["observations"].names if l in trace2.keys()]
        # avoid arrays with almost no variation
        var_names = [
            l
            for l in var_names
            if (trace2[l].data.max() - trace2[l].data.min())
            > abs(1e-5 * trace2[l].data.mean())
        ]
        if len(var_names) > 0:
            fig = az.plot_violin(
                trace2,
                var_names=var_names,
                sharey=True,
                sharex=True,
                hdi_prob=ci,
            )
            for f in fig.flatten():
                f.axhline(0, color="black")
            fig.flatten()[0].get_figure().savefig(
                "{}/Violin_obs_norm.{}".format(plotdir, ext)
            )
            plt.close("all")

        logging.info("- violin/norm (all observables)")

        trace2 = combine_vars(trace2, inp["observations"].names, "allobs")
        fig = az.plot_violin(trace2, hdi_prob=ci)
        for f in fig.flatten():
            f.axhline(0, color="black")
        fig[0].get_figure().savefig("{}/Violin_all_obs.{}".format(plotdir, ext))
        plt.close("all")
        del trace2

    # ======================================
    if "violin_params" in kind:
        logging.info("- violin (parameters)")

        trace2 = deepcopy(trace)
        tmp = [
            "{}_0".format(p)
            for p in paramnames_forplots
            if "{}_0".format(p) in trace2.keys()
        ]
        tmp += [
            "{}".format(p)
            for p in paramnames_forplots
            if "{}".format(p) in trace2.keys()
        ]
        fig = az.plot_violin(trace2, tmp, sharey=False)
        for s in np.arange(1, n_comps):
            fig = az.plot_violin(
                trace2,
                var_names=[
                    "{}_{}".format(p, s)
                    for p in paramnames_forplots
                    if "{}_{}".format(p, s) in trace2.keys()
                ],  # test for if ncomps>1 with LOC and not
                ax=fig,
                sharey=False,
            )
        fig.flatten()[0].get_figure().savefig(
            "{}/Violin_params.{}".format(plotdir, ext)
        )
        plt.close("all")
        del trace2

    # ======================================
    # ======================================
    # ======================================
    # POSTERIOR PLOTS
    # ======================================
    # ======================================
    # ======================================

    # ======================================
    if "posterior_observables" in kind:
        logging.info("- posterior distribution (observables)")

        idx = (
            []
        )  # for.plot_posterior we wanna exclude tracers with mostly inf/nan values
        for i, o in enumerate(inp["observations"].names):
            if len(trace[o].data[np.isfinite(trace[o].data)]) > 0.1 * len(
                trace[o].data.flatten()
            ):
                idx += [
                    i,
                ]
            else:
                logging.error(
                    "/!\\ Not enough valid points for plot_posterior for observable {}".format(
                        o
                    )
                )

        ref_val = {}
        for i, o in enumerate(np.array(inp["observations"].names)[idx]):
            ref_val.update({o: [{"ref_val": inp["observations"].values[i]}]})
        try:
            axes = az.plot_posterior(
                trace,
                var_names=np.array(inp["observations"].names)[idx],
                point_estimate="median",
                hdi_prob=ci,
                ref_val=ref_val,
            )
            fig = axes.ravel()[0].figure
            fig.savefig(plotdir + "Posterior_dist_obs.pdf")
            plt.close("all")
        except:
            logging.error("/!\\ plot_posterior giving issues...")

    # ======================================
    if "posterior_params" in kind and len(params_forplots) > 0:
        logging.info("- posterior distribution (parameters)")
        tmp = [p for p in params_forplots if p in trace.keys()]
        axes = az.plot_posterior(
            trace,
            var_names=tmp,
            point_estimate="median",
            hdi_prob=ci,
        )  # 3: Posterior distributions
        if "ravel" in dir(axes):
            fig = axes.ravel()[0].figure
        else:
            fig = axes.figure
        fig.savefig(plotdir + "Posterior_dist_params.pdf")
        plt.close("all")

    # ======================================
    if "autocorr" in kind and len(params_forplots) > 0:
        logging.info("- autocorr plot (parameters)")
        tmp = [p for p in params_forplots if p in trace.keys()]
        # tracedf1 = pm.trace_to_dataframe(trace, varnames=params)
        # fig = sns.pairplot(tracedf1); #6: corner plot bis
        # fig.savefig(plotdir + "Pair_plot.png") #PDF takes too much space
        az.plot_autocorr(trace, var_names=tmp, combined=True)
        fig = plt.gcf()
        fig.savefig(plotdir + "autocorr_combined.pdf")
        plt.close("all")

    # if 'geweke' in kind:
    # logging.info('- geweke plot (params: {})'.format(params_forplots))
    # score = pm.geweke(trace, first=0.1, last=0.5, intervals=20)
    # fig, ax = plt.subplots(1, 1, figsize=(8,6))
    # p = 'age_s0'
    # ax.scatter(score[0][p][:,0], score[0][p][:,1], marker='o', s=100)
    # ax.axhline(-1.98, c='r')
    # ax.axhline(1.98, c='r')
    # ax.ylim(-2.5,2.5)
    # ax.xlim(0-10,.5*trace[p].shape[0]/2+10)
    # ax.title('Geweke Plot Comparing first 10% and Slices of the Last 50% of Chain\nDifference in Mean Z score')
    # fig.savefig(plotdir + "geweke.pdf")

    # ======================================
    # ======================================
    # ======================================
    # KDE PLOTS
    # ======================================
    # ======================================
    # ======================================

    # ======================================
    if "KDE" in kind and inp["grid"].params.names_notinf == []:
        logging.info("- KDE plot (parameters)")

        tmp_params = list(set([getp(ps) for ps in inp["grid"].params.names_inf]))

        for p in tmp_params:
            tmp = np.array([trace["{}_{}".format(p, s)].values for s in range(n_comps)])
            trace[p] = xarray.DataArray(np.rollaxis(tmp, 0, 3))

        tmp = [
            p
            for p in tmp_params
            if "idx_{}_0".format(p) in resdict_search["unobserved_RVs"]
        ]
        if n_comps > 1:
            tmp += [
                "w",
            ]
        try:
            fig = kde_plots(trace, inp, tmp)
        except:
            logging.error("KDE plot not working")
        fig.get_figure().savefig(plotdir + "kde_plot.pdf")
        plt.close("all")

        # for s in range(config[g]):
        #    logging.info('  - group/sector {}/{}'.format(g, s))
        #    #try:
        #    tmp = ['{}_{}{}'.format(p, g, s) for p in inp['grid'].params.names if 'idx_{}_00'.format(p) in RVs]
        #    fig = kde_plots(trace, inp, tmp)
        #    fig.get_figure().savefig(plotdir +"kde_plot_{}{}.pdf".format(g,s))
        #    #except:
        #    #   logging.warning('/!\\ KDE plot giving issues...')

    # ======================================
    # ======================================
    # ======================================
    # CORNER PLOTS
    # ======================================
    # ======================================
    # ======================================

    # ======================================
    if "corner" in kind and inp["plaw_params"] == []:
        logging.info("- corner plot (parameters)")

        if "true_params" in inp.keys():
            truths = inp["true_params"]
        else:
            truths = None

        quantiles = 0.5 * (1 - rcParams["ci"])
        quantiles = [quantiles, 0.5, 1 - quantiles]
        for s in range(n_comps):
            tmp = [p for p in params_forplots]
            if len(tmp) > 5:
                logging.warning(
                    "/!\\ Cowardly refusing to do plot (too many parameters)"
                )
            else:
                fig = corner.corner(
                    trace[tmp],
                    label_kwargs={"fontsize": 20},
                    truths=truths,
                    quantiles=quantiles,
                    scale_hist=True,
                    smooth=1,
                )
                fig.savefig(plotdir + "Corner_plot_{}.{}".format(s, ext))
                plt.close("all")

            # #fig = corner.corner(np.vstack([np.array(np.mean(trace['{}_{}{}'.format(p, g, s)].data, axis=0)).flatten() for p in paramnames_forplots
            # #                               if np.min(np.mean(trace['{}_{}{}'.format(p, g, s)].data, axis=0))!=np.max(np.mean(trace['{}_{}{}'.format(p, g, s)].data, axis=0))]).T,
            # #                    quantiles=[0.16, 0.5, 0.84], show_titles=True, labels=['{}_{}{}'.format(p, g, s) for p in paramnames_forplots], title_kwargs={"fontsize": 12}, truths=truths)
            # if n_comps==1:
            #    fig = corner.corner(np.vstack(
            #       np.concatenate([
            #          [np.array(np.mean(trace['{}_{}'.format(p, s)].data, axis=0)).flatten() for p in paramnames_forplots
            #           if np.min(np.mean(trace['{}_{}'.format(p, s)].data, axis=0))!=np.max(np.mean(trace['{}_{}'.format(p, s)].data, axis=(0,1)))]])
            #    ).T,
            #                        quantiles=[0.16, 0.5, 0.84], show_titles=True, labels=['{}_{}'.format(p, s) for p in paramnames_forplots], title_kwargs={"fontsize": 12}, truths=truths)
            # else: #we add w
            #    fig = corner.corner(np.vstack(
            #       np.concatenate([
            #          [np.array(np.mean(trace['{}_{}'.format(p, s)].data, axis=0)).flatten() for p in paramnames_forplots
            #           if np.min(np.mean(trace['{}_{}'.format(p, s)].data, axis=0))!=np.max(np.mean(trace['{}_{}'.format(p, s)].data, axis=(0,1)))],
            #          [np.array(np.mean(trace['w'].data, axis=0))[:,s].flatten()]])
            #    ).T,
            #                        quantiles=[0.16, 0.5, 0.84], show_titles=True, labels=['{}_{}'.format(p, s) for p in paramnames_forplots]+['w',], title_kwargs={"fontsize": 12}, truths=truths)
            # fig.savefig(plotdir + "Corner_plot_{}.{}".format(s, ext))
            # plt.close('all')
        # axes = np.array(figure.axes).reshape((ndim, ndim)) #if we wanna plot over

    # ======================================
    if "forest" in kind:
        logging.info("- forest plot")
        az.plot_forest(
            trace,
            var_names=params_forplots,
            kind="forestplot",
            r_hat=True,
            ess=True,
            hdi_prob=ci,
        )
        fig = plt.gcf()  # to get the current figure...
        # fig[1][1].set_xlim(left= min(pm.summary(trace)['n_eff'])-10, right= max(pm.summary(trace)['n_eff'])+10)
        # fig[1][2].set_xlim(left= 0.8, right= 1.2)
        fig.savefig(plotdir + "forest_plot_rhat_neff.pdf")  # and save it directly
        plt.close("all")

    # ======================================
    if "ridge" in kind:
        logging.info("- ridge plot")
        az.plot_forest(trace, var_names=params_forplots, kind="ridgeplot", hdi_prob=ci)
        fig = plt.gcf()  # to get the current figure.
        fig.savefig(plotdir + "ridge_plot.pdf")
        plt.close("all")

    # ======================================
    if "colored" in kind:
        logging.info("- colored plot")
        try:
            fig = colored_trace(0, trace, inp)
            fig.get_figure().savefig(
                plotdir + "colored_trace.png"
            )  # PDF takes too much space
            plt.close("all")
        except:
            logging.error(
                "/!\\ Full (colored) trace plot giving issues..."
            )  # temporary solution: restricting to smaller trace (500 last points)

        # if 'pair' in kind:
        # logging.info('- pair plot (params: {})'.format(params_forplots))
        # tracedf1 = pm.trace_to_dataframe(trace, varnames=params)
        # fig = sns.pairplot(tracedf1); #6: corner plot bis
        # fig.savefig(plotdir + "Pair_plot.png") #PDF takes too much space

    # save/close plots
    plt.close("all")


# -------------------------------------------------------------------
def kde_plots(trace, inp, params):  # trace is actually trace.posterior
    n_params = len(params)
    fig, axs = plt.subplots(
        nrows=n_params,
        ncols=n_params,
        sharex="col",
        sharey="row",
        figsize=(2 * n_params, 2 * n_params),
    )
    # fig, axs = plt.subplots(n_params, n_params, figsize=(21,21), sharex=True, sharey=True)

    cmap = sns.color_palette("mako", as_cmap=True)
    # cmap = sns.cubehelix_palette(light=1, as_cmap=True)

    # remove frames
    for i in range(n_params):
        for j in range(n_params):
            axs[j, i].set_axis_off()

    for i, pi in enumerate(params):
        for j, pj in enumerate(params[i:][::-1]):
            # print(pi, pj)
            ax = axs[n_params - j - 1, i]
            if np.min(trace[pi].data.flatten()) != np.max(
                trace[pi].data.flatten()
            ) and np.min(trace[pj].data.flatten()) != np.max(trace[pj].data.flatten()):
                fig = az.plot_kde(
                    trace[pi],
                    trace[pj],
                    ax=ax,
                    fill_last=True,
                    contour_kwargs={"colors": None, "cmap": cmap},
                    contourf_kwargs={"colors": None, "cmap": cmap},
                )
            ax.set_axis_on()
            if trace[pi].ndim == 2:  # single component
                ax.axvline(trace[pi].median(axis=(0, 1)), color="white", alpha=0.2)
                ax.axhline(trace[pj].median(axis=(0, 1)), color="white", alpha=0.2)
            else:  # multi component
                for k in range(trace[pi].shape[2]):
                    ax.axvline(
                        trace[pi].median(axis=(0, 1))[k], color="white", alpha=0.2
                    )
                    ax.axhline(
                        trace[pj].median(axis=(0, 1))[k], color="white", alpha=0.2
                    )

            # if pi in inp['grid'].params.names:
            #    ax.set_xlim([np.min(inp['grid'].params.values[pi]), np.max(inp['grid'].params.values[pi])])
            # else: # w
            #    ax.set_xlim([0, 1])

            # if pj in inp['grid'].params.names:
            #    ax.set_ylim([np.min(inp['grid'].params.values[pj]), np.max(inp['grid'].params.values[pj])])
            # else: # w
            #    ax.set_ylim([0, 1])

            ax.tick_params(axis="both", which="major", labelsize=16)
            if i == 0:
                ax.set_ylabel(pj, fontsize=20)
            if j == 0:
                ax.set_xlabel(pi, fontsize=20)
            # ax.set_xlabel(pi, fontsize=16)

    plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, left=0.1, hspace=0.0, wspace=0)
    plt.tight_layout()
    return fig


# #-------------------------------------------------------------------
# def kde_plots_sns(s, trace, inp): #trace is actually trace.posterior
#    params_name = inp['params_name']
#    Params_values = inp['Params_values']
#    fig, axs = plt.subplots(len(params_name), len(params_name), figsize=(21,21))
#    for i in range(inp['n_params']):
#        for j in range(inp['n_params']):
#            axs[j, i].set_axis_off()
#    for i, pi in enumerate(params_name):
#        for j, pj in enumerate(params_name[i:][::-1]):
#            xt, yt = np.mean(trace[pi+'_s{}'.format(s)].data, axis=0)[-500:], np.mean(trace[pj+'_s{}'.format(s)].data, axis=0)[-500:] # //!\\\ I selected only the 500 last points of trace. Also: combine chains
#            xt, yt = np.array(xt).flatten(), np.array(yt).flatten()
#            ax = axs[inp['n_params']-j-1, i]
#            ax.set_axis_on()
#            ax.set_xlim([np.min(Params_values[pi]), np.max(Params_values[pi])])
#            ax.set_ylim([np.min(Params_values[pj]), np.max(Params_values[pj])])
#            ax.set_xlabel(pi+'_s{}'.format(s))
#            ax.set_ylabel(pj+'_s{}'.format(s))
#            cmap = sns.cubehelix_palette(light=1, as_cmap=True)
#            #fig = pm.plots.kdeplot(xt, yt, ax=ax, contour=False, fill_last=True, fill_kwargs={"colors": None, "cmap": cmap},)
#            fig = sns.kdeplot(xt, yt, ax=ax, cmap=cmap, shade=True, shade_lowest=True, n_levels=10, cbar=True)
#    return fig


# -------------------------------------------------------------------
def colored_trace(s, trace, inp, grd):  # trace is actually trace.posterior
    fig, axs = plt.subplots(
        inp["grid"].params.n, inp["grid"].params.n, figsize=(21, 21)
    )
    for i in range(inp["grid"].params.n):
        for j in range(inp["grid"].params.n):
            axs[j, i].set_axis_off()
    for i, pi in enumerate(inp["grid"].params.names):
        for j, pj in enumerate(inp["grid"].params.names[i:][::-1]):
            xt, yt = (
                np.mean(trace[pi + "_s{}".format(s)].data, axis=0)[-500:],
                np.mean(trace[pj + "_s{}".format(s)].data, axis=0)[-500:],
            )  # //!\\\ I selected only the 500 last points of trace
            xt, yt = np.array(xt).flatten(), np.array(yt).flatten()
            alpha = np.arange(len(xt)) / len(xt)
            ax = axs[inp["grid"].params.n - j - 1, i]
            ax.set_xlim(
                [
                    np.min(inp["grid"].params.values[pi]),
                    np.max(inp["grid"].params.values[pi]),
                ]
            )
            ax.set_ylim(
                [
                    np.min(inp["grid"].params.values[pj]),
                    np.max(inp["grid"].params.values[pj]),
                ]
            )
            ax.set_xlabel(pi + "_s0")
            ax.set_ylabel(pj + "_s0")
            for k in range(len(xt)):
                scatter = ax.scatter(
                    xt[k], yt[k], c=k, alpha=alpha[k], zorder=1, vmin=0, vmax=len(xt)
                )
    return scatter


# -------------------------------------------------------------------
def extents(f):
    delta = f[1] - f[0]
    return [f[0] - delta / 2, f[-1] + delta / 2]


# -------------------------------------------------------------------
def make_obs_2dplots(
    inp, obs_grid, observations, grid, highlight=None, label="", scale=0
):
    """
    Plot the 2D distribution of each observable for each set of parameter.
    We plot the distribution using indices to give the representation of how the grid is used by the MCMC model.
    Actual parameter values are indicated in the alternate axes.
    """

    plotdir = inp["output_directory"] + "/Plots/2D_gridplots/"
    os.makedirs(plotdir, exist_ok=True)

    n_obs = len(obs_grid)  # that's because we may plot observables or lines_to_infer

    n_params = grid.params.n_params()

    logging.info("Making 2D plots for grid observable tracers...")
    if observations is None:
        obs = [label]
    else:
        obs = observations.names
    for io, o in enumerate(obs):
        outname = plotdir + "{}.png".format(o)
        if FileExists(outname):
            logging.error(
                "File exists, remove {} if you want to replot...".format(outname)
            )
            # return

        logging.info("{} / {}".format(io + 1, n_obs))

        fig, axs = plt.subplots(
            n_params - 1, n_params - 1, figsize=(4 * (n_params - 1), 4 * (n_params - 1))
        )

        for i in range(n_params - 1):
            for j in range(n_params - 1):
                axs[j, i].set_axis_off()

        ki = -1
        for i, pi in enumerate(grid.params.names):
            kj = -1
            ki += 1
            for j, pj in enumerate(grid.params.names):
                if i >= j:
                    continue

                kj += 1

                ax = axs[n_params - 2 - ki, kj]
                ax.set_axis_on()

                # collapse all axes except the two that interest us
                collapsax = tuple([n for n in range(n_params) if n not in (i, j)])
                tmp = np.nanmean(
                    obs_grid[io] + inp["order_of_magnitude"] + scale,
                    axis=collapsax,
                    dtype=np.float32,
                )

                # extent = extents(grid.params.values[pi]) + extents(grid.params.values[pj])
                p = ax.pcolor(
                    tmp.T, cmap=plt.cm.hot
                )  # , aspect='auto', interpolation='none', origin='lower')
                ax.set_xticks(np.arange(len(grid.params.values[pi])) + 0.5)
                ax.set_xticklabels([round(x, 2) for x in grid.params.values[pi]])

                ax.set_yticks(np.arange(len(grid.params.values[pj])) + 0.5)
                ax.set_yticklabels([round(x, 2) for x in grid.params.values[pj]])

                # if highlight and observations is not None: #doesn't make much sense because collapsing (mean)
                #    cs = ax.contour(tmp.T, (observations.obs[o].value,),
                #                    origin='lower',
                #                    linewidths=5,
                #                    #extent=extent,
                #                    antialias=False,
                #                    colors='green')

                fig.colorbar(p, ax=ax, orientation="horizontal")

                # axes
                # ax.set_title('{}'.format(observations.names[io]))

                ax.set_xlabel(pi)
                ax.set_ylabel(pj)

                # ax.set_xticks(np.arange(0, len(inp['grid'].params.values[inp['grid'].params.names[i0]])))
                # ax.set_yticks(np.arange(0, len(inp['grid'].params.values[inp['grid'].params.names[i1]])))

                # axx = ax.twinx()
                # axx.set_ylim([extent[2], extent[3]])
                # axy = ax.twiny()
                # axy.set_xlim([extent[0], extent[1]])

                # axy.set_xticks(ax.get_xticks())
                # axx.set_yticks(ax.get_yticks())

                # actual values on alternate axis
                # axy.set_xticklabels(inp['grid'].params.values[inp['grid'].params.names[i0]])
                # axx.set_yticklabels(inp['grid'].params.values[inp['grid'].params.names[i1]])

                # axy.set_xlabel('{} (value)'.format(inp['grid'].params.names[i0]))
                # axx.set_ylabel('{} (value)'.format(inp['grid'].params.names[i1]))

        fig.suptitle("Mean across all other parameters")
        fig.tight_layout(pad=1, w_pad=5, h_pad=5)
        # fig.subplots_adjust(hspace=0.2, wspace=0.2)
        fig.savefig(outname)


# -----------------------------------------------------------------
def plot_hist(label, t, filename):
    """
    Simple routine to make trace plot like pm.plot_trace for non
    multi-trace objects
    """
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    ax.hist(t)
    ax.set_xlabel(label)
    plt.tight_layout()
    fig.savefig(filename)


# -----------------------------------------------------------------
def plot_trace(label, t, filename):
    """
    Simple routine to make trace plot like pm.plot_trace for non
    multi-trace objects
    """
    fig = plt.figure(figsize=(15, 7))
    ax0 = plt.subplot2grid((1, 3), (0, 0))
    ax0.hist(t)
    ax0.set_xlabel(label)
    ax1 = plt.subplot2grid((1, 3), (0, 1), colspan=2)
    ax1.plot(t)
    ax1.set_xlabel("Sample #")
    plt.tight_layout()
    fig.savefig(filename)


# -----------------------------------------------------------------
def plot_trace_chains(t, p, filename):
    """
    Simple routine to make trace plot like pm.plot_trace for non
    multi-trace objects
    """
    cols = ("red", "blue", "green")
    fig = plt.figure(figsize=(15, 7))
    ns = trace.posterior["w_0"].values.shape[2]

    ax0 = plt.subplot2grid((1, 3), (0, 0))
    for i in range(ns):
        if p == "w":
            ax0.hist(
                t.posterior["{}_0".format(p)].values[:, :, i].flatten(),
                alpha=1 / ns,
                color=cols[i],
            )
        else:
            ax0.hist(
                t.posterior["{}_0{}".format(p, i)].values.flatten(),
                alpha=1 / ns,
                color=cols[i],
            )
    ax0.set_xlabel(p)

    ax1 = plt.subplot2grid((1, 3), (0, 1), colspan=2)
    for i in range(ns):
        if p == "w":
            for c in range(t.posterior["w_0"].shape[0]):
                ax1.plot(
                    t.posterior["{}_0".format(p)].values[c, :, i].flatten(),
                    color=cols[i],
                    alpha=1 / t.posterior["{}_0".format(p)].shape[0] / ns,
                )
        else:
            for s in t.posterior["{}_0{}".format(p, i)]:
                ax1.plot(
                    s,
                    color=cols[i],
                    alpha=1 / len(t.posterior["{}_0{}".format(p, i)]) / ns,
                )
    ax1.set_xlabel("Sample #")

    plt.tight_layout()
    if filename is not None:
        fig.savefig(filename)


# -----------------------------------------------------------------
def kde_singleplot(labels, txy, filename):
    """
    Simple routine to make simple KDE plot with 2 parameters
    """
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    cmap = sns.cubehelix_palette(light=1, as_cmap=True)
    fig = sns.kdeplot(
        txy[0],
        txy[1],
        ax=ax,
        cmap=cmap,
        shade=True,
        shade_lowest=True,
        n_levels=10,
        cbar=True,
    )
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    fig.get_figure().savefig(filename)


# -------------------------------------------------------------------
def fracplot(
    trace, inp, ci=None, compare="components"
):  # trace is actually trace.posterior
    if ci is None:
        ci = inp["ci"]

    n_comps = inp["n_comps"]
    groups = inp["groups"]
    n_groups = len(groups)

    lw = 5

    if compare == "components":
        if n_comps > 1:
            plt.clf()
            fig, axs = plt.subplots(1, 1, figsize=(5, 0.5 * inp["observations"].n))

            colors = inp["sector_colors"]
            for i, o in enumerate(inp["observations"].names):
                frac = 0
                for s in range(n_comps):
                    if i == 0:
                        l = "Comp. {}".format(s)
                    else:
                        l = ""
                    # isbest = 10**float(trace['f_s_{}'.format(o)][:,:,s].median().data)
                    best = 10 ** float(
                        trace["f_s_{}".format(o)][:, :, s].mean().data
                    ) / np.sum(
                        [
                            10 ** float(trace["f_s_{}".format(o)][:, :, s].mean().data)
                            for s in range(n_comps)
                        ]
                    )  # need to normalize if using mean
                    axs.axhspan(
                        i + 0.3,
                        i - 0.3,
                        xmin=frac,
                        xmax=frac + best,
                        color=colors[min(s, len(colors) - 1)],
                        alpha=1,
                        label=l,
                        # linewidth=lw / n_comps,
                    )
                    frac += best

            axs.set_ylim([-0.5, inp["observations"].n - 0.5])
            axs.set_yticks(np.arange(inp["observations"].n))
            axs.set_yticklabels(inp["observations"].names)

            axs.set_xlabel("Fraction")

            axs.legend(
                fontsize=10 / n_comps,
                ncol=n_comps,
                facecolor="white",
                framealpha=0.5,
                bbox_to_anchor=(0.05, 1.02),
            )

            plt.tight_layout()
            fig.savefig(inp["output_directory"] + "/Plots/fracplot_components.pdf")

    elif compare == "groups":
        if n_groups > 1:
            plt.clf()
            fig, axs = plt.subplots(
                1, 1, figsize=(5, 0.5 * inp["observations"].n), sharey=True
            )

            colors = inp["sector_colors"]
            for i, o in enumerate(inp["observations"].names):
                frac = 0
                for g in range(n_groups):
                    if i == 0:
                        l = "Group {}".format(g)
                    else:
                        l = ""
                    # best = 10**float(trace['f_g{}_{}'.format(g,o)][:,:].median().data)
                    best = 10 ** float(
                        trace["f_g{}_{}".format(g, o)][:, :].mean().data
                    ) / np.sum(
                        [
                            10
                            ** float(trace["f_g{}_{}".format(g, o)][:, :].mean().data)
                            for g in range(n_groups)
                        ]
                    )  # need to normalize if using mean
                    axs.axhspan(
                        i - 0.3,
                        i + 0.3,
                        xmin=frac,
                        xmax=frac + best,
                        color=colors[g],
                        alpha=1,
                        label=l,
                        # linewidth=lw / n_comps,
                    )
                    frac += best

            axs.set_ylim([-0.5, inp["observations"].n - 0.5])
            axs.set_yticks(np.arange(inp["observations"].n))
            axs.set_yticklabels(inp["observations"].names)

            axs.set_xlabel("Fraction")

            axs.legend(
                fontsize=10 / n_groups,
                ncol=n_comps,
                facecolor="white",
                framealpha=0.5,
                bbox_to_anchor=(0.05, 1.02),
            )

            plt.tight_layout()
            fig.savefig(inp["output_directory"] + "/Plots/fracplot_groups.pdf")

    elif compare == "both":
        if n_groups > 1 and n_comps > 2:
            plt.clf()
            fig, axs = plt.subplots(
                1,
                n_groups,
                figsize=(5 * n_groups, 0.5 * inp["observations"].n),
                sharey=True,
            )
            if n_groups == 1:
                axs = [
                    axs,
                ]

            for g in range(n_groups):
                colors = inp["sector_colors"]
                for i, o in enumerate(inp["observations"].names):
                    frac = 0
                    for j, s in enumerate(groups[g]):
                        if i == 0:
                            l = "Comp. {}".format(s)
                        else:
                            l = ""
                        # best = 10**float(trace['f_g{}_s_{}'.format(g,o)][:,:,s].median().data)
                        best = 10 ** float(
                            trace["f_g{}_s_{}".format(g, o)][:, :, s].mean().data
                        ) / np.sum(
                            [
                                10
                                ** float(
                                    trace["f_g{}_s_{}".format(g, o)][:, :, s]
                                    .mean()
                                    .data
                                )
                                for s in groups[g]
                            ]
                        )  # need to normalize if using mean
                        axs[g].axhspan(
                            i - 0.3 / len(groups[g]),
                            i + 0.3 / len(groups[g]),
                            xmin=frac,
                            xmax=frac + best,
                            color=colors[j],
                            alpha=1,
                            label=l,
                            # linewidth=lw,
                        )
                        frac = best

                axs[g].set_ylim([-0.5, inp["observations"].n - 0.5])
                axs[g].set_yticks(np.arange(inp["observations"].n))
                axs[g].set_yticklabels(inp["observations"].names)

                axs[g].set_xlabel("Fraction")
                axs[g].set_title("Group {}".format(g))

                axs[g].legend(
                    fontsize=8,
                    ncol=len(groups[g]),
                    facecolor="white",
                    framealpha=0.5,
                    bbox_to_anchor=(0.05, 1.02),
                )

                plt.tight_layout()
                fig.savefig(inp["output_directory"] + "/Plots/fracplot_both.pdf")

    return


# -------------------------------------------------------------------
def matchplot(trace, inp, ci=None):  # trace is actually trace.posterior
    if ci is None:
        ci = inp["ci"]

    fig, ax = plt.subplots(1, 1, figsize=(6, int(0.5 * inp["observations"].n)))
    hpdi = az.hdi(trace, hdi_prob=ci, skipna=True)

    # l = [(i, inp['observations'].values[i]-trace[o].quantile(0.5).data) for i,o in enumerate(inp['observations'].names)]
    l = [
        (i, inp["observations"].values[i] - trace[o].mean().data)
        for i, o in enumerate(inp["observations"].names)
    ]
    inds = sorted(l, key=lambda x: x[1], reverse=True)
    inds = [i[0] for i in inds]

    face_alpha = 0.1
    lw = 10
    minmax = [0, 0]
    # for i,o in enumerate(inp['observations'].names):
    for ii, i in enumerate(inds):
        o = inp["observations"].names[i]
        # best = trace[o].quantile(0.5).data
        best = trace[o].mean().data
        hpdi2 = hpdi[o].data  # to_dict()['data']

        # update minmax (for UL/LL :only if not respected)
        if (o not in inp["observations"].mask_obs_lims) or (
            o in inp["observations"].mask_obs_lo_lims
            and inp["observations"].values[i] > best
        ):
            minmax[0] = np.min(
                [
                    minmax[0],
                    -inp["observations"].delta[i][0],
                ]
            )
        if (o not in inp["observations"].mask_obs_lims) or (
            o in inp["observations"].mask_obs_up_lims
            and inp["observations"].values[i] < best
        ):
            minmax[1] = np.max(
                [
                    minmax[1],
                    inp["observations"].delta[i][1],
                ]
            )

        if ~np.isfinite(best):
            continue

        if (
            (
                (
                    (inp["observations"].values[i] + inp["observations"].delta[i][0])
                    > best
                )
                and (
                    (inp["observations"].values[i] - inp["observations"].delta[i][1])
                    < best
                )
            )
            or (
                hpdi2[0] < inp["observations"].values[i]
                and hpdi2[1] > inp["observations"].values[i]
            )
            or (
                inp["observations"].values[i] > best
                and o in inp["observations"].mask_obs_up_lims
            )
            or (
                inp["observations"].values[i] < best
                and o in inp["observations"].mask_obs_lo_lims
            )
        ):
            # col = 'green'
            col = (0, 1, 0, face_alpha)
        elif (
            hpdi2[0] < (inp["observations"].values[i] + inp["observations"].delta[i][0])
        ) and (
            hpdi2[1] > (inp["observations"].values[i] - inp["observations"].delta[i][1])
        ):
            # col = 'yellow'
            col = (1, 1, 0, face_alpha)

        else:
            # col = 'red'
            col = (1, 0, 0, face_alpha)

        # alpha of edge color
        # if i in inp['mask_obs_lims']:
        a = 0.25 if o in inp["observations"].mask_obs_lims else 0.75
        art = ax.axhspan(
            ii - 0.4,
            ii + 0.4,
            linewidth=1,
            facecolor=col,
            zorder=30,
            edgecolor=(0, 0, 0, a),
        )

        # obs
        l = "Observation" if i == 0 else ""

        # model err
        modxerr = np.array([[np.abs(best - t) for t in hpdi2]]).T

        # obs value
        ax.plot(
            0,
            ii,
            color="red",
            marker="o",
            alpha=1,
            ms=lw - 1,
            label=l,
        )

        if o in inp["observations"].mask_obs_up_lims:
            ax.errorbar(
                0,
                ii,
                xerr=np.array([[0], [inp["observations"].delta[i][1]]]),
                color="red",
                fmt="o",
                capsize=5,
                alpha=0.2,
                lw=lw,
            )  # ref is best because obs can be
            ax.errorbar(
                0,
                ii,
                xerr=np.abs(modxerr[0]),
                xuplims=True,
                color="red",
                fmt="o",
                capsize=5,
                alpha=0.5,
                lw=lw,
            )  # ref is best because obs can be UL/LL
        elif o in inp["observations"].mask_obs_lo_lims:
            ax.errorbar(
                0,
                ii,
                xerr=np.array([[inp["observations"].delta[i][0]], [0]]),
                color="red",
                fmt="o",
                capsize=5,
                alpha=0.2,
                lw=lw,
            )  # ref is best because obs can be
            ax.errorbar(
                0,
                ii,
                xerr=np.abs(modxerr[1]),
                xlolims=True,
                color="red",
                fmt="o",
                capsize=5,
                alpha=0.5,
                lw=lw,
            )  # ref is best because obs can be UL/LL
        else:
            xerr = [
                [inp["observations"].delta[i][0]],
                [inp["observations"].delta[i][1]],
            ]  # lower, upper
            ax.errorbar(
                0,
                ii,
                xerr=xerr,
                color="red",
                fmt="o",
                capsize=5,
                alpha=0.2,
                lw=lw,
            )  # ref is best because obs can be UL/LL

        # model
        modxerr = np.array([[np.abs(best - t) for t in hpdi2]]).T
        l = "Model" if ii == 0 else ""
        ax.errorbar(
            inp["observations"].values[i] - best,
            ii,
            xerr=modxerr,
            color="black",
            fmt="o",
            capsize=5,
            label=l,
            zorder=30,
        )

        # update minmax
        minmax[0] = np.min(
            [minmax[0], float(-modxerr[0] + inp["observations"].values[i] - best)]
        )
        minmax[1] = np.max(
            [minmax[1], float(modxerr[1] + inp["observations"].values[i] - best)]
        )

    ax.set_xlim(minmax)

    ax.set_ylim([-0.5, inp["observations"].n - 0.5])
    ax.set_yticks(np.arange(inp["observations"].n))
    labels = [inp["observations"].names[i] for i in inds]
    ax.set_yticklabels(labels)

    # ax.minorticks_on()
    # ax.tick_params(axis='x', which='minor', direction='out')
    minor_locator = AutoMinorLocator(10)
    ax.xaxis.set_minor_locator(minor_locator)

    ax.xaxis.grid(which="minor", color="#EEEEEE")  # vertical lines
    ax.xaxis.grid(which="major", color="#CCCCCC")  # vertical lines

    ax.set_xlabel("Difference (absolute values)")
    ax.axvline(0, color="black")
    ax.set_title("y=obs-mod")
    ax.legend(fontsize=8, ncol=2, facecolor="white", framealpha=1)
    plt.tight_layout()
    fig.savefig(inp["output_directory"] + "/Plots/matchplot.pdf")


# -------------------------------------------------------------------
def matchplot_disk(trace, inp, ci=None):  # trace is actually trace.posterior
    if ci is None:
        ci = inp["ci"]

    face_alpha = 0.1
    lw = 10
    hpdi = az.hdi(trace, hdi_prob=ci, skipna=True)

    # sorting tracers

    # absolute difference
    # l = [(i, inp['observations'].values[i]-trace[o].quantile(0.5).data) for i,o in enumerate(inp['observations'].names)]

    # (O-M)/M
    # l = [(i, (inp['observations'].values[i]-float(trace[o].mean()))/float(trace[o].mean())) for i,o in enumerate(inp['observations'].names)]

    # n-sigma
    l = [
        (
            i,
            (inp["observations"].values[i] - float(trace[o].mean()))
            / np.mean(inp["observations"].delta[i]),
        )
        for i, o in enumerate(inp["observations"].names)
    ]

    inds = sorted(l, key=lambda x: x[1], reverse=True)
    inds = [i[0] for i in inds]

    angles = np.linspace(0, 2 * np.pi, inp["observations"].n, endpoint=False)
    delta = angles[1] - angles[0] if len(angles) > 1 else 360

    angles2 = np.linspace(0, 2 * np.pi, 100, endpoint=True)  # for the origin circle

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, polar=True)

    # ax.set_yticklabels(plot_str_markers)
    # ax.set_title('')
    ax.grid(True)
    ax.spines["polar"].set_visible(False)

    ax.plot(angles2, np.repeat(0, len(angles2)), color="red", lw=2)

    lims = [0, 0]
    for ii, i in enumerate(inds):
        o = inp["observations"].names[i]

        # best = trace[o].quantile(0.5).data
        best = trace[o].mean().data
        best_norm = 0.5 * (trace[o].mean().data + inp["observations"].values[i])
        hpdi2 = hpdi[o].data

        if ~np.isfinite(best):
            continue

        l = "Observation" if i == 0 else ""

        # obs value
        ax.plot(
            angles[ii],
            0,
            marker="o",
            color="red",
            alpha=1,
            ms=lw - 1,
        )

        # get yerr and update limits
        if o in inp["observations"].mask_obs_up_lims:
            yerr = np.array([[0], [inp["observations"].delta[i][1]]])
            if inp["observations"].values[i] < best:  # UL not respected, show it
                lims[1] = np.max([lims[1], float(yerr[0] / best_norm)])

        elif o in inp["observations"].mask_obs_lo_lims:
            yerr = np.array([[inp["observations"].delta[i][0]], [0]])
            if inp["observations"].values[i] > best:  # LL not respected, show it
                lims[0] = np.min([lims[0], float(yerr[1] / best_norm)])

        else:
            yerr = np.array(
                [[inp["observations"].delta[i][0]], [inp["observations"].delta[i][1]]]
            )
            lims[0] = np.min([lims[0], float(yerr[1] / best_norm)])
            lims[1] = np.max([lims[1], float(yerr[0] / best_norm)])

        # obs error
        markers, caps, bars = ax.errorbar(
            angles[ii],
            0,
            yerr=yerr / best_norm,
            color="red",
            fmt="o",
            elinewidth=lw,
            alpha=0.2,
            label=l,
            clip_on=True,
        )
        # [cap.set_alpha(1) for cap in caps] #limits and caps don't rotate in polar plots...

        # models
        yerr = np.array([[np.abs(best - t) for t in hpdi2]]).T
        l = "Model" if i == 0 else ""
        # markers, caps, bars = ax.errorbar(angles[ii], 0, yerr=yerr, color='black', fmt='o', elinewidth=2, alpha=1, label=l, zorder=30, clip_on=True)
        markers, caps, bars = ax.errorbar(
            angles[ii],
            (best - inp["observations"].values[i]) / best_norm,
            yerr=yerr / best_norm,
            color="black",
            fmt="o",
            elinewidth=2,
            alpha=1,
            label=l,
            zorder=30,
            clip_on=True,
        )
        lims[0] = np.min(
            [
                lims[0],
                float((best - inp["observations"].values[i] - yerr[0]) / best_norm),
            ]
        )
        lims[1] = np.max(
            [
                lims[1],
                float((best - inp["observations"].values[i] + yerr[1]) / best_norm),
            ]
        )

    # get symmetric limits
    lims2 = 1.1 * np.array([-max(np.abs(lims)), max(np.abs(lims))])

    # limits
    for ii, i in enumerate(inds):
        o = inp["observations"].names[i]
        # best = trace[o].quantile(0.5).data
        best = trace[o].mean().data
        hpdi2 = hpdi[o].data  # to_dict()['data']

        if ~np.isfinite(best):
            continue

        if o in inp["observations"].mask_obs_up_lims:
            # ax.annotate("", xy=(angles[ii], lims2[0]), xytext=(angles[ii], inp['observations'].values[i]-best), arrowprops=dict(arrowstyle="-|>", lw=3, alpha=0.5, color='red'))
            tmp = np.max(
                [
                    lims2[0],
                    np.min([lims2[1], 0]),
                ]
            )
            ax.annotate(
                "",
                xy=(angles[ii], lims2[0]),
                xytext=(angles[ii], tmp),
                # xycoords="polar",
                arrowprops=dict(arrowstyle="-|>", lw=3, alpha=0.5, color="red"),
            )

        elif o in inp["observations"].mask_obs_lo_lims:
            # ax.annotate("", xy=(angles[ii], lims2[1]), xytext=(angles[ii], inp['observations'].values[i]-best), arrowprops=dict(arrowstyle="-|>", lw=3, alpha=0.5, color='red'))
            tmp = np.max(
                [
                    lims2[0],
                    np.min([lims2[1], 0]),
                ]
            )
            ax.annotate(
                "",
                xy=(angles[ii], lims2[1]),
                xytext=(angles[ii], tmp),
                # xycoords="polar",
                arrowprops=dict(arrowstyle="-|>", lw=3, alpha=0.5, color="red"),
            )

    # wrap up
    ax.set_ylim(lims2)
    # for i,o in enumerate(inp['observations'].names):
    for ii, i in enumerate(inds):
        o = inp["observations"].names[i]
        # best = trace[o].quantile(0.5).data
        best = trace[o].mean().data
        hpdi2 = hpdi[o].data  # to_dict()['data']

        if ~np.isfinite(best):
            continue

        if (
            (
                (
                    (inp["observations"].values[i] + inp["observations"].delta[i][0])
                    > best
                )
                and (
                    (inp["observations"].values[i] - inp["observations"].delta[i][1])
                    < best
                )
            )
            or (
                hpdi2[0] < inp["observations"].values[i]
                and hpdi2[1] > inp["observations"].values[i]
            )
            or (
                inp["observations"].values[i] > best
                and o in inp["observations"].mask_obs_up_lims
            )
            or (
                inp["observations"].values[i] < best
                and o in inp["observations"].mask_obs_lo_lims
            )
        ):
            # col = 'green'
            col = (0, 1, 0, face_alpha)
        elif (
            hpdi2[0] < (inp["observations"].values[i] + inp["observations"].delta[i][0])
        ) and (
            hpdi2[1] > (inp["observations"].values[i] - inp["observations"].delta[i][1])
        ):
            # col = 'yellow'
            col = (1, 1, 0, face_alpha)

        else:
            # col = 'red'
            col = (1, 0, 0, face_alpha)

        # if i in inp['mask_obs_lims']:
        a = 0.25 if o in inp["observations"].mask_obs_lims else 0.75

        tmp = angles[ii] + 0.4 * delta * np.array([-1, 1])
        art = ax.fill_between(
            np.linspace(tmp[0], tmp[1], 100),
            lims[0],
            lims[1],
            facecolor=col,
            lw=1,
            ec=(0, 0, 0, a),
        )

        # if i in inp['mask_obs_lims']:

    lines2, labels2 = ax.set_thetagrids(angles * 180 / np.pi, [""] * len(angles))
    labels = np.array([inp["observations"].names[i] for i in inds])
    for i, angle in enumerate(angles):
        if angle > np.pi / 2 and angle < 3 * np.pi / 2:
            angle2 = angle - np.pi
            ha = "right"
        else:
            angle2 = angle
            ha = "left"
        ax.text(
            angle,
            lims[1],
            labels[i],
            rotation=angle2 * 180 / np.pi,
            rotation_mode="anchor",
            horizontalalignment=ha,
            verticalalignment="center",
            color="blue",
        )

    ax.legend(loc="upper right")
    ax.set_title("y=(obs-mod)/(0.5*(obs+mod))", loc="left")
    fig.tight_layout()
    fig.savefig(inp["output_directory"] + "/Plots/matchplot_disk.pdf")

    return


# -------------------------------------------------------------------
# from F. Galliano
# SUEs (1 sigma contour of a bivariate split-normal distribution)
# -----------
def skunc(
    xmean=None,
    ymean=None,
    xstdev=None,
    ystdev=None,
    rho=None,
    xskew=None,
    yskew=None,
    xmin=None,
    xmax=None,
    ymin=None,
    ymax=None,
    Npt=300,
    xlog=False,
    ylog=False,
):
    """
    SKEWED UNCERTAINTY ELLIPSES (SUE)

    Function to plot uncertainty SUEs (or 1 sigma contour of a bivariate
    split-normal distribution). The parameters are the means (xmean,ymean), the 
    standard deviations (xstdev,ystdev), the skewnesses (xskew,yskew) and the 
    correlation coefficients (rho). The optional bounds (xmin,xmax,ymin,ymax) 
    have the effect of truncating the SUEs in case there is a range of 
    parameter space that is forbidden.

    It is important to notice that the xlog/ylog parameters are not related to
    the log settings of the axes where we plot the SUE, but are here to 
    indicate that the moments of the variable to plot correspond to the natural 
    logarithm (ln) of the variable we want to display. For instance, for 
    displaying the ellipses of (x,y) where, for x, the moments are those of lnx,
    we would write:
        SUE(xmean=mean_of_lnx,ymean=mean_of_y,xstdev=stdev_of_lnx, \
            ystdev=stdev_of_y,xskew=skewness_of_lnx,yskew=skewness_of_y, \
            rho=correl_coeff_of_lnx_and_y,xlog=True)
    """

    # Function for SUEs
    def Btau(tau):
        return (1 - 2 / np.pi) * (tau - 1) ** 2 + tau

    # Function for SUEs
    def Ctau(tau):
        return ((4 / np.pi - 1) * tau**2 + (3 - 8 / np.pi) * tau + 4 / np.pi - 1) * (
            tau - 1
        )

    # Rotation angle
    theta = 1.0 / 2 * np.arctan(2 * rho * xstdev * ystdev / (xstdev**2 - ystdev**2))

    # Numerically solve for taux and tauy (tau=1.D2 ==> skew=0.99)
    taugrid = np.logspace(-2, 2, 10000)
    Ax = (
        np.sqrt(np.pi / 2)
        * (
            (np.cos(theta)) ** 3 * xskew * xstdev**3
            + (np.sin(theta)) ** 3 * yskew * ystdev**3
        )
        / ((np.sin(theta)) ** 6 + (np.cos(theta)) ** 6)
        * (
            ((np.cos(theta)) ** 2 - (np.sin(theta)) ** 2)
            / ((np.cos(theta)) ** 2 * xstdev**2 - (np.sin(theta)) ** 2 * ystdev**2)
        )
        ** 1.5
    )
    Ay = (
        np.sqrt(np.pi / 2)
        * (
            (np.cos(theta)) ** 3 * yskew * ystdev**3
            - (np.sin(theta)) ** 3 * xskew * xstdev**3
        )
        / ((np.cos(theta)) ** 6 + (np.sin(theta)) ** 6)
        * (
            ((np.cos(theta)) ** 2 - (np.sin(theta)) ** 2)
            / ((np.cos(theta)) ** 2 * ystdev**2 - (np.sin(theta)) ** 2 * xstdev**2)
        )
        ** 1.5
    )
    taux = np.exp(
        np.interp(Ax, Ctau(taugrid) / (Btau(taugrid)) ** 1.5, np.log(taugrid))
    )
    tauy = np.exp(
        np.interp(Ay, Ctau(taugrid) / (Btau(taugrid)) ** 1.5, np.log(taugrid))
    )
    if not np.isfinite(taux) or taux > 1.0e2:
        taux = 1.0e2
    if not np.isfinite(tauy) or tauy > 1.0e2:
        tauy = 1.0e2

    # Rest of the parameters
    lambdax = np.sqrt(
        ((np.cos(theta)) ** 2 * xstdev**2 - (np.sin(theta)) ** 2 * ystdev**2)
        / ((np.cos(theta)) ** 2 - (np.sin(theta)) ** 2)
        / Btau(taux)
    )
    lambday = np.sqrt(
        ((np.cos(theta)) ** 2 * ystdev**2 - (np.sin(theta)) ** 2 * xstdev**2)
        / ((np.cos(theta)) ** 2 - (np.sin(theta)) ** 2)
        / Btau(tauy)
    )
    x0 = xmean - np.sqrt(2 / np.pi) * (
        np.cos(theta) * lambdax * (taux - 1) - np.sin(theta) * lambday * (tauy - 1)
    )
    y0 = ymean - np.sqrt(2 / np.pi) * (
        np.sin(theta) * lambdax * (taux - 1) + np.cos(theta) * lambday * (tauy - 1)
    )

    # Draw the SUE
    matrot = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
    xell_ax1 = np.zeros(2)
    yell_ax1 = np.zeros(2)
    xell_ax2 = np.zeros(2)
    yell_ax2 = np.zeros(2)
    for k in np.arange(4):
        if k == 0:
            xell_sub = np.linspace(-lambdax, 0, Npt) + x0
            rx = 1 - (xell_sub - x0) ** 2 / lambdax**2
            yell_sub = np.zeros(Npt)
            yell_sub[rx >= 0] = -lambday * np.sqrt(rx[rx >= 0]) + y0
            yell_sub[rx < 0] = np.nan
        elif k == 1:
            xell_sub = np.linspace(0, lambdax * taux, Npt) + x0
            rx = 1 - (xell_sub - x0) ** 2 / lambdax**2 / taux**2
            yell_sub = np.zeros(Npt)
            yell_sub[rx >= 0] = -lambday * np.sqrt(rx[rx >= 0]) + y0
            yell_sub[rx < 0] = np.nan
        elif k == 2:
            xell_sub = (np.linspace(0, lambdax * taux, Npt))[::-1] + x0
            rx = 1 - (xell_sub - x0) ** 2 / lambdax**2 / taux**2
            yell_sub = np.zeros(Npt)
            yell_sub[rx >= 0] = lambday * tauy * np.sqrt(rx[rx >= 0]) + y0
            yell_sub[rx < 0] = np.nan
        elif k == 3:
            xell_sub = (np.linspace(-lambdax, 0, Npt))[::-1] + x0
            rx = 1 - (xell_sub - x0) ** 2 / lambdax**2
            yell_sub = np.zeros(Npt)
            yell_sub[rx >= 0] = lambday * tauy * np.sqrt(rx[rx >= 0]) + y0
            yell_sub[rx < 0] = np.nan

        # Add the limit case (half ellipse)
        mask = np.logical_and(np.isfinite(yell_sub), np.isfinite(xell_sub))
        xell_sub = xell_sub[mask]
        yell_sub = yell_sub[mask]
        Nsub = np.count_nonzero(mask)

        # Rotate the ellipse
        for j in np.arange(Nsub):
            vecell = np.matmul(matrot, np.array([xell_sub[j] - x0, yell_sub[j] - y0]))
            xell_sub[j] = vecell[0] + x0
            yell_sub[j] = vecell[1] + y0
        if k == 0:
            xell = xell_sub
            yell = yell_sub
        else:
            xell = np.concatenate((xell, xell_sub))
            yell = np.concatenate((yell, yell_sub))
    xplot = np.concatenate((xell, [xell[0]]))
    yplot = np.concatenate((yell, [yell[0]]))

    # Logs and limits
    if xlog:
        xplot = np.exp(xplot)
        x0 = np.exp(x0)
    if ylog:
        yplot = np.exp(yplot)
        y0 = np.exp(y0)
    if xmin is not None:
        xplot[xplot < xmin] = xmin
        if x0 < xmin:
            x0 = xmin
    if xmax is not None:
        xplot[xplot > xmax] = xmax
        if x0 > xmax:
            x0 = xmax
    if ymin is not None:
        yplot[yplot < ymin] = ymin
        if y0 < ymin:
            y0 = ymin
    if ymax is not None:
        yplot[yplot > ymax] = ymax
        if y0 > ymax:
            y0 = ymax

    return xplot, yplot, x0, y0


# -------------------------------------------------------------------
def plot_2DPDF(
    X,
    Y,
    xlabel="",
    ylabel="",
    filename="",
    do_kde=False,
    do_skunc=False,
    do_cloud=False,
):
    inds = np.isfinite(X) * np.isfinite(Y)
    X = X[inds]
    Y = Y[inds]

    if do_kde:
        ax = az.plot_kde(X, Y)
    else:
        fig, ax = plt.subplots(1, 1)

    if do_cloud:
        for i in range(len(X)):
            ax.plot(X[i], Y[i], "ko", alpha=50 / len(X))

    if do_skunc:
        r = robust_moments(X, Y, skew=True)
        mu, lambd, tau = convert_to_splitnorm(
            mean=r["robust_mean"],
            stdev=r["robust_stdev"],
            skewness=r["robust_skewness"],
        )
        r.update({"mu": mu})
        r.update({"lambda": lambd})
        r.update({"tau": tau})
        xell, yell, x0, y0 = skunc(
            xmean=r["robust_mean"][0],
            ymean=r["robust_mean"][1],
            xstdev=r["robust_stdev"][0],
            ystdev=r["robust_stdev"][1],
            xskew=r["robust_skewness"][0],
            yskew=r["robust_skewness"][1],
            rho=r["rho"],
        )

        ax.fill(xell, yell, color="red", alpha=0.4)
        ax.plot([x0], [y0], "ko")

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.get_figure().savefig(filename)

    return


# #-------------------------------------------------------------------
# def corr_obs_params(inp, params_forplots, addobs=[], post_process=False, method='spearman', savemosaic=False, verbose=False):
#     d = inp['output_directory']

#     with open(d+'/input.pkl', 'rb') as f:
#         res = pickle.load(f)
#     if verbose:
#         print(res.keys())

#     with open(d+'/results_search.pkl', 'rb') as f:
#         res2 = pickle.load(f)
#     if verbose:
#         print(res2.keys())

#     if post_process:
#        t = az.from_netcdf(d+'/trace_post-process.netCDF')
#        fname_pp = '_pp'
#     else:
#        t = az.from_netcdf(d+'/trace_process.netCDF')
#        fname_pp = ''

#     params = params_forplots #['{}_{}'.format(r, s) for r in res['params_name'] for s in range(res['n_comps']) if 'idx_{}_{}'.format(r, s) in res2['unobserved_RVs']] + ['w_{}'.format(s) for s in range(res['n_comps'])] + ['scale']
#     if verbose:
#         print(params)
#     n_params = len(params)

#     obs = [r for r in res['observations'].names if r in t.posterior.keys()] + addobs
#     if verbose:
#         print(obs)
#     n_obs = len(obs)

#     tab = np.zeros([n_params, n_obs])

#     #calculate corr. coeff. tab
#     for i,p in enumerate(params):
#         for j,o in enumerate(obs):
#             x = t.posterior[o].data.flatten()
#             y = t.posterior[p].data.flatten()

#             #take middle 50%
#             #ind = np.where((y>np.quantile(y, 0.25))&(y<np.quantile(y, 0.75)))[0]

#             if method=='spearman':
#                 corr, _ = spearmanr(x, y)
#             elif method=='pearson':
#                 corr, _ = pearsonr(x, y)
#             tab[i,j] = corr

#     #make mosaic
#     if savemosaic:
#         fig, axs = plt.subplots(n_params, n_obs, figsize=(4*n_obs, 3*n_params), sharey='row')
#         for i,p in enumerate(params):
#             for j,o in enumerate(obs):
#                 ax = axs[i,j]
#                 x = t.posterior[o].data.flatten()
#                 y = t.posterior[p].data.flatten()
#                 az.plot_kde(x, y, ax=ax)
#                 if j==0:
#                     ax.set_ylabel(p)
#                 if i==len(params)-1:
#                     ax.set_xlabel(o)
#                 ax.set_title('{}'.format(tab[i,j]))
#         fig.subplots_adjust(wspace=0, hspace=0.2)
#         plt.tight_layout()
#         fig.savefig(inp['output_directory'] + '/Plots/corr_mosaic{}.pdf'.format(fname_pp))

#     #for a given param get tracers that correlate best
#     fig, axs = plt.subplots(n_params, 1, figsize=(4, 4*n_params), sharey='col')
#     for i,p in enumerate(params):
#         ax = axs[i]

#         inds = np.argsort(tab[i,:])

#         fs = 200/len(inds)
#         if fs<2:
#            fs = 2.
#         if fs>15:
#            fs = 15.

#         for jj,j in enumerate(inds):
#             ax.scatter(jj, tab[i,j], color='black')

#         ax.set_xticks(np.arange(n_obs))
#         ax.set_xticklabels(np.array(obs)[inds], rotation='vertical', fontsize=fs)
#         ax.set_title(p)
#         ax.set_ylabel('Corr. coeff. [{}]'.format(p))

#         ax.axhline(0, color='black')
#         ax.set_xlim([-0.5, n_obs-0.5])
#         ax.set_ylim([-1, 1])

#     plt.tight_layout()
#     fig.savefig(inp['output_directory'] + '/Plots/corr_params_obs{}.pdf'.format(fname_pp))

#     #for a given tracer get params that correlate best
#     fig, axs = plt.subplots(n_obs, 1, figsize=(4, 3*n_obs), sharey='col')
#     for j,o in enumerate(obs):
#         ax = axs[j]

#         inds = np.argsort(tab[:,j])

#         fs = 200/len(inds)
#         if fs<2:
#            fs = 2.
#         if fs>15:
#            fs = 15.

#         for ii,i in enumerate(inds):
#             ax.scatter(ii, tab[i,j], color='black')

#         ax.set_xticks(np.arange(n_params))
#         ax.set_xticklabels(np.array(params)[inds], rotation='vertical', fontsize=fs)
#         ax.set_title(o)
#         ax.set_ylabel('Corr. coeff. [{}]'.format(o))

#         ax.axhline(0, color='black')
#         ax.set_xlim([-0.5, n_params-0.5])
#         ax.set_ylim([-1, 1])

#     plt.tight_layout()
#     fig.savefig(inp['output_directory'] + '/Plots/corr_obs_params{}.pdf'.format(fname_pp))


# -------------------------------------------------------------------
def corr_xy_plot(
    inp,
    xnames,
    ynames,
    post_process=False,
    # method="spearman",
    savemosaic=False,
    verbose=False,
    marginalize=True,
    label="",
    resdict=None,
):
    d = inp["output_directory"]

    if post_process:
        fname_pp = "secondary_"
        if resdict is None:
            with open(d + "results_post-process.pkl", "rb") as f:
                resdict = pickle.load(f)
    else:
        fname_pp = ""
        if resdict is None:
            with open(d + "results_process.pkl", "rb") as f:
                resdict = pickle.load(f)

    if "rescorr_{}".format(label) not in resdict.keys():
        return
    tab = resdict["rescorr_{}".format(label)]["tabcorr"]
    corrcomp = resdict["rescorr_{}".format(label)]["corrcomp_max"]

    # #1D version, useful to know what tracers correlated best/worst
    # couples, diff = [], []
    # for i,xx in enumerate(xnames):
    #    for i2 in range(i+1, len(xnames)):
    #       diff += [1-0.5*np.nanmax(np.abs(tab[i,:]-tab[i2,:]))]
    #       couples += ['{}, {}'.format(xx, xnames[i2])]
    # couples = np.array(couples)[np.argsort(diff)]
    # diff = np.array(diff)[np.argsort(diff)]
    # s = int(0.75*len(xnames))
    # fig, ax = plt.subplots(1, 1, figsize=(s, 5))
    # for kk,k in enumerate(couples):
    #    ax.scatter(kk, diff[kk], color='black')
    # ax.set_title('Correlation between {}'.format(label))
    # ax.axhline(rcParams['corr_significant'], color='black', linestyle='--')
    # ax.set_xticks(np.arange(len(couples)))
    # ax.set_xticklabels(couples, rotation='vertical', fontsize=12)
    # plt.tight_layout()
    # fig.savefig(inp['output_directory'] + '/Plots/correlation_between_{}{}.pdf'.format(fname_pp, label))

    if label not in ("tracers_obs", "parameters_obs"):
        # 2D version, useful to check any couple
        # sort corrcomp
        idx_sorted = [0]
        for n in range(1, len(xnames)):
            tmp = corrcomp[idx_sorted[-1], :]

            imax, diff = 0, 1e8
            for m in range(1, len(xnames)):
                if m in idx_sorted:
                    continue
                tmp2 = corrcomp[m, :]  # compare to previous rows
                d = np.max(np.abs(tmp - tmp2))  # mean/max, both are interesting
                if d < diff:
                    diff = d
                    imax = m
            idx_sorted += [
                imax,
            ]
        corrcomp = corrcomp[np.ix_(idx_sorted, idx_sorted)]
        s = int(0.5 * len(xnames))
        fig, ax = plt.subplots(1, 1, figsize=(s, s))
        # if we wanna mask the upper triangle
        mask = np.zeros_like(corrcomp)
        # mask[np.triu_indices_from(mask)] = True
        with sns.axes_style("white"):
            ax = sns.heatmap(
                corrcomp,
                mask=mask,
                square=True,
                cmap="mako_r",
                xticklabels=np.array(xnames)[idx_sorted],
                yticklabels=np.array(xnames)[idx_sorted],
                cbar=True,
                vmax=1,
                cbar_kws={"shrink": 0.70},
            )
        ax.tick_params(
            right=True,
            top=True,
            labelright=False,
            labeltop=False,
            rotation=0,
            labelsize=12,
        )
        plt.xticks(rotation="vertical")
        # mask[np.triu_indices_from(mask)] = True
        # ax.set_title('Global correlation coefficient {}'.format(np.round(np.nanmean(corrcomp[mask==1]), 2)))
        plt.subplots_adjust(
            bottom=0.1, right=0.9, top=0.9, left=0.1, hspace=0.0, wspace=0
        )
        plt.tight_layout()
        fig.savefig(
            inp["output_directory"]
            + "/Plots/correlation_map_{}{}.pdf".format(fname_pp, label)
        )

        # #2D version, useful to check any couple [not sorted]
        # s = int(0.5*len(xnames))
        # fig, ax = plt.subplots(1, 1, figsize=(s, s))
        # #if we wanna mask the upper triangle
        # mask = np.zeros_like(corrcomp)
        # #mask[np.triu_indices_from(mask)] = True
        # with sns.axes_style("white"):
        #    ax = sns.heatmap(corrcomp, mask=mask, square=True,  cmap="magma_r", xticklabels=xnames, yticklabels=xnames, cbar=True, vmax=1)
        # ax.tick_params(right=True, top=True, labelright=False, labeltop=True, rotation=0)
        # #mask[np.triu_indices_from(mask)] = True
        # #ax.set_title('Global correlation coefficient {}'.format(np.round(np.nanmean(corrcomp[mask==1]), 2)))
        # plt.xticks(rotation='vertical')
        # plt.tight_layout()
        # fig.savefig(inp['output_directory'] + '/Plots/correlation_map_{}{}.pdf'.format(fname_pp, label))

    # make correlation plots
    nlines = len(xnames)
    ncols = rcParams["plot_ncols"]
    nrows = int(np.ceil(nlines / ncols))

    fig, axs = plt.subplots(
        nrows=nrows, ncols=ncols, sharey="col", figsize=(4 * ncols, 4 * nrows)
    )
    if nrows == 1:
        axs = np.array(axs, ndmin=2)

    # remove frames
    for i in range(ncols):
        for j in range(nrows):
            axs[j, i].set_axis_off()

    l = 0
    n_x = len(xnames)
    n_y = len(ynames)

    xnames = [x.replace("likelihood_", "") for x in xnames]
    ynames = [y.replace("likelihood_", "") for y in ynames]

    for i in range(ncols):
        for j in range(nrows):
            if l >= n_x:
                continue

            ax = axs[j, i]
            ax.set_axis_on()

            inds = np.argsort(tab[l, :])

            # font size
            fs = 200 / len(inds)
            # if fs<2:
            #   fs = 2.
            if fs > 15:
                fs = 15.0

            for kk, k in enumerate(inds):
                ax.scatter(kk, tab[l, k], color="black")

                ax.set_xticks(np.arange(n_y))
                ax.set_xticklabels(
                    np.array(ynames)[inds], rotation="vertical", fontsize=fs
                )
                ax.set_title(xnames[l])
                ax.set_ylabel("Corr. coeff.")

                if label in ("tracers_obs", "parameters_obs"):
                    ax.set_ylim([0, 1])
                    ax.set_xlim([-0.5, n_y - 0.5])
                    ax.axhline(
                        rcParams["corr_significant"], color="black", linestyle="--"
                    )
                else:
                    ax.set_ylim([-1, 1])
                    ax.set_xlim([-0.5, n_y - 0.5])
                    ax.axhline(0, color="black")
                    ax.axhline(
                        rcParams["corr_significant"], color="black", linestyle="--"
                    )
                    ax.axhline(
                        -rcParams["corr_significant"], color="black", linestyle="--"
                    )

            l += 1

    plt.tight_layout()
    fig.savefig(
        inp["output_directory"] + "/Plots/correlation_{}{}.pdf".format(fname_pp, label)
    )


# -------------------------------------------------------------------
def joyplot(x, y, ybins, labels=None):
    sns.set(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})
    if labels is None:
        labels = ["x", "y"]

    step = ybins.step
    ybins = np.array(ybins)

    x2, y2 = [], []
    for yy in ybins:
        ind = np.where((y > yy) & (y < (yy + step)))[0]
        y2 += [
            np.repeat(yy, len(ind)),
        ]
        x2 += [
            x[ind],
        ]
    x2 = np.concatenate(x2).ravel()
    y2 = np.concatenate(y2).ravel()

    df = pd.DataFrame(dict(x=x2, y=y2))
    df = df.rename(columns={"x": labels[0], "y": labels[1]})

    # Initialize the FacetGrid object
    pal = sns.cubehelix_palette(
        10, rot=0.5, light=0.7
    )  # change color with rot, dynamic range with light
    g = sns.FacetGrid(
        df,
        row=labels[1],
        hue=labels[1],
        aspect=15,
        size=0.5,
        palette=pal,
        row_order=range(100, 0, -step),
    )

    # Draw the densities in a few steps
    g.map(sns.kdeplot, labels[0], clip_on=False, shade=True, alpha=1, lw=1.5, bw=0.2)
    g.map(sns.kdeplot, labels[0], clip_on=False, color="w", lw=2, bw=0.2)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)

    # Define and use a simple function to label the plot in axes coordinates
    def label(x, color, label):
        ax = plt.gca()
        ax.text(
            0,
            0.2,
            "[{},{}]".format(label, str(float(label) + step)),
            fontweight="bold",
            color=color,
            ha="left",
            va="center",
            transform=ax.transAxes,
        )

    g.map(label, labels[0])

    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-0.25)

    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)
    plt.savefig("joyplot.pdf")

    # example:
    # with open('/local/home/vleboute/Downloads/tracefesc_linear_all_sample.pkl', 'rb') as f:
    #    fesc = np.array(pickle.load(f))
    # with open('/local/home/vleboute/Downloads/traceZ_all_sample.pkl', 'rb') as f:
    #    Z = np.array(pickle.load(f))

    # #use range for bins, not np.arange
    # joyplot(Z, fesc, range(0, 100, 10), labels=('Z', 'fesc'))


def joyplot_irr(x, y, ybins, labels=None):
    sns.set(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})
    if labels is None:
        labels = ["x", "y"]

    ybins = np.array(ybins)

    x2, y2 = [], []
    for i, yy in enumerate(ybins[:-1]):
        ind = np.where((y >= ybins[i]) & (y < ybins[i + 1]))[0]
        y2 += [
            np.repeat(yy, len(ind)),
        ]
        x2 += [
            x[ind],
        ]
    x2 = np.concatenate(x2).ravel()
    y2 = np.concatenate(y2).ravel()

    df = pd.DataFrame(dict(x=x2, y=y2))
    df = df.rename(columns={"x": labels[0], "y": labels[1]})

    # Initialize the FacetGrid object
    pal = sns.cubehelix_palette(
        10, rot=0.5, light=0.7
    )  # change color with rot, dynamic range with light
    g = sns.FacetGrid(
        df,
        row=labels[1],
        hue=labels[1],
        aspect=15,
        size=0.5,
        palette=pal,
        row_order=range(100, 0, -step),
    )

    # Draw the densities in a few steps
    g.map(sns.kdeplot, labels[0], clip_on=False, shade=True, alpha=1, lw=1.5, bw=0.2)
    g.map(sns.kdeplot, labels[0], clip_on=False, color="w", lw=2, bw=0.2)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)

    # Define and use a simple function to label the plot in axes coordinates
    def label(x, color, label):
        ax = plt.gca()
        ax.text(
            0,
            0.2,
            "[{},{}]".format(label, str(float(label) + step)),
            fontweight="bold",
            color=color,
            ha="left",
            va="center",
            transform=ax.transAxes,
        )

    g.map(label, labels[0])

    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-0.25)

    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)


# -------------------------------------------------------------------
def set_axis_style(ax, labels, text=False):
    ax.xaxis.set_tick_params(direction="out")
    ax.xaxis.set_ticks_position("bottom")
    ax.set_xlim(0.25, len(labels) + 0.75)
    if text:
        ax.set_xticks(np.arange(1, len(labels) + 1), labels=labels)
        ax.set_xlabel("Model")


def boxplot(traces, var_names, labels=[], units=[], ref=[], extras={}, ncols=None):
    plt.clf()
    if type(traces) is not list:
        traces = [
            traces,
        ]

    nm = len(traces)

    if labels == []:
        labels = [f"Model {i}" for i in range(nm)]

    nv = len(var_names)

    if units == []:
        units = [""] * nv

    colors = [k for k in mcolors.TABLEAU_COLORS]
    colors = colors[0 : len(labels)]

    nn = traces[0].dims["draw"] * traces[0].dims["chain"]

    if ncols is None:
        ncols = rcParams["plot_ncols"]
    nrows = int(np.ceil(nv / ncols))

    alpha = 0.2
    width = 0.9

    fig, axs = plt.subplots(nrows, ncols, figsize=(3 * ncols, 2 * nrows), sharex="col")
    if nrows == 1:
        axs = np.array(axs, ndmin=2)
    if ncols == 1:
        axs = np.array(axs, ndmin=2)

    if nrows != axs.shape[0]:
        ncols, nrows = nrows, ncols

    # remove frames
    for i in range(ncols):
        for j in range(nrows):
            axs[j, i].set_axis_off()

            # set style for the axes
            # print(v, nv-1, k, i, j)
            axs[j, i].xaxis.set_tick_params(direction="out")
            axs[j, i].xaxis.set_ticks_position("bottom")
            axs[j, i].set_xlim(0.25, len(labels) + 0.75)
            axs[j, i].set_xticks(np.arange(1, len(labels) + 1), labels=labels)
            axs[j, i].set_xlabel("Model")

    k = -1
    for i in range(ncols):
        for j in range(nrows):
            k += 1

            if k > nv - 1:
                continue

            axs[j, i].set_axis_on()

            v = var_names[k]

            if v == "pvalue":
                data = extras["pvalue"]
            else:
                data = [np.array(t[v]).flatten() for t in traces]

            parts = axs[j, i].boxplot(
                data,
                widths=width,
                patch_artist=True,
                notch=False,
                whis=(100 - 100 * rcParams["ci"], 100 * rcParams["ci"]),
            )

            if ref != []:
                axs[j, i].axhline(ref[k], color="black")

            # changing color and linewidth of
            # medians
            for median in parts["medians"]:
                median.set(color="red", linewidth=3)

            # changing color and linewidth of whiskers
            for whisker in parts["whiskers"]:
                whisker.set(color="gray", linewidth=2)

            # changing color and linewidth of caps
            for cap in parts["caps"]:
                cap.set(color="gray", linewidth=2)

            # changing style of fliers
            for flier in parts["fliers"]:
                flier.set(marker="o", color="gray", alpha=10 / nn)

            for patch, color in zip(parts["boxes"], colors):
                patch.set_facecolor(color)
                patch.set_alpha(alpha)

            # Removing top axes and right axes ticks
            # ax.get_xaxis().tick_bottom()
            # ax.get_yaxis().tick_left()

            # set style for the axes
            # print(v, nv-1, k, i, j)
            axs[j, i].xaxis.set_tick_params(direction="out")
            axs[j, i].xaxis.set_ticks_position("bottom")
            axs[j, i].set_xlim(0.25, len(labels) + 0.75)
            axs[j, i].set_xticks(np.arange(1, len(labels) + 1), labels=labels)
            axs[j, i].set_xlabel("Model")

            axs[j, i].set_ylabel(f"{v} {units[k]}")

    if nm > 1:
        fig.autofmt_xdate(rotation=25)
    fig.tight_layout()

    return fig


# --------------------------------------


def getp(ps):
    "from <param>_<sector> get only <param>"
    if re.search(".*_[0-9]", ps) is None:
        return ps
    else:
        return "_".join(ps.split("_")[:-1])
