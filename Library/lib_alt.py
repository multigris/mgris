# alternative methods

from Library.lib_misc import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

import pandas as pd
import logging
import os
from tqdm import tqdm

from scipy import misc
from scipy import stats
import scipy
import itertools

from Library.lib_class import ModelGrid


# -------------------------------------------------------------------
def delta_chi2(nsigma, dof):
    """
    http://www.reid.ai/2012/09/chi-squared-distribution-table-with.html
    """

    return scipy.stats.chi2.ppf(scipy.stats.chi2.cdf(nsigma**2, 1), dof)


# -------------------------------------------------------------------
def chi2value(pvalue, dof):
    """
    Get the Chi2 distribution knowing the probability value and degree of freedom
    https://www.science-emergence.com/Articles/Cr%C3%A9er-une-table-du-chi2-avec-python/
    """

    def newtons_method(f, x, tolerance=0.0001):
        while True:
            x1 = x - f(x) / misc.derivative(f, x)
            t = abs(x1 - x)
            if t < tolerance:
                break
            x = x1
        return x

    def f(x):
        return 1 - stats.chi2.cdf(x, dof) - pvalue

    return newtons_method(f, dof)


# -------------------------------------------------------------------
def build_covmat(inp, verbose=False):
    # Using Monte-Carlo
    # each iteration is a randomized observation: draw random sigma_line, sigma_cal, sigma_stitch...
    # add them linearly for each line

    nsamples = int(1e5)  # the larger the bette

    sysunc = inp["sysunc"]
    observations = inp["observations"]

    # prepare sysunc to be used for all observables
    sigsyunc = {}
    if sysunc is not None:
        for s in sysunc:
            sigsysunc.update({s: np.random.normal(0, sysunc[s][0], nsamples)})

    res = {}
    for i, o in enumerate(observations.names):
        # sig = np.random.normal(0, 0.1, nsamples) #test
        sig = np.random.normal(0, np.mean(observations.delta[i]), nsamples)
        if sysunc is not None:
            for s in sysunc[s][1]:
                if o in s:
                    sig += sigsysunc[s]
        res.update({o: sig})

    tab = np.array([[res[o][i] for o in observations.names] for i in range(nsamples)]).T

    return np.cov(tab)


# -------------------------------------------------------------------
def alt_chi2pdf(grid, inp, verbose=False):
    # sum of pdf for which param = value / sum of pdf on all models

    plotdir = inp["output_directory"] + "/Plots/chi2/"
    os.makedirs(plotdir, exist_ok=True)

    for param_name in grid.params.names:
        norm = np.sum(
            np.exp(-0.5 * grid.values["chi2red"])
        )  # / len(grid.params.values[param_name])

        pdf = []
        vals = grid.params.values[param_name]
        for val in vals:
            pdf += [
                np.sum(
                    np.exp(
                        -0.5
                        * grid.values.loc[grid.values[param_name] == val]["chi2red"]
                    )
                )
                / norm,
            ]

        fig, ax = plt.subplots(1, 1, figsize=(7, 7))
        ax.step(vals, pdf, where="mid")
        ax.set_xlabel(param_name)
        ax.set_ylabel("PDF")

        fig.tight_layout(pad=1, w_pad=5, h_pad=5)
        # fig.subplots_adjust(hspace=0.2, wspace=0.2)
        outname = "{}/PDF_{}.pdf".format(plotdir, param_name)
        fig.savefig(outname)

    return


# -------------------------------------------------------------------
def alt_chi2pdf2d(grid, inp, verbose=False):
    # sum of pdf for which param = value / sum of pdf on all models

    plotdir = inp["output_directory"] + "/Plots/chi2/"
    os.makedirs(plotdir, exist_ok=True)

    n_params = len(grid.params.names)
    fig, axs = plt.subplots(n_params, n_params, figsize=(4 * n_params, 4 * n_params))

    norm = np.sum(np.exp(-0.5 * grid.values["chi2red"]))

    # remove frames
    for i in range(n_params):
        for j in range(n_params):
            axs[j, i].set_axis_off()

    for i, pi in enumerate(grid.params.names):
        for j, pj in enumerate(grid.params.names):

            if j < i:
                continue

            ax = axs[j, i]
            ax.set_axis_on()

            pdf = np.zeros([len(grid.params.values[pj]), len(grid.params.values[pi])])

            for ii, vi in enumerate(grid.params.values[pi]):
                for jj, vj in enumerate(grid.params.values[pj]):

                    pdf[jj, ii] += [
                        np.sum(
                            np.exp(
                                -0.5
                                * grid.values.loc[grid.values[pi] == vi].loc[
                                    grid.values[pj] == vj
                                ]["chi2red"]
                            )
                        )
                        / norm,
                    ]

            ax.imshow(
                pdf,
                extent=extents(grid.params.values[pi])
                + extents(grid.params.values[pj]),
                cmap=plt.cm.hot,
                aspect="auto",
                interpolation="none",
                origin="lower",
            )
            # ax.colorbar(im)

            ax.set_xlabel(pi)
            ax.set_ylabel(pj)

            # axs[j,i].set_aspect('equal')

    fig.tight_layout(pad=1, w_pad=5, h_pad=5)
    # fig.subplots_adjust(hspace=0.2, wspace=0.2)
    outname = "{}/PDF_2D.pdf".format(plotdir)
    fig.savefig(outname)

    return


# -------------------------------------------------------------------
def satisfiesconstraints(values, constraints, i, j=None):
    result = True
    for constraint in constraints:
        p = constraint[0]
        s = constraint[1]

        if constraint[2][0] == "|":  # wrt other sector
            if constraint[2] == "=":
                result *= values[p].iloc[j] == values[p].iloc[i]
            elif constraint[2] == ">":
                result *= values[p].iloc[j] > values[p].iloc[i]
            elif constraint[2] == ">=":
                result *= values[p].iloc[j] >= values[p].iloc[i]
            elif constraint[2] == "<":
                result *= values[p].iloc[j] < values[p].iloc[i]
            elif constraint[2] == "<=":
                result *= values[p].iloc[j] <= values[p].iloc[i]
        else:  # wrt value
            if constraint[2] == "=":
                result *= values[p].iloc[i] == constraint[3]
            elif constraint[2] == ">":
                result *= values[p].iloc[i] > constraint[3]
            elif constraint[2] == ">=":
                result *= values[p].iloc[i] >= constraint[3]
            elif constraint[2] == "<":
                result *= values[p].iloc[i] < constraint[3]
            elif constraint[2] == "<=":
                result *= values[p].iloc[i] <= constraint[3]

    return result


# -------------------------------------------------------------------
def alt_chi2(grid, observations, inp=None, verbose=False):
    """
    chi2
    with covariance matrix
    e-X2/2
    """

    # ======================================
    # calculate covariance matrix once
    covmat = build_covmat(inp)
    covmat2 = np.linalg.inv(covmat).T

    # all possible combinations of parameters
    grid.values["chi2"] = grid.values[grid.params.names[0]] * np.nan

    # epsilon to find the right row
    mininterval = np.min(
        [
            np.min(abs(grid.params.values[p] - np.roll(grid.params.values[p], 1)))
            for p in grid.params.names
        ]
    )

    med = np.nanmedian(grid.values[observations.names])

    # inp['n_sectors'] = 2 #test

    logging.info("Constraints:")
    logging.info(inp["configuration_constraints"]["constraints_chi2"])

    logging.info("Calculating...")

    if len(inp["config"]) > 1:
        msg = "/!\\ Chi2 method working only for one group"
        logging.error(msg)
        raise Exception(msg)

    if inp["config"][0] > 2:
        msg = "/!\\ Chi2 method working only for one or two sectors"
        logging.error(msg)
        raise Exception(msg)

    elif inp["config"][0] == 1:
        for i in tqdm(range(len(grid.values))):

            if not satisfiesconstraints(
                grid.values, inp["configuration_constraints"]["constraints_chi2"], i
            ):
                continue

            mod = np.array(grid.values[observations.names].iloc[i])

            # normal chi2 (not accounting for correlated uncertainties)
            # chi2 = np.sum((observations.values-mod)**2 / np.mean(observations.delta, axis=1)**2) #sum of the chi2

            # accounting for correlated uncertainties
            x = np.array(observations.values - mod)
            chi2 = np.dot(np.dot(x, np.linalg.inv(covmat).T), x.T)

            grid.values["chi2"].iloc[i] = chi2  # chi2

    elif inp["config"][0] == 2:
        ws = [0.25, 0.5, 0.75]
        minchi2 = 1e8

        # new grid
        plabels = []
        for s in (0, 1):
            plabels += [
                "w_s{}".format(s),
            ]
            for p in grid.params.names:
                plabels += [
                    "{}_s{}".format(p, s),
                ]
        plabels += ["chi2"]

        newgrid = ModelGrid(params=ParameterSet(names=plabels[0:-1]))
        for s in (0, 1):
            newgrid.params.values["w_s{}".format(s)] = ws
            for p in grid.params.names:
                newgrid.params.values["{}_s{}".format(p, s)] = grid.params.values[p]
        newgrid.values = pd.DataFrame(
            data=np.zeros([1, len(observations.names) + len(plabels)]),
            columns=observations.names + plabels,
        )

        k = 0

        for i in tqdm(range(len(grid.values))):
            tmparri = np.array(grid.values[observations.names].iloc[i])
            for j in range(i + 1, len(grid.values)):

                if not satisfiesconstraints(
                    newgrid.values,
                    inp["configuration_constraints"]["constraints_chi2"],
                    i,
                    j=j,
                ):
                    continue

                # mods = np.array([med + np.log10( w*10**(tmparri-med) + (1-w)*10**(np.array(grid.values[observations.names].iloc[j])-med)) for w in ws])
                mods = np.array(
                    [
                        np.log10(
                            w * 10 ** tmparri.astype(np.float64)
                            + (1 - w)
                            * 10
                            ** grid.values[observations.names]
                            .iloc[j]
                            .values.astype(np.float64)
                        )
                        for w in ws
                    ]
                )

                # mods = [med + w*(tmparri-med) + (1-w)*(tmparri-med) for w in ws]

                chi2s = np.array(
                    [
                        np.dot(
                            np.dot(observations.values - m, covmat2),
                            (observations.values - m).T,
                        )
                        for m in mods
                    ]
                )

                inds = np.where(chi2s < minchi2 + 10 * np.isfinite(chi2s))[0]

                if len(inds) > 0:
                    print("*", end="", flush=True)
                    for n in inds:
                        new_p0 = [
                            ws[n],
                        ] + [v for v in grid.values[grid.params.names].iloc[i].values]
                        new_p1 = [
                            1 - ws[n],
                        ] + [v for v in grid.values[grid.params.names].iloc[j].values]
                        mod = [v for v in mods[n]]

                        tmp = pd.DataFrame(
                            data=[mod + new_p0 + new_p1 + [chi2s[n]]],
                            columns=observations.names + plabels,
                        )
                        newgrid.values = newgrid.values.append(tmp, ignore_index=True)

                        k += 1
                        minchi2 = chi2s[n]

        newgrid.values = newgrid.values.drop(0, axis="index")

        grid = newgrid
        outname = inp["output_directory"] + "results_chi2.pkl"
        grid.values.to_pickle(outname)
        if verbose:
            logging.info("Results values saved: {}".format(outname))

    # ========================================
    logging.info("Making plots...")

    # Andrae2010b
    # linear models without priors
    dof_naive = len(observations.names) - len(grid.params.names)
    dof_max = len(observations.names) - 1
    dof_min = 0  # non-linear models / linear models with priors
    dof = dof_max

    grid.values["chi2red"] = grid.values["chi2"] / dof

    # plot PDFs
    alt_chi2pdf(grid, inp)

    # integrate in from best value until sum of PDF equals some wanted percentage (e.g., 60%)
    alt_chi2pdf2d(grid, inp)

    minchi2 = np.min(
        grid.values["chi2red"]
    )  # variance = 2/N (only true for true model, i.e., with chi2=1), if N is large sigma = sqrt(2/N)
    minchi2unc = np.sqrt(
        2 / len(observations.names)
    )  # This is not a robust measurement of uncertainty on reduced chi2 since we do not have many observations, and also this is supposing the best model is true
    dchi2 = delta_chi2(1, dof)
    # models within minchi2 +/- sigma are equally as good
    # assuming minchi2 = 1 for delta_chi2
    idxs_bestmodels = np.where(
        grid.values["chi2red"] < (np.max([minchi2, 1]) + minchi2unc)
    )[
        0
    ]  # sqrt to get sigma is not quite reliable as N is rather small for us...
    idxs_within1sigma = np.where(
        grid.values["chi2red"] < (np.max([minchi2, 1]) + minchi2unc + dchi2)
    )[
        0
    ]  # sqrt to get sigma is not quite reliable as N is rather small for us...

    logging.info("min chi2    = {}".format(minchi2))
    logging.info("delta chi2  = {}".format(dchi2))
    logging.info("d+m chi2    = {}".format(dchi2 + minchi2unc))
    logging.info("idxs [best] = {}".format(idxs_bestmodels))
    logging.info("idxs [1sig] = {}".format(idxs_within1sigma))

    grid.values[grid.params.names].iloc[idxs_within1sigma]
    for p in grid.params.names:
        print(
            p,
            np.min(grid.values[p].iloc[idxs_within1sigma], axis=0),
            np.max(grid.values[p].iloc[idxs_within1sigma], axis=0),
        )

    return


# -------------------------------------------------------------------
# probas
def alt_normal(obs_grid, grid, inp, verbose=False):
    """
    Calculate probability for each line from distance to observed value
    We ignore the covariance matrix
    Provides the parameter range for which the probability that all lines are reproduced is larger than p, or than the average is larger than p

    Main limitations is how to combine the probabilities of all lines

    Can provide obs_grid and Params_values or obs_finegrid and Params_valuesfine
    """

    p = obs_grid[0 : inp["n_obs"]] * np.nan

    for i in range(inp["n_obs"]):
        # distance to observed values gives some kind of probability calculated from the normal distribution mu,sigma
        # logging.info(obs_grid[i], inp['observed'][i])
        # logging.info(inp['delta'][i]-inp['observed'][i])
        p[i] = Gauss1D(
            obs_grid[i],
            inp["observed"][i],
            psig=abs(0.1 * (inp["delta"][i] - inp["observed"][i])),
            pmax=1,
        )
        # not sure about the delta
        # proba is an array with same dims as obs_(fine)grid

    # probability considering all lines
    p_tot_avg = (
        np.nansum(p, axis=0) / inp["n_obs"]
    )  # probability that lines are reproduced on average is larger than p
    p_tot_min = np.nanmin(
        p, axis=0
    )  # probability that *all* lines are reproduced is larger than p

    # logging.info(p_tot_avg-p_tot_min)
    # fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    # ax.plot(np.arange(len(p_tot_avg.flatten())), p_tot_avg.flatten())
    # ax.plot(np.arange(len(p_tot_min.flatten())), p_tot_min.flatten())
    # fig.savefig(inp['output_directory']+'/tmp.pdf')

    # if verbose:
    #    logging.info(obs_grid.shape)
    #    logging.info(p.shape)
    #    logging.info(p_tot_avg.shape)
    #    logging.info(p_tot_min.shape)
    #    logging.info(grid.shape)
    #    logging.info(grid[0].shape)

    fig, axs = plt.subplots(inp["n_params"], 2, figsize=(4 * inp["n_params"], 8 * 2))
    for ip, p_tot in enumerate((p_tot_avg, p_tot_min)):
        # for ip, p_tot in enumerate((p_tot_min,)):
        # logging.info(np.nanmax(p_tot), np.nansum(p_tot))
        for i in range(inp["n_params"]):
            p_mean, p_arr, v_arr, v_mean = [], [], [], []
            for logp in np.arange(-3, 0, 0.1):
                p = 1 - 10**logp
                ind = [
                    i for i, pp in enumerate(p_tot.flatten()) if pp > p
                ]  # somehow p_tot_min gives issues with np.where which provides a bunch of '---' and messes up the selection with grid
                if len(ind) > 1:
                    l, u = (
                        np.nanmin(grid[i].flatten()[ind]),
                        np.nanmax(grid[i].flatten()[ind]),
                    )
                    if np.isfinite(l) and np.isfinite(u):
                        p_mean += [
                            p,
                        ]
                        v_mean += [
                            0.5 * np.sum([l, u]),
                        ]
                        p_arr += [
                            p,
                            p,
                        ]
                        v_arr += [
                            l,
                            u,
                        ]
                        axs[i, ip].plot([l, u], [p, p], "b-", alpha=0.1)
            axs[i, ip].plot(v_mean, p_mean, "r-", alpha=1)
            p_max, v_max = [], []
            for v in inp["Params_values"][inp["params_name"][i]]:
                ind = np.where(v_arr == v)[0]
                if len(ind) > 1:
                    v_max += [
                        v,
                    ]
                    p_max += [np.nanmax(np.array(p_arr)[ind])]
            axs[i, ip].plot(v_max, p_max, "bs-", alpha=0.9)
            axs[i, ip].set_xlabel(inp["params_name"][i])
            axs[i, ip].set_ylim([np.nanmin(p_max), np.nanmax(p_max)])
            # p_arr = ArraySort(p_arr, xarray=r_arr)
            # r_arr = ArraySort(r_arr)
    # fig.subplots_adjust(hspace=0.2, wspace=0.2)
    axs[0, 0].set_title("P>p on average")
    axs[0, 1].set_title("P>p for all lines")
    fig.tight_layout(pad=1, w_pad=5, h_pad=5)
    fig.savefig(inp["output_directory"] + "/Alt_normal.pdf")


# -------------------------------------------------------------------
