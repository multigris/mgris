import numpy as np
from pathlib import Path
import logging
from scipy.interpolate import interp1d
import sys
import readline
from scipy.stats import norm
import pyarrow.ipc as ipc
import subprocess
import glob
import os


# -----------------------------------------------------------------
def str2bool(v):
    """
    Converts any reasonable string input to a bool
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1", "True"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0", "False"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# -----------------------------------------------------------------
def ArraySort(yarray, xarray=None, reverse=False):
    # sortarrays([10,2,3]) or sortarrays([10,2,3], [5,2,4])
    if xarray is None:
        yarray = np.array(sorted(yarray))
        if reverse:
            yarray = yarray[::-1]
        return yarray
    else:
        yarray = np.array(
            [y for y, x in sorted(zip(yarray, xarray), key=lambda x: x[1])]
        )
        if reverse:
            yarray = yarray[::-1]
        return yarray


# --------------------------------------------------------------------
def ChiSquare(observations, model, uncertainties=None, dof=0):
    if uncertainties is None:
        uncertainties = observations * 0.0 + 1
    chi2 = 0.0
    for i, o in enumerate(observations):
        chi2 += (observations[i] - model[i]) ** 2 / uncertainties[
            i
        ] ** 2  # supposed to be for uncorrelated errors
    chi2 /= len(observations) - dof  # N-P for linear models
    return chi2


# --------------------------------------------------------------------
def ci2sigma(ci):
    ci_arr = [
        0.7979,
        7.9655,
        15.8519,
        23.5823,
        38.2925,
        48.4308,
        54.6745,
        60.4675,
        68.2689,
        95.4499,
        99.73,
        99.9936,
        99.99994,
    ]
    sig_arr = [0.01, 0.1, 0.2, 0.3, 0.5, 0.65, 0.75, 0.85, 1, 2, 3, 4, 5]
    return interp1d(ci_arr, sig_arr, fill_value="extrapolate")(ci)


# --------------------------------------------------------------------
def pvalue2sigma(
    pvalue, center=True
):  # for a centered variable, i.e., with pvalue=0.5 for perfect agreement
    ci = pvalue2pc(pvalue, center=center)
    return ci2sigma(ci)


# --------------------------------------------------------------------
def pvalue2pc(
    pvalue, center=True
):  # for a centered variable, i.e., with pvalue=0.5 for perfect agreement
    tmp = 100 * np.min([pvalue, 1 - pvalue])
    if center:
        tmp *= 2
    return tmp


# --------------------------------------------------------------------
def Gauss1D(x, pctr, pmax=None, parea=None, psig=None, pfwhm=None):
    if pmax is None and parea is None:
        print("Provide any of pmax or parea...")
        return None
    if pmax is not None and parea is not None:
        print("Cannot use max and area at the same time...")
        return None
    if psig is not None and pfwhm is not None:
        print("Cannot use sigma and fwhm at the same time...")
        return None
    if psig is None and pfwhm is None:
        print("Use either psig or pfwhm...")
        return None

    if pfwhm is not None:
        psig = sig2fwhm(pfwhm)
    if parea is not None:
        pmax = parea / (psig * np.sqrt(2.0 * pi))

    return pmax * np.exp(-((x - pctr) ** 2) / (2 * psig**2))


# --------------------------------------------------------------------
# def logsum(logs):
#    oom = np.nanmedian(logs) #order of magnitude
#    return oom+np.log10(np.sum([10**(l-oom) for l in logs], axis=0))


# --------------------------------------------------------------------
def FileExists(inp, is_dir=False):
    test = Path(inp)
    if is_dir:
        if test.is_dir():
            return True
        else:
            return False
    else:
        if test.is_file():
            return True
        else:
            return False


# --------------------------------------------------------------------
def GetUniques(InputList):
    return [x for n, x in enumerate(InputList) if x not in InputList[:n]]


# -------------------------------------------------------------------
def unique(list1, sort=False, keepsort=False):
    """
    Function to get unique values
    """
    if keepsort:
        seen = set()
        seen_add = seen.add
        return [x for x in list1 if not (x in seen or seen_add(x))]
    else:
        # insert the list to the set
        list_set = set(list1)
        # convert the set to the list
        list_set = list(list_set)
        if sort:
            list_set = sorted(list_set)
    return list_set


# -------------------------------------------------------------------
def printsectionraw(text, level=1):
    l = len(text) + 4
    if level == 1:
        print(
            """
{}
▓ {} ▓
{}

""".format(
                "▓" * l, text.upper(), "▓" * l
            )
        )

    elif level == 2:
        print(
            """
 {}
 ▒ {} ▒
 {}

""".format(
                "▒" * l, text.upper(), "▒" * l
            )
        )

    else:
        print(
            """
  {}
  ░ {} ░
  {}

""".format(
                "░" * l, text.upper(), "░" * l
            )
        )


# -------------------------------------------------------------------
def printsection(text, level=1):
    l = len(text) + 4
    if level == 1:
        logging.info(
            """
{}
▓ {} ▓
{}

""".format(
                "▓" * l, text.upper(), "▓" * l
            )
        )

    elif level == 2:
        logging.info(
            """
 {}
 ▒ {} ▒
 {}

""".format(
                "▒" * l, text.upper(), "▒" * l
            )
        )

    else:
        logging.info(
            """
  {}
  ░ {} ░
  {}

""".format(
                "░" * l, text.upper(), "░" * l
            )
        )


# -------------------------------------------------------------------
def sizeof(l):  # size in bytes
    if type(l) == np.ndarray:
        return l.nbytes
    elif type(l) == dict:
        return sum([sizeof(value) for key, value in l.items()])
    elif type(l) == list:
        return sum([sizeof(value) for value in l])
    else:
        return sys.getsizeof(l)


# -------------------------------------------------------------------
def arr2str(l):
    return "[" + ",".join(np.array(l, dtype="str")) + "]"


# -------------------------------------------------------------------
def panic(msg):
    if type(msg) == str:
        msg = [msg]
    logging.critical("")
    logging.critical("/!\\ /!\\ /!\\ /!\\ ERROR /!\\ /!\\ /!\\ /!\\ /!\\ ")
    logging.critical("")
    for m in msg:
        logging.critical("/!\\ {}".format(m))
    logging.critical("")
    logging.critical("/!\\ /!\\ /!\\ /!\\ ERROR /!\\ /!\\ /!\\ /!\\ /!\\ ")
    logging.critical("")
    raise Exception(msg[0])


# -------------------------------------------------------------------
def inputRL(prompt, prefill=""):
    # readline input
    readline.set_startup_hook(lambda: readline.insert_text(prefill))
    try:
        return input(prompt)
    finally:
        readline.set_startup_hook()


# ------------------------------
def sigareatab():
    sigtab, areatab, fractab = [], [], []
    mean, SD = 0.0, 1.0
    for ns in np.arange(0, 10, 0.01):
        result = norm.cdf(ns * SD, mean, SD) - norm.cdf(-ns * SD, mean, SD)
        result /= 2.0
        sigtab += [
            ns,
        ]
        areatab += [
            result,
        ]
        fractab += [
            (0.5 - result) / (0.5 + result),
        ]
    return sigtab, areatab, fractab


# --------------------------------------


def read_feather_in_chunks(filepath):
    with ipc.RecordBatchFileReader(filepath) as reader:
        for batch_index in range(reader.num_record_batches):
            batch = reader.get_batch(batch_index)
            print(f"Read in batch {batch_index+1} which had {batch.num_rows} rows")
            data_df = batch.to_pandas(
                use_threads=True,
                timestamp_as_object=True,
            )
            yield data_df


# --------------------------------------
def getgitv(root_directory):
    if FileExists(root_directory + "/.git/HEAD"):
        try:
            gitv = (
                subprocess.check_output(
                    [
                        "git",
                        # "--git-dir",
                        # f"{root_directory}/.git",
                        "describe",
                        "--abbrev=7",
                        "--always",
                        "--long",
                        "--match",
                        "v*",
                    ],
                    cwd=root_directory,
                )
                .decode("ascii")
                .strip()
            )

            gitv_remote = (
                subprocess.check_output(
                    [
                        "git",
                        # "--git-dir",
                        # f"{root_directory}/.git",
                        "describe",
                        "--abbrev=7",
                        "--always",
                        "--long",
                        "--match",
                        "v*",
                        "origin/master",
                    ],
                    cwd=root_directory,
                )
                .decode("ascii")
                .strip()
            )
            gitv_date = subprocess.check_output(
                [
                    "git",
                    # "--git-dir",
                    # f"{root_directory}/.git",
                    "show",
                    "-s",
                    "--format=%ci",
                    gitv,
                ],
                cwd=root_directory,
            )
            gitv_date = gitv_date.decode("ascii").replace("\n", "")
            gitv_submodules = (
                subprocess.check_output(
                    [
                        "git",
                        # "--git-dir",
                        # f"{root_directory}/.git",
                        "submodule",
                        "foreach",
                        "git",
                        "rev-parse",
                        "--short",
                        "HEAD",
                    ],
                    cwd=root_directory,
                )
                .decode("ascii")
                .strip()
                .replace("\nEntering ", ", ")
                .replace("Entering ", "")
                .replace("\n", ": ")
            )

            # gitv_submodules = (
            #     subprocess.check_output(
            #         [
            #             "git",
            #             "--git-dir",
            #             f"{root_directory}/.git",
            #             "submodule",
            #             "status",
            #         ]
            #     )
            #     .decode("ascii")
            #     .strip()
            #     .replace("\n", ", ")
            # )

        except:
            gitv, gitv_remote, gitv_date, gitv_submodules = "n/a", "n/a", "n/a", "n/a"
    else:
        gitv, gitv_remote, gitv_date, gitv_submodules = "n/a", "n/a", "n/a", "n/a"

    return gitv, gitv_remote, gitv_date, gitv_submodules


# --------------------------------------
# autocompletion from anywhere
# https://gist.github.com/iamatypeofwalrus/5637895
def inputAC(prompt):
    """
    Prompt input with auto-completion capabilities
    """

    class tabCompleter(object):
        """
        A tab completer that can either complete from
        the filesystem or from a list.

        Partially taken from:
        http://stackoverflow.com/questions/5637124/tab-completion-in-pythons-raw-input
        """

        def pathCompleter(self, text, state):
            """
            This is the tab completer for systems paths.
            Only tested on *nix systems
            """

            text = text.replace("~", str(Path.home()))
            line = readline.get_line_buffer().split()

            res = [x for x in glob.glob(text + "*")][state]
            if os.path.isdir(res):
                res += "/"
            return res

    t = tabCompleter()

    readline.set_completer_delims("\t")
    readline.parse_and_bind("tab: complete")

    readline.set_completer(t.pathCompleter)
    ans = input(prompt)
    return ans


# --------------------------------------
