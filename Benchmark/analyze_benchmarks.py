import pickle
import matplotlib.pyplot as plt
import numpy as np

#===================================================================
#===================================================================
#===================================================================

def main(n_iter=5, outdir='BenchmarkResults', methods=('SMC',), testnames=('1S_rb',), n_samples=[10000], order=[0], normality=[20]):

    for testname in testnames:
        testdir = '{}/{}'.format(outdir, testname)
        print(testdir)
        with open('{}/results.pkl'.format(testdir), 'rb') as f:
            res = pickle.load(f)
            
        #print(res)
        params = ('pnsigma', 'diff_params', 'diff_obs', 'fullruntime', 'maxmem')

        cols = ('red', 'blue', 'green', 'black')

        for ns in n_samples:
            for orde in order:
                for norm in normality:

                    fig, axs = plt.subplots(len(params), 1, figsize=(3, 3*len(params)), sharex=True)
                    
                    for im,method in enumerate(methods):
                        print(method)

                        for ip,param in enumerate(params):
                            print(param)

                            tmp = [res[iiter][ns][orde][method][param] for iiter in range(1, n_iter+1)]
                            if param in ('diff_params', 'diff_obs'):
                                tmp = np.abs(tmp).flatten()
                            axs[ip].scatter([im]*len(tmp), tmp, marker='o', alpha=0.2, facecolors='none', edgecolors=cols[im])
                            axs[ip].errorbar(im, np.mean(tmp), yerr=np.std(tmp), marker='o', color=cols[im], markersize=10, label=method)
                            axs[ip].set_xlabel('Methods')
                            axs[ip].set_xticklabels('')
                            axs[ip].set_ylabel(param)
                            if ip==0:
                                axs[ip].legend()
                            if param in ('pvalue',):
                                axs[ip].axhline(0.5)
                                axs[ip].set_ylabel('low p-value can be due to overfitting')
                            elif param in ('diff_obs', 'diff_params'):
                                axs[ip].axhline(0)
                                axs[ip].set_ylabel('Abs. diff. 50% quantile {} - true'.format(param))

                    fig.tight_layout(h_pad=0.5)
                    plt.subplots_adjust(hspace=0)
                    fig.savefig('{}/results_plot_n{}_o{}_norm{}.pdf'.format(testdir, ns, orde, norm))
                    plt.close(fig)

    #===================================================================
    #===================================================================
    #===================================================================
    
#--------------------------------------------------------------------
if __name__ == "__main__":

    main()
