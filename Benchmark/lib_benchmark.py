# from astropy.io import ascii
import numpy as np
from pathlib import Path
from Library.lib_input import *
from random import sample
from numpy.random import normal
import pandas as pd
from copy import deepcopy

# -------------------------------------------------------------------
def unique(list1, sort=False):
    """
   Function to get unique values 
   """
    # insert the list to the set
    list_set = set(list1)
    # convert the set to the list
    list_set = list(list_set)
    if sort:
        list_set = sorted(list_set)
    return list_set


# -------------------------------------------------------------------


def generate_test(
    resdir,
    testname,
    lines,
    params_name,
    models,
    ws=[1],
    context="mgris_sfgx",
    verbose=True,
    fracoutliers=0,
    sorting="none",
    n_comps_vary=0,
    n_comps_fix=0,
    fixcut=0,
    normality=20,
    order=0,
):

    params = get_params()

    # fluxes = ascii.read('./Contexts/{}/{}'.format(context, filename), format='commented_header', delimiter='\t')
    fluxes = pd.read_feather("./Contexts/{}/Grids/model_grid.fth".format(context))
    if verbose:
        print(fluxes.keys())

    ppvalues = pd.read_feather(
        "./Contexts/{}/Grids/model_grid_post_processing.fth".format(context)
    )
    secondary_parameters = [
        "MHII",
    ]
    if verbose:
        print(ppvalues.keys())

    n_sectors = len(models)

    unc = 0.02

    Params_values = {}
    for p in params_name:
        Params_values.update({p: np.array(unique(fluxes[p], sort=True))})
    if verbose:
        print(Params_values)

    obsfluxes = deepcopy(models)
    pptab = deepcopy(models)
    for im, m in enumerate(models):
        tmp = pd.DataFrame(models[im]).transpose()
        tmp.columns = params_name

        tmp2 = pd.merge(
            tmp.astype(np.float32),
            fluxes.astype(np.float32),
            on=params_name,
            how="outer",
        ).iloc[0]
        obsfluxes[im] = tmp2[lines].values

        tmp2 = pd.merge(
            tmp.astype(np.float32),
            ppvalues.astype(np.float32),
            on=params_name,
            how="outer",
        ).iloc[0]
        pptab[im] = tmp2[secondary_parameters].values

    if verbose:
        print("Original fluxes & pp values")
        print(ws)
        print(obsfluxes)
        print(pptab)

    obsfluxes = np.log10(
        np.sum(
            [ws[i] * 10 ** obsfluxes[i].astype(np.float64) for i in range(n_sectors)],
            axis=0,
        ).astype(np.float64)
    )

    # for now assuming extensive secondary parameters
    pptab = np.log10(
        np.sum(
            [ws[i] * 10 ** pptab[i].astype(np.float64) for i in range(n_sectors)],
            axis=0,
        ).astype(np.float64)
    )

    obsfluxes_orig = deepcopy(obsfluxes)
    if fracoutliers > 0:
        n = int(np.ceil(len(lines) * fracoutliers))
        outliers = sample(set(np.arange(len(lines))), n)
        for im in outliers:
            print("Modifying ", lines[im])
            print(obsfluxes[im])
            obsfluxes[im] += normal(0, 100 * unc)  # 100 sigma
            print(obsfluxes[im])
        if verbose:
            print(obsfluxes_orig)

    if verbose:
        print("Final fluxes & pp values")
        print(obsfluxes)
        print(pptab)

    # eps = 1e-2 #pretty large because we may now know exactly the log value
    # try:
    #    inds = [np.where( np.prod([abs(fluxes[param]-m[i])<eps for i,param in enumerate(params_name)], axis=0) )[0][0] for m in models]
    # except:
    #    breakpoint()
    # if verbose:
    #     print(fluxes[params_name].iloc[inds])
    #     print(fluxes[lines].iloc[inds])

    # obsfluxes = np.log10(np.sum([ws[s]*10**fluxes[lines].iloc[inds[s]] for s in range(n_sectors)], axis=0))
    # for o in obsfluxes:
    #    if random()<fracoutliers:
    #       obsfluxes[o] += normal(0, 100*unc) #100 sigma
    # if verbose:
    #     print(obsfluxes)

    params["context"].value = "./Contexts/{}".format(context)
    params["output"].value = "{}/".format(resdir)

    if sorting == "none":
        params["USE configuration"].value = "1C{}S_benchmark".format(n_sectors)
    else:
        params["USE configuration"].value = "1C{}S_benchmark_sortw".format(n_sectors)

    # make ncomps vary
    if n_comps_vary > 0:
        n_sectors = n_comps_vary
        params["BEGIN configuration"].value = "n_comps_vary"

    if order == -1:
        params["BEGIN configuration"].value += "\ndistrib Z single linear"

    # force nsectors for LOO comparison benchmark?
    if n_comps_fix > 0:
        n_sectors = n_comps_fix

    # params['BEGIN configuration'].value = '''BEGIN configuration
    # age (0) = 0.477 2
    # n (0) = 1 2
    # u (0) = -3 2
    # n (1) = 2 2
    # u (1) = -2 2
    # Lx (0) = -2 2
    # Tx (0) = 6 2
    # Z (0) = -1 2
    # END'''
    params["use_scaling"].value = "'all'"
    params["StudentT_nu"].value = str(normality)
    params["StudentT_nu"].uncomment()
    params[
        "secondary_parameters"
    ].value = "['fesc(1-infty)', 'Q0', 'MH2_total', 'MH2_CO', 'MH2_C', 'MH2_C+', 'MHI', 'MHII']"
    # params['use_scaling'].value = "'none'"
    if "rb" in testname:
        params["select"].value = ["cut [2,2]", "lum [9,9]", "Z_dust [0,0]"]
    else:
        params["select"].value = [
            "cut [0,2]",
            "lum [9,9]",
            "Z_dust [0,0]",
        ]  # 0,2 for cuts because between 2 and 3 we don't know whether we interpolate to Av=10 or not

    # force cut? overwrite
    if "meanprops" in testname:
        if fixcut > 0:
            params["select"].value = [
                "cut [{},{}]".format(fixcut, fixcut),
                "lum [9,9]",
                "Z_dust [0,0]",
            ]
            # this is for the Z test probably
            # params['linterp_params'].value = "['Z',]"
        else:
            params["select"].value = ["cut [0,2.25]", "lum [9,9]", "Z_dust [0,0]"]

    params["BEGIN observations"].value = ""
    for i, l in enumerate(lines):
        params["BEGIN observations"].value += "{} {}\n".format(l, obsfluxes[i])
    params["BEGIN observations"].extras = {
        "delta_ref": unc,
        "scale": "log",
    }

    outfile = "/tmp/input_generated_{}.txt".format(testname.replace("/", "_"))
    make_input(params, outfile)

    return outfile, obsfluxes, obsfluxes_orig, pptab


# --------------------------------------------------------------------
def FileExists(file):
    testfile = Path(file)
    if testfile.is_file():
        return True
    else:
        return False
