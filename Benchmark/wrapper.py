from run_benchmark import main
from process_benchmarks import main as main_process
from analyze_benchmarks import main as main_analyze
import numpy as np

infer, process, analyze = True, True, False

tests = [
    "single",
    # "12S_nsamples",
    #'12S_rec',
    #'3S_nsamples',
    #'12S_rnd_nsamples',
    #'12S_rnd_rec',
    # '12S_nlines3',
    # '12S_nlines5',
    # '12S_nlines7',
    # '12S_nlines9',
    # '12S_nlines11',
    # '12S_nlines13',
    # '12S_ncompsfix',
    # '12S_fewlines_ncompsfix',
    # '12S_fewlines_ncompsvary2',
    # '12S_fewlines_ncompsvary3',
    # '12S_ncompsvary2',
    # '12S_ncompsvary3',
    #'outliers_nu_2_frac_10',
    #'outliers_nu_20_frac_10',
    #'outliers_nu_2_frac_25',
    #'outliers_nu_20_frac_25',
    # 'outliers_nu_20_frac_10',
    # 'outliers_nu_2_frac_10',
    # 'outliers_nu_20_frac_50',
    # 'outliers_nu_2_frac_50',
    # 'fesc_low_h2_ncompsfix',
    # 'fesc_low_hi_ncompsfix',
    # 'fesc_low_h+_ncompsfix',
    # 'fesc_med_h2_ncompsfix',
    # 'fesc_med_hi_ncompsfix',
    # 'fesc_med_h+_ncompsfix',
    # 'fesc_high_h2_ncompsfix',
    # 'fesc_high_hi_ncompsfix',
    # 'fesc_high_h+_ncompsfix',
    #'mean_props_wH',
    #'mean_props_woH',
]


for test in tests:
    order = [0]
    n_samples = [5000]  # theres's no quick/default anymore, at least for now
    normality = 20
    rnd = True
    sorting = "none"
    context = "mgris_sfgx"
    n_comps_vary = 0  # ie False
    fitwith = [0]  # ie False
    fixcut = [0]
    localdir = "/local/home/vleboute/Downloads/"
    outdir = "{}/BenchmarkResults/{}".format(localdir, test)
    methods = ("SMC",)
    fracoutliers = 0

    print("=====================================================================")
    print("")
    print("=====================================================================")
    print("")
    print("=====================================================================")
    print("")
    print("=====================================================================")
    print("")
    print(test)
    print("")
    print("=====================================================================")
    print("")
    print("=====================================================================")
    print("")
    print("=====================================================================")
    print("")
    print("=====================================================================")
    print("")

    # order 0:
    # 10000 is enough for SMC with 9 RVs
    # 10000-50000 for NUTS with 6 RVs (1rb)

    # order 1
    # NUTS>10000 with 6 RVs (1rb)

    # globally NUTS needs at least 50000

    # NUTS needs anything between 10000 and 100000 depending on start value and divergences.
    # Check BOND...: 50000 probably needed for 8 RVs
    # one solution is SMC then NUTS+sfp
    # disc NUTS? --> no
    # SMC ok with o=1 for 1S only

    # ===========================================================================
    # ===========================================================================
    # ===========================================================================
    if test == "single":  # time to get good solution vs n_params ==> test different n

        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        methods = ("NUTS",)
        # methods = ("NUTS", "SMC")
        testnames = [
            "1S_rb",
        ]
        order = [
            0,
            1,
        ]
        n_samples = [
            10000,
        ]
        n_iter = 1
        rnd = False

    # SFGX

    if (
        test == "12S_nsamples"
    ):  # time to get good solution vs n_params ==> test different n

        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        # testnames = ['1S_rb', '1S_mb','2S_rb','2S_mb']
        testnames = [
            "2S_rb",
        ]
        methods = ("SMC", "NUTS")
        order = [0, 1]
        n_samples = [5000, 10000]
        n_iter = 1
        rnd = False

    if test == "12S_rec":  # using recommended values

        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb"]
        order = [0, 1]
        n_samples = [
            None,
        ]
        n_iter = 1
        rnd = False

    if (
        test == "3S_nsamples"
    ):  # time to get good solution vs n_params ==> test different n

        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        testnames = ["3S_rb", "3S_mb"]
        order = [0, 1]
        n_samples = [5000, 10000, 50000]
        n_iter = 1
        rnd = False

    if test == "12S_rnd_nsamples":  # random model sets; testing n samples

        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb"]
        order = [
            0,
            1,
        ]
        n_samples = [
            5000,
            10000,
        ]
        n_iter = 3
        rnd = True

    if test == "12S_rnd_rec":  # random model sets; using recommended values

        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb"]
        order = [
            0,
        ]
        n_samples = [
            None,
        ]
        n_iter = 5
        rnd = True

    if "12S_nlines" in test:  # varying nlines, recommended values
        lines = [
            "C2157.636m",
            "O388.3323m",
            "O163.1679m",
            "N2205.244m",
            "O1145.495m",
            "Ne212.8101m",
            "Ne315.5509m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "H217.0300m",
            "Si234.8046m",
            "Ne514.3228m",
        ]
        sub = int(test.replace("12S_nlines", ""))
        lines = lines[0:sub]
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb"]
        testnames = [t + "_lines{}".format(sub) for t in testnames]
        order = [0, 1]
        n_samples = [
            None,
        ]
        n_iter = 1
        rnd = False

    if "12S_ncompsvary" in test:  # vary ncomps; using recommended values
        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        n_comps_vary = int(
            test[-1]
        )  # will force nS and use n_comps_vary True in configuration block
        outdir = "{}/BenchmarkResults/12S_ncompsvary/".format(localdir)
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb", "3S_rb", "3S_mb"]
        testnames = [t + "_allowmax{}".format(n_comps_vary) for t in testnames]
        order = [
            0,
        ]
        n_samples = [
            None,
        ]
        n_iter = 1
        rnd = False

    if "12S_fewlines_ncompsvary" in test:  # vary ncomps; using recommended values
        lines = ["C2157.636m", "O163.1679m", "N357.3238m"]
        n_comps_vary = int(
            test[-1]
        )  # will force nS and use n_comps_vary True in configuration block
        outdir = "{}/BenchmarkResults/12S_fewlines_ncompsvary/".format(localdir)
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb", "3S_rb", "3S_mb"]
        testnames = [t + "_allowmax{}".format(n_comps_vary) for t in testnames]
        order = [
            0,
        ]
        n_samples = [
            None,
        ]
        n_iter = 1
        rnd = False

    if (
        test == "mean_props_woH"
    ):  # time to get good solution vs n_params ==> test different n
        lines = [
            "O34958.91A",
            "O35006.84A",
            "O16300.30A",
            "N26548.05A",
            "N26583.45A",
            "S26716.44A",
            "S26730.82A",
        ]
        fitwith = [
            1,
            2,
        ]  # will force nS and use n_comps_vary True in configuration block
        testnames = [
            "2S_meanprops_mb",
        ]
        fixcut = [0, 1.25]
        order = [
            0,
        ]
        n_samples = [
            10000,
        ]
        n_iter = 10
        rnd = True

    if (
        test == "mean_props_wH"
    ):  # time to get good solution vs n_params ==> test different n
        lines = [
            "O34958.91A",
            "O35006.84A",
            "O16300.30A",
            "N26548.05A",
            "N26583.45A",
            "S26716.44A",
            "S26730.82A",
            "H14861.33A",
        ]
        fitwith = [
            1,
            2,
        ]  # will force nS and use n_comps_vary True in configuration block
        testnames = [
            "2S_meanprops_mb",
        ]
        fixcut = [0, 1.25]
        order = [
            0,
        ]
        n_samples = [
            10000,
        ]
        n_iter = 10
        rnd = True

    if (
        "12S_ncompsfix" in test
    ):  # fix ncomps for LOO model comparison; using recommended values
        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        fitwith = [
            1,
            2,
            3,
        ]  # will force nS and use n_comps_vary True in configuration block
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb", "3S_rb", "3S_mb"]
        order = [
            0,
        ]
        n_samples = [
            None,
        ]
        n_iter = 1
        rnd = False

    if (
        "12S_fewlines_ncompsfix" in test
    ):  # fix ncomps for LOO model comparison; using recommended values
        lines = ["C2157.636m", "O163.1679m", "N357.3238m"]
        fitwith = [
            1,
            2,
            3,
        ]  # will force n_sectors and use n_comps_vary True in configuration block
        testnames = ["1S_rb", "1S_mb", "2S_rb", "2S_mb", "3S_rb", "3S_mb"]
        order = [
            0,
        ]
        n_samples = [
            None,
        ]
        n_iter = 1
        rnd = False

    if "fesc_" in test:
        if "h2" in test:
            lines = [
                "CO2600.05m",
                "H217.0300m",
                "O1145.495m",
                "O163.1679m",
                "C2157.636m",
                "O388.3323m",
                "N2205.244m",
                "Ne212.8101m",
                "Ne315.5509m",
                "N357.3238m",
                "Ar26.98337m",
                "Ar38.98898m",
            ]
        elif "hi" in test:
            lines = [
                "O1145.495m",
                "O163.1679m",
                "C2157.636m",
                "O388.3323m",
                "N2205.244m",
                "Ne212.8101m",
                "Ne315.5509m",
                "N357.3238m",
                "Ar26.98337m",
                "Ar38.98898m",
            ]
        elif "h+" in test:
            lines = [
                "O388.3323m",
                "N2205.244m",
                "Ne212.8101m",
                "Ne315.5509m",
                "N357.3238m",
                "Ar26.98337m",
                "Ar38.98898m",
            ]
        fitwith = [
            2,
            3,
        ]  # will force nS and use n_comps_vary True in configuration block
        outdir = "{}/BenchmarkResults/fesc/".format(localdir)
        testnames = [
            "3S_mb_{}".format("_".join(test.split("_")[1:3])),
        ]
        order = [
            0,
        ]
        n_samples = [
            10000,
        ]
        n_iter = 1
        rnd = False

    if "outliers" in test:
        lines = [
            "C2157.636m",
            "O163.1679m",
            "N357.3238m",
            "Ar26.98337m",
            "Ar38.98898m",
            "O388.3323m",
            "N2205.244m",
            "H217.0300m",
            "Si234.8046m",
            "Ne212.8101m",
            "Ne315.5509m",
            "O1145.495m",
            "Ne514.3228m",
        ]
        outdir = "{}/BenchmarkResults/outliers/{}".format(localdir, test)
        testnames = [
            "2S_mb",
        ]
        fracoutliers = 0.01 * float(test.split("_")[4])
        normality = float(test.split("_")[2])
        order = [0, -1]
        n_samples = [
            5000,
            10000,
        ]
        n_iter = 1
        rnd = False  # outliers are drawn randomly so we can stick to the fixed parameter set

    # ===========================================================================
    # ===========================================================================
    # ===========================================================================
    # BOND

    # if test==30.01: #min time to get good solution vs n_params ==> test different n

    #     lines = ['H14861.33A', 'H14340.46A', 'H16562.81A', 'O34958.91A', 'O35006.84A', 'O16300.30A', 'N26583.45A', 'S26716.44A', 'S26730.82A', 'S39068.62A', 'Blnd4363.00A', 'Blnd3727.00A']
    #     methods = ('SMC', 'NUTS')
    #     outdir = '/local/home/vleboute/Downloads/BenchmarkResults_BOND'
    #     testnames = ['1S_mb','2S_mb',] #'always use 'mb' for BOND in testnames
    #     order = [0,1]
    #     n_samples = [10000,50000]
    #     n_iter = 1
    #     rnd = False
    #     context = 'BOND' #'always use 'mb' for BOND in testnames

    # if test==30.1: #using recommended values

    #     lines = ['H14861.33A', 'H14340.46A', 'H16562.81A', 'O34958.91A', 'O35006.84A', 'O16300.30A', 'N26583.45A', 'S26716.44A', 'S26730.82A', 'S39068.62A', 'Blnd4363.00A', 'Blnd3727.00A']
    #     methods = ('SMC', 'NUTS')
    #     outdir = '/local/home/vleboute/Downloads/BenchmarkResults_BOND_rec'
    #     testnames = ['1S_mb','2S_mb',] #'always use 'mb' for BOND in testnames
    #     order = [0,1]
    #     n_samples = [None,]
    #     n_iter = 1
    #     rnd = False
    #     context = 'BOND' #'always use 'mb' for BOND in testnames

    # #===========================================================================
    # #===========================================================================
    # #===========================================================================

    # elif test==30.2:
    #     #============================================================================================
    #     #comparing number of lines

    #     lines = ['C2157.636m', 'O163.1679m', 'N357.3238m', 'Ar26.98337m', 'Ar38.98898m', 'O388.3323m']
    #     methods = ('SMC', 'NUTS',)
    #     outdir = '/local/home/vleboute/Downloads/BenchmarkResults_nlines'
    #     n_samples = [10000, 100000]
    #     testnames = ['2S_rb_{}'.format(len(lines)), ]
    #     n_iter = 5

    # elif test==30.1:
    #     #============================================================================================
    #     #comparing number of lines

    #     lines = ['C2157.636m', 'O163.1679m', 'N357.3238m', 'Ar26.98337m', 'Ar38.98898m', 'O388.3323m', 'N2205.244m', 'H217.0300m', 'Si234.8046m', 'Ne212.8101m', 'Ne315.5509m', 'Ne514.3228m', 'O1145.495m']
    #     methods = ('SMC', 'NUTS',)
    #     outdir = '/local/home/vleboute/Downloads/BenchmarkResults_nlines'
    #     n_samples = [10000, 100000]
    #     testnames = ['2S_rb_{}'.format(len(lines)),]
    #     n_iter = 5

    # elif test==6:
    # ============================================================================================
    # testing outlier fraction (outliers with small EB) w norm = 20 or 2?

    # testing unrealistically low EB

    # elif test==7:
    # ============================================================================================
    # 1) chi2 comparison
    # 2) testing with/without upper limits <> chi2

    # test also:
    #  upsample vs no upsample
    #  linear vs nearest for >=2 sectors, upsample or not

    if infer:
        main(
            outdir=outdir,
            n_iter=n_iter,
            methods=methods,
            testnames=testnames,
            n_samples=n_samples,
            order=order,
            rnd=rnd,
            lines=lines,
            sorting=sorting,
            context=context,
            n_comps_vary=n_comps_vary,
            fitwith=fitwith,
            fixcut=fixcut,
            normality=normality,
            fracoutliers=fracoutliers,
        )

    if process and "ncompsfix" not in test and "ncompsvary" not in test:
        main_process(
            outdir=outdir,
            n_iter=n_iter,
            methods=methods,
            testnames=testnames,
            n_samples=n_samples,
            order=order,
        )

    if analyze and "ncompsfix" not in test and "ncompsvary" not in test:
        main_analyze(
            outdir=outdir,
            n_iter=n_iter,
            methods=methods,
            testnames=testnames,
            n_samples=n_samples,
            order=order,
        )
