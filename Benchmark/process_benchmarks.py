import pickle
import matplotlib.pyplot as plt
import numpy as np
import glob
import pickle
import os
import arviz as az
from Library.lib_misc import FileExists
from Library.lib_mc import *
from Library.lib_main import make_grid_wrapper
from tqdm import tqdm
from statsmodels.graphics.boxplots import violinplot

# ===================================================================
# ===================================================================
# ===================================================================


def main(
    n_iter=5,
    outdir="BenchmarkResults",
    methods=("SMC",),
    testnames=("1S_rb",),
    n_samples=[10000],
    order=[0],
):

    print("Process results...")

    for testname in testnames:
        print("Test name: {}".format(testname))
        testdir = "{}/{}".format(outdir, testname)

        benchmark_allresults = {}

        for iiter in range(1, n_iter + 1):
            benchmark_allresults.update({iiter: {}})
            for ns in n_samples:
                benchmark_allresults[iiter].update({ns: {}})
                for orde in order:
                    benchmark_allresults[iiter][ns].update({orde: {}})

                    iterdir = "{}/i{}_n{}_o{}".format(testdir, iiter, ns, orde)

                    plotdir = "{}/Results".format(iterdir)
                    os.makedirs(plotdir, exist_ok=True)
                    os.makedirs(plotdir + "/Details/", exist_ok=True)

                    bm = benchmark_allresults[iiter][ns][orde]

                    for im, method in enumerate(methods):
                        bm.update({method: {}})

                        # where inference results are located
                        fresdir = "{}/{}/".format(iterdir, method)

                        pfile = glob.glob("{}/results_benchmark.pkl".format(fresdir))[0]
                        with open(pfile, "rb") as f:
                            benchmark_results = pickle.load(f)

                        lines = benchmark_results["lines"]
                        fluxes = benchmark_results["fluxes"]
                        ws = benchmark_results["ws"]
                        params_name = benchmark_results["params_name"]
                        models = benchmark_results["models"]

                        if "lum" in params_name:  # SFGX temporary fix
                            models = [
                                [m for i, m in enumerate(m) if params_name[i] != "lum"]
                                for m in models
                            ]
                            params_name = [p for p in params_name if p != "lum"]
                            tmp = np.array(params_name)

                        if "geom" in params_name:  # BOND temporary fix
                            models = [
                                [m for i, m in enumerate(m) if params_name[i] != "geom"]
                                for m in models
                            ]
                            params_name = [p for p in params_name if p != "geom"]
                            tmp = np.array(params_name)
                            tmp[np.array(params_name) == "Z"] = "Zsun"
                            params_name = list(tmp)
                            for i, m in enumerate(models):
                                models[i][
                                    np.where(np.array(params_name) == "Zsun")[0][0]
                                ] = (
                                    models[i][
                                        np.where(np.array(params_name) == "Zsun")[0][0]
                                    ]
                                    + 12
                                    - 8.69
                                )
                            print(params_name, models)

                        n_params = len(params_name)
                        n_sectors = len(models)
                        n_lines = len(lines)

                        # ifile = glob.glob("{}/input.pkl".format(fresdir))[0]
                        # with open(ifile, 'rb') as f:
                        #     inp = pickle.load(f)

                        # sort by decreasing w
                        models = np.array(models)[np.argsort(ws)[::-1]]
                        ws = np.array(ws)[np.argsort(ws)[::-1]]

                        # read resdict
                        with open(fresdir + "/input.pkl", "rb") as f:
                            inp = pickle.load(f)
                        with open(fresdir + "/results_search.pkl", "rb") as f:
                            resdict = pickle.load(f)
                        with open(fresdir + "/results_process.pkl", "rb") as f:
                            resdict2 = pickle.load(f)
                        # with open(fresdir+'/results_post-process.pkl','rb') as f:
                        #    resdict3 = pickle.load(f) #e.g., resdict['MHII']['mean']

                        # merge (once python 3.9 is used, use resdict | resdict2 for union)
                        resdict = {**resdict, **resdict2}  # , **resdict3}

                        grid = make_grid_wrapper(inp)
                        inp["grid"] = grid

                        # post_processing_file, commoncolumn = inp['post_processing_file']
                        # grid = make_grid_wrapper(inp, verbose=verbose, add=[commoncolumn,]+obs_to_predict)
                        # grid.mergewith(directory_models+post_processing_file, commoncolumn, add=params_to_predict, inp=inp)
                        # grid.checkconversions(tolog=tolog, checkinf=False)
                        # grid.checkallnans(obs_to_predict+params_to_predict, checkinf=False, verbose=verbose)
                        # grid.values = grid.values[obs_to_predict+params_to_predict+grid.params.names]

                        # get function
                        if benchmark_results["order"] == 0:
                            interp_f = nearest_f
                        else:
                            # interp_f = make_interp_f_expand(n_params)
                            interp_f = make_interp_f(inp)

                        print("==================================")
                        print("==================================")
                        print("PROCESS")
                        print(iterdir)
                        print(method)
                        print(models)
                        print(ws)
                        print("==================================")
                        print("==================================")

                        # remove cut from params_name for rb models
                        if "rb" in testname:
                            params_name = [p for p in params_name if "cut" not in p]
                            n_params = len(params_name)
                            models = [m[0:-1] for m in models]

                        params_name = [p for p in params_name if "lum" not in p]

                        # ================================================================
                        # time
                        stime = float(resdict["time_sampling"]) / 60.0

                        itime = float(resdict["time_inf"].total_seconds()) / 60.0

                        n = os.popen(
                            'grep "Duration" {}/output_search.txt  | tr -dc "0-9"'.format(
                                fresdir
                            )
                        ).read()
                        try:
                            searchtime = float(n[:-1] + "." + n[-1])
                        except:
                            searchtime = np.nan

                        n = os.popen(
                            'grep "Duration" {}/output_process.txt  | tr -dc "0-9"'.format(
                                fresdir
                            )
                        ).read()
                        try:
                            processtime = float(n[:-1] + "." + n[-1])
                        except:
                            processtime = np.nan

                        bm[method].update({"stime": stime})
                        bm[method].update({"itime": itime})
                        bm[method].update({"searchtime": searchtime})
                        bm[method].update({"processtime": processtime})
                        bm[method].update({"fullruntime": searchtime + processtime})

                        # ================================================================
                        # memory
                        memf = np.genfromtxt(
                            "{}/memory-profile-search.dat".format(fresdir),
                            skip_header=1,
                        )
                        maxmem = np.max([float(t[1]) for t in memf])

                        memf = np.genfromtxt(
                            "{}/memory-profile-process.dat".format(fresdir),
                            skip_header=1,
                        )
                        maxmemp = np.max([float(t[1]) for t in memf])

                        bm[method].update({"maxmem": maxmem})
                        bm[method].update({"maxmemp": maxmemp})

                        # ================================================================
                        # ================================================================
                        # indiv method plots
                        # ================================================================
                        # ================================================================

                        # ================================================================
                        # time + mem

                        fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                        ax.plot(
                            maxmem * 0.001,
                            searchtime + processtime,
                            "o",
                            label="Full run",
                            color="red",
                        )
                        ax.plot(
                            maxmem * 0.001,
                            searchtime,
                            "o",
                            label="Search",
                            color="blue",
                        )
                        ax.plot(
                            maxmem * 0.001, itime, "o", label="Inference", color="green"
                        )
                        ax.plot(
                            maxmem * 0.001, stime, "o", label="Sampling", color="black"
                        )
                        # ax.plot(maxmemp*0.001, processtime, 'o', label='Process program')
                        ax.set_xlabel("Memory [Gb]")
                        ax.set_ylabel("Time [min.]")
                        ax.legend()
                        fig.savefig(
                            "{}/Details/{}_time_memory.pdf".format(plotdir, method)
                        )
                        plt.close(fig)

                        # ================================================================
                        # ESS & RHAT

                        # For SMC the value of ESS seems to be overly optimistic when the model runs OK, but is very useful when the model has problems. i.e. if ESS is OK it does not mean too much, but if the ESS is low, then most likely than not SMC had troubles and you can not trust its results.
                        # ESSs < 100 is not so good but this may be liberal and > 200 would be better. On the other hand chasing ESSs > 10000 may be a waste of computational resources

                        trace = az.from_netcdf(
                            fresdir + "/trace_process.netCDF"
                        )  # post process contains primary and seconday parameters
                        # trace = az.from_netcdf(fresdir+'/trace_post-process.netCDF') #post process contains primary and seconday parameters

                        params_name = [
                            p
                            for p in params_name
                            if "idx_{}_0".format(p) in trace.posterior.keys()
                        ]
                        ess = [
                            float(
                                az.ess(trace.posterior, method="median")
                                .data_vars["idx_{}_{}".format(p, s)]
                                .data
                            )
                            for p in params_name
                            for s in range(n_sectors)
                        ]
                        if n_sectors > 1:
                            ess += [
                                float(
                                    az.ess(trace.posterior, method="median")
                                    .data_vars["w"]
                                    .data[s]
                                )
                                for s in range(n_sectors)
                            ]

                        rhat = [
                            float(
                                az.rhat(trace.posterior)
                                .data_vars["idx_{}_{}".format(p, s)]
                                .data
                            )
                            for p in params_name
                            for s in range(n_sectors)
                        ]
                        if n_sectors > 1:
                            rhat += [
                                float(az.rhat(trace.posterior).data_vars["w"].data[s])
                                for s in range(n_sectors)
                            ]

                        df = pd.DataFrame(
                            {"ESS": np.array(ess), "RHAT": np.array(rhat)}
                        )

                        sns.jointplot(
                            data=df, x="RHAT", y="ESS"
                        )  # , marker="o", marginal_kws=dict(fill=False))
                        plt.savefig(
                            "{}/Details/{}_ess_rhat.pdf".format(plotdir, method)
                        )

                        bm[method].update({"ESS": ess})
                        bm[method].update({"RHAT": rhat})

                        # ================================================================
                        # DIFF (params)

                        # bm[method].update({'diff_params': []})
                        if n_sectors > 1:
                            params_name2 = params_name + [
                                "w",
                            ]
                            models2 = [
                                np.concatenate((m, [ws[i],]))
                                for i, m in enumerate(models)
                            ]
                        else:
                            params_name2 = params_name
                            models2 = models

                        trace2 = deepcopy(trace)
                        for ip, p in enumerate(params_name2):
                            for s in np.arange(n_sectors):
                                print(
                                    p,
                                    s,
                                    models2[s][ip],
                                    float(
                                        trace2.posterior["{}_{}".format(p, s)].mean()
                                    ),
                                )
                                trace2.posterior["{}_{}".format(p, s)] -= models2[s][ip]
                                if p != "w":
                                    trace2.posterior["{}_{}".format(p, s)] /= np.max(
                                        grid.params.values[p]
                                    ) - np.min(
                                        grid.params.values[p]
                                    )  # it's 1 for w...
                                # bm[method]['diff_params'] += [float(trace2.posterior['{}_0{}'.format(p,s)].median()),]

                        fig = az.plot_violin(
                            trace2, ["{}_0".format(p) for p in params_name2]
                        )
                        for s in np.arange(1, n_sectors):
                            fig = az.plot_violin(
                                trace2,
                                var_names=["{}_{}".format(p, s) for p in params_name2],
                                ax=fig,
                            )
                        for f in fig.flatten():
                            f.axhline(0, color="black", alpha=0.2)
                        fig[0, 0].get_figure().savefig(
                            "{}/Details/{}_params.pdf".format(plotdir, method)
                        )
                        plt.close("all")

                        # fig = az.plot_violin(trace2, ['{}_00'.format(params_name2[0])])
                        # for ip, p in enumerate(params_name2):
                        #     for s in np.arange(n_sectors):
                        #         tmp = az.plot_violin(trace2, var_names='{}_0{}'.format(p,s), ax=fig)
                        # for f in fig.flatten():
                        #     f.axhline(0, color='black')
                        # fig[0].autoscale()
                        # fig[0].get_figure().savefig("{}/{}_all_params0.pdf".format(plotdir, method))
                        # plt.close('all')

                        # combine all diffs
                        trace2 = combine_vars(
                            trace2,
                            [
                                "{}_{}".format(p, s)
                                for p in params_name2
                                for s in range(n_sectors)
                            ],
                            "allparams",
                        )
                        bm[method].update({"allparams": trace2})
                        fig = az.plot_violin(trace2)
                        for f in fig.flatten():
                            f.axhline(0, color="black", alpha=0.2)
                        fig[0].autoscale()
                        fig[0].get_figure().savefig(
                            "{}/Details/{}_all_params.pdf".format(plotdir, method)
                        )
                        plt.close("all")

                        del trace2

                        # ================================================================
                        # DIFF (fluxes)

                        # bm[method].update({'diff_obs': []})
                        trace2 = deepcopy(trace)
                        for ip, p in enumerate(lines):
                            if p not in trace2.posterior.keys():
                                continue  # happens if obs = inf or nan
                            trace2.posterior[p] -= fluxes[ip]
                            # bm[method]['diff_obs'] += [trace2.posterior[p].median()-fluxes[ip],]

                        fig = az.plot_violin(
                            trace2,
                            var_names=[
                                l for l in lines if l in trace2.posterior.keys()
                            ],
                        )
                        for f in fig.flatten():
                            f.axhline(0, color="black", alpha=0.2)
                        fig.flatten()[0].get_figure().savefig(
                            "{}/Details/{}_obs.pdf".format(plotdir, method)
                        )
                        plt.close("all")

                        trace2 = combine_vars(trace2, lines, "allobs")
                        bm[method].update({"allobs": trace2})
                        fig = az.plot_violin(trace2)
                        for f in fig.flatten():
                            f.axhline(0, color="black", alpha=0.2)
                        fig.flatten()[0].get_figure().savefig(
                            "{}/Details/{}_all_obs.pdf".format(plotdir, method)
                        )
                        plt.close("all")
                        del trace2

                        # ================================================================
                        # pnsigma + WAIC

                        fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                        ax.plot(
                            2 * [resdict["pnsigma"][2] * 100],
                            [resdict["loo_elpd"], resdict["waic_elpd"]],
                            "o-",
                        )
                        # ax.plot(maxmemp*0.001, processtime, 'o', label='Process program')
                        ax.set_xlabel("% draws within 3 sigma")
                        ax.set_ylabel("LOO/WAIC")
                        # ax.legend()
                        fig.savefig(
                            "{}/Details/{}_pnsigma_loowaic.pdf".format(plotdir, method)
                        )
                        plt.close(fig)

                        bm[method].update({"pnsigma": resdict["pnsigma"]})
                        bm[method].update({"waic": resdict["waic_elpd"]})
                        bm[method].update({"loo": resdict["loo_elpd"]})

                        # ================================================================
                        # ================================================================

                        benchmark_allresults[iiter][ns][orde][method] = bm[method]

                        # ================================================================
                        # ================================================================

                    # ================================================================
                    # ================================================================
                    # ================================================================
                    # global plots

                    cols = {"SMC": "red", "NUTS": "blue", "DEM": "green", "M": "black"}

                    # ================================================================
                    # breakpoint()
                    df = pd.DataFrame(
                        {
                            "method": np.array(
                                [[m] * len(bm[m]["ESS"]) for m in methods]
                            ).flatten(),
                            "ESS": np.array([bm[m]["ESS"] for m in methods]).flatten(),
                            "RHAT": np.array(
                                [bm[m]["RHAT"] for m in methods]
                            ).flatten(),
                        }
                    )
                    sns.jointplot(
                        data=df, x="RHAT", y="ESS", hue="method"
                    )  # , marker="o", marginal_kws=dict(fill=False))
                    plt.savefig("{}/ess_rhat.pdf".format(plotdir))

                    # ================================================================

                    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                    ax.plot(
                        [bm[m]["maxmem"] * 0.001 for m in methods],
                        [bm[m]["searchtime"] + bm[m]["processtime"] for m in methods],
                        "o-",
                        label="Full run",
                        color="red",
                    )
                    ax.plot(
                        [bm[m]["maxmem"] * 0.001 for m in methods],
                        [bm[m]["searchtime"] for m in methods],
                        "o-",
                        label="Search",
                        color="blue",
                    )
                    ax.plot(
                        [bm[m]["maxmem"] * 0.001 for m in methods],
                        [bm[m]["itime"] for m in methods],
                        "o-",
                        label="Inference",
                        color="green",
                    )
                    ax.plot(
                        [bm[m]["maxmem"] * 0.001 for m in methods],
                        [bm[m]["stime"] for m in methods],
                        "o-",
                        label="Sampling",
                        color="black",
                    )

                    for m in methods:
                        ax.text(bm[m]["maxmem"] * 0.001, bm[m]["stime"], m)

                    ax.set_xlabel("Memory [Gb]")
                    ax.set_ylabel("Time [min.]")
                    ax.legend()
                    fig.savefig("{}/time_memory.pdf".format(plotdir))
                    plt.close(fig)

                    # ================================================================

                    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                    for m in methods:
                        ax.plot(
                            2 * [bm[m]["pnsigma"][2] * 100],
                            [bm[m]["loo"], bm[m]["waic"]],
                            "o-",
                            color=cols[m],
                            label=m,
                        )
                    ax.set_xlabel("% draws within 3 sigma")
                    ax.set_ylabel("LOO/WAIC")
                    ax.legend()
                    fig.savefig("{}/pnsigma_loowaic.pdf".format(plotdir))
                    plt.close(fig)

                    # ================================================================

                    fig, ax = plt.subplots()

                    if "SMC" in bm.keys():
                        data = (
                            bm["SMC"]["allparams"]
                            .posterior["allparams"]
                            .values.flatten()
                        )
                        data = data[np.isfinite(data)]
                        violinplot(
                            [data],
                            positions=[0],
                            show_boxplot=False,
                            side="left",
                            ax=ax,
                            plot_opts={"violin_fc": "C0"},
                            labels=["SMC/NUTS"],
                        )

                    if "NUTS" in bm.keys():
                        data = (
                            bm["NUTS"]["allparams"]
                            .posterior["allparams"]
                            .values.flatten()
                        )
                        data = data[np.isfinite(data)]
                        violinplot(
                            [data],
                            positions=[0],
                            show_boxplot=False,
                            side="right",
                            ax=ax,
                            plot_opts={"violin_fc": "C1"},
                        )

                    ax.axhline(0, color="black", alpha=0.2)
                    fig.savefig("{}/all_params.pdf".format(plotdir))
                    plt.close(fig)

                    fig, ax = plt.subplots()

                    if "SMC" in bm.keys():
                        data = bm["SMC"]["allobs"].posterior["allobs"].values.flatten()
                        data = data[np.isfinite(data)]
                        violinplot(
                            [data],
                            positions=[0],
                            show_boxplot=False,
                            side="left",
                            ax=ax,
                            plot_opts={"violin_fc": "C0"},
                            labels=["SMC/NUTS"],
                        )

                    if "NUTS" in bm.keys():
                        data = bm["NUTS"]["allobs"].posterior["allobs"].values.flatten()
                        data = data[np.isfinite(data)]
                        violinplot(
                            [data],
                            positions=[0],
                            show_boxplot=False,
                            side="right",
                            ax=ax,
                            plot_opts={"violin_fc": "C1"},
                        )

                    ax.axhline(0, color="black", alpha=0.2)
                    fig.savefig("{}/all_obs.pdf".format(plotdir))
                    plt.close(fig)

                    # fig, axs = plt.subplots(1, 2, figsize=(5, 5))
                    # for i,m in enumerate(methods):
                    #     axs[0].plot([i+1]*len(bm[m]['diff_params']), bm[m]['diff_params'], marker=0, color=cols[m], label=m)
                    #     axs[1].plot([i+1]*len(bm[m]['diff_obs']), bm[m]['diff_obs'], marker=0, color=cols[m], label=m)

                    # axs[0].set_ylabel('Delta params')
                    # axs[0].legend()

                    # axs[1].set_ylabel('Delta obs')

                    # for ax in axs:
                    #     ax.set_xlabel('Method')
                    #     ax.set_xlim([0.5, i+1.5])
                    #     ax.axhline(0, color='black')
                    #     ax.set_xticklabels([])

                    # fig.tight_layout(h_pad=2)
                    # fig.savefig('{}/params_obs.pdf'.format(plotdir))
                    # plt.close(fig)

                    # ================================================================

                    # convert to PNG
                    # os.system("gs -dBATCH -dNOPAUSE -sDEVICE=png16m -r300 -dUseCropBox -sOutputFile='{}/benchmark.png' '{}/benchmark.pdf'".format(plotdir, plotdir))

        with open("{}/results.pkl".format(testdir), "wb") as f:
            pickle.dump(benchmark_allresults, f, protocol=4)

    # ===================================================================
    # ===================================================================
    # ===================================================================


# --------------------------------------------------------------------
if __name__ == "__main__":

    main()
