import sys
sys.path.append('../') #to get make_input from the parent directory
from mgris_compare import main, args

#df = '/local/home/vleboute/Downloads/BenchmarkResults_ncompsfix/'
df = '/local/home/vleboute/Downloads/BenchmarkResults/12S_ncompsfix/'

dv = df.replace('ncompsfix', 'ncompsvary')

for test in ('1S_rb', '2S_rb','1S_mb','2S_mb','3S_rb','3S_mb'): #,'3S_mb',):
#for test in ('3S_mb',):
    dirs = [df + '/{}/i1_nNone_o0_fitwith{}/SMC'.format(test, i) for i in (1, 2, 3)]
    dirs += [dv + '/{}_allowmax{}/i1_nNone_o0/SMC'.format(test, i) for i in (2, 3)]
    main(args=args(ncomps=True,
                   ncompsexpected=int(test[0]),
                   result_directories=dirs,
                   output=test))
