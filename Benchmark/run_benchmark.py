# /!\ /!\ /!\ /!\ /!\ /!\
# run as:
# runlow python Benchmark/run_benchmark.py
# /!\ /!\ /!\ /!\ /!\ /!\

import os
import numpy as np
from lib_benchmark import generate_test

# import arviz as az
# import matplotlib.pyplot as plt
import pickle  # dill as pickle
from copy import deepcopy
import pandas as pd
from random import randint
from Library.lib_mc import FileExists
from numpy.random import random

# ===================================================================
# ===================================================================
# ===================================================================


def main(
    n_iter=10,
    methods=("SMC",),
    testnames=("1S_rb",),
    outdir="BenchmarkResults",
    lines=[
        "C2157.636m",
        "O163.1679m",
        "N357.3238m",
        "Ar26.98337m",
        "Ar38.98898m",
        "O388.3323m",
    ],
    n_samples=[10000,],
    order=[0,],
    fracoutliers=0,
    rnd=True,
    sorting="none",
    context="SFGX",
    n_comps_vary=0,
    fitwith=[0,],
    fixcut=[0],
    normality=20,
):

    reuse = False

    # bresdir = '{}/Results/'.format(outdir)
    # os.makedirs(bresdir, exist_ok=True)

    if context == "mgris_sfgx":
        params_name = ["age", "n", "u", "Z", "Lx", "Tx", "cut", "lum", "Z_dust"]
        samep = ("age", "Z", "Lx", "Tx", "lum")  # if same environment for cluster
    else:
        params_name = ["U", "age", "geom", "NO", "hbfrac", "Z"]
        samep = ("age", "NO", "Z")  # if same environment for cluster

    params_ref = deepcopy(params_name)

    for testname in testnames:
        print("test = ", testname)

        testdir = "{}/{}".format(outdir, testname)
        os.makedirs(testdir, exist_ok=True)

        for iiter in range(1, n_iter + 1):
            print("iter = ", iiter)

            # models are the same for each iteration, method, n_samples

            # ---------------------------
            if "1S" in testname:
                n_sectors = 1
            elif "2S" in testname:
                n_sectors = 2
            else:
                n_sectors = 3

            # ================================================================
            n_params = len(params_name)
            n_lines = len(lines)

            #    age, n, u, z, Lx, Tx(, cut)

            tab = pd.read_feather("Contexts/{}/Grids/model_grid.fth".format(context))

            # # ===================
            # # ##bimodality/degeneracy test
            # print("!!!!!!!! BIMODALITY TEST ON !!!!!!!!!!!!!!!")
            # ##duplicate
            # # d = {
            # #     "age": [[0.699, 0], [0.778, 0.301], [0.903, 0.477], [1, 0.602]],
            # #     "Z": [[-0.668, -2.19], [-0.491, -1.899], [-0.19, -1.19], [0.111, -0.889]],
            # #     "n": [[3, 0], [4, 1]],
            # #     "u": [[-3, 0], [-4, -1]],
            # #     "Lx": [[-2, -40], [-1, -3]],
            # #     "Tx": [[6, 0], [7, 5]],
            # # }
            # ## mirror
            # # d = {
            # #     "age": [[0, 1], [0.903, 0.301], [0.778, 0.477], [0.699, 0.602],],
            # #     "u": [[0, -4], [-1, -3]],
            # #     "n": [[0, 4], [1, 3]],
            # #     "Z": [[0.111, -2.19], [-0.19, -1.889], [-0.491, -1.19]],
            # #     "Lx": [[-1, -40], [-2, -3]],
            # #     "Tx": [[7, 0], [6, 5]],
            # # }
            # d = {
            #     "Z": [[-0.668, -1.19]],
            # }
            # for dd in d:
            #     for vals in d[dd]:
            #         ttt = pd.merge(
            #             tab.loc[tab[dd] == vals[0]],
            #             tab.loc[tab[dd] == vals[1]],
            #             how="left",
            #             on=[g for g in params_name if g != dd],
            #             suffixes=("_x", None),
            #         ).set_index(tab.loc[tab[dd] == vals[0]].index)
            #         for o in lines:  # works per column only dont know why
            #             tab[o].loc[tab[dd] == vals[0]] = ttt[o]
            # # ===================

            # remove edges...?
            if rnd:
                for p in params_name:
                    if p in (
                        "geom",
                        "lum",
                        "Z_dust",
                    ):  # only two/three values for these
                        continue
                    tab = tab.loc[(tab[p] > np.min(tab[p])) & (tab[p] < np.max(tab[p]))]

            # remove rows with nan fluxes
            # tab = tab.replace([np.inf, -np.inf], np.nan) #first replace infs with nans
            tab = tab[~tab[lines].isna().any(axis=1)]  # then drop nans

            if context == "mgris_sfgx":
                if "meanprops" in testname:
                    tab = tab[params_name].loc[
                        (tab["cut"] <= 1.25) & (tab["lum"] == 9) & (tab["Z_dust"] == 0)
                    ]
                else:
                    if "mb" in testname:
                        tab = tab[params_name].loc[
                            (tab["cut"] <= 2) & (tab["lum"] == 9) & (tab["Z_dust"] == 0)
                        ]
                    else:
                        tab = tab[params_name].loc[
                            (tab["cut"] == 2) & (tab["lum"] == 9) & (tab["Z_dust"] == 0)
                        ]
            elif context == " BOND":
                tab = tab[params_name].loc[tab["geom"] == 0.03]

            if n_sectors == 1:
                if "mb" in testname:
                    if rnd:
                        # random
                        models = [
                            tab[params_name].iloc[randint(0, len(tab) - 1)].values
                        ]
                    else:
                        # fixed
                        if context == "BOND":
                            models = [[-2.75, 0.602, 0.03, -1.19, 0.8, -3.2]]
                        else:
                            models = [[0.477, 2, -2, -1.19, -2, 6, 1, 9, 0]]
                else:  # rb, SFGX only
                    if rnd:
                        # random
                        models = [
                            tab[params_name].iloc[randint(0, len(tab) - 1)].values
                        ]
                    else:
                        # fixed
                        models = [
                            [0.477, 2, -2, -1.19, -2, 6, 2, 9, 0],
                        ]
                ws = [
                    1,
                ]

            elif n_sectors == 2:
                if "mb" in testname:
                    if rnd:
                        # choose rnd the environment
                        for p in samep:  # same environment and cluster
                            tmp = list(set(tab[p]))
                            tab = tab.loc[tab[p] == tmp[randint(0, len(tmp) - 1)]]

                        same = True
                        while same:
                            models = [
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                            ]
                            same = np.prod(models[0] == models[1])
                    else:
                        # fixed
                        if context == "BOND":
                            models = [
                                [-3.0, 0.477, 0.03, -1.19, 0.8, -3.2],
                                [-1.25, 0.477, 0.03, -1.19, 0.2, -3.2],
                            ]
                        else:
                            models = [
                                [0.477, 1, -3, -1.19, -2, 6, 0.5, 9, 0],
                                [0.477, 2, -2, -1.19, -2, 6, 1.5, 9, 0],
                            ]
                else:  # rb, SFGX only
                    if rnd:
                        # choose rnd the environment
                        for p in samep:  # same environment and cluster
                            tmp = list(set(tab[p]))
                            tab = tab.loc[tab[p] == tmp[randint(0, len(tmp) - 1)]]

                        same = True
                        while same:
                            models = [
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                            ]
                            same = np.prod(models[0] == models[1])
                    else:
                        # fixed
                        models = [
                            [0.602, 2, -3, -1.19, -2, 6, 2, 9, 0],
                            [0.602, 3, -1, -1.19, -2, 6, 2, 9, 0],
                        ]
                        # models = [[0.602, 2, -3, -1.19, -2, 6, 2, 9, 0],
                        #          [0.477, 3, -1, -1.19, -2, 6, 2, 9, 0]]
                if rnd:
                    # random
                    tmp = random()
                    ws = [tmp, 1 - tmp]
                else:
                    # fixed
                    ws = [0.35, 0.65]

                # force
                # ws = [0.52, 0.48]
                # models = [[0.845, 2, -2, -0.478, -2.301, 6, 2],
                #          [0.845, 2, -1, -0.478, -2.301, 6, 2]]
                # ws = [0.9, 0.1]

            elif n_sectors == 3:
                if "mb" in testname:
                    if rnd:
                        # choose rnd the environment
                        for p in samep:  # same environment and cluster
                            tmp = list(set(tab[p]))
                            tab = tab.loc[tab[p] == tmp[randint(0, len(tmp) - 1)]]

                        same = True
                        while same:
                            models = [
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                            ]
                            same = (
                                np.prod(models[0] == models[1])
                                or np.prod(models[1] == models[2])
                                or np.prod(models[0] == models[2])
                            )
                    else:
                        # fixed
                        if "_low" in testname:
                            models = [
                                [0.477, 1, -4, -1.19, -2, 6, 0.75, 9, 0],
                                [0.477, 2, -2, -1.19, -2, 6, 1, 9, 0],
                                [0.477, 2, -1, -1.19, -2, 6, 2, 9, 0],
                            ]
                        elif "_med" in testname:
                            models = [
                                [0.477, 1, -4, -1.19, -2, 6, 0.5, 9, 0],
                                [0.477, 2, -2, -1.19, -2, 6, 0.75, 9, 0],
                                [0.477, 2, -1, -1.19, -2, 6, 2, 9, 0],
                            ]
                        elif "_high" in testname:
                            models = [
                                [0.477, 1, -4, -1.19, -2, 6, 0.25, 9, 0],
                                [0.477, 2, -2, -1.19, -2, 6, 0.5, 9, 0],
                                [0.477, 2, -1, -1.19, -2, 6, 2, 9, 0],
                            ]
                        else:
                            models = [
                                [0.477, 1, -4, -1.19, -2, 6, 1, 9, 0],
                                [0.477, 2, -2, -1.19, -2, 6, 1.5, 9, 0],
                                [0.477, 2, -1, -1.19, -2, 6, 2, 9, 0],
                            ]
                else:  # rb, SFGX only
                    if rnd:
                        # random
                        for p in samep:  # same environment and cluster
                            tmp = list(set(tab[p]))
                            tab = tab.loc[tab[p] == tmp[randint(0, len(tmp) - 1)]]

                        same = True
                        while same:
                            models = [
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                                tab[params_name].iloc[randint(0, len(tab) - 1)].values,
                            ]
                            same = (
                                np.prod(models[0] == models[1])
                                or np.prod(models[1] == models[2])
                                or np.prod(models[0] == models[2])
                            )
                    else:
                        # fixed
                        models = [
                            [0.477, 1, -4, -1.19, -2, 6, 2, 9, 0],
                            [0.477, 2, -2, -1.19, -2, 6, 2, 9, 0],
                            [0.477, 2, -1, -1.19, -2, 6, 2, 9, 0],
                        ]
                if rnd:
                    # random
                    tmp = [random(), random(), random()]
                    ws = np.array(tmp) / sum(tmp)
                else:
                    # fixed
                    if "_low" in testname:
                        ws = [0.3, 0.6, 0.1]
                    elif "_med" in testname:
                        ws = [0.3, 0.6, 0.1]
                    elif "_high" in testname:
                        ws = [0.3, 0.6, 0.1]
                    else:
                        ws = [0.5, 0.3, 0.2]

                # force
                # ws = [0.5, 0.3, 0.2]

            models_ref = deepcopy(models)

            # ================================================================
            # ================================================================
            # ================================================================
            # ================================================================
            # for each model values we compare n_samples and methods

            # ================================================================
            for orde in order:
                print("order = ", orde)

                ostr = ""
                if orde is not None:
                    if orde >= 0:  # if <0 we do only Z
                        ostr = "-o {}".format(orde)

                # ================================================================
                for ns in n_samples:
                    print("n_samples = ", ns)

                    # ================================================================
                    for n_comps_fix in fitwith:
                        print("n_comps_fix = ", n_comps_fix)
                        ncomps_str = (
                            ""
                            if (len(fitwith) == 1)
                            and (n_comps_fix == 0 or n_comps_fix == n_sectors)
                            else "_fitwith{}".format(n_comps_fix)
                        )

                        # ================================================================
                        for cut in fixcut:
                            print("fixcut = ", cut)
                            cut_str = (
                                ""
                                if (len(fixcut) == 1) and (cut == 0)
                                else "_fixcut{}".format(cut)
                            )

                            testroot = "{}/i{}_n{}_o{}{}{}".format(
                                testdir, iiter, ns, orde, ncomps_str, cut_str
                            )

                            if ns is not None:
                                nsamplesstr = "-n {}".format(ns)
                            else:
                                nsamplesstr = " "

                            # run methods for given rnd/set obs
                            # ================================================================
                            for im, method in enumerate(methods):
                                print("method = ", method)
                                # plotdir = './{}/{}/Plots'.format(testroot, testname)
                                resdir = "{}/{}".format(testroot, method)
                                os.makedirs(resdir, exist_ok=True)

                                # reinit because we remove cut for rb models
                                params_name = params_ref
                                n_params = len(params_name)
                                models = models_ref

                                # ================================================================
                                # ================================================================
                                # pip install memory_profiler
                                if FileExists(resdir + "/trace.netCDF") and not reuse:
                                    print("Skipping inference...")
                                    with open(
                                        "{}/results_benchmark.pkl".format(resdir), "rb"
                                    ) as f:
                                        benchmark_results = pickle.load(f)
                                    inputfile = benchmark_results["inputfile"]

                                else:
                                    print("Running inference...")
                                    print("==================================")
                                    print("==================================")
                                    print("INFER")
                                    print(models)
                                    print(ws)
                                    print(iiter)
                                    print("==================================")
                                    print("==================================")

                                    if method == "SMC":
                                        os.system("rm /tmp/trace*.pkl")

                                    if reuse:
                                        print("Re-using old input file")
                                        with open(
                                            "{}/results_benchmark.pkl".format(resdir),
                                            "rb",
                                        ) as f:
                                            benchmark_results = pickle.load(f)
                                        inputfile = "{}/input.txt".format(resdir)
                                    else:
                                        # generate input files
                                        (
                                            inputfile,
                                            fluxes,
                                            fluxes_orig,
                                            pptab,
                                        ) = generate_test(
                                            resdir,
                                            testname,
                                            lines,
                                            params_name,
                                            models,
                                            ws=ws,
                                            context=context,
                                            fracoutliers=fracoutliers,
                                            sorting=sorting,
                                            n_comps_vary=n_comps_vary,
                                            n_comps_fix=n_comps_fix,
                                            fixcut=cut,
                                            normality=normality,
                                            order=orde,
                                        )

                                        with open(
                                            resdir + "/real_values.dat", "w"
                                        ) as f:
                                            f.write(pd.DataFrame(models).to_string())
                                            f.write(pd.DataFrame(ws).to_string())

                                        # write results
                                        benchmark_results = {}
                                        benchmark_results.update({"testname": testname})
                                        benchmark_results.update({"iiter": iiter})
                                        benchmark_results.update({"method": method})
                                        benchmark_results.update(
                                            {"inputfile": inputfile}
                                        )
                                        benchmark_results.update({"ws": ws})
                                        benchmark_results.update({"fluxes": fluxes})
                                        benchmark_results.update(
                                            {"fluxes_orig": fluxes_orig}
                                        )
                                        benchmark_results.update({"pptab": pptab})
                                        benchmark_results.update({"models": models})
                                        benchmark_results.update(
                                            {"params_name": params_name}
                                        )
                                        benchmark_results.update({"lines": lines})
                                        benchmark_results.update(
                                            {"fracoutliers": fracoutliers}
                                        )
                                        benchmark_results.update({"n_samples": ns})
                                        benchmark_results.update({"order": orde})
                                        benchmark_results.update(
                                            {"n_comps_fix": n_comps_fix}
                                        )
                                        benchmark_results.update({"fixcut": cut})

                                        with open(
                                            "{}/results_benchmark.pkl".format(resdir),
                                            "wb",
                                        ) as f:
                                            pickle.dump(
                                                benchmark_results, f, protocol=4
                                            )

                                    # ================================================================
                                    print(inputfile)
                                    memfile = "{}/memory-profile-search.dat".format(
                                        resdir
                                    )
                                    if FileExists(memfile):
                                        os.remove(memfile)
                                    command = "mprof run --include-children --output {} python mgris_search.py -v {} {} -m {} {}".format(
                                        memfile, nsamplesstr, ostr, method, inputfile
                                    )
                                    print(command)
                                    os.system(command)
                                    os.system(
                                        "mprof plot --output {}/memory-profile-search.png {}".format(
                                            resdir, memfile
                                        )
                                    )

                                    # command = "python mgris_search.py -v -pfp {} {} -m {} {}".format(nsamplesstr, ostr, method, inputfile)
                                    # os.system(command)

                                    # update, otherwise it will remember /tmp/input...
                                    inputfile = "{}/input.txt".format(resdir)
                                    benchmark_results.update({"inputfile": inputfile})

                                    if method == "SMC":
                                        os.system("mv /tmp/trace*.pkl " + resdir)

                                # ================================================================
                                # ================================================================
                                inputfile = "{}/input.txt".format(resdir)

                                if FileExists(resdir + "/trace_process.netCDF"):
                                    print("Skipping processing..")
                                else:
                                    print("Running processing...")
                                    print(inputfile)
                                    memfile = "{}/memory-profile-process.dat".format(
                                        resdir
                                    )
                                    if FileExists(memfile):
                                        os.remove(memfile)
                                    command = "mprof run --include-children --output {} python mgris_process.py -c -v {}".format(
                                        memfile, inputfile
                                    )
                                    os.system(command)
                                    os.system(
                                        "mprof plot --output {}/memory-profile-process.png {}".format(
                                            resdir, memfile
                                        )
                                    )

                                # ================================================================
                                # ================================================================
                                inputfile = "{}/input.txt".format(resdir)

                                if FileExists(resdir + "/trace_post-process.netCDF"):
                                    print("Skipping post-processing..")
                                else:
                                    print("Running post-processing...")
                                    print(inputfile)
                                    command = "python mgris_post_process.py -c -v {}".format(
                                        inputfile
                                    )
                                    os.system(command)

                                # ================================================================
                                # ================================================================
                                # if FileExists(resdir+'/trace_post-process.netCDF'):
                                #     print('Skipping post-processing..')
                                # else:
                                #     print('Running post-processing...')
                                #     print(inputfile)
                                #     memfile = '{}/memory-profile-post-process.dat'.format(resdir)
                                #     if FileExists(memfile):
                                #         os.remove(memfile)
                                #     command = "mprof run --include-children --output {} python mgris_post-process.py -c -v {}".format(memfile, inputfile)
                                #     os.system(command)
                                #     os.system("mprof plot --output {}/memory-profile-post-process.png {}".format(resdir, memfile))

                                # ================================================================
                                # ================================================================
                                # os.system("python mgris_animate.py {}".format(resdir))

    # ===================================================================
    # ===================================================================
    # ===================================================================


# --------------------------------------------------------------------
if __name__ == "__main__":

    main()
