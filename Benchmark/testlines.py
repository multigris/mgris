import matplotlib.pyplot as plt
import arviz as az
from statsmodels.graphics.boxplots import violinplot
import numpy as np
import pickle

for m in ('1S_rb', '1S_mb', '2S_rb', '2S_mb'):

    n_sectors = int(m[0])

    nlines = (3,5,9,13)
    n_nlines = len(nlines)

    if 'mb' in m:
        params = ('age', 'n', 'u', 'Z', 'Tx', 'Lx', 'cut')
    else:
        params = ('age', 'n', 'u', 'Z', 'Tx', 'Lx')

    n_params = len(params)

    fig, axs = plt.subplots(n_params, n_nlines, figsize=(2*n_nlines, 2*n_params), sharey='row')

    for ip,p in enumerate(params):

        for il,n in enumerate(nlines):

            ax = axs[ip, il]

            #d = '/local/home/vleboute/Downloads/BenchmarkResults_nlines/{}_lines{}/i1_nNone_o0/SMC/'.format(m, n)
            d = '/local/home/vleboute/extra/herschel1/Vianney/MultiGris/MULTIGRIS/BenchmarkResults/12S_nlines{}/{}_lines{}/i1_nNone_o0/SMC/'.format(n, m, n)

            t = az.from_netcdf(d + 'trace_process.netCDF').posterior
            with open(d + 'results_benchmark.pkl', 'rb') as f:
                res = pickle.load(f)

            tmp_params = np.array(['age', 'n', 'u', 'Z', 'Lx', 'Tx', 'cut', 'lum'])

            if il<n_nlines-1:
                ec = 'none'
                lw = 0
            else:
                ec = 'black'
                lw = 0.5

            for s in np.arange(len(res['ws'])):
                ax.axhline(res['models'][s][np.where(tmp_params==p)[0][0]], color='black')
                violinplot([t['{}_{}'.format(p, s)].data.flatten()], positions=[0], show_boxplot=False, side='right', ax=ax, plot_opts={'violin_fc': 'C{}'.format(il), 'violin_ec': ec, 'violin_alpha': 0.5/n_sectors, 'violin_lw': lw, 'cutoff_val': 1.}, labels=[n,])

            ax.set_xlim([0,ax.get_xlim()[1]])

            if il==0:
                ax.set_ylabel(p)

    fig.subplots_adjust(wspace=0, hspace=0)
    fig.tight_layout()
    fig.savefig('nlines_{}.pdf'.format(m))
