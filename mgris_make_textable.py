import os
import pickle
import arviz as az
import numpy as np
import argparse
import glob

# --------------------------------------------------------------------
# local libraries
import importlib
import Library.lib_main
import Library.lib_misc

importlib.reload(Library.lib_main)
importlib.reload(Library.lib_misc)
from Library.lib_class import *
from Library.lib_main import readinputfile
from Library.lib_misc import FileExists
from rcparams import rcParams

# --------------------------------------------------------------------
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self, result_directories="", verbose=False,
    ):
        args.result_directories = result_directories
        args.fmt = str(fmt)
        args.verbose = bool(verbose)


# ===================================================================
# ===================================================================
# ===================================================================
def clean(name):
    if "{" in name:
        name2 = name[0 : name.index("{")]
    else:
        name2 = name
    return name2


# ===================================================================
def pow10(s):
    if "e" in str(s):
        return (
            str(s).replace("1e", "e").replace("1.e", "e").replace("e", "\\times10^{")
            + "}"
        )
    else:
        return str(s)


# ===================================================================
def make_obs_str(inpobs, o, scale, fmt):

    olabel = clean(o)

    obs = inpobs.obs[o].value
    if inpobs.obs[o].obs_valuescale == "linear":
        obs = 10 ** obs
    delta = inpobs.obs[o].delta  # delta in linear already

    if scale is not None:
        obs /= scale
        delta[0] /= scale
        delta[1] /= scale

    if float(f"{obs:{fmt}}") == 0.0:  # change to 10^ format if <0.01 (,2f)
        fmt = fmt.replace("f", "e")
        norm_str = "1.e" + f"{obs:{fmt}}".split("e")[1]
        norm = float(norm_str)
    else:
        norm = 1.0

    fmt2 = ".2f"
    if delta[0] == delta[1]:
        obserr = pow10(f"\pm{{{delta[0]/norm:{fmt2}}}}")
    else:
        obserr = (
            "^"
            + pow10(f"{delta[0]/norm:{fmt2}}")
            + "_"
            + pow10(f"{delta[1]/norm:{fmt2}}")
        )

    if inpobs.obs[o].upper:
        obs = pow10(f"$<{obs/norm:{fmt2}}$")
    else:
        obs = "$" + pow10(f"{{{obs/norm:{fmt2}}}}") + f"{obserr}$"

    if abs(norm) < 0.01 or abs(norm) > 100:
        obs += pow10(norm_str)

    return olabel, obs, obserr


# ===================================================================
def make_pred_str(t, o, scale, fmt):
    pred = float(t.posterior[o].median())
    hdi = np.array(
        az.hdi(t.posterior[o], hdi_prob=rcParams["ci"], skipna=True).to_array()
    )[0]
    hdi = hdi - pred

    if scale is not None:
        pred /= scale
        hdi[0] /= scale
        hdi[1] /= scale

    if float(f"{pred:{fmt}}") == 0.0:  # change to 10^ format if <0.01 (,2f)
        fmt = fmt.replace("f", "e")
        norm_str = "1.e" + f"{pred:{fmt}}".split("e")[1]
        norm = float(norm_str)
    else:
        norm = 1.0

    prederr = ""  # f"^{{+{hdi[1]/norm:{fmt}}}} _{{{hdi[0]/norm:{fmt}}}}$"

    fmt2 = ".2f"
    pred = "$" + pow10(f"{{{pred/norm:{fmt2}}}}")

    fmt2 = fmt if abs(hdi[1] / norm) > 1e3 or abs(hdi[1] / norm) < 1e-3 else ".2f"
    pred += "^" + pow10(f"{{+{hdi[1]/norm:{fmt2}}}}")

    fmt2 = fmt if abs(hdi[0] / norm) > 1e3 or abs(hdi[0] / norm) < 1e-3 else ".2f"
    pred += "_" + pow10(f"{{{hdi[0]/norm:{fmt2}}}}")

    if abs(norm) < 0.01 or abs(norm) > 100:
        pred += pow10(norm_str)

    pred += "$"

    return pred, prederr


# ===================================================================
def main(args=None):

    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS analysis step")
        parser.add_argument(
            "result_directories",
            type=str,
            help="Result directory names (can be several directories if the tracers are exactly the same and in the same order)",
            nargs="+",
        )
        parser.add_argument("--scale", "-s", type=float, help="scaling factor")
        parser.add_argument(
            "--fmt", "-f", type=str, default=".2f", help="format (e.g., '.2f')"
        )
        parser.add_argument(
            "--verbose",
            "-v",
            help="verbose (use for extra information and debugging)",
            action="store_true",
        )
        args = parser.parse_args()

    verbose = args.verbose

    models = args.result_directories

    fmt = args.fmt  # ".2f", '.3e'

    n_models = len(models)

    table = """
    \\begin{table}
    \\begin{tabular}{lc"""
    for n in range(n_models):
        table += "c"
    table += """}
\\hline\\hline"""

    table += """
    Tracer & Observation """
    for n in range(n_models):
        table += f" & Model {n+1} "
    table += """\\\\
\\hline"""

    tracers = []
    tracers_pp = []
    rows = []

    for n, model in enumerate(models):

        input_f = model
        if not FileExists(input_f):
            # try and find if there's only one input*.txt file
            f = glob.glob(model + "/input*.txt")
            if len(f) == 1:
                input_f = f[0]
            elif len(f) > 1:
                raise OSError("More than one input file found: {}".format(f))
            else:
                raise OSError("No input file found, expected {}".format(input_f))

        input_params = readinputfile(input_f, root_directory, verbose=verbose)
        output_directory = input_params["output_directory"]
        # possibly update output_directory
        if not FileExists(output_directory, is_dir=True):
            output_directory = "/".join(os.path.abspath(input_f).split("/")[:-1]) + "/"

        # read
        with open(output_directory + "/input.pkl", "rb") as f:
            inp = pickle.load(f)
        with open(output_directory + "/results_search.pkl", "rb") as f:
            res = pickle.load(f)
        with open(output_directory + "/results_process.pkl", "rb") as f:
            resp = pickle.load(f)

        t = az.from_netcdf(output_directory + "/trace_process.netCDF")

        tracers += [
            [clean(o) for o in inp["observations"].names],
        ]

        names = inp["observations"].names
        if n > 0:
            if tracers[n - 1][0 : len(tracers[n])] != tracers[n]:
                print("Problem, list of tracers not the same!")
                print("*******************************")
                print(models[n - 1])
                print(tracers[n - 1][0 : len(tracers[n])])
                print("*******************************")
                print(models[n])
                print(tracers[n])
                print("*******************************")
                if len(tracers[n - 1][0 : len(tracers[n])]) == len(tracers[n]):
                    print("Trying to sort...")
                    names = []
                    for o in tracers[0][0 : len(tracers[n])]:
                        for o2 in inp["observations"].names:
                            if o in o2:
                                names += [
                                    o2,
                                ]

        for i, o in enumerate(names):

            olabel, obs, obserr = make_obs_str(inp["observations"], o, args.scale, fmt)

            pred, prederr = make_pred_str(t, o, args.scale, fmt)

            if n == 0:
                rows += [
                    f"""
    {olabel: <20} & {obs} & {pred} """,
                ]
                if n_models == 1:
                    rows[-1] += "\\\\"
            else:
                rows[i] += f" & {pred} "
                if n == n_models - 1:
                    rows[i] += "\\\\ "

        if FileExists(output_directory + "/trace_post-process.netCDF"):
            if len(input_params["obs_to_predict"]) > 0:

                # reread observation sets in case it was updated
                observation_sets = input_params["observation_sets"]
                allobservations = Observations(
                    observation_sets, input_params, verbose=verbose
                )
                # switch back to single obs set now that all obs blocks have been processed
                allobservations = allobservations.oset
                allobservations.set_masks()

                tpp = az.from_netcdf(output_directory + "/trace_post-process.netCDF")

                tracers[n] += [clean(o) for o in input_params["obs_to_predict"]]

                # print(input_params['obs_to_predict'])
                for j, o in enumerate(input_params["obs_to_predict"]):

                    idx = [
                        i2 for i2, o2 in enumerate(allobservations.names) if o in o2
                    ][0]
                    o2 = allobservations.names[idx]
                    olabel, obs, obserr = make_obs_str(
                        allobservations, o2, args.scale, fmt
                    )

                    pred, prederr = make_pred_str(tpp, o, args.scale, fmt)

                    if n == 0:
                        rows += [
                            f"""
    {olabel: <20} & {obs} & ({pred}) """,
                        ]
                        if n_models == 1:
                            rows[-1] += "\\\\"
                    else:
                        rows[i + j + 1] += f" & ({pred}) "
                        if n == n_models - 1:
                            rows[i + j + 1] += "\\\\ "

        # if n>0:
        #     if tracers[n]!=tracers[n-1]:
        #         print('Problem, list of tracers not the same!')
        #         print('*******************************')
        #         print(models[n-1])
        #         print(tracers[n-1])
        #         print('*******************************')
        #         print(models[n])
        #         print(tracers[n])
        #         print('*******************************')
        #         breakpoint()

    for row in rows:
        table += row

    table += """
    \\hline
    \\end{tabular}\\label{tab:}
    \\tablefoot{}
    \\end{table}
    """

    print(table)


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":

    main()
