import importlib
import Library.lib_input
import pandas as pd
import numpy as np
from tqdm import tqdm

importlib.reload(Library.lib_input)
from Library.lib_input import *

# ------------------------------------------------
# read table
data = pd.read_csv("DGS_observation_table.csv")
print(data)
data = data.reset_index()  # make sure indexes pair with number of rows

zpriors = {
    "HS0017+1055": "-1.06 0.2",
    "HS0052+2536": "-0.65 0.2",
    "HS0822+3542": "-1.37 0.13",
    "HS1222+3741": "-0.9 0.11",
    "HS1304+3529": "-0.76 0.2",
    "HS1319+3224": "-0.88 0.2",
    "HS1330+3651": "-0.71 0.2",
    "HS1442+4250": "-1.09 0.11",
    "HS2352+2733": "-0.29 0.2",
    "Haro11": "-0.33 0.11",
    "Haro2": "-0.46 0.13",
    "Haro3": "-0.41 0.11",
    "He2-10": "-0.26 0.11",
    "IIZw40": "-0.46 0.11",
    "IZw18": "-1.55 0.11",
    "Mrk1089": "-0.59 0.18",
    "Mrk1450": "-0.85 0.11",
    "Mrk153": "-0.83 0.14",
    "Mrk209": "-0.95 0.11",
    "Mrk930": "-0.66 0.11",
    "NGC1140": "-0.31 0.11",
    "NGC1569": "-0.67 0.12",
    "NGC1705": "-0.42 0.21",
    "NGC4214-c": "-0.43 0.11",
    "NGC4214-s": "-0.43 0.11",
    "NGC5253": "-0.44 0.12",
    "NGC625": "-0.47 0.12",
    "Pox186": "-0.99 0.11",
    "SBS0335-052": "-1.44 0.11",
    "SBS1159+545": "-1.25 0.11",
    "SBS1211+540": "-1.11 0.11",
    "SBS1249+493": "-1.01 0.12",
    "SBS1415+437": "-1.14 0.11",
    "SBS1533+574": "-0.64 0.11",
    "Tol1214-277": "-1.17 0.11",
    "UGC4483": "-1.23 0.12",
    "UM133": "-0.87 0.11",
    "UM448": "-0.37 0.11",
    "UM461": "-0.96 0.11",
    "VIIZw403": "-1.03 0.11",
}

params = get_params()
print(params.keys())

# lines used for inference
lineset = [
    "O388.3323m",
    "N357.3238m",
    "N2121.767m",
    "Ne315.5509m",
    "Ne212.8101m",
    "S410.5076m",
    "S318.7078m",
    "S333.4704m",
    "C2157.636m",
    "Si234.8046m",
    "O425.8832m",
    "Ar26.98337m",
    "Ar38.98898m",
    "Fe217.9314m",
    "Fe225.9811m",
    "Ar321.8253m",
    "H112.3684m",
    "N2205.244m",
    "O163.1679m",
    "O1145.495m",
    "Ne514.3228m",
    "Ne524.2065m",
    "LTIR500.500m",
    "Fe322.9190m",
    "H217.0300m",
    "H29.66228m",
    "H228.2130m",
    "H212.2752m",
]

os.makedirs("DGS_run/", exist_ok=True)
os.makedirs("DGS_run/inputs/", exist_ok=True)
os.makedirs("DGS_run/outputs/", exist_ok=True)

for index, entry in tqdm(data.iterrows()):
    params = get_params()

    label = entry["galaxy"]
    if label == "HS2352+2733" or "4214" in label:
        continue
    params["output"].value = f"DGS_run/outputs/output_{label}"
    params["context"].value = f"Contexts/mgris_sfgx"

    # observations
    params["BEGIN observations"].value = ""
    for l in lineset:
        if np.isnan(entry[l]):
            continue
        if entry[l] > 0:  # detection
            params["BEGIN observations"].value += f"{l} {entry[l]} {entry[l+'_err']}\n"
        else:  # upper limit
            params["BEGIN observations"].value += f"{l} <{abs(entry[l])}\n"
    params["BEGIN observations"].extras = {"scale": "linear", "delta_add": 0.1}
    params["use_scaling"].value = "'all'"

    # configuration
    # LOC
    tmp = f"""distrib n plaw
    alpha_n_0 = 0 2
    lowerbound_n_0 = 1 1
    upperbound_n_0 = 3 1
    distrib u plaw
    alpha_u_0 = 0 2
    lowerbound_u_0 = -3 1
    upperbound_u_0 = -1 1
    distrib cut normal
    mu_cut_0 = 0 2
    sigma_cut_0 = 1 1 0.25 2
    lowerbound_cut_0 = 0.5 1
    upperbound_cut_0 = 1.5 1
    Z (0) = {zpriors[label]}"""
    params["BEGIN configuration"].value = tmp

    # 1C2S
    # params["BEGIN configuration"].value = f"Z (0) = {zpriors[label]}"
    # params["USE configuration"].value = "1C2S"

    # fixed parameters
    tmp = [
        "Z_dust [0,0]",
    ]
    params["select"].value = tmp

    # predictions
    params["secondary_parameters"].value = [
        "Q0",
        "Q(1-infty)",
        "fesc(1-infty)",
        "fescL(1-infty)",
        "MH2_total",
        "MH2_CO",
        "MH2_C",
        "MH2_C+",
        "CII_H+",
        "CII_H",
        "CII_H2",
    ]
    # ['Av_stop', 'depth_stop', 'Nh_stop', 'G0', 'Q0', 'MH2_total', 'MH2_CO', 'MH2_C', 'MH2_C+', 'MHI', 'MHII', 'Mdust', 'Q(1-infty)', 'Q(1-1.8)', 'Q(1.8-4)', 'Q(4-20)', 'Q(20-infty)', 'CII_H+', 'CII_H', 'CII_H2', 'L(1-infty)', 'L(1-1.8)', 'L(1.8-4)', 'L(4-20)', 'L(20-infty)', 'Lx_sun', 'fescL(1-infty)', 'fescL(1-1.8)', 'fescL(1.8-4)', 'fescL(4-20)', 'fescL(20-infty)', 'U_mean_vol']
    params["obs_to_predict"].value = []
    # ['O23726.03A', 'O23728.81A', 'Ar44711.26A', 'Ar44740.12A', 'Ar57.89971m', 'Ar513.0985m', 'O23726+9A', 'Ar44711+40A', 'C1609.590m', 'C1370.269m', 'H16562.81A', 'O16300+63A', 'O23726+9A', 'O27320+30A', 'O35007+4959A', 'S39069+532A', 'S26716+30A', 'Ar44711+40A', 'N26548+84A', 'Ar37135+751A', 'PAH6-15um', 'S26716+30A', 'O35006.84A', 'O34363.21A', 'O34958.91A', 'He24685.64A', 'He14471.49A', 'He17065.22A', 'PAH11.3000m', 'PAH11.8000m', 'PAH13.3000m', 'PAH3.30000m', 'PAH6.20000m', 'PAH7.90000m', 'PAHC10.9000m', 'PAHC12.6500m', 'PAHC14.1000m', 'PAHC3.23000m', 'PAHC3.37000m', 'PAHC5.65000m', 'PAHC6.90000m', 'NH3523.514m', 'C18727.13A', 'C19088.00A', 'C19850.26A', 'C22837.00A', 'C24267.00A', 'C26578.05A', 'C26582.88A', 'C29903.00A', 'C32512.20A', 'C34069.00A', 'C34649.00A', 'C44659.00A', 'F100100.000m', 'F1212.0000m', 'F2525.0000m', 'F6060.0000m', 'H14861.33A', 'N26548.05A', 'N26583.45A', 'IRAC3.60000m', 'IRAC4.50000m', 'IRAC5.80000m', 'IRAC8.00000m', 'LBOL500.050m', 'LFIR350.000m', 'MIPS160.000m', 'MIPS24.0000m', 'MIPS70.0000m', 'Inci1215.00A', 'Inci4860.00A', 'PAC170.0000m', 'PAC2100.000m', 'PAC3160.000m', 'SPR1250.000m', 'SPR2350.000m', 'SPR3500.000m', 'WISE12.0820m', 'WISE22.1940m', 'WISE3.36800m', 'WISE4.61800m', 'CO1300.05m', 'CO325.137m', 'CO371.549m', 'CO433.438m', 'CO520.089m', 'CO650.074m', 'CO866.727m', 'CO1300.05m', 'CO866.727m', 'H217.0300m', 'H29.66228m', 'H228.2130m', 'H212.2752m']

    # global parameters can be overridden (needs uncomment)
    params["smc_iterations_to_diagnose"].value = 500  # 500
    params["smc_iterations_to_diagnose"].uncomment()

    params["smc_integration_steps"].value = 100  # 100
    params["smc_integration_steps"].uncomment()

    params["smc_mcmc_steps"].value = 12  # 10
    params["smc_mcmc_steps"].uncomment()

    params["smc_step_size"].value = 0.002  # 0.005
    params["smc_step_size"].uncomment()

    # write input file
    make_input(params, f"DGS_run/inputs/input_{label}.txt")
