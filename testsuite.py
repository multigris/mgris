import os
import argparse
import pandas as pd
import numpy as np
from rcparams import rcParams
from Library.lib_input import get_params, make_input
from Library.lib_misc import printsectionraw
import mgris_search, mgris_process, mgris_post_process
import pickle
import arviz as az

# ===================================================================
# ===================================================================
# ===================================================================


resdir = "/tmp/mgris_testsuite/"

# ===================================================================

# Primary parameter: lum
# [7.0, 8.0, 9.0]

# Primary parameter: age
# [0.0, 0.301, 0.47700000000000004, 0.602, 0.6990000000000001, 0.778, 0.903, 1.0]

# Primary parameter: n
# [0.0, 1.0, 2.0, 3.0, 4.0]

# Primary parameter: u
# [-4.0, -3.0, -2.0, -1.0, 0.0]

# Primary parameter: Z
# [-2.19, -1.889, -1.19, -0.889, -0.668, -0.491, -0.19, 0.111]

# Primary parameter: Lx
# [-40.0, -3.0, -2.0, -1.0]

# Primary parameter: Tx
# [0.0, 5.0, 6.0, 7.0]

# Primary parameter: q_PAH
# [0.0]

# Primary parameter: Z_dust
# [-1.0, 0.0, 1.0]

# Primary parameter: cut
# [0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0]


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS testing suite")
        parser.add_argument("--run", "-r", action="store_true", help="Run tests")
        parser.add_argument("--check", "-c", action="store_true", help="Check tests")
        parser.add_argument("--test", "-t", help="Test number", type=int, default=1)
        parser.add_argument(
            "--nsamples_per_job",
            "-n",
            help="Force number of particles (independent chains) per job for SMC or number of draws per chain for other samplers",
            type=int,
        )
        #                 "[TEST 1] Single component model, nearest neighbor interpolation",
        #                 "[TEST 2] Two component model, nearest neighbor interpolation",
        #                 "[TEST 3] Single component model, linear interpolation on one parameter (Z)",
        #                 "[TEST 4] Single component model, linear interpolation on two parameters (Z, u)",
        #                 "[TEST 5] Large grid"
        #                 "[TEST 6] Single component model, nearest neighbor interpolation, bimodality test",
        #                 "[TEST 7] LOC",
        parser.add_argument("--add-noise", "-a", action="store_true", help="Add noise?")
        args = parser.parse_args()

    # ===================================================================
    if not (args.run + args.check):
        print(
            "Provide at least -r (run the test suite) or -c (check results) as an argument"
        )
        exit()

    # ===================================================================
    if args.run:
        # =================================================================================
        printsectionraw("Run...")

        context = "mgris_sfgx"
        params_name = ["age", "n", "u", "Z", "Lx", "Tx", "cut", "lum", "Z_dust"]

        testlinear = False
        bimodality_test = False
        if args.nsamples_per_job:
            nsamples_per_job = args.nsamples_per_job
        else:
            nsamples_per_job = False

        if args.test == 1:
            # =================================================================================
            # =================================================================================
            # =================================================================================
            printsectionraw(
                "[TEST 1 Single component model, nearest neighbor interpolation",
                level=2,
            )
            print(
                "\nMake sure to have the mgris_sfgx context installed.\nThis test runs a simple model with 4 random variables and nearest neighbor interpolation.\nMore tests can be run with -t flag.\nThis should run quickly... [press c and ENTER to continue; or q to quit]\n"
            )
            breakpoint()
            # 1C1S
            models = [
                # matter-bounded
                # [0.602, 2, -3, -1.19, -2, 6, 0.5, 9, 0],
                # radiation-bounded
                [0.602, 2, -3, -1.19, -2, 6, 2, 9, 0],
                # test
                # [0.778, 2, -3, -0.491, -3, 6, 1, 8, 0],
            ]
            ws = [
                1,
            ]
            if not nsamples_per_job:
                if rcParams["inference_backend"] == "cpu":
                    nsamples_per_job = 200
                else:
                    nsamples_per_job = 500

        elif args.test == 2:
            # =================================================================================
            # =================================================================================
            # =================================================================================
            printsectionraw(
                "[TEST 2] Two component model, nearest neighbor interpolation", level=2
            )
            print(
                "\nMake sure to have the mgris_sfgx context installed.\nThis test runs a model with two components and nearest neighbor interpolation.\nMore tests can be run with -t flag.\nThis may take a while... [press c and ENTER to continue; or q to quit]\n"
            )
            breakpoint()
            # 1C2S: same age, Z, Lx, Tx, lum, Z_dust | rb: same cut
            models = [
                # matter-bounded
                # [0.602, 2, -3, -1.19, -2, 6, 0.5, 9, 0],
                # [0.602, 3, -1, -1.19, -2, 6, 2, 9, 0],
                # radiation-bounded
                [0.602, 2, -3, -1.19, -2, 6, 2, 9, 0],
                [0.602, 3, -1, -1.19, -2, 6, 2, 9, 0],
                # test
                # [0.778, 2, -3, -0.491, -3, 6, 1, 8, 0],
                # [0.778, 1, -2, -0.491, -3, 6, 1, 8, 0],
            ]
            ws = [0.65, 0.35]
            if not nsamples_per_job:
                if rcParams["inference_backend"] == "cpu":
                    nsamples_per_job = 300
                else:
                    nsamples_per_job = 500

        elif args.test == 3:
            # =================================================================================
            # =================================================================================
            # =================================================================================
            printsectionraw(
                "[TEST 3] Single component model, linear interpolation on one parameter (Z)",
                level=2,
            )
            print(
                "\nMake sure to have the mgris_sfgx context installed.\nThis test runs a model with one component and linear interpolation on one parameter.\nMore tests can be run with -t flag.\nThis may take a while... [press c and ENTER to continue; or q to quit]\n"
            )
            breakpoint()
            # interpolation between 2 models -- single parameter
            models = [
                # matter-bounded
                # [0.602, 2, -3, -1.19, -2, 6, 0.5, 9, 0],
                # [0.602, 3, -1, -1.19, -2, 6, 2, 9, 0],
                # radiation-bounded
                [0.602, 2, -2, -1.19, -2, 6, 2, 9, 0],
                [0.602, 2, -2, -0.889, -2, 6, 2, 9, 0],
                # test
                # [0.778, 2, -3, -0.491, -3, 6, 1, 8, 0],
                # [0.778, 1, -2, -0.491, -3, 6, 1, 8, 0],
            ]
            testlinear = True
            ws = [0.5] * 2
            if not nsamples_per_job:
                if rcParams["inference_backend"] == "cpu":
                    nsamples_per_job = 300
                else:
                    nsamples_per_job = 500

        elif args.test == 4:
            # =================================================================================
            # =================================================================================
            # =================================================================================
            printsectionraw(
                "[TEST 4] Single component model, linear interpolation on two parameters (Z, u)",
                level=2,
            )
            print(
                "\nMake sure to have the mgris_sfgx context installed.\nThis test runs a model with one component and linear interpolation on two parameters.\nFor this test the inference is not expected to return outputs correspond precisely to the input values since we use linear interpolation on *each* parameter and not a N-dimensional interpolation.\nMore tests can be run with -t flag.\nThis may take a while... [press c and ENTER to continue; or q to quit]\n"
            )
            breakpoint()
            # interpolation between 2 models -- single parameter
            models = [
                # matter-bounded
                # [0.602, 2, -3, -1.19, -2, 6, 0.5, 9, 0],
                # [0.602, 3, -1, -1.19, -2, 6, 2, 9, 0],
                # radiation-bounded
                [0.602, 2, -2, -1.19, -2, 6, 2, 9, 0],
                [0.602, 2, -3, -0.889, -2, 6, 2, 9, 0],
                [0.602, 2, -2, -0.889, -2, 6, 2, 9, 0],
                [0.602, 2, -3, -1.19, -2, 6, 2, 9, 0],
                # test
                # [0.778, 2, -3, -0.491, -3, 6, 1, 8, 0],
                # [0.778, 1, -2, -0.491, -3, 6, 1, 8, 0],
            ]
            testlinear = True
            ws = [0.25] * 4
            if not nsamples_per_job:
                if rcParams["inference_backend"] == "cpu":
                    nsamples_per_job = 200
                else:
                    nsamples_per_job = 500

        elif args.test == 5:
            # =================================================================================
            # =================================================================================
            # =================================================================================
            printsectionraw(
                "[TEST 5] Single component w/ nearest neighbors & parameters to produce a large subgrid",
                level=2,
            )
            print(
                "\nMake sure to have the mgris_sfgx context installed.\nThis test runs a simple model with 4 random variables and nearest neighbor interpolation.\nMore tests can be run with -t flag.\nThis should run quickly... [press c and ENTER to continue; or q to quit]\n"
            )
            breakpoint()
            # 1C1S
            models = [
                # matter-bounded
                # [0.602, 2, -3, -1.19, -2, 6, 0.5, 9, 0],
                # radiation-bounded
                [0.602, 2, -3, -1.19, -2, 6, 2, 9, 0],
                # test
                # [0.778, 2, -3, -0.491, -3, 6, 1, 8, 0],
            ]
            ws = [
                1,
            ]
            if not nsamples_per_job:
                if rcParams["inference_backend"] == "cpu":
                    nsamples_per_job = 400
                else:
                    nsamples_per_job = 1000

        elif args.test == 6:
            # =================================================================================
            # =================================================================================
            # =================================================================================
            printsectionraw(
                "[TEST 6] Single component model, nearest neighbor interpolation",
                level=2,
            )
            print(
                "\nMake sure to have the mgris_sfgx context installed.\nThis test runs a simple model with 4 random variables and nearest neighbor interpolation.\nMore tests can be run with -t flag.\nThis should run quickly... [press c and ENTER to continue; or q to quit]\n"
            )
            breakpoint()
            # 1C1S
            models = [
                # matter-bounded
                # [0.602, 2, -3, -1.19, -2, 6, 0.5, 9, 0],
                # radiation-bounded
                [0.602, 2, -3, -1.19, -2, 6, 2, 9, 0],
                # test
                # [0.778, 2, -3, -0.491, -3, 6, 1, 8, 0],
            ]
            ws = [
                1,
            ]
            bimodality_test = True
            if not nsamples_per_job:
                if rcParams["inference_backend"] == "cpu":
                    nsamples_per_job = 1000
                else:
                    nsamples_per_job = 1000

        elif args.test == 7:
            # =================================================================================
            # =================================================================================
            # =================================================================================
            printsectionraw(
                "[TEST 7] model with parameters distributed as power laws", level=2
            )
            print(
                "\nMake sure to have the mgris_sfgx context installed.\nThis test runs a model integrating grid outputs for some parameters described by to some statistical distribution.\nMore tests can be run with -t flag.\nThis may take a while... [test not yet implemented]\n"
            )
            breakpoint()
            # LOC

            alpha = {"Z": 0, "u": -2, "n": -1, "age": 0}
            boundaries = {
                # "Z": [-1.20, -1.18],
                # "u": [-3, -2],
                # "n": [1, 1],
                # "age": [0.300, 0.302],
                "Z": [-1.20, -0.5],
                "u": [-3, -2],
                "n": [1, 3],
                "age": [0.300, 0.702],
            }

            if not nsamples_per_job:
                if rcParams["inference_backend"] == "cpu":
                    nsamples_per_job = 1000
                else:
                    nsamples_per_job = 200

        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        # =================================================================================
        if args.test < 7:

            ncomps = len(ws)

            # infrared
            lines = [
                "C2157.636m",
                "O163.1679m",
                "N357.3238m",
                "Ar26.98337m",
                "Ar38.98898m",
                "O388.3323m",
                "N2205.244m",
                "Si234.8046m",
                "Ne212.8101m",
                "Ne315.5509m",
                "O1145.495m",
                "Ne514.3228m",
                "Fe225.9811m",
            ]
            # optical
            # lines = [
            #     "O23726+9A",
            #     "Ne33868.76A",
            #     "H14340.46A",
            #     "H14861.33A",
            #     "O34958.91A",
            #     "O35006.84A",
            #     "O16300.30A",
            #     "H16562.81A",
            #     "N26583.45A",
            #     "S26716+30A",
            # ]

            values = pd.read_feather(
                f"Contexts/{context}/Grids/model_grid.fth", columns=params_name + lines
            ).astype(np.float32)
            secondary_parameters = [
                "MHII",  # extensive
                "Q0",  # extensive
                "Q(1-infty)",  # extensive
                "fescL(1-infty)",  # intensive
            ]
            ppvalues = pd.read_feather(
                f"Contexts/{context}/Grids/model_grid_post_processing.fth",
                columns=params_name + secondary_parameters,
            ).astype(np.float32)

            # =================================================================================
            printsectionraw(
                f"Generating model set from {ncomps} component(s)...", level=2
            )
            obstab = [[]] * len(models)
            pptab = [[]] * len(models)
            for im, m in enumerate(models):
                tmp = pd.DataFrame(models[im]).transpose().astype(np.float32)
                tmp.columns = params_name

                r = np.prod(
                    [np.isclose(values[p], tmp[p].iloc[0]) for p in params_name], axis=0
                )
                tmp2 = values.loc[r == 1].iloc[0]
                obstab[im] = tmp2[lines].values

                r = np.prod([ppvalues[p] == tmp2[p] for p in params_name], axis=0)
                tmp2 = ppvalues.loc[r == 1].iloc[0]
                pptab[im] = tmp2[secondary_parameters].values

            # =================================================================================
            printsectionraw(
                "Original mixing weights, line fluxes & secondary parameter values...",
                level=2,
            )
            print("Mixing weights = ", ws)
            print("Line fluxes = ", obstab)
            print("Secondary fluxes = ", pptab)

            # =================================================================================
            printsectionraw("Combined fluxes & secondary parameter values...", level=2)

            if args.test in (3, 4):
                # for tests 3 and 4 we do a linear combination in *log* space
                # combine input fluxes
                inputvalues = np.sum(
                    [ws[i] * obstab[i].astype(np.float64) for i in range(ncomps)],
                    axis=0,
                ).astype(np.float64)
                if args.add_noise:
                    inputvalues += np.random.normal(0, 0.1, len(inputvalues))

                # Assuming secondary parameters scale with mixing weight
                inputpp = np.sum(
                    [ws[i] * pptab[i].astype(np.float64) for i in range(ncomps)],
                    axis=0,
                ).astype(np.float64)
                models = [
                    np.sum([ws[i] * np.array(models[i]) for i in range(ncomps)], axis=0)
                ]
            else:
                # combine input fluxes
                inputvalues = np.log10(
                    np.sum(
                        [
                            ws[i] * 10 ** obstab[i].astype(np.float64)
                            for i in range(ncomps)
                        ],
                        axis=0,
                    ).astype(np.float64)
                )
                if args.add_noise:
                    inputvalues += np.random.normal(0, 0.1, len(inputvalues))

                # Assuming secondary parameters scale with mixing weight
                inputpp = np.log10(
                    np.sum(
                        [
                            ws[i] * 10 ** pptab[i].astype(np.float64)
                            for i in range(ncomps)
                        ],
                        axis=0,
                    ).astype(np.float64)
                )
            print(lines)
            print(inputvalues)
            print(inputpp)

            # =================================================================================
            printsectionraw("Writing input file...", level=2)
            os.makedirs(resdir, exist_ok=True)
            params = get_params()
            params["context"].value = "./Contexts/{}".format(context)
            params["output"].value = resdir
            if args.test in (3, 4):
                # we wanna recover the one - interpolated - model
                params["USE configuration"].value = "1C1S"
            else:
                params["USE configuration"].value = f"1C{ncomps}S"

            if testlinear:
                # linearly interpolate some parameters?
                if args.test == 4:
                    params[
                        "BEGIN configuration"
                    ].value = """distrib Z single linear
    distrib u single linear"""
                else:
                    params["BEGIN configuration"].value = """distrib Z single linear"""

            params["use_scaling"].value = "'all'"
            # params["obs_to_predict"].value = [
            #     "O388.3323m",
            # ]
            params["secondary_parameters"].value = str(secondary_parameters)

            if args.test == 5:
                params["select"].value = [
                    # matter-bounded
                    # "lum [9,9]",
                    # "Lx [-2,-2]",
                    # "Tx [6,6]",
                    # "Z_dust [0,0]",
                    # radiation-bounded
                    # "cut [2,2]",
                    # "lum [9,9]",
                    "Lx [-2,-2]",
                    "Tx [6,6]",
                    "Z_dust [0,0]",
                    # test
                    # "lum [8,8]",
                    # "Lx [-3,-3]",
                    # "Tx [6,6]",
                    # "Z_dust [0,0]",
                ]
            else:
                params["select"].value = [
                    # matter-bounded
                    # "lum [9,9]",
                    # "Lx [-2,-2]",
                    # "Tx [6,6]",
                    # "Z_dust [0,0]",
                    # radiation-bounded
                    "cut [2,2]",
                    "lum [9,9]",
                    "Lx [-2,-2]",
                    "Tx [6,6]",
                    "Z_dust [0,0]",
                    # test
                    # "lum [8,8]",
                    # "Lx [-3,-3]",
                    # "Tx [6,6]",
                    # "Z_dust [0,0]",
                ]

            # for nearest 1C1S (tests)
            # params["BEGIN configuration"].comment()
            # params["USE configuration"].value = f"1C1S"

            params["BEGIN observations"].value = ""
            for i, l in enumerate(lines):
                if i < len(lines) - 1:
                    params["BEGIN observations"].value += "{} {} {}\n".format(
                        l, 10 ** inputvalues[i], 0.02 * 10 ** inputvalues[i]
                    )
                else:  # include an upper limit *10 the expected flux
                    params["BEGIN observations"].value += "{} <{}\n".format(
                        l, 10 ** (inputvalues[i] + 1)
                    )
            params["BEGIN observations"].extras = {
                "scale_factor": 1,  # 0 if scale is log
                "scale": "linear",
            }
            inputfile = f"{resdir}/input_generated.txt"
            make_input(params, inputfile)
            print("Input file location: ", inputfile)

            # save reference values
            res = {}
            res.update(
                {
                    "ws": ws,
                    "obstab": obstab,
                    "pptab": pptab,
                    "models": models,
                    "lines": lines,
                    "inputvalues": inputvalues,
                    "inputpp": inputpp,
                    "params_name": params_name,
                    "secondary_parameters": secondary_parameters,
                }
            )

        else:
            # infrared
            lines = [
                "C2157.636m",
                "O163.1679m",
                "N357.3238m",
                "Ar26.98337m",
                "Ar38.98898m",
                "O388.3323m",
                "N2205.244m",
                "Si234.8046m",
                "Ne212.8101m",
                "Ne315.5509m",
                "O1145.495m",
                "Fe225.9811m",
            ]
            # optical
            # lines = [
            #     "O23726+9A",
            #     "Ne33868.76A",
            #     "H14340.46A",
            #     "H14861.33A",
            #     "O34958.91A",
            #     "O35006.84A",
            #     "O16300.30A",
            #     "H16562.81A",
            #     "N26583.45A",
            #     "S26716+30A",
            # ]

            values = pd.read_feather(
                f"Contexts/{context}/Grids/model_grid.fth", columns=params_name + lines
            ).astype(np.float32)

            # =================================================================================
            printsectionraw(f"Generating model set...", level=2)

            values = (
                values.loc[values["cut"] == 1]
                .loc[values["lum"] == 9]
                .loc[values["Lx"] == -2]
                .loc[values["Tx"] == 6]
                .loc[values["Z_dust"] == 0]
            )
            values = values.reset_index(drop=True)
            print(values)

            # apply boundaries and weights
            for p in alpha.keys():
                pgrid = np.sort(list(set(values[p])))
                dgrid = np.append(
                    abs(pgrid[:-1] - np.roll(pgrid, -1)[:-1]),
                    abs((pgrid - np.roll(pgrid, -1))[-2]),
                )
                # temporary
                dgrid = np.median(dgrid)

                values = values.loc[values[p] >= boundaries[p][0]].loc[
                    values[p] <= boundaries[p][1]
                ]
                values = values.reset_index(drop=True)

                for i in range(len(values)):
                    for l in lines:
                        values.at[i, l] += (1 + alpha[p]) * values[p].iloc[
                            i
                        ] + np.log10(dgrid)
            print(values)

            # =================================================================================
            printsectionraw(
                "Integrated line fluxes...",
                level=2,
            )

            # integrate
            fluxes = {}
            for l in lines:
                fluxes.update({l: np.log10(np.sum(10 ** values[l]))})

            print("Line fluxes = ", fluxes)

            breakpoint()

            # =================================================================================
            printsectionraw("Writing input file...", level=2)
            os.makedirs(resdir, exist_ok=True)
            params = get_params()
            params["context"].value = "./Contexts/{}".format(context)
            params["output"].value = resdir
            params["njobs"].value = 2
            params["njobs"].uncomment()

            secondary_parameters = [
                "MHII",  # extensive
                "Q0",  # extensive
                "Q(1-infty)",  # extensive
            ]
            params["secondary_parameters"].value = str(secondary_parameters)

            # params["USE configuration"].value = "1C1S"

            params[
                "BEGIN configuration"
            ].value = """distrib Z plaw
    alpha_Z_0 = 0 2
    lowerbound_Z_0 = -1.5 0.3
    upperbound_Z_0 = -0.5 0.3
    distrib u plaw
    alpha_u_0 = 0 2
    lowerbound_u_0 = -3 1
    upperbound_u_0 = -1 1
    distrib n plaw
    alpha_n_0 = 0 2
    lowerbound_n_0 = 1 1
    upperbound_n_0 = 3 1
    distrib age plaw
    alpha_age_0 = 0 2
    lowerbound_age_0 = 0.3 0.3
    upperbound_age_0 = 0.7 0.3
    """

            params["use_scaling"].value = "'all'"

            params["select"].value = [
                "cut [1,1]",
                "lum [9,9]",
                "Lx [-2,-2]",
                "Tx [6,6]",
                "Z_dust [0,0]",
            ]

            params["BEGIN observations"].value = ""
            for i, l in enumerate(lines):
                if i < len(lines) - 1:
                    params["BEGIN observations"].value += "{} {} {}\n".format(
                        l, 10 ** fluxes[l], 0.1 * 10 ** fluxes[l]
                    )
                else:  # include an upper limit *10 the expected flux
                    params["BEGIN observations"].value += "{} <{}\n".format(
                        l, 10 ** (fluxes[l] + 1)
                    )
            params["BEGIN observations"].extras = {
                "scale_factor": 1,  # 0 if scale is log
                "scale": "linear",
            }
            inputfile = f"{resdir}/input_generated.txt"
            make_input(params, inputfile)
            print("Input file location: ", inputfile)

            # save reference values
            res = {}
            res.update(
                {
                    "fluxes": fluxes,
                    "lines": lines,
                    "params_name": params_name,
                }
            )

        # =================================================================================

        with open(f"{resdir}/test.pkl", "wb") as f:
            pickle.dump(res, f, protocol=4)

        # =================================================================================
        printsectionraw("Running search...", level=2)
        mgris_search.main(
            args=mgris_search.args(
                inputfile=inputfile,
                nsamples_per_job=nsamples_per_job,
                verbose=True,
                bimodality_test=bimodality_test,
            )
        )

        # =================================================================================
        printsectionraw("Running process...", level=2)
        mgris_process.main(
            args=mgris_process.args(
                inputfile=inputfile, verbose=True, force=True, clean=True
            )
        )

        if args.test <= 7:
            # =================================================================================
            printsectionraw("Running post-process...", level=2)
            mgris_post_process.main(
                args=mgris_post_process.args(
                    inputfile=inputfile, verbose=True, force=True, clean=True
                )
            )

        print("")
        print("Everything OK... run script with -c to check results")

    # ===================================================================
    if args.check:
        printsectionraw("Checking tests...")

        with open(f"{resdir}/test.pkl", "rb") as f:
            res = pickle.load(f)

        printsectionraw(
            "Checking rhat (should be <~ 1.01)",
            level=2,
        )
        t = az.from_netcdf(f"{resdir}/trace_process.netCDF")
        for k in t.posterior.keys():
            if "scale" in k or "idx_" in k:
                val = float(az.rhat(t)[k])
                print(f"{k: <10} {val: <10,.3f}")

        printsectionraw(
            "Comparing to observations (inferred | observed | % diff)", level=2
        )
        t = az.from_netcdf(f"{resdir}/trace_process.netCDF")
        for i, l in enumerate(res["lines"]):
            inf = float(t.posterior[l].mean())
            try:
                obs = 10 ** res["inputvalues"][i]
            except:
                obs = 10 ** res["fluxes"][l]
            print(f"{l: <20} {inf: <10,.2e} {obs: <10,.2e} {(inf-obs)/obs: <10,.2e}")

        try:
            printsectionraw("Comparing parameters to expected values", level=2)
            for i, p in enumerate(res["params_name"]):
                for s in range(2):
                    ps = f"{p}_{s}"
                    if ps not in t.posterior.keys():
                        continue
                    inf = float(t.posterior[ps].mean())
                    obs = res["models"][s][i]
                    print(
                        f"{ps: <10}: {inf: <10,.2f} {obs: <10,.2f} {inf-obs: <10,.2f}"
                    )
            ps = "scale_eff"
            inf = float(t.posterior[ps].mean())
            obs = 0.0
            print(f"{ps: <10}: {inf: <10,.2e} {obs: <10,.2e} {inf-obs: <10,.2e}")

            printsectionraw(
                "Comparing secondary parameters to expected values", level=2
            )
            t = az.from_netcdf(f"{resdir}/trace_post-process.netCDF")
            for i, p in enumerate(res["secondary_parameters"]):
                if p not in t.posterior.keys():
                    continue
                inf = float(t.posterior[p].mean())
                obs = res["inputpp"][i]
                print(f"{p: <10}: {inf: <10,.2f} {obs: <10,.2f} {inf-obs: <10,.2f}")
        except:
            pass

        with open(f"{resdir}/results_search.pkl", "rb") as f:
            results_search = pickle.load(f)


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    main()
