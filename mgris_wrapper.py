import os
import argparse
import mgris_search, mgris_process, mgris_post_process, mgris_diagnose
from Library.lib_main import closelogfiles, readinputfile_standalone, FileExists


# ===================================================================
# ===================================================================
# ===================================================================
class args:
    def __init__(
        self,
        inputfiles="",
        nsamples_per_job=None,
        skip_plots=True,
        diagnose=False,
        incomplete_ok=True,
        force=False,
        forcesearch=False,
        forceprocess=False,
        forcepostprocess=False,
        verbose=False,
    ):
        args.inputfiles = inputfiles
        args.nsamples_per_job = nsamples_per_job
        args.skip_plots = bool(skip_plots)
        args.diagnose = bool(diagnose)
        args.incomplete_ok = bool(incomplete_ok)
        args.force = bool(force)
        args.forcesearch = bool(forcesearch)
        args.forceprocess = bool(forceprocess)
        args.forcepostprocess = bool(forcepostprocess)
        args.verbose = bool(verbose)


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS wrapper")
        parser.add_argument("inputfiles", type=str, help="input files", nargs="+")
        parser.add_argument(
            "--nsamples_per_job",
            "-n",
            help="force number of particles (independent chains) per job for SMC or number of draws per chain for other samplers. ",
            type=int,
        )
        parser.add_argument(
            "--diagnose", "-d", help="Use diagnostic tool", action="store_true"
        )
        parser.add_argument(
            "--skip-plots",
            "-sp",
            help="Skip plots [default=True]",
            action="store_true",
        )
        parser.add_argument(
            "--incomplete_ok",
            "-iok",
            help="OK to use incomplete grids with distributions (power-law, normal etc...)",
            action="store_true",
        )
        parser.add_argument(
            "--force",
            "-f",
            help="Force all steps",
            action="store_true",
        )
        parser.add_argument(
            "--forcesearch",
            "-fs",
            help="Force search step",
            action="store_true",
        )
        parser.add_argument(
            "--forceprocess",
            "-fp",
            help="Force processing step",
            action="store_true",
        )
        parser.add_argument(
            "--forcepostprocess",
            "-fpp",
            help="Force post-processing step",
            action="store_true",
        )
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        args = parser.parse_args()

    verbose = args.verbose
    skip_plots = args.skip_plots

    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    for inputfile in args.inputfiles:
        output_directory = readinputfile_standalone(inputfile, root_directory)[
            "output_directory"
        ]
        if (
            args.force
            or args.forcesearch
            or not FileExists(output_directory + "/trace.netCDF")
        ):
            mgris_search.main(
                args=mgris_search.args(
                    inputfile=inputfile,
                    nsamples_per_job=args.nsamples_per_job,
                    incomplete_ok=args.incomplete_ok,
                    verbose=verbose,
                )
            )
        else:
            print(f"Skipping mgris_search for {inputfile} (override with --force/-f)")

        if (
            args.force
            or args.forceprocess
            or not FileExists(output_directory + "/trace_process.netCDF")
        ):
            mgris_process.main(
                args=mgris_process.args(
                    inputfile=inputfile,
                    skip_plots=skip_plots,
                    clean=args.force or args.forceprocess,
                    verbose=verbose,
                )
            )
        else:
            print(f"Skipping mgris_process for {inputfile} (override with --force/-f)")

        if (
            args.force
            or args.forcepostprocess
            or not FileExists(output_directory + "/trace_post-process.netCDF")
        ):
            mgris_post_process.main(
                args=mgris_post_process.args(
                    inputfile=inputfile,
                    skip_plots=skip_plots,
                    clean=args.force or args.forcepostprocess,
                    verbose=verbose,
                )
            )
        else:
            print(
                f"Skipping mgris_post_process for {inputfile} (override with --force/-f)"
            )

        if args.diagnose:
            mgris_diagnose.main(
                args=mgris_diagnose.args(inputfile=inputfile, verbose=verbose)
            )

        # in case run is interrupted
        closelogfiles()


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    main()
