import argparse
from copy import deepcopy

import pickle

import arviz as az
import numpy as np
import pandas as pd
import json

import os
import logging
from glob import glob
from tqdm import tqdm

# global parameters
from rcparams import rcParams

from Library.lib_misc import FileExists, inputAC
from Library.lib_main import setup_custom_logger, closelogfiles, printsection


# ===================================================================
# ===================================================================
# ===================================================================
def usingwhat(d):
    # trace_post-process file should include trace_process
    if FileExists(d + f"/trace_post-process.netCDF"):
        fname = d + f"/trace_post-process.netCDF"
        tracekind = "post-process"
        logging.info("Using trace_post-process files")
    elif FileExists(d + f"/trace_process.netCDF"):
        fname = d + f"/trace_process.netCDF"
        tracekind = "process"
        logging.info("Using trace_process files")
    else:
        fname, tracekind = "", ""
    return fname, tracekind


# ===================================================================
# ===================================================================
# ===================================================================
def addcolumn(posterior, newcolumns):
    for n in newcolumns:
        for k in posterior.keys():
            newcolumns[n] = newcolumns[n].replace(k, f"posterior['{k}']")
        posterior[n] = deepcopy(posterior["scale_eff"]) * np.nan
        try:
            exec(f"posterior['{n}'] = " + newcolumns[n])
        except:
            logging.warning(
                "Exec failed: {}".format(f"posterior['{n}'] = " + newcolumns[n])
            )

    return posterior


# ===================================================================
# ===================================================================
# ===================================================================
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        results="",
        labels_prepend="",
        labels_append="",
        labels="",
        output="",
        operators="mean, median, hdi",
        keys="",
        add=True,
        prefix="",
        suffix="",
        combinations={},
        include_fractions=False,
        include_indices=False,
        include_pvalues=False,
        random_subset="",
        merge=0,
        verbose=False,
        debug=False,
    ):
        args.results = results
        args.labels = labels
        args.labels_prepend = labels_prepend
        args.labels_append = labels_append
        args.output = output
        args.operators = operators
        args.keys = keys
        args.add = add
        args.prefix = str(prefix)
        args.suffix = str(suffix)
        args.combinations = combinations  # it's a dict
        args.include_fractions = bool(include_fractions)
        args.include_indices = bool(include_indices)
        args.include_pvalues = bool(include_pvalues)
        args.random_subset = str(random_subset)
        args.merge = int(merge)
        args.verbose = bool(verbose)
        args.debug = bool(debug)


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    # root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS model comparison")
        parser.add_argument(
            "--results",
            "-r",
            type=str,
            help="Results location (directory or can also be a netCDF file)",
            nargs="+",
        )
        parser.add_argument("--output", "-o", type=str, help="Output name", default="")
        parser.add_argument(
            "--operators",
            "-op",
            type=str,
            help="Operator list",
            default="mean, median, hdi",
        )
        parser.add_argument(
            "--labels-prepend",
            "-lp",
            type=str,
            help="Prepend some string to the label column.",
            default="",
        )
        parser.add_argument(
            "--labels-append",
            "-la",
            type=str,
            help="Append some string to the label column.",
            default="",
        )
        parser.add_argument(
            "--labels",
            "-l",
            type=str,
            help="Use labels instead of directory names.",
            nargs="+",
        )
        parser.add_argument(
            "--keys",
            "-k",
            type=str,
            help="Only consider specific keys (end of command line). Default is all keys from first file",
            nargs="+",
        )
        parser.add_argument(
            "--add",
            "-a",
            help="Add new keys when found",
            action="store_true",
        )
        parser.add_argument(
            "--prefix", "-p", type=str, help="Prefix to all columns", default=""
        )
        parser.add_argument(
            "--suffix", "-s", type=str, help="Suffix to all columns", default=""
        )
        # using example for help message
        h = '{"ratio1": "O388.3323m - O351.8004m","ratio2": "O388.3323m - S333.4704m"}'
        parser.add_argument(
            "--combinations",
            "-c",
            type=str,
            help=f"Add new columns based on combinations of existing ones (e.g.,  --combinations {h})",
        )
        parser.add_argument(
            "--include-fractions",
            "-if",
            help="Includes fractions component/group",
            action="store_true",
        )
        parser.add_argument(
            "--include-indices",
            "-ii",
            help="Includes indices output",
            action="store_true",
        )
        parser.add_argument(
            "--include-pvalues",
            "-ip",
            help="Includes pvalues for all observables",
            action="store_true",
        )
        parser.add_argument(
            "--random-subset",
            "-rs",
            type=str,
            help="Only consider a random subset among the input files/directories to produce the metrics",
        )
        parser.add_argument(
            "--merge",
            "-m",
            type=int,
            help="Concatene/merge [n] random draws from all output results instead of applying specific metrics",
            default=0,
        )
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        parser.add_argument(
            "--debug", "-d", help="Debugging information", action="store_true"
        )
        args = parser.parse_args()

    # =====
    global logger
    # start logging now
    # logging console + file
    logname = "gather"
    loglevel = logging.DEBUG if args.debug else logging.INFO
    os.makedirs("./Logs/", exist_ok=True)
    logger = setup_custom_logger(logname, directory="./Logs/", loglevel=loglevel)
    # =====

    printsection("Preparation")
    verbose = args.verbose

    dirs = args.results
    if dirs is None:
        dirs = inputAC(
            "Provide list of directories or directory with .netCDF files (--results/-r) [~ and tab completion possible]: "
        )
        if len(dirs.split(" ")) == 1:
            if dirs[-1] == "*/":
                dirs = dirs[0:-1]
            if dirs[-1] not in ("/", "*"):
                dirs += "/*"
            if dirs[-1] == "/":
                dirs += "*"

        args.add = input("Add new keys as found (-add/-a) [Y/n]? ").lower() in ("y", "")

    if type(dirs) is str:
        if dirs[-1] == "*/":
            dirs = dirs[0:-1]
        if dirs[-1] not in ("/", "*"):
            dirs += "/*"
        if dirs[-1] == "/":
            dirs += "*"
        dirs = glob(dirs)
    else:  # list
        if len(dirs) == 1:
            dirs = dirs[0]
            if dirs[-1] == "*/":
                dirs = dirs[0:-1]
            if dirs[-1] not in ("/", "*"):
                dirs += "/*"
            if dirs[-1] == "/":
                dirs += "*"
            dirs = glob(dirs)
    logging.info("First 10 entries:")
    logging.info(dirs[0:10])

    # Only consider specific keys (end of command line)? Default is all keys from first file
    onlykeys = deepcopy(
        args.keys
    )  # copy because we may modify it and it can be run in a script

    output = args.output
    if output == "":
        logging.warning(
            "Will not save the resulting table, use -o to set output file name. "
        )
    else:
        if ".csv" not in output and ".fth" not in output:
            output += ".csv"

    if args.labels:
        labels = args.labels
        if len(labels) != len(dirs):
            logging.error("Number of labels does not match number of directories")

    # methods for DataArrays or arviz operators, can also include min, max...
    operators_init = [o.strip() for o in args.operators.split(",")]

    operators = []
    for o in operators_init:
        if o == "hdi":
            operators += ["hdi_lo"]
            operators += ["hdi_up"]
        else:
            operators += [o]

    # are the inputs netCDF files themselves or directories (containing the netCDF files and potentially pkl files as well)?
    if ".netCDF" in dirs[0]:
        # netCDF files themselves
        usefiles = True
    else:
        # directories
        usefiles = False

    # are we taking a random subset among the input dirs/files?
    rndsubset = None
    if args.random_subset is not None:
        if args.random_subset not in ("", "0"):
            if "[" in args.random_subset:
                rndsubset = (
                    args.random_subset.replace("[", "").replace("]", "").split(",")
                )
                rndsubset = [int(r) for r in rndsubset]
            else:
                rndsubset = np.random.default_rng().choice(
                    np.arange(len(dirs)),
                    np.min([int(args.random_subset), len(dirs)]),
                    replace=False,
                )
                rndsubset = list(
                    rndsubset
                )  # easier to parse when mutiple calls with rndsubset re-used
            dirs = list(np.array(dirs)[rndsubset])

    # read primary/secondary params from first file
    if usefiles:
        fname = dirs[0]
    else:
        fname, tracekind = usingwhat(dirs[0])
        if fname == "":
            fname, tracekind = usingwhat(dirs[1])
        if fname == "":
            logging.error(
                f"Not able to find netCDF files, please provide directories containing trace_process.netCDF or trace_post-process.netCDF files..."
            )
            exit()
    t = az.from_netcdf(fname)
    d = {"result": []}

    # potentially add combination of parameters
    if args.combinations:
        if type(args.combinations) is str:  # command-line
            comb = json.loads(args.combinations)
        else:
            comb = deepcopy(args.combinations)
        t.posterior = addcolumn(t.posterior, comb)  # takes dict as input
        # Only consider specific keys (end of command line)? Default is all keys from first file
        if onlykeys:
            for n in args.combinations:
                onlykeys += [
                    n,
                ]

    dfkeys = onlykeys if onlykeys else t.posterior.keys()
    # by default we take only the parameters with real values and ignore component/group fractions
    if not args.include_fractions:
        dfkeys = [d for d in dfkeys if "f_s" not in d and "f_g" not in d]
    if not args.include_indices:
        dfkeys = [d for d in dfkeys if "idx_" not in d]

    # if only one group no need to keep individual likelihoods
    if "log_likelihood1" not in dfkeys:
        dfkeys = list(set(dfkeys) - {"log_likelihood0"})

    if args.add:
        logging.info(f"\nConsidered keys [initial] [{len(dfkeys)}]: {dfkeys}")
    else:
        logging.info(f"\nConsidered keys [{len(dfkeys)}]: {dfkeys}")

    columns = [
        "result",
    ]
    if args.merge > 0:
        columns += dfkeys
    else:
        for k in dfkeys:
            for o in operators:
                columns += [
                    o + "_" + k,
                ]

        columns += [
            "LOO",
            "LML",
            "pnsigma1",
            "pnsigma2",
            "pnsigma3",
            "pvalue",
        ]

    logging.info(f"\nFinal columns [{len(columns)}]: {columns}")

    for k in columns:
        d.update({k: []})
    df = pd.DataFrame(data=d)

    if args.include_pvalues:
        logging.info("Will also add pvalues for all observables")

    # =================
    for i, d in tqdm(enumerate(dirs), total=len(dirs)):
        if verbose:
            printsection("Reading directory: {}".format(d), level=2)

        fname = d if usefiles else d + f"/trace_{tracekind}.netCDF"
        if not FileExists(fname):
            logging.error(f"File {fname} does not exist...")
            continue
            # exit()
        t = az.from_netcdf(fname)

        if args.combinations:
            if type(args.combinations) is str:  # command-line
                comb = json.loads(args.combinations)
            else:
                comb = deepcopy(args.combinations)
            t.posterior = addcolumn(t.posterior, comb)

        tmp = [np.nan] * len(df.keys())

        # calculate HDI for all keys (faster)
        vals = {}
        if args.merge == 0:
            for o in operators:
                if "hdi" in o:
                    vals.update(
                        {
                            "hdi": az.hdi(
                                t.posterior, hdi_prob=rcParams["ci"], skipna=True
                            )
                        }
                    )
                else:
                    vals.update({o: getattr(t.posterior, o)()})

        # ===============================================================
        # we wanna reduce to simple metrics
        if args.merge == 0:
            df2 = pd.DataFrame([tmp], columns=columns)

            if args.labels:
                df2["result"] = labels[i]
            else:
                if usefiles:
                    df2["result"] = d.split("/")[-1].replace("/", "").split(".")[0]
                else:
                    df2["result"] = d.split("/")[-1]  # .replace("/", "")
            df2["result"] = args.labels_prepend + df2["result"] + args.labels_append

            # cycle through final keys
            for k in dfkeys:
                # checks
                if verbose:
                    logging.info(k)
                if k not in t.posterior.keys():
                    if args.verbose:
                        logging.warning(f"Key {k} not existing...")
                    if args.add:
                        if args.verbose:
                            logging.warning("Will add new column")
                    else:
                        logging.warning(f"key {k} not in posterior")
                        # exit()
                if k in t.posterior.keys():
                    if len(t.posterior[k].shape) > 2:  # only expecting draws and chains
                        if verbose:
                            logging.warning(f"Skipping >1-dimension variable {k}")
                        continue
                # fill
                for o in operators:
                    k2 = o + "_" + k
                    if k not in t.posterior.keys():  #  and args.add:
                        df2[k2] = np.nan
                    else:
                        if "hdi_lo" in o:
                            df2[k2] = float(vals["hdi"][k][0])
                        elif "hdi_up" in o:
                            df2[k2] = float(vals["hdi"][k][1])
                        else:
                            df2[k2] = float(vals[o][k])

            # retrieve pkl file depending on whether the input was the netCDF file itself or the directory
            if usefiles:
                fname = d.replace(".netCDF", ".pkl")
                if not FileExists(fname):
                    fname = d.replace("trace", "results").replace(".netCDF", ".pkl")
                    if not FileExists(fname):
                        logging.error(f"File {fname} does not exist...")
                        logging.error(
                            "Make sure to provide either directories containing trace_process.netCDF or trace_post-process.netCDF files"
                        )
                        logging.error(
                            "or a list of files named trace*_process.netCDF or trace*_post-process.netCDF files"
                        )
                        exit()
            else:
                fname = d + "/results_process.pkl"

            with open(fname, "rb") as f:
                res = pickle.load(f)
                df2["LOO"] = res["loo_elpd"]
                df2["LML"] = res["log_marginal_likelihood"]
                df2["pnsigma1"] = res["pnsigma"][0]
                df2["pnsigma2"] = res["pnsigma"][1]
                df2["pnsigma3"] = res["pnsigma"][2]
                df2["pvalue"] = res["pvalue_combined"]

                if args.include_pvalues:
                    for k in res["pvalues"].keys():
                        kk = f"pvalue_{k}"
                        if kk not in columns:
                            columns += [
                                kk,
                            ]
                            df[kk] = np.nan
                        df2[kk] = pd.Series(res["pvalues"][k])

        # ===============================================================
        # we wanna merge traces instead of reducing to simple metrics
        else:
            rndsample = np.random.default_rng().choice(
                np.arange(t.posterior.dims["draw"]),
                np.min([args.merge, t.posterior.dims["draw"]]),
                replace=False,
            )
            t.posterior = t.posterior.sel(draw=rndsample)
            t.posterior = t.posterior.assign_coords(
                {"draw": np.arange(t.posterior.dims["draw"])}
            )
            n = t.posterior.dims["chain"] * t.posterior.dims["draw"]

            df2 = pd.DataFrame([tmp] * n, columns=columns)

            if args.labels:
                df2["result"] = labels[i]
            else:
                if usefiles:
                    df2["result"] = d.split("/")[-1].replace("/", "").split(".")[0]
                else:
                    df2["result"] = d.split("/")[-1]  # .replace("/", "")
            df2["result"] = args.labels_prepend + df2["result"] + args.labels_append

            # cycle through final keys
            for k in dfkeys:
                if k not in t.posterior.keys():
                    continue
                if len(t.posterior[k].shape) > 2:  # only expecting draws and chains
                    if verbose:
                        logging.warning(f"Skipping >1-dimension variable {k}")
                    continue
                df2[k] = np.array(t.posterior[k]).flatten()

        # ===============================================================

        df = pd.concat([df, df2], ignore_index=True)

    # =====================================

    printsection("Cleaning table")

    df = df.dropna(axis=1, how="all")

    # add a prefix/suffix  to all columns (e.g., if the resulting table needs to be merged with another)
    if args.prefix != "":
        tmp = {c: f"{args.prefix}_" + c for c in df.columns if c != "result"}
        df.rename(columns=tmp, inplace=True)
    if args.suffix != "":
        tmp = {c: c + f"_{args.suffix}" for c in df.columns if c != "result"}
        df.rename(columns=tmp, inplace=True)

    printsection("Final table")
    logging.info(df)

    if output != "":
        if ".csv" in output:
            df.to_csv(output, header=True, index=False)
        else:
            df.to_feather(output)
        logging.info(f"Final table saved in {output}")
    else:
        logging.warning(
            "Will not save the resulting table, use -o to set output file name. "
        )
        # ...
    # ...
    # =====

    closelogfiles()
    if rndsubset is not None:
        return df, rndsubset
    else:
        return df


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    global logger
    # ===================================================================
    # logging console + file
    logger = setup_custom_logger("gather")

    closelogfiles()
    main()
