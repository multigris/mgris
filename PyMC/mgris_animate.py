from copy import deepcopy
import pickle
import argparse
from matplotlib import cm
import matplotlib.pyplot as plt
import arviz as az
import numpy as np
from celluloid import Camera

from Library.lib_mc import mergetrace, characterize_sectors
from Library.lib_main import make_grid_wrapper
from Library.lib_misc import FileExists


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS analysis step")
        parser.add_argument("inputdirectory", type=str, help="name_of_input_directory")
        parser.add_argument(
            "--verbose",
            "-v",
            help="verbose (use for extra information and debugging)",
            action="store_true",
        )
        parser.add_argument(
            "--params",
            "-p",
            nargs="*",
            help="parameters to plot (use last in command line!)",
        )
        args = parser.parse_args()

    verbose = args.verbose

    resdir = args.inputdirectory

    # trace = az.from_netcdf(resdir + "/trace_process.netCDF")
    trace = az.from_netcdf(resdir + "/trace.netCDF")

    verbose = True

    # ======================================================================

    with open(resdir + "/input.pkl", "rb") as f:
        inp = pickle.load(f)

    if args.params is None:
        print(
            "Please provide parameters with -p flag *at the end of the command line* (e.g., -p u n)"
        )
        exit()
    param_names = args.params  # doesn't work yet with w

    method = inp["method_step"]

    print("Making grid...")
    grid = make_grid_wrapper(inp, verbose=verbose)
    inp["grid"] = grid

    nsectors = trace.posterior.dims["w_dim_0"]
    print(resdir)

    # ======================================================================
    if method == "SMC":
        print("/!\\ Only works if modified version of sample_smc.py is used!")

        fulltrace = {}
        for s in range(15):  # will skip if doesn't exist...
            fulltrace.update({s: {}})
            for c in range(trace.posterior.dims["chain"]):
                fn = "{}/trace_{}{}.pkl".format(resdir, s, c)
                print(fn)
                if not FileExists(fn):
                    if s in fulltrace.keys():
                        del fulltrace[s]
                    continue
                if s not in fulltrace.keys():
                    continue
                with open(fn, "rb") as f:
                    fulltrace[s].update({c: pickle.load(f)})
        nstages = len(fulltrace)

        # ======================================================================

        px = ["idx_{}_{}".format(param_names[0], s) for s in range(nsectors)]
        py = ["idx_{}_{}".format(param_names[1], s) for s in range(nsectors)]

        colors = cm.brg(np.linspace(0, 1, nstages))
        # colors = cm.coolwarm(np.linspace(0, 1, trace.posterior.dims['draw']))

        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        fig2, ax2 = plt.subplots(len(param_names), 2, figsize=(12, 6))

        tmp = [trace.posterior[p] for p in px]
        ax.set_xlim([np.min(tmp), np.max(tmp)])

        tmp = [trace.posterior[p] for p in py]
        ax.set_ylim([np.min(tmp), np.max(tmp)])

        camera = Camera(fig)
        camera2 = Camera(fig2)
        for i in range(nstages):
            print("Stage {}".format(i))

            trace2 = deepcopy(trace)
            for c in range(trace.posterior.dims["chain"]):
                for p in [
                    "idx_{}_{}".format(p, s)
                    for p in param_names
                    for s in range(nsectors)
                    if p != "w"
                ]:
                    trace2.posterior[p].values[c, :] = fulltrace[i][c][p]
                if "w" in param_names:
                    trace2.posterior["w_{}".format(g)].values[c, :, :] = fulltrace[i][
                        c
                    ]["w"]

            print("Characterizing sectors...")
            trace2 = characterize_sectors(inp, trace2)

            # ======================================================
            cols = ["tab:blue", "tab:orange", "tab:green", "tab:red"]
            for chain in range(trace.posterior.dims["chain"]):
                alpha = 1000 / (trace.posterior.dims["draw"]) / (i + 1) ** 2
                alpha = np.min([alpha, 1])
                alpha = np.max([alpha, 0.01])
                ax.scatter(
                    trace2.posterior[px[0]].values[chain, :].flatten(),
                    trace2.posterior[py[0]].values[chain, :].flatten(),
                    # color=colors[i],
                    color=cols[chain],
                    marker="o",
                    alpha=alpha,
                )

            if nsectors > 1:
                ax.scatter(
                    trace2.posterior[px[1]].values.flatten(),
                    trace2.posterior[py[1]].values.flatten(),
                    # color=colors[i],
                    color="tab:orange",
                    marker="o",
                    alpha=1 / (trace.posterior.dims["chain"] * nstages),
                )

            ax.set_xlabel(param_names[0])
            ax.set_ylabel(param_names[1])

            ax.set_xticks(np.arange(len(inp["grid"].params.values[param_names[0]])))
            ax.set_xticklabels(inp["grid"].params.values[param_names[0]])

            ax.set_yticks(np.arange(len(inp["grid"].params.values[param_names[1]])))
            ax.set_yticklabels(inp["grid"].params.values[param_names[1]])

            # cols = ['tab:blue', 'tab:orange']
            # for ii,x in enumerate((-3,-1)):
            #     plt.axvline(inp['grid'].params.interp_vals2idx(param_names[0])(x), color=cols[ii])
            # for ii,y in enumerate((1,3)):
            #     plt.axhline(inp['grid'].params.interp_vals2idx(param_names[1])(y), color=cols[ii])

            camera.snap()

            # ======================================================
            if nsectors > 1:
                tmp = ["idx_{}".format(p) for p in param_names if p != "w"]
                tmp2 = deepcopy(tmp)
                if "w" in param_names:
                    tmp2 += [
                        "w",
                    ]
                az.plot_trace(
                    mergetrace(trace2.posterior, tmp),
                    var_names=tmp2,
                    divergences="bottom",
                    compact=True,
                    axes=ax2,
                    combined=True,
                )  # , show=False)   )
            else:
                tmp = ["idx_{}_0".format(p) for p in param_names if p != "w"]
                if "w" in param_names:
                    tmp += [
                        "w",
                    ]
                alpha_plot_trace = 0.5
                az.plot_trace(
                    trace2,
                    var_names=tmp,
                    combined=True,
                    divergences="bottom",
                    compact=True,
                    axes=ax2,
                    plot_kwargs={"alpha": alpha_plot_trace},
                    trace_kwargs={
                        "alpha": 0.01,
                        "linewidth": 0,
                        "marker": "o",
                        "ms": 3,
                    },
                    hist_kwargs={"alpha": alpha_plot_trace},
                    rug_kwargs={"alpha": alpha_plot_trace},
                    fill_kwargs={"alpha": alpha_plot_trace},
                )  # , show=False)
            camera2.snap()

        anim = camera.animate(blit=True)
        anim2 = camera2.animate(blit=True)
        plt.close()
        anim.save(resdir + "/animated_scatter.gif", fps=1)
        anim2.save(resdir + "/animated_trace.gif", fps=1)

    else:  # method == NUTS

        trace2 = deepcopy(trace)

        bins = np.int(trace.posterior.dims["draw"] / 50)  # coarse
        # bins = np.int(trace.posterior.dims["draw"] / 100)  # detailed

        # ======================================================================

        px = ["idx_{}_{}".format(param_names[0], s) for s in range(nsectors)]
        py = ["idx_{}_{}".format(param_names[1], s) for s in range(nsectors)]

        colors = cm.brg(np.linspace(0, 1, trace.posterior.dims["draw"]))
        # colors = cm.coolwarm(np.linspace(0, 1, trace.posterior.dims['draw']))

        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        fig2, ax2 = plt.subplots(len(param_names), 2, figsize=(12, 6))

        print("Characterizing sectors...")
        trace = characterize_sectors(inp, trace)

        tmp = [trace.posterior[p] for p in px]
        ax.set_xlim([np.min(tmp), np.max(tmp)])

        tmp = [trace.posterior[p] for p in py]
        ax.set_ylim([np.min(tmp), np.max(tmp)])

        camera = Camera(fig)
        camera2 = Camera(fig2)
        cols = ["tab:blue", "tab:orange", "tab:green", "tab:red"]
        for i in range(0, trace.posterior.dims["draw"], bins):
            print(i, trace.posterior.dims["draw"])

            # bins2 = 1 if i < bins else bins

            for chain in range(trace.posterior.dims["chain"]):
                # s = trace.posterior[px[0]][:,0:i].values.flatten().shape[0]

                xx = [
                    float(trace.posterior[px[0]][chain, j].values.flatten())
                    for j in range(0, i, bins)
                ]
                yy = [
                    float(trace.posterior[py[0]][chain, j].values.flatten())
                    for j in range(0, i, bins)
                ]

                # cols = np.array(
                #     [colors[j] for c in range(trace.posterior.dims["chain"])]
                # ).reshape(trace.posterior.dims["chain"], 4)
                for k in range(len(xx)):
                    ax.plot(
                        xx[k : k + 2],
                        yy[k : k + 2],
                        marker="o",
                        alpha=k / len(xx),
                        color=cols[chain],
                    )

            ax.set_xlabel(param_names[0])
            ax.set_ylabel(param_names[1])

            ax.set_xticks(np.arange(len(inp["grid"].params.values[param_names[0]])))
            ax.set_xticklabels(inp["grid"].params.values[param_names[0]])

            ax.set_yticks(np.arange(len(inp["grid"].params.values[param_names[1]])))
            ax.set_yticklabels(inp["grid"].params.values[param_names[1]])

            # ax.title('[after initialization]')

            # ax.legend(fontsize=10)

            # cols = ['tab:blue', 'tab:orange']
            # for ii, x in enumerate((-3, -1)):
            #     ax.axvline(
            #         inp["grid"].params.interp_vals2idx(param_names[0])(x),
            #         color="black",
            #         alpha=0.5,
            #     )
            # for ii, y in enumerate((1, 3)):
            #     ax.axhline(
            #         inp["grid"].params.interp_vals2idx(param_names[1])(y),
            #         color="black",
            #         alpha=0.5,
            #     )

            camera.snap()

            # ======================================================
            trace2 = deepcopy(trace.posterior.sel(draw=np.arange(0, i + bins)))
            if nsectors > 1:
                tmp = ["idx_{}".format(p) for p in param_names if p != "w"]
                tmp2 = deepcopy(tmp)
                if "w" in param_names:
                    tmp2 += [
                        "w",
                    ]
                az.plot_trace(
                    mergetrace(trace2, tmp),
                    var_names=tmp2,
                    # divergences="bottom",
                    compact=True,
                    axes=ax2,
                    combined=True,
                )  # , show=False)   )
            else:
                tmp = ["idx_{}_0".format(p) for p in param_names if p != "w"]
                if "w" in param_names:
                    tmp += [
                        "w",
                    ]
                alpha_plot_trace = 0.5
                az.plot_trace(
                    trace2,
                    var_names=tmp,
                    combined=True,
                    # divergences="bottom",
                    compact=True,
                    axes=ax2,
                    plot_kwargs={"alpha": alpha_plot_trace},
                    trace_kwargs={
                        "alpha": 0.01,
                        "linewidth": 0,
                        "marker": "o",
                        "ms": 3,
                    },
                    # hist_kwargs={"alpha": alpha_plot_trace},
                    # rug_kwargs={"alpha": alpha_plot_trace},
                    fill_kwargs={"alpha": alpha_plot_trace},
                )  # , show=False)

            camera2.snap()

            # =======================================

        anim = camera.animate(blit=True)
        anim2 = camera2.animate(blit=True)
        plt.close()
        anim.save(resdir + "/animated_scatter.gif", fps=10)
        anim2.save(resdir + "/animated_trace.gif", fps=10)

    # =======================================
    print(resdir + "/animated_scatter.gif")
    print(resdir + "/animated_trace.gif")


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":

    main()
