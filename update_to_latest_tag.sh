#!/bin/sh
git tag -l
latest=`git describe --abbrev=0 --tags`
git checkout tags/$latest
git pull >/dev/null
if [ $? = 0 ] ; then
    echo "Repository updated."
else
    echo "\n\n \!\!\! \!\!\! Update failed. Please check messages above for locally modified files. \!\!\! \!\!\!  \n\n\n"
fi
#update restricted context(s)
git submodule update --remote --force --depth 1 #--init to force init
#LFS?
git submodule foreach git lfs pull
