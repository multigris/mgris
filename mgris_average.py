import os
import argparse

import arviz as az
import pickle
import numpy as np
import xarray as xr

from copy import deepcopy
import random

import logging

from Library.lib_misc import FileExists
from Library.lib_main import setup_custom_logger, closelogfiles, printsection


# ===================================================================
# ===================================================================
# ===================================================================
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        result_directories="",
        verbose=False,
        debug=False,
    ):
        args.result_directories = result_directories
        args.verbose = bool(verbose)
        args.debug = bool(debug)


# ===================================================================
# ===================================================================
# ===================================================================
def main(args=None):
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS model comparison")
        parser.add_argument(
            "result_directories", type=str, help="Result directory names", nargs="+"
        )
        parser.add_argument("--verbose", "-v", help="Verbose", action="store_true")
        parser.add_argument("--weights", "-w", help="Weights", nargs="+", type=int)
        parser.add_argument(
            "--debug", "-d", help="Debugging information", action="store_true"
        )
        args = parser.parse_args()

    outdir = "ModelComparison/"
    os.makedirs("ModelComparison/", exist_ok=True)

    global logger
    # start logging now
    # logging console + file
    logname = "average"
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logger = setup_custom_logger(logname, directory=outdir, loglevel=loglevel)

    models = args.result_directories
    weights = args.weights

    if len(models) < 2:
        logging.error("Provide at least two models")
        exit()

    if weights is None:
        logging.error("Please provide the weights")
        exit()

    if len(weights) != len(models):
        logging.error("Please provide the same number of models and weights")
        exit()

    weights /= np.sum(weights)

    # preparation
    kk = []
    for i, mdir in enumerate(models):
        # take only those variable that are from post-process
        tp = az.from_netcdf(mdir + "/trace_process.netCDF").posterior
        tpp = az.from_netcdf(mdir + "/trace_post-process.netCDF").posterior
        k = set(tpp.keys()) - set(tp.keys())
        kk += [
            k,
        ]

        if i == 0:
            n = tp.dims["draw"]
            w = np.array(weights * n).astype(int)
            logging.info("Number of draws: {}/{}".format(w, n))

    n = int(np.sum(w))  # rounding errors

    # take minimal list of keys present in all models
    tmp = [len(k) for k in kk]
    if np.min(tmp) != np.max(tmp):
        logging.warning("Not the same list of keys")
        k = kk[np.argmin(tmp)]
    else:
        k = kk[0]
    logging.info(k)

    for i, mdir in enumerate(models):
        logging.info("===============================")
        logging.info(mdir)
        tpp = az.from_netcdf(mdir + "/trace_post-process.netCDF").posterior
        tpp = tpp[k]

        rndsample = random.sample(sorted(set(np.arange(n))), w[i])

        if i == 0:
            tm = deepcopy(tpp)
            tm = tm.sel(draw=np.arange(w[0])).assign_coords({"draw": np.arange(w[0])})
            tm = tm[k]
        elif w[i] > 0:
            tm = xr.concat(
                [tm, tpp.sel(draw=rndsample).assign_coords({"draw": np.arange(w[i])})],
                "draw",
            )

    tm = tm.assign_coords({"draw": np.arange(n)})

    logging.info("===============================")
    fname = "ModelComparison/trace_average.netCDF"
    logging.info("Writing file: {}".format(fname))
    tm = az.convert_to_inference_data(tm)
    tm.to_netcdf(fname)

    var_names = [k for k in tm.posterior.keys() if k[0:2] != "f_"]
    print(az.summary(tm, skipna=True, fmt="wide", var_names=var_names).to_string())


# ===================================================================
# ===================================================================
# ===================================================================

# --------------------------------------------------------------------
if __name__ == "__main__":
    global logger
    # ===================================================================
    # logging console + file
    logger = setup_custom_logger("average")

    closelogfiles()
    main()
